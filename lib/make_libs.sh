#!/bin/bash

#3 Requirements : 
# 1) javac in the PATH (JDK)
# 2) git also in the PATH
# 3) curl and in the PATH

#increase command verbosity to raise awareness about what's going on
set -x

#json.jar
rm -rf json.jar JSON-java && git clone https://github.com/douglascrockford/JSON-java.git && cd JSON-java && git checkout b0191a6acf6024b6b3efbd48dba8826ca79b950d && mkdir -pv org/json && mv -vf *.java ./org/json &&   javac -cp `pwd`:. org/json/*.java ; jar -cvf json.jar ./org && mv -vf json.jar .. && cd .. && rm -rf JSON-java

#commons-math3-3.3.jar
rm -rf commons-math3-3.3.jar && wget http://central.maven.org/maven2/org/apache/commons/commons-math3/3.3/commons-math3-3.3.jar


#biojava 3.0.7
rm -rf biojava* &&   wget https://github.com/biojava/biojava/archive/biojava-3.0.7.tar.gz && tar -xvzf biojava-3.0.7.tar.gz    &&  cd biojava-biojava-3.0.7/biojava3-core/src/main/java &&   javac -cp `pwd`:. `find -iname "*.java"` && jar cvf biojava3-core-3.0.7.jar ./org && mv -vf biojava3-core-3.0.7.jar  ../../../../..  && cd ../../../../.. && echo `pwd` && rm -rf biojava-3.0.7.tar.gz && rm -rf biojava-biojava-3.0.7

#forester
rm -rf forest* &&    git clone https://github.com/cmzmasek/forester.git && cd forester && git checkout b90b0b84a2328ac2bded888fdcd5357977de6553 &&   cd forester/java  && cp -vi forester.jar  ../../../ && cd ../../.. && rm -rf forester

#BigDecimalMath
rm -rf unpack BigDecimalMath.jar arXiv-0908-3030* &&  curl   -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" https://arxiv.org/src/0908.3030v3 2>/dev/null 1> arXiv-0908-3030v3.tar.gz && mkdir -v unpack && mv -v arXiv-0908-3030v3.tar.gz ./unpack && cd unpack && tar -xvzf arXiv-0908-3030v3.tar.gz && cd anc &&  javac -cp .:$PWD ./org/nevec/rjm/*.java && jar cvf BigDecimalMath.jar org && mv -v BigDecimalMath.jar ../.. && cd ../../ && rm -rf unpack
