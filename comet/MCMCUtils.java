/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author esalina
 */
public class MCMCUtils
    {

    public static long getMostFrequentStateStep(ArrayList<Long> states)
        {
        if(states!=null)
            {
            if(states.size()==0)
                {
                //too little data!
                throw new IllegalArgumentException("Can't get most frequent state step from state list of size zero!");
                }
            else if(states.size()==1)
                {
                //too little data!
                return Math.abs(states.get(0));
                }
            else
                {
                HashMap countDiffMap=null;
                for(int i=1;i<states.size();i++)
                    {
                    long diff=states.get(i)-states.get(i-1);
                    countDiffMap=TreeUtils.addCountToMap(countDiffMap,diff);
                    }
                long mostFrequentDiff=(long)(TreeUtils.getKeyWithHighestCount(countDiffMap));
                return mostFrequentDiff;
                }
            }
        else
            {
            throw new IllegalArgumentException("Can't get most frequent state step from state list that is null!");
            }
        }
    
    
    public static void ESSReport(String logTraceFile,String outputFile)
        {

        //make a TSV report file of ESS values various column headers, starts and stops
        try
            {
            String[] colHeaders=null;
            HashMap countDiffMap=null;
            HashMap stateToDataMap=new HashMap();
            BufferedReader br=new BufferedReader(new FileReader(logTraceFile));
            ArrayList<Long> statesArrList=new ArrayList<Long>();
            boolean firstData=true;
            long prevState=0;
            while(br.ready())
                {
                String tempLine=br.readLine();
                if(tempLine.startsWith("#"))
                    {
                    //ignore headers
                    }
                else if(tempLine.startsWith("State"))
                    {
                    //get column headers
                    colHeaders=tempLine.split("\t");
                    }
                else
                    {
                    String[] data=tempLine.split("\t");
                    long state=Long.parseLong(data[0]);
                    stateToDataMap.put(state,data);
                    statesArrList.add(state);
                    if(firstData)
                        {
                        //don't log a diff
                        }
                    else
                        {
                        long diff=state-prevState;
                        countDiffMap=TreeUtils.addCountToMap(countDiffMap,diff);
                        }
                    firstData=false;
                    prevState=state;
                    }
                }//reading the lines
            //obtain the step for ESS calculation
            long mostFrequentDiff=(long)(TreeUtils.getKeyWithHighestCount(countDiffMap));
            //iterate over columns and over all possible states for ESS table
            BufferedWriter bw=new BufferedWriter(new FileWriter(outputFile));
            bw.write("Value\tStartState\tStopState\tESS\n");
            for(int c=1;c<colHeaders.length;c++)
                {
                int essIndexInStr=colHeaders[c].indexOf("_ESS");
                int psIndex=colHeaders[c].indexOf("Proposal_scheme");
                if(essIndexInStr==(-1) && psIndex==(-1) )
                    {
                    //generate data for this column only if it doesn't contain the substring "_ESS"
                    //and if it doesn't contain the substring 'Proposal_scheme'
                    for(int startStateIdx=0;startStateIdx<statesArrList.size()-1;startStateIdx++)
                        {
                        for(int stopStateIdx=startStateIdx+1;stopStateIdx<statesArrList.size();stopStateIdx++)
                            {
                            ArrayList<Double> valsForESS=new ArrayList<Double>();
                            for(int i=startStateIdx;i<=stopStateIdx;i++)
                                {
                                long state=statesArrList.get(i);
                                String[] data=(String[])(stateToDataMap.get(state));
                                Double valForESS=Double.parseDouble(data[c]);
                                valsForESS.add(valForESS);
                                }
                            HashMap statMap=analyseCorrelationContinuous(
                                    TreeUtils.getDoubleArrFromDoubleArrayList(valsForESS),
                                    (int)(mostFrequentDiff));
                            double ESS=(double)(statMap.get("ESS"));
                            String lineToWrite=colHeaders[c]+"\t"+
                                    statesArrList.get(startStateIdx)+"\t"+
                                    statesArrList.get(stopStateIdx)+"\t"+
                                    ESS+"\n";
                            bw.write(lineToWrite);
                            }
                        }
                    }
                }
            bw.flush();
            bw.close();
            
            }
        catch(Exception e)
            {
            System.err.println("Error in computing/writintg ESS data from "+
                    logTraceFile+" and writing it to "+outputFile);
            e.printStackTrace();
            }            
        }
    
    
    
    private static final int MAX_LAG = 200000;

    public static double readInputForESSFromSTDIN()
        {
        
        try 
            {
            ArrayList<Long> stateNums=new ArrayList<Long>();
            ArrayList<Double> vals=new ArrayList<Double>();
            InputStreamReader isr=new InputStreamReader(System.in);
            BufferedReader br=new BufferedReader(isr);
            while(br.ready())
                {
                String dataLine=br.readLine();
                String[] dataLinePieces=dataLine.split("\t");
                stateNums.add(Long.parseLong(dataLinePieces[0].trim()));
                vals.add(Double.parseDouble(dataLinePieces[1].trim()));
                }
            double[] dVals=new double[vals.size()];
            for(int d=0;d<dVals.length;d++)
                {
                dVals[d]=vals.get(d);
                }
            long stateStep=getMostFrequentStateStep(stateNums);
            //System.out.println("StateStep is "+stateStep);
            //System.out.println("Input vals : "+Arrays.toString(dVals));
            HashMap traceStats=analyseCorrelationContinuous(dVals,(int)(stateStep));
            double ess=(double)(traceStats.get("ESS"));
            return ess;
            }   
        catch(Exception e)
            {
            e.printStackTrace();
            }
        throw new RuntimeException("Error in computing ESS!");
        }
    
    
    
    
    public static HashMap analyseCorrelationContinuous(double[] values, int stepSize)
        {

        double v_sum=0;
        for(int i=0;i<values.length;i++)
            {
            v_sum+=values[i];
            }
        double mean=v_sum/(double)(values.length);
        
        
        int samples = values.length;
        int maxLag = Math.min(samples - 1, MAX_LAG);

        double[] gammaStat = new double[MAX_LAG];
        //System.err.println("len of gammastat is "+gammaStat.length);
        double varStat = 0.0;


        for (int lag = 0; lag < maxLag; lag++) 
            {
            for (int j = 0; j < samples - lag; j++)
                {
                double del1 = values[j] - mean;
                double del2 = values[j + lag] - mean;
                gammaStat[lag] += (del1 * del2);
                }

            gammaStat[lag] /= ((double) (samples - lag));
        
            if (lag == 0)
                {
                varStat = gammaStat[0];
                }
            else if (lag % 2 == 0) 
                {
                // fancy stopping criterion :)
                if (gammaStat[lag - 1] + gammaStat[lag] > 0)
                    {
                    varStat += 2.0 * (gammaStat[lag - 1] + gammaStat[lag]);
                    }
                else
                    {
                    maxLag = lag;
                    }
                }
            }//lag loop

        // standard error of mean
        double stdErrorOfMean = Math.sqrt(varStat / samples);

        // auto correlation time
        double ACT = stepSize * varStat / gammaStat[0];

        // effective sample size
        double ESS = (stepSize * samples) / ACT;

        // standard deviation of autocorrelation time
        double stdErrOfACT = (2.0 * Math.sqrt(2.0 * (2.0 * (double) (maxLag + 1)) / samples) * (varStat / gammaStat[0]) * stepSize);

        HashMap traceStats=new HashMap();
        traceStats.put("mean",mean);
        traceStats.put("ACT",ACT);
        traceStats.put("ESS",ESS);
        traceStats.put("stdErrOfACT",stdErrOfACT);
        return traceStats;
        }    

    /*public static HashMap analyseCorrelationContinuous(double[] values, int[] states)
        {
        
        }*/
    
    
}
