/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author esalina
 */
public class SWPaperAlgOneGenerator implements TreeGenerator {
    
    public double computePriordouble(coa_lineage current)
        {
        throw new RuntimeException("Error, SWA1 prior needed");
        }
    
    
    double beta=1;
    
    private static final double blStep_init=0.1;
    private double blStep=blStep_init;

    
    public double getBeta()
        {
        return beta;
        }

    public void setBeta(double b)
        {
        if(b<0 || b>1)
            {
            throw new IllegalArgumentException("Error, supplied beta is "+b+", but is not within range [0,1]!");
            }
        beta=b;
        }
    
    public HashMap getAdaptiveMap()
        {
        HashMap h=new HashMap();
        h.put("blStep",this.blStep);
        return h;
        }
    
    public void setAdaptiveMap(HashMap i)
        {
        this.blStep=(double)(i.get("blStep"));
        }
    
    private boolean topBeta=false;
    public void setTopBeta(boolean tb)
        {
        topBeta=tb;
        }
    
    
    
    private Double maxDistanceFromProbMatrix=null;
    private Double minDistanceFromProbMatrix=null;
    
    public void setMinDistanceFromProbMatrix(double d)
        {
        if(d<=0)
            {
            throw new RuntimeException("Error, trying to set maximum distance for branch that is not positive : "+d);
            }
        minDistanceFromProbMatrix=new Double(d);
        System.out.println("Alg#1 min branch length set at : "+minDistanceFromProbMatrix);
        }
    
    public void setMaxDistanceFromProbMatrix(double d)
        {
        if(d<=0)
            {
            throw new RuntimeException("Error, trying to set maximum distance for branch that is not positive : "+d);
            }
        maxDistanceFromProbMatrix=new Double(d);
        System.out.println("Alg#1 max branch length set at : "+maxDistanceFromProbMatrix);
        }
    

    public double estThetaHatIP278(coa_lineage current)
        {
        int mvd=SWPaperAlgOneGenerator.getMaxValidDepth(current);
        int numLeaves=current.getNumLeavesUnder(true);
        double sum=0;
        for(int depth=0;depth<=mvd;depth++)
            {
            ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(current,depth);
            int k=0;
            for(coa_lineage nodeAtADepth:nodesAtDepth)
                {
                k+=nodeAtADepth.getNumKids();
                }
            double contribution=k*(k-1)*nodesAtDepth.get(0).getDist(0);
            sum+=contribution;
            //System.err.println("At depth="+depth+" we have k="+k+" and contribution="+contribution);
            }
        double ratio=sum/(numLeaves-1);
        return ratio;
        }
    
    
    
    FastaIO myFastaIO=null;
    public void setFasta(FastaIO fio)
        {
        this.myFastaIO=fio;
        }
    
    
    public static boolean betweenInclusive(double x,double low,double high)
        {
        if((low<=x) && (x<=high))
            {
            return true;
            }
        else
            {
            return false;
            }
        }
    
    boolean addBranchLogLike=false;
    
    
    public SWPaperAlgOneGenerator(int num_lineages,double nminpsi,double nmaxpsi,boolean initAddBranchLL)
        {
        addBranchLogLike=initAddBranchLL;
        if(addBranchLogLike)
            {
            String msg="SWA1 TreeGeneration code initialized with addBranchLogLike set to TRUE.";
            msg+="\nTo add branch-length log-likelihoods for tree prior calculation.";
            System.out.println(msg);
            }
        else
            {
            String msg="SWA1 TreeGeneration code initialized with addBranchLogLike set to FALSE.";
            msg+="\nTo get average branch-length log-likelihoods for tree prior calculation.";
            System.out.println(msg);
            }
        //double bound=TreeUtils.findBoundaryWhereExpDistMeanOnePDFGoesToZeroAndCDFGoesToOne(false,false);
        this.min_psi=nminpsi;
        this.max_psi=nmaxpsi;
        params=new HashMap();
        params.put("num_lineages",num_lineages);
        proposeParams();        
        }
    
    
    
    public SWPaperAlgOneGenerator(int num_lineages,boolean acceptDefaults)
        {

        if(!acceptDefaults)
            {
            max_psi=2;
            min_psi=2;
            while(!betweenInclusive(max_psi,0,1) || !betweenInclusive(min_psi,0,1) || max_psi<min_psi)
                {
                String minp=TreeUtils.readLine("Enter min psi");
                String maxp=TreeUtils.readLine("Enter max psi");
                max_psi=Double.parseDouble(maxp);
                min_psi=Double.parseDouble(minp);
                }
            }//if not accept defaults, get them from user
        params=new HashMap();
        params.put("num_lineages",num_lineages);
        proposeParams();
        }
    
    double min_psi=1.0;
    double max_psi=1.0;
    HashMap params;
    
    public HashMap cloneParams()
        {
        return TreeUtils.cloneMap(params);
        }
    
    
    public void proposeParams()
        {
        if(params==null)
            {
            params=new HashMap();
            }
        }
    
    
    public HashMap getParams()
        {
            
        return this.params;
        }
    

    
    public void setParams(HashMap paramMap)
        {

        this.params=paramMap;
        }
    

    public static boolean haveSameHeigts(ArrayList<coa_lineage> t)
        {
        if(t==null)
            {
            throw new NullPointerException("Error, t, null?!");
            }
        else if(t.size()<=1)
            {
            return true;
            }
        else
            {
            double minH=t.get(0).getHeight();
            double maxH=t.get(0).getHeight();
            for(int i=0;i<t.size();i++)
                {
                if(minH>t.get(i).getHeight())
                    {
                    minH=t.get(i).getHeight();
                    }
                if(maxH<t.get(i).getHeight())
                    {
                    maxH=t.get(i).getHeight();
                    }
                }
            double dist=Math.abs(maxH-minH);
            if(dist>=0.00001)
                {
                return false;
                }
            else
                {
                return true;
                }
            }
            
        }
    
    
    private BigDecimal[] piJointDist(int num_lineages)
        {
        BigDecimal psi=null;
        psi=new BigDecimal(TreeUtils.getRandNumBet(min_psi, max_psi,true));
        int logBTwo=(int)(TreeUtils.logBaseTwo(num_lineages));
        if(logBTwo==0)
            {
            logBTwo+=1;
            }
        //BigDecimal Y=(BigDecimal)(params.get("Y"));
        int binY=TreeUtils.binomialDraw(0.5,logBTwo*2);
        binY++;
        BigDecimal Y=new BigDecimal(binY);
        BigDecimal[] return_pack=new BigDecimal[2];
        return_pack[0]=psi;
        return_pack[1]=Y;
        return return_pack;
        }
    

    private static boolean atLeastOneMerger(int[] bins)
        {
        //0 is overlap bin
        for(int b=1;b<bins.length;b++)
            {
            if(bins[b]>=2)
                {
                return true;
                }
            }
        return false;
        }
    
    
    public Object[] generateTree()
        {
 
        /*
            STEP #1 Start with K = n. (K is the number of the
            ancestors of the sample as we follow them back in time.)
        */
        int num_lineages_to_get=Integer.parseInt(params.get("num_lineages").toString());        

        ArrayList<coa_lineage> tempLineages=new ArrayList<coa_lineage>();
        for(int l=0;l<num_lineages_to_get;l++)
            {
            coa_lineage tempLineage=new coa_lineage(""+(l+1)+"");
            tempLineages.add(tempLineage);
            }


        
        double maxHeight=0;
        ArrayList<SREvent> sreList=new ArrayList<SREvent>();
        int max_val_depth=(-1);
        while(tempLineages.size()>1)
            {
            /*
            STEP #2 Sample (phi, Y) from a joint distribution pi (.,.)
            acquire model parameters
            */
            BigDecimal[] piDist=piJointDist(tempLineages.size());
            BigDecimal psi=piDist[0];
            BigDecimal Y=piDist[1];

            /*
            STEP #3 Generate the waiting time t to the first SRE (looking backwards
            in time): It is an exponential random variable with mean one.
            */
            //get the SRE wait time
            double sreWait=TreeUtils.drawExponentialMeanOne();
            //System.out.println("The value of maxDistanceFromProbMatrix is "+maxDistanceFromProbMatrix);
            while(sreWait>=this.maxDistanceFromProbMatrix.doubleValue())
                {
                sreWait=TreeUtils.drawExponentialMeanOne();
                }
            
            /*
            after the increment (by maxHeight) here, the srewait now points to the
            time of the next coalescent event (where the time is
            relative to time=0)
            */
            SREvent newSRE=new SREvent(Y.intValue(),psi.doubleValue(),sreWait+maxHeight);
            sreList.add(newSRE);

            

            /*
            STEP#4 
            Sample P (k0, k1, .... kY) from Mn(K,p0,p1,...,pY), where Y i=0
            ki = K, p0 = 1 - phi and pi = phi/Y i=1,...,Y. (Here
            Mn(.) denotes the multinomial distribution.)
            */
            //obtain the sample from the multinomial distribution
            //use the p for p_i values
            //define the p_i values in terms of Y
            double[] p=new double[Y.intValue()+1];
            p[0]=1.0-psi.doubleValue();
            for(int pi=1;pi<p.length;pi++)
                {
                p[pi]=psi.doubleValue()/Y.doubleValue();
                }
            int n_sum=tempLineages.size();
            int[] bins=TreeUtils.multinomialVector(n_sum, p);
            //always have at least one merger!
            //int numRetry=0;
            while(!atLeastOneMerger(bins))
                {
                bins=TreeUtils.multinomialVector(n_sum, p);
                //numRetry++;
                }
            
            /*System.out.println("##########################################################");
            System.out.println("ROUND "+round+" : sre="+sreWait+" bins="+Arrays.toString(bins));
            System.out.println("ROUND "+round+" ; size of lineages : "+tempLineages.size());
            for(int s=0;s<tempLineages.size();s++)
                {
                System.out.println("Lineage #"+s+" : "+tempLineages.get(s).toString());
                }*/

            
            
            
            /*
            STEP #5 
            If ki 6= 0, i = 1, . . . , Y then randomly choose ki individuals
            from K and merge them into a common ancestor.
            */
            //use the bin assignments to choose b_i lineages
            //and assign them to that merger
            //create a new list of next lineages
            ArrayList<coa_lineage> newLineages=new ArrayList<coa_lineage>();
            for(int b=0;b<bins.length;b++)
                {
                //iterate through the bins and merge the number found
                int numToPick=bins[b];
                //System.out.println("At bin="+b+", num to pick is "+numToPick);


                
                if(numToPick>0 && b>0)
                    {
                    //require b>0 because i>=1 in step #5 of Alg #1
                    //create a new lineage for the merger
                    coa_lineage aNewLineage=new coa_lineage("(");


                    
                    int[] randomLineages=TreeUtils.chooseRandomSubset(numToPick,tempLineages.size());
                    //System.out.println("The randomly picked lineages : "+Arrays.toString(randomLineages));
                    for(int r=0;r<randomLineages.length;r++)
                        {
                        /*System.out.println(
                        "Adding a child ("+tempLineages.get(randomLineages[r]).getName()+
                        ") to the new lineage...");*/

                            
                        double newCoaDist=sreWait;
                        aNewLineage.addChild(
                                tempLineages.get(randomLineages[r]),
                                newCoaDist                                
                                );

                        if(randomLineages.length==1)
                            {
                            aNewLineage.setName(tempLineages.get(randomLineages[r]).getName());
                            }//exactly one random lineage to merge in
                        else if(randomLineages.length>1)
                            {
                            if(r<randomLineages.length-1)
                                {
                                aNewLineage.setName(aNewLineage.getName()+" & "+
                                        tempLineages.get(randomLineages[r]).getName());
                                }
                            else
                                {
                                aNewLineage.setName(aNewLineage.getName()+
                                        tempLineages.get(randomLineages[r]).getName());
                                }
                            }

                        }//multiple lineages to merge in 
                    aNewLineage.setName(aNewLineage.getName()+")");
                    
                    
                    //remove from tempLineages the ones that were merged in
                    Arrays.sort(randomLineages);
                    for(int r=randomLineages.length-1;r>=0;r--)
                        {
                        tempLineages.remove(randomLineages[r]);
                        }

                    //add to the new lineages the new lineage
                    newLineages.add(aNewLineage);
                    
                    /*System.out.println("After coalescing :");
                    for(int j=0;j<tempLineages.size();j++)
                        {
                        System.out.println("Lineage # "+j+" : "+tempLineages.get(j).toString());
                        }
                    System.out.println("\n\n");*/
                    
                    }
                else if(b==0)
                    {
                    for(int n=0;n<numToPick;n++)
                        {
                        coa_lineage aNewLineage=new coa_lineage("");
                        //getRandomInt is inclusize
                        int randomLineageIndex=TreeUtils.getRandomInt(0,tempLineages.size()-1);
                        double newDist=sreWait;
                        aNewLineage.addChild(tempLineages.get(randomLineageIndex),newDist);
                        tempLineages.remove(tempLineages.get(randomLineageIndex));
                        newLineages.add(aNewLineage);
                        }

                    //bin zero represents the 'overlap' bin!
                    }
 
                }//for each bin in this round
            
            if(tempLineages.size()!=0)
                {
                throw new RuntimeException("Error, some lineage didn't get merged in!");
                }
            
            
            //set the lineages for the next round after any merging
            for(int l=0;l<tempLineages.size();l++)
                {
                newLineages.add(tempLineages.get(l));
                }
            tempLineages=null;
            tempLineages=newLineages;
            maxHeight=maxHeight+sreWait;
            
            /*
            STEP #6
            Reset the number of the ancestors to K = k0 + i=1
            1{ki 6= 0}, where 1{x} is one if the x is true and zero, otherwise. Stop if K
            is equal to one, otherwise return to Step 2.
            */
            
            //a layer is added
            max_val_depth++;
            
            
            
            }//tempLineages.size()>1
        tempLineages.get(0).setPosteriorSampledValue("max_val_depth",max_val_depth);
        
        
        if(SWPaperAlgOneGenerator.setPosteriors)
            {
            SREvent[] SREArr=SREvent.makeArray(sreList);
            double[] sreWaits=SREvent.getSREWaits(SREArr);
            String sreWaitsAsStr=Arrays.toString(sreWaits);
            double[] sreHeights=SREvent.getSREHeights(SREArr);
            String sreHeightsAsStr=Arrays.toString(sreHeights);
            tempLineages.get(0).setPosteriorSampledValue("sre_heights",sreHeightsAsStr);
            tempLineages.get(0).setPosteriorSampledValue("sre_waits",sreWaitsAsStr);
            tempLineages.get(0).setPosteriorSampledValue("sre_profile",SREvent.JSONString(sreList));
            }

        Object[] retPackage=new Object[2];
        retPackage[0]=tempLineages.get(0);
        retPackage[1]=new BigDecimal("1");
        
        recentCode=0;
        return retPackage;
            
        }
    
    private static SREvent[] newPosteriors(coa_lineage current)
        {
            
        String cometDisablePhyloXMLVar=System.getenv("COMET_DISABLE_POSTERIORS");
        if(cometDisablePhyloXMLVar!=null)
            {
            return null;
            }
            
            
        int maxValDepth=SWPaperAlgOneGenerator.getMaxValidDepth(current);
        SREvent[] SREvents=new SREvent[maxValDepth+1];
        for(int s=0;s<=maxValDepth;s++)
            {
            int mergerCount=0;
            int overlapCount=0;
            int depth=s;
            ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(current,depth);
            //System.err.println("For a depth "+depth+" a node at the depth has h="+nodesAtDepth.get(0).getHeight());
            int newY=0;
            for(coa_lineage m:nodesAtDepth)
                {
                int m_kids=m.getNumKids();
                if(m_kids>=2)
                    {
                    newY++;
                    }
                if(m_kids>1)
                    {
                    mergerCount+=m_kids;
                    }
                else if(m.getNumKids()==1)
                    {
                    overlapCount++;
                    }
                else
                    {
                    System.out.println("For a node m,at depth "+s+", mvd="+maxValDepth+", its num kids is : "+m.getNumKids());
                    throw new RuntimeException("Error, invalid number of kids at depth!");
                    }
                }
            if(newY==0)
                {
                newY=1;
                }            
            double pctOverLap=(double)(overlapCount)/(double)(mergerCount+overlapCount);
            SREvents[s]=new SREvent(newY,1.0-pctOverLap,nodesAtDepth.get(0).getHeight());
            }
        SREvent.sortByHeight(SREvents);
        current.setPosteriorSampledValue("sre_profile",SREvent.JSONString(SREvents));
        return SREvents;
        }
    
    
    
    private Object[] getNodesForExtensionAndContractionSREDropDownAndMergeUp(coa_lineage tRoot,int mvd)
        {

        ArrayList<coa_lineage> usableForExtension=new ArrayList<coa_lineage>();
        ArrayList<Integer> extDepthLevels=new ArrayList<Integer>();
        ArrayList<coa_lineage> usableForContracton=new ArrayList<coa_lineage>();
        ArrayList<Integer> contrDepthLevels=new ArrayList<Integer>();
        ArrayList<coa_lineage> SREDropDownNodes=new ArrayList<coa_lineage>();
        ArrayList<Integer> SREDropDownLevels=new ArrayList<Integer>();
        ArrayList<coa_lineage> SRECoalUpNodes=new ArrayList<coa_lineage>();
        ArrayList<Integer> SRECoalUpNodesLevels=new ArrayList<Integer>();
        
        for(int d=0;d<=mvd;d++)
            {
            if(d==0)
                {
                //root is only eligible for extension or SREdrop if it has 3 or more kids
                if(tRoot.getNumKids()>=3)
                    {
                    //extension
                    usableForExtension.add(tRoot);
                    extDepthLevels.add(0);
                    //SRE dropdown if root has 3 or more kids
                    SREDropDownNodes.add(tRoot);
                    SREDropDownLevels.add(0);
                    }
                }//for depth=0
            else
                {
                ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(tRoot,d);
                //get number of nodes with 2 or more kids at this level
                int numNodesWithTwoOrMoreKidsAtThisLevel=0;
                int numNodesWithExactlyTwoKidsAtThisLevel=0;
                for(coa_lineage nodeAtADepth:nodesAtDepth)
                    {
                    if(nodeAtADepth.getNumKids()>=2)
                        {
                        numNodesWithTwoOrMoreKidsAtThisLevel++;
                        }
                    if(nodeAtADepth.getNumKids()==2)
                        {
                        numNodesWithExactlyTwoKidsAtThisLevel++;
                        }
                    }
                for(coa_lineage nodeAtADepth:nodesAtDepth)
                    {
                    if(nodeAtADepth.getNumKids()==1 && !nodeAtADepth.isTip())
                        {
                        coa_lineage p=nodeAtADepth.getParentRef();
                        if(p.getNumKids()>=2)
                            {
                            //eligible for lineage retraction if parent has enough kids
                            usableForContracton.add(nodeAtADepth);
                            contrDepthLevels.add(d);
                            }
                        }
                    if(numNodesWithTwoOrMoreKidsAtThisLevel>=2 &&  nodeAtADepth.getNumKids()>=2)
                        {
                        /*to be eligible for extension, 
                            the number of nodes with two
                            or more kids at the level must be 2 or more.
                            That way, there's always at least one merging per SRE.
                          Also, the particular node must have two or more kids, one of
                            whom will be extended!
                        */
                        usableForExtension.add(nodeAtADepth);
                        extDepthLevels.add(d);
                        SREDropDownNodes.add(nodeAtADepth);
                        SREDropDownLevels.add(d);
                        }
                    if(numNodesWithExactlyTwoKidsAtThisLevel==1 && nodeAtADepth.getNumKids()==2)
                        {
                        /*
                        If the SRE level has exactly one merger and it's of two lineages, then it can 
                            coalesce up */
                        SRECoalUpNodes.add(nodeAtADepth);
                        SRECoalUpNodesLevels.add(d);
                        }
                    }
                }//depth not zero
            }
        
        Object[] ret=new Object[8];
        ret[0]=usableForExtension;
        ret[1]=extDepthLevels;
        ret[2]=usableForContracton;
        ret[3]=contrDepthLevels;
        ret[4]=SREDropDownNodes;
        ret[5]=SREDropDownLevels;
        ret[6]=SRECoalUpNodes;
        ret[7]=SRECoalUpNodesLevels;

                
        return ret;        
        }
    
    
    private static double[] getBranchLengths(coa_lineage r)
        {
        coa_lineage temp=r;
        ArrayList<Double> l=new ArrayList<Double>();
        while(!temp.isTip())
            {
            l.add(temp.getDist(0));
            temp=temp.getChild(0);
            }
        double[] ls=new double[l.size()];
        for(int x=0;x<l.size();x++)
            {
            ls[x]=l.get(x);
            }
        return ls;
        }
    
    private static double getPriorUsingBranchLengths(double[] ls,boolean doAvg)
        {
        double lnsum=0;
        for(int i=0;i<ls.length;i++)
            {
            double prob=TreeUtils.exponentialPDF(1,ls[i]);
            double ln_prob=Math.log(prob);
            lnsum+=ln_prob;
            }
        if(!doAvg)
            {
            return lnsum;
            }
        else
            {
            return lnsum/(double)(ls.length);
            }
        }

    
    
    
    public Object[] proposeTree(coa_lineage current,BigDecimal cPrior)
        {
        Object[] retPack=null;
        HashMap curMap=current.getPosteriorSampleMap();
        Object l30Ratio=curMap.get("last_30_acc_ratio");
        double acc30=(double)l30Ratio;
        int numOp=1;
        if(acc30<=0.25)
            {
            numOp=1;
            }
        else
            {
            numOp=2;
            }
        for(int opRound=0;opRound<numOp;opRound++)
            {
            double randVal=TreeUtils.getRandNumBet(0,1,false);
            if(randVal<=0.1)
                {
                retPack=adjustBranchLengthsOnly(current,cPrior,false,1);
                }
            else
                {
                retPack=adjustBranchLengthsOnly(current,cPrior,true,0.01);
                randVal=TreeUtils.getRandNumBet(0,1,false);
                BigDecimal tempCPrior=(BigDecimal)(retPack[1]);
                if(randVal<=0.25)
                    {
                    int mvd=SWPaperAlgOneGenerator.getMaxValidDepth(current);
                    Object[] extContrNodes=getNodesForExtensionAndContractionSREDropDownAndMergeUp(current,mvd);
                    ArrayList<coa_lineage> extNodes=(ArrayList<coa_lineage>)extContrNodes[0];
                    ArrayList<coa_lineage> contrNodes=(ArrayList<coa_lineage>)extContrNodes[2];
                    ArrayList<coa_lineage> SREDropDownNodes=(ArrayList<coa_lineage>)extContrNodes[4];
                    ArrayList<Integer> SREDropDownNodesDepths=(ArrayList<Integer>)extContrNodes[5];
                    ArrayList<coa_lineage> SRECoalNodes=(ArrayList<coa_lineage>)extContrNodes[6];
                    ArrayList<Integer> SRECoalNodesDepths=(ArrayList<Integer>)extContrNodes[7];
                    if(numOp==2)
                        {
                        if(opRound%2==0)
                            {
                            //System.out.println("removing sre nodes");
                            SRECoalNodes.removeAll(SRECoalNodes);
                            SREDropDownNodes.removeAll(SREDropDownNodes);
                            }
                        else
                            {
                            extNodes.removeAll(extNodes);
                            contrNodes.removeAll(contrNodes);
                            //System.out.println("removing e/c nodes");
                            }
                        }
                    int[] styleCounts={extNodes.size(),contrNodes.size(),SREDropDownNodes.size(),SRECoalNodes.size()};
                    double[] styleProbs=TreeUtils.makeMultinomialVector(styleCounts);
                    int styleChoice=TreeUtils.drawMultinomial(styleProbs);

                    /*double extRatio=
                                (double)(extNodes.size())
                            /
                                (
                                    (double)(contrNodes.size())
                                        +
                                    (double)(extNodes.size())
                                );*/
                    if(styleChoice==0)
                        {
                        //overlap extension
                        if(extNodes.size()>0)
                            {
                            retPack=preScreenOverlapExtension(
                                    current,
                                    tempCPrior,
                                    mvd,
                                    extNodes
                                    );                        
                            }
                        }
                    else if(styleChoice==1)
                        {
                        //overlap contraction
                        if(contrNodes.size()>0)
                            {
                            retPack=preScreenOverlapRetraction(
                                    current,
                                    tempCPrior,
                                    mvd,
                                    contrNodes
                                    );
                            }
                        }
                    else if(styleChoice==2)
                        {
                        //SRE drop-down
                        if(SREDropDownNodes.size()>0)
                            {
                            retPack=preScreenSREDropDown(
                                        current,
                                        cPrior,
                                        mvd,
                                        SREDropDownNodes,
                                        SREDropDownNodesDepths
                                    );
                            }
                        }
                    else if(styleChoice==3)
                        {
                        //SRE coal-up
                        if(SRECoalNodes.size()>0)
                            {
                            retPack=preScreenSRECoalUp(
                                    current,
                                    cPrior,
                                    mvd,
                                    SRECoalNodes,
                                    SRECoalNodesDepths           
                                    );
                            }
                        }
                    }
                else// if(randVal<=0.75)
                    {
                    retPack=lineageAdoption(current,tempCPrior);
                    }
                }
            if(setPosteriors)
                {
                newPosteriors((coa_lineage)retPack[0]);
                }
            else
                {
                Integer mvd=(Integer)(((coa_lineage)retPack[0]).getPosteriorSampleMap().get("max_val_depth"));
                HashMap prevMap=((coa_lineage)retPack[0]).getPosteriorSampleMap();
                ((coa_lineage)retPack[0]).resetPosteriorSampledValues();
                ((coa_lineage)retPack[0]).setPosteriorSampledValue("max_val_depth", mvd);
                ((coa_lineage)retPack[0]).setPosteriorSampledValue("iter_num",prevMap.get("iter_num"));
                ((coa_lineage)retPack[0]).setPosteriorSampledValue("last_30_acc_ratio",prevMap.get("last_30_acc_ratio"));
                }
            double[] branch_lengths=getBranchLengths((coa_lineage)retPack[0]);
            //addBranchLogLike
            boolean doAvg=true;
            if(addBranchLogLike)
                {
                doAvg=false;
                }
            else
                {
                doAvg=true;
                }
            double ln_priro_branch_lengths=getPriorUsingBranchLengths(branch_lengths,doAvg);
            retPack[1]=new BigDecimal(ln_priro_branch_lengths);
            current=(coa_lineage)(retPack[0]);
            }//round/operator loop
        return retPack;
        }
    
    public static boolean setPosteriors=true;
    
    int recentCode=0;
    
    public int getLastProposalCode()
        {
        return recentCode;
        }
    
    public static final String[] proposalDescriptions={
        "GENERIC_PROPOSAL",         //0
        "BRANCH_LENGTH_ADJUSTMENT", //1
        "SRE_BIN_SWAPPING",         //2
        "OVERLAP_EXTENSION",        //3
        "OVERLAP_RETRACTION",       //4
        "LINEAGE_ADOPTION",         //5
        "SRE_LAYER_REMOVAL",        //6
        "SRE_DROP_DOWN",            //7
        "SRE_COAL_UP"               //8
        };
    
    
    public String getProposalDescription(int code)
        {
        try
            {
            return proposalDescriptions[code];
            }
        catch(Exception e)
            {
            throw new RuntimeException("Error, unknown proposal code "+code);
            }
        }

    private static int getMaxValidDepth(coa_lineage aRoot)
        {
        if(aRoot==null)
            {
            throw new RuntimeException("Error a root is null?!?!");
            }
        HashMap psMap=aRoot.getPosteriorSampleMap();
        try
            {
            Integer mvd_int_obj=(Integer)psMap.get("max_val_depth");
            return mvd_int_obj;
            }
        catch(Exception e)
            {
            //System.err.println("couldn't get MVD!");
            }
        coa_lineage temp=aRoot;
        int maxValDepth=0;
        while(temp!=null)
            {
            if(temp.getNumKids()==0)
                {
                //we're at a leaf
                maxValDepth--;
                temp=null;
                }
            else
                {
                maxValDepth++;
                temp=temp.getChild(0);
                }
            }
        aRoot.setPosteriorSampledValue("max_val_depth",maxValDepth);       
        return maxValDepth;
        }
    
    private Object[] lineageAdoption(coa_lineage current,BigDecimal cPrior)
        {
        /*
            For adoption:
            1) Any node m with at least 2 children is eligible to give one up for adoption.
                One other node at its SRE level is eligible to adopt the lineage
            2) Choose such a node m at random and choose one of its children c at random
            3) Chose a random node a at its depth to adopt the child
            4) Update the posteriors and return the tre
        */
        
        ArrayList<coa_lineage> nodesToGiveUpKids=new ArrayList<coa_lineage>();
        ArrayList<Integer> correspondingDepths=new ArrayList<Integer>();
        HashMap nodesAtADepth=new HashMap();
        int maxValDepth=SWPaperAlgOneGenerator.getMaxValidDepth(current);
        for(int s=0;s<=maxValDepth;s++)
            {
            int depth=s;
            ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(current,depth);
            //this check makes sure there are at least two nodes (giver and receiver)
            //at the level!
            if(nodesAtDepth.size()>=2)
                {
                nodesAtADepth.put(depth,nodesAtDepth);
                for(coa_lineage m:nodesAtDepth)
                    {
                    //make sure the giver will still have a child after giving
                    if(m.getNumKids()>=2)
                        {
                        nodesToGiveUpKids.add(m);
                        correspondingDepths.add(depth);
                        }
                    }
                }
            }
        if(nodesToGiveUpKids.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        
        //chose a node at random to give up a child
        int selectionIndex=TreeUtils.chooseRandomIndex(nodesToGiveUpKids.size());
        coa_lineage fullNode=nodesToGiveUpKids.get(selectionIndex);
        int itsDepth=correspondingDepths.get(selectionIndex);
        //chose a node at random to adopt a child
        ArrayList<coa_lineage> eligibleToAdopt=new ArrayList<coa_lineage>();
        ArrayList<coa_lineage> nodesAtTheDepth=(ArrayList<coa_lineage>)(nodesAtADepth.get(itsDepth));
        for(coa_lineage p:nodesAtTheDepth)
            {
            //make sure that the adopter isn't the receiver!
            if(p!=fullNode)
                {
                eligibleToAdopt.add(p);
                }
            }
        //chose a node to adopt
        coa_lineage a=(coa_lineage)(TreeUtils.chooseRandom(eligibleToAdopt));
        //chose a random child to let go
        int randomChildToLetGoIndex=TreeUtils.chooseRandomIndex(fullNode.getNumKids());
        double randomChildDist=fullNode.getDist(randomChildToLetGoIndex);
        //perform the adoption
        a.addChild(fullNode.getChild(randomChildToLetGoIndex),randomChildDist);
        fullNode.removeChild(randomChildToLetGoIndex);
        //update the posteriors
        
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        recentCode=5;
        return  ret_pack;
        }

    
    private Object[] preScreenSRECoalUp(
            coa_lineage current,
            BigDecimal cPrior,
            int mvd,
            ArrayList<coa_lineage> eligibleNodes,
            ArrayList<Integer> nodesDepths            
            )
        {
            
        if(eligibleNodes.size()<=0)
            {
            //if no candidate nodes exist try recursion
            //return proposeTree(current,cPrior);
            Object[] ret_pack=new Object[2];
            ret_pack[0]=current;
            ret_pack[1]=cPrior;
            return ret_pack;
            }  
        
        int randIndex=TreeUtils.chooseRandomIndex(eligibleNodes.size());
        int mergerDepth=nodesDepths.get(randIndex);
        int parentDepth=mergerDepth-1;
        ArrayList<coa_lineage> parentsAbove=SWPaperAlgOneGenerator.getNodesAtDepth(current,parentDepth);
        double newSREDist=parentsAbove.get(0).getDist(0)+parentsAbove.get(0).getChild(0).getDist(0);
        for(coa_lineage p:parentsAbove)
            {
            ArrayList<coa_lineage> grandKidList=new ArrayList<coa_lineage>();
            //ArrayList<coa_lineage>
            for(int k=0;k<p.getNumKids();k++)
                {
                coa_lineage a_kid=p.getChild(k);
                int numGrandKids=a_kid.getNumKids();
                for(int kk=0;kk<numGrandKids;kk++)
                    {
                    grandKidList.add(a_kid.getChild(kk));
                    }
                }
            p.removeAllKids();
            for(int gk=0;gk<grandKidList.size();gk++)
                {
                p.addChild(grandKidList.get(gk),newSREDist);
                }            
            }
         
        mvd--;
        current.setPosteriorSampledValue("max_val_depth",mvd);        
        
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        
        recentCode=8;
        return ret_pack;            
        }
    
    
    
    private Object[] preScreenSREDropDown(
            coa_lineage current,
            BigDecimal cPrior,
            int mvd,
            ArrayList<coa_lineage> eligibleNodes,
            ArrayList<Integer> nodesDepths
        )
        {

        if(eligibleNodes.size()<=0)
            {
            //if no candidate nodes exist try recursion
            //return proposeTree(current,cPrior);
            Object[] ret_pack=new Object[2];
            ret_pack[0]=current;
            ret_pack[1]=cPrior;
            return ret_pack;
            }            
        int randIndex=TreeUtils.chooseRandomIndex(eligibleNodes.size());
        coa_lineage s=eligibleNodes.get(randIndex);
        int s_depth=nodesDepths.get(randIndex);
        ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(current,s_depth);        
        double existingSREDistUnder=s.getDist(0);
        if(existingSREDistUnder/10.0<=this.minDistanceFromProbMatrix)
            {
            //if the dist under is very short increase the dist so the new SRE has room
            for(coa_lineage n:nodesAtDepth)
                {
                n.setAllDists(existingSREDistUnder*2);
                }
            }
        double newSREDists=existingSREDistUnder/2.0;

        //choose two lineages at random from the node
        //System.out.println("Number of kids of s is "+s.getNumKids());
        int kid_1=0;
        kid_1=TreeUtils.chooseRandomIndex(s.getNumKids());
        int kid_2=kid_1;
        while(kid_2==kid_1)
            {
            kid_2=TreeUtils.chooseRandomIndex(s.getNumKids());
            }
        //System.out.println("k1 and k2 are "+kid_1+" and "+kid_2);
        //System.out.println("the number of nodes at the depth "+s_depth+" is "+nodesAtDepth.size());
        for(coa_lineage n:nodesAtDepth)
            {
            int numDescBefore=n.getNumLeavesUnder(false);
            //System.out.println("Before edit, numdescendants is "+numDescBefore);
            if(n==s)
                {
                //perform the drop down
                //System.out.println("looking at s");
                ArrayList<coa_lineage> the_kids=n.removeAllKids();
                //System.out.println("size of the_kids is "+the_kids.size());
                coa_lineage newMergerNode=new coa_lineage("");
                newMergerNode.addChild(the_kids.get(kid_1),newSREDists);
                newMergerNode.addChild(the_kids.get(kid_2),newSREDists);
                for(int i=0;i<the_kids.size();i++)
                    {
                    if(i!=kid_1 && i!=kid_2)
                        {
                        coa_lineage newNode=new coa_lineage("");
                        newNode.addChild(the_kids.get(i),newSREDists);
                        n.addChild(newNode,newSREDists);
                        }
                    }
                n.addChild(newMergerNode,newSREDists);
                }
            else
                {
                //System.out.println("looking at other");
                
                ArrayList<coa_lineage> the_kids=n.removeAllKids();
                //System.out.println("size of the_kids is "+the_kids.size());
                for(int i=0;i<the_kids.size();i++)
                    {
                    coa_lineage newNode=new coa_lineage("");
                    newNode.addChild(the_kids.get(i),newSREDists);
                    n.addChild(newNode,newSREDists);
                    }
                }
            int numDescAfter=n.getNumLeavesUnder(false);
            //System.out.println("After edit, numdescendants is "+numDescAfter);
            if(numDescBefore!=numDescAfter)
                {
                throw new RuntimeException("Desc mismatch!");
                }
            }

        mvd++;
        current.setPosteriorSampledValue("max_val_depth",mvd);        
        
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        
        recentCode=7;
        return ret_pack;        
        }
    
    private Object[] preScreenOverlapRetraction(
            coa_lineage current,
            BigDecimal cPrior,
            int mvd,
            ArrayList<coa_lineage> eligibleNodes
            )
        {
            
            
        int maxValDepth=mvd;
        ArrayList<coa_lineage> retractableNodes=eligibleNodes;
        if(retractableNodes.size()<=0)
            {
            //if no candidate nodes exist try recursion
            //return proposeTree(current,cPrior);
            Object[] ret_pack=new Object[2];
            ret_pack[0]=current;
            ret_pack[1]=cPrior;
            return ret_pack;
            }
                    
        ArrayList<coa_lineage> correspondingParents=new ArrayList<coa_lineage>();
        for(coa_lineage rNode:retractableNodes)
            {
            correspondingParents.add(rNode.getParentRef());
            }
        int randIndex=TreeUtils.chooseRandomIndex(retractableNodes.size());
        coa_lineage m=retractableNodes.get(randIndex);
        //since m has only one child get its pointer/data
        coa_lineage c=m.getChild(0);
        double c_dist=m.getDist(0);
        coa_lineage p=correspondingParents.get(randIndex);
        //find the node at m's level that will "adopt" m's child
        int numKidsOfParent=p.getNumKids();
        ArrayList<coa_lineage> potentialAdopters=new ArrayList<coa_lineage>();
        for(int i=0;i<numKidsOfParent;i++)
            {
            coa_lineage possAdopt=p.getChild(i);
            if(possAdopt==m)
                {
                //m won't readopt its own child it just gave up!
                }
            else
                {
                potentialAdopters.add(possAdopt);
                }
            }
        int randomAdopterIndex=TreeUtils.chooseRandomIndex(potentialAdopters.size());
        coa_lineage randomAdopter=potentialAdopters.get(randomAdopterIndex);
        p.removeChild(m);
        randomAdopter.addChild(c,c_dist);
        if(p.getNumKids()==1 && p==current)
            {
            /*
            if the parent is the root with only one child,
            then the new root should be the remaining child
            */
            coa_lineage new_root=p.getChild(0);
            HashMap prevMap=current.getPosteriorSampleMap();
            current=new_root;
            current.resetParentRef();
            maxValDepth--;
            current.setPosteriorSampledValue("iter_num",prevMap.get("iter_num"));
            current.setPosteriorSampledValue("last_30_acc_ratio",prevMap.get("last_30_acc_ratio"));
            current.setPosteriorSampledValue("max_val_depth",maxValDepth);
            }
        
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        
        recentCode=4;
        return ret_pack;

        
        }
    
    
    private Object[] overlapRetraction(coa_lineage current,BigDecimal cPrior,int mvd)
        {

        /*
        For retraction:
            1)  Let M be the set of nodes at SRE levels (except the first SRE)
            such that they have one child and whose parent p has at least two children.
            These are overlap lineages that are eligible for retraction 
            2)  choose m one of the M nodes at random
                2.1) if no such node exists, call propose tree recursively
            3)  detach m from p and then let a node at the same level as m (but not m)
                have m's child (c) as it's own (adoption).
                3.1) if p is the root, and m was p's only child 
                     then p's only other child becomes the new root
            4)  update the posterior sampled values/map
            5)  return the tree
        */
        //int maxValDepth=SWPaperAlgOneGenerator.getMaxValidDepth(current);
        int maxValDepth=mvd;
        ArrayList<coa_lineage> retractableNodes=new ArrayList<coa_lineage>();
        ArrayList<Integer> correspondingRetractableDepths=new ArrayList<Integer>();
        ArrayList<coa_lineage> correspondingParents=new ArrayList<coa_lineage>();
        HashMap nodesAtADepth=new HashMap();
        for(int s=0;s<=maxValDepth;s++)
            {
            int depth=s;
            ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(current,depth);
            nodesAtADepth.put(depth,nodesAtDepth);
            for(coa_lineage m:nodesAtDepth)
                {
                if(m.getNumKids()==1)
                    {
                    coa_lineage p=current.findTheParentOfGivenNode(m);
                    if(p!=null)
                        {
                        if(p.getNumKids()>=2)
                            {
                            /*
                            For a node to be retractable, it must have exactly one child and
                                it must have a parent that has at least two kids.
                            */
                            retractableNodes.add(m);
                            correspondingRetractableDepths.add(depth);
                            correspondingParents.add(p);
                            }
                        }
                    }
                }
            }
        if(retractableNodes.size()<=0)
            {
            //if no candidate nodes exist try recursion
            return proposeTree(current,cPrior);
            }
        int randIndex=TreeUtils.chooseRandomIndex(retractableNodes.size());
        coa_lineage m=retractableNodes.get(randIndex);
        //since m has only one child get its pointer/data
        coa_lineage c=m.getChild(0);
        double c_dist=m.getDist(0);
        coa_lineage p=correspondingParents.get(randIndex);
        //find the node at m's level that will "adopt" m's child
        int numKidsOfParent=p.getNumKids();
        ArrayList<coa_lineage> potentialAdopters=new ArrayList<coa_lineage>();
        for(int i=0;i<numKidsOfParent;i++)
            {
            coa_lineage possAdopt=p.getChild(i);
            if(possAdopt==m)
                {
                //m won't readopt its own child it just gave up!
                }
            else
                {
                potentialAdopters.add(possAdopt);
                }
            }
        int randomAdopterIndex=TreeUtils.chooseRandomIndex(potentialAdopters.size());
        coa_lineage randomAdopter=potentialAdopters.get(randomAdopterIndex);
        p.removeChild(m);
        randomAdopter.addChild(c,c_dist);
        if(p.getNumKids()==1 && p==current)
            {
            /*
            if the parent is the root with only one child,
            then the new root should be the remaining child
            */
            coa_lineage new_root=p.getChild(0);
            current=new_root;
            current.resetParentRef();
            maxValDepth--;
            current.setPosteriorSampledValue("max_val_depth",maxValDepth);
            }
        
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        
        recentCode=4;
        return ret_pack;
        }

    
    public coa_lineage tagForPhyloXML(coa_lineage root)
        {
        coa_lineage rootClone=root.cloneRec();
        SREvent[] SREvents=newPosteriors(root);
        for(int e=0;e<SREvents.length;e++)
            {
            int depth=SREvent.getDepthFromSREIndex(e,SREvents);
            String tag="SRE_"+e+"";
            ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(rootClone,depth);
            for(coa_lineage node:nodesAtDepth)
                {
                node.setTag(tag);
                }            
            }
        
        return rootClone;
        }
    
    
    
    private Object[] SRELayerRemoval(coa_lineage current,BigDecimal cPrior,boolean allowOptionalPassThru)
        {
        /*
        If all nodes of an SRE layer have exactly one parent and exactly 
        one child, then the layer is eligible for removal.
            
        If removal proceeds, then the children of the layer become children
        of their grand parents instead.  Thus the layer is removed!
        */
            
        int mvd=getMaxValidDepth(current);
        ArrayList<Integer> depthsEligible=new ArrayList<Integer>();
        //iterate from 1 to LESS THAN mvd...
        //to avoid considering root and also the tips
        for(int depth=1;depth<mvd;depth++)
            {
            ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(current,depth);
            int numWithExactlyOneKid=0;
            for(coa_lineage aNode:nodesAtDepth)
                {
                if(aNode.getNumKids()==1)
                    {
                    numWithExactlyOneKid++;
                    }
                }
            if(numWithExactlyOneKid==nodesAtDepth.size())
                {
                depthsEligible.add(depth);
                }
            }
        if(depthsEligible.isEmpty())
            {
            if(allowOptionalPassThru)
                {
                Object[] opt_ret_pack=new Object[2];
                opt_ret_pack[0]=current;
                opt_ret_pack[1]=cPrior;
                return opt_ret_pack;                
                }
            return proposeTree(current,cPrior);
            }
        
        int randomDepthToEliminate=(Integer)TreeUtils.chooseRandom(depthsEligible);
        ArrayList<coa_lineage> nodesToRemove=SWPaperAlgOneGenerator.getNodesAtDepth(current,randomDepthToEliminate);
        double distAbove=nodesToRemove.get(0).getParentRef().getDist(0);
        double distBelow=nodesToRemove.get(0).getDist(0);
        double newDist=Math.max(distAbove,distBelow);
        for(coa_lineage nodeToRemove:nodesToRemove)
            {
            coa_lineage theParent=nodeToRemove.getParentRef();
            coa_lineage theChild=nodeToRemove.getChild(0);
            nodeToRemove.removeChild(theChild);
            theParent.removeChild(nodeToRemove);
            theChild.resetParentRef();
            theParent.addChild(theChild,newDist);
            }
        int newMVD=mvd-1;
        current.setPosteriorSampledValue("max_val_depth",newMVD);
        
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        recentCode=6;
        //System.err.println("Leaving with rc=6");
        return ret_pack;
        
        }
    
    
    private Object[] preScreenOverlapExtension(
            coa_lineage current,
            BigDecimal cPrior,
            int mvd,
            ArrayList<coa_lineage> eligibleNodes
        )
        {

            
        if(eligibleNodes.isEmpty())
            {
            //return proposeTree(current,cPrior);
            Object[] retPack=new Object[2];
            retPack[0]=current;
            retPack[1]=cPrior;
            return retPack;            
            }            
            

        //choose a node
        coa_lineage nAryNode=(coa_lineage)(TreeUtils.chooseRandom(eligibleNodes));            
        //find a random child of the node to detach and create overlap with!
        //get the dist to the kid
        int randKidIndex=TreeUtils.getRandomInt(0,nAryNode.getNumKids()-1);
        double distToKid=nAryNode.getDist(randKidIndex);            

        //find the parent of the node and attach to it a new overlap node
        coa_lineage nAryParent=current.findTheParentOfGivenNode(nAryNode);
        if(nAryParent==null && nAryNode==current)
            {
            //the root was used!
            coa_lineage new_root=new coa_lineage("");
            double newSREDist=TreeUtils.drawExponentialWithMean(1.0);
            while(newSREDist>=this.maxDistanceFromProbMatrix.doubleValue())
                {
                newSREDist=TreeUtils.drawExponentialWithMean(1.0);
                }
            /*
                set new SRE
                Y=1 
                phi=1.0 (0% overlap)
                height=existing root heigh+new SREwait time
            */
            int previous_max_val_depth=mvd;
            int updated_max_val_depth=previous_max_val_depth+1;
            HashMap prevMap=current.getPosteriorSampleMap();
            current.resetPosteriorSampledValues();
            //add current as a new child of the root
            //because nAry parent adds the child after this if/else block
            new_root.addChild(current, newSREDist);
            current=new_root;
            current.setPosteriorSampledValue("iter_num",prevMap.get("iter_num"));
            current.setPosteriorSampledValue("last_30_acc_ratio",prevMap.get("last_30_acc_ratio"));
            nAryParent=new_root;
            current.setPosteriorSampledValue("max_val_depth", updated_max_val_depth);
            }
        else if(nAryParent==null && nAryNode!=current)
            {
            System.out.println("Parent null, but node isn't root?!?!");
            throw new RuntimeException("");
            }
        int indexOfParentToNAryNode=nAryParent.getKidIndex(nAryNode);
        double distToOverlapNode=nAryParent.getDist(indexOfParentToNAryNode);
        coa_lineage newOverLapNode=new coa_lineage("");
        newOverLapNode.addChild(nAryNode.getChild(randKidIndex),distToKid);
        nAryParent.addChild(newOverLapNode,distToOverlapNode);
        
        //detach the child from the parent
        nAryNode.removeChild(randKidIndex);

        Object[] retPack=new Object[2];
        retPack[0]=current;
        retPack[1]=cPrior;
        recentCode=3;
        return retPack;
        
        
        }
    
    
    private Object[] overlapExtension(coa_lineage current,BigDecimal cPrior,int mvd)
        {
        /*
        For extension:
            1)  Define a set of M nodes as follows: m is in M if :
                a) it has at least one n-ary merger (>=2 kids)
            2)  From M the set of n-ary (n>=2 kids) chose one node m from M at random
                2.1) let p be the parent of m
            3)  From the kids/lineages merging to m, chose one lineage l at random
            4)  detach l from m and create a new node at the SRE level (overlap node) and 
            make it a child of p OR make it a child of a random node of a node at p's level
            5)  update the SRE profile
            6)  return the tree
        */
        //int mvd=getMaxValidDepth(current);
        int numLeaves=current.getNumLeavesUnder(true);
        ArrayList<coa_lineage> allNodes=current.get_descendants(true);
        ArrayList<coa_lineage> eligibleNodes=new ArrayList<coa_lineage>();
        for(coa_lineage aNode:allNodes)
            {
            if(aNode.getNumKids()>=2)
                {
                if(aNode==current && mvd>=numLeaves)
                    {
                    //if the node is the root and the SRE depth is 
                    //extensive fail to consider it for extension
                    }
                else
                    {
                    eligibleNodes.add(aNode);
                    }
                }
            }
        
        if(eligibleNodes.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        
        
        
        coa_lineage nAryNode=(coa_lineage)(TreeUtils.chooseRandom(eligibleNodes));
        
        //find a random child of the node to detach and create overlap with!
        //get the dist to the kid
        int randKidIndex=TreeUtils.getRandomInt(0,nAryNode.getNumKids()-1);
        double distToKid=nAryNode.getDist(randKidIndex);
        
        //find the parent of the node and attach to it a new overlap node
        coa_lineage nAryParent=current.findTheParentOfGivenNode(nAryNode);
        boolean nAryNodeWasRoot=false;
        if(nAryParent==null && nAryNode==current)
            {
            nAryNodeWasRoot=true;
            //the root was used!
            coa_lineage new_root=new coa_lineage("");
            double newSREDist=TreeUtils.drawExponentialWithMean(1.0);
            /*
                set new SRE
                Y=1 
                phi=1.0 (0% overlap)
                height=existing root heigh+new SREwait time
            */
            int previous_max_val_depth=(Integer)current.getPosteriorSampleMap().get("max_val_depth");
            int updated_max_val_depth=previous_max_val_depth+1;
            current.resetPosteriorSampledValues();
            //add current as a new child of the root
            //because nAry parent adds the child after this if/else block
            new_root.addChild(current, newSREDist);
            current=new_root;
            nAryParent=new_root;
            current.setPosteriorSampledValue("max_val_depth", updated_max_val_depth);
            }
        else if(nAryParent==null && nAryNode!=current)
            {
            System.out.println("Parent null, but node isn't root?!?!");
            throw new RuntimeException("");
            }
        if(!nAryNodeWasRoot)
            {
            //throw new RuntimeException("update the SRE posteriors");
            }
        int indexOfParentToNAryNode=nAryParent.getKidIndex(nAryNode);
        double distToOverlapNode=nAryParent.getDist(indexOfParentToNAryNode);
        coa_lineage newOverLapNode=new coa_lineage("");
        newOverLapNode.addChild(nAryNode.getChild(randKidIndex),distToKid);
        nAryParent.addChild(newOverLapNode,distToOverlapNode);
        
        //detach the child from the parent
        nAryNode.removeChild(randKidIndex);

        Object[] retPack=new Object[2];
        retPack[0]=current;
        retPack[1]=cPrior;
        recentCode=3;
        return retPack;
        }
    
    private Object[] adjustBinningInRandomSRE(coa_lineage current,BigDecimal cPrior)
        {
        /*
        Choose an SRE at Random and reassign lineages to bins.
        This amounts to re-sampling in step #5 in Algorithm #1.
            
        Part 1) choose an SRE at random
        Part 2) from two randomly chosen lineages and
            swap them!
        */

        int mvd=SWPaperAlgOneGenerator.getMaxValidDepth(current);
        ArrayList<Integer> depthsToChooseFrom=new ArrayList<Integer>();
        for(int d=0;d<=mvd;d++)
            {
            depthsToChooseFrom.add(d);
            }
        if(depthsToChooseFrom.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        int randomDepth=(Integer)(TreeUtils.chooseRandom(depthsToChooseFrom));
        ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(current,randomDepth);
        if(nodesAtDepth.size()<=1)
            {
            //two lineages doesn't change the tree
            return proposeTree(current,cPrior);
            }

        int[] twoRandomIndices=TreeUtils.chooseRandomIndices(nodesAtDepth.size(),2);
        int n1=twoRandomIndices[0];
        int n2=twoRandomIndices[1];
        coa_lineage na=nodesAtDepth.get(n1);
        int na_kid_idx=TreeUtils.chooseRandomIndex(na.getNumKids());
        coa_lineage a_kid=na.getChild(na_kid_idx);
        coa_lineage nb=nodesAtDepth.get(n2);
        int nb_kid_idx=TreeUtils.chooseRandomIndex(nb.getNumKids());
        coa_lineage b_kid=nb.getChild(nb_kid_idx);
        double dist_under=na.getDist(0);
        na.removeChild(a_kid);
        nb.removeChild(b_kid);
        na.addChild(b_kid,dist_under);
        nb.addChild(a_kid,dist_under);
        

            
 
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        recentCode=2;
        return ret_pack;
        }
    
    
    private double proposeNewBranchLengthDynamicExponentialTruncatedWithMedianAnchorOnBottomOrUni(
            double currentLength,
            double halfWindow
        )
        {
        double currentCDF=TreeUtils.exponentialDistCDFEval(currentLength,1);
        double bottomCDF=TreeUtils.exponentialDistCDFEval(currentLength-halfWindow,1);
        double upperCDF=TreeUtils.exponentialDistCDFEval(currentLength+halfWindow,1);
        if( 
                bottomCDF==currentCDF ||
                currentCDF==upperCDF  ||
                upperCDF==bottomCDF   ||
                bottomCDF==1          ||
                currentCDF==1         ||
                upperCDF==1
                )
            {
            /*
                because this is the uniform distribution, the probablity of 
                above current is the same as that below so this uses a median
            */
            System.out.println("currentLength "+currentLength);
            System.out.println("halfWindow "+halfWindow);
                
            double uniUpperBound=currentLength+halfWindow;
            System.out.println("initial UB : "+uniUpperBound);
            if(uniUpperBound>=this.maxDistanceFromProbMatrix.doubleValue())
                {
                uniUpperBound=this.maxDistanceFromProbMatrix.doubleValue();
                System.out.println("edited UB : "+uniUpperBound);
                }
            System.out.println("this.minDistanceFromProbMatrix is "+this.minDistanceFromProbMatrix);
            System.out.println("uniUpperBound-2*halfWindow is "+(uniUpperBound-2*halfWindow));
            double uniLowerBound=Math.max(uniUpperBound-2*halfWindow,this.minDistanceFromProbMatrix);
            System.out.println("initial lb "+uniLowerBound);
            if(uniLowerBound<=0)
                {
                throw new RuntimeException("Error, upperBound is "+uniUpperBound+" and lower is "+uniLowerBound+", size of window is too big!");
                }
	    if(uniLowerBound<this.minDistanceFromProbMatrix.doubleValue())
		{
		uniLowerBound=this.minDistanceFromProbMatrix.doubleValue();
                System.out.println("edited lb "+uniLowerBound);
		}
            boolean inclusive=false;
            double randUniNumInWindow=TreeUtils.getRandNumBet(uniLowerBound,uniUpperBound,inclusive);
            return randUniNumInWindow;
            }//uniform-based proposal
            

        double lowerBound=currentLength-halfWindow;
        if(lowerBound<0)
            {
            return TreeUtils.drawTruncatedExponentialMeanOne(currentLength*0.75,currentLength+halfWindow);
            }
        double currCDF=TreeUtils.exponentialDistCDFRateAndMeanOneEval(currentLength);
        double lowerCDF=TreeUtils.exponentialDistCDFRateAndMeanOneEval(lowerBound);
        double massBelow=currCDF-lowerCDF;
        double delta=(-currentLength)-Math.log(Math.exp(-currentLength)-massBelow);
        double upperBound=currentLength+delta;
        double valToReturn=TreeUtils.drawTruncatedExponentialMeanOne(
                Math.max(lowerBound,this.minDistanceFromProbMatrix.doubleValue()),
                Math.min(upperBound,this.maxDistanceFromProbMatrix.doubleValue())
            );
        return valToReturn;
        }
    
    
    public static double evalDerivativeAtLength(double x)
        {
        double deriv=(-1)*Math.exp(-x);
        return deriv;
        }
    
    boolean allowPullBackOfStep=false;
    
    private  Object[] adjustBranchLengthsOnly(coa_lineage current,BigDecimal cPrior,
            boolean truncate,double halfWindow)
        {
        /*
        the purpose of this is to receive a tree, perturb ONLY
        the branch lengths!            
        */
        /*
        STEP 1: choose an SRE at random    
        STEP 2: choose a random value from the exp
            with mean 1
        STEP 3: all lineages across the SRE (including
            overlap generation lineages) must have their
            lengths adjusted by the value from step 2.
            The SRE and posterious sampled map should be
            updated accordingly.
            */
            
        //STEP 1
        double chosenSREWait;
        int maxValDepth=SWPaperAlgOneGenerator.getMaxValidDepth(current);
        int randomDepth=TreeUtils.chooseRandomIndex(maxValDepth+1);
        if(randomDepth==0)
            {
            chosenSREWait=current.getDist(0);
            }
        else
            {
            coa_lineage temp=current;
            int depthLooper=0;
            while(depthLooper<randomDepth)
                {
                temp=temp.getChild(0);
                depthLooper++;
                }
            chosenSREWait=temp.getDist(0);
            }
        
           
        //STEP 2
        double newSREWait=0;
        if(!truncate || this.getBeta()==0)
            {
            //System.out.println("In beta=0 gen!");
            newSREWait=TreeUtils.drawExponentialWithMean(1.0);
            while(newSREWait>=this.maxDistanceFromProbMatrix.doubleValue() || newSREWait<=this.minDistanceFromProbMatrix.doubleValue())
                {
                newSREWait=TreeUtils.drawExponentialWithMean(1.0);
                }
            }
        else
            {

            
                
            do
                {
                if(chosenSREWait<=this.minDistanceFromProbMatrix*10)
                    {
                    newSREWait=TreeUtils.getRandNumBet(this.minDistanceFromProbMatrix,this.minDistanceFromProbMatrix*100,false);
                    }
                else
                    {
                    newSREWait=proposeNewBranchLengthDynamicExponentialTruncatedWithMedianAnchorOnBottomOrUni(
                                        chosenSREWait,
                                        chosenSREWait*0.1
                                    );
                    }


                /*newSREWait=chosenSREWait+TreeUtils.getRandNormal(0,this.blStep);
                double betaSample=0;
                while(betaSample==0)
                    {
                    betaSample=TreeUtils.getRandomBetaDistSample(0.5,0.5);
                    betaSample-=0.5;
                    }
                double deltaEffective=TreeUtils.linearMap(-0.5,0.5,-this.blStep,this.blStep,betaSample);
                newSREWait=chosenSREWait+deltaEffective;*/

                if(newSREWait<=this.minDistanceFromProbMatrix) { newSREWait=this.minDistanceFromProbMatrix ;}
                if(newSREWait>=this.maxDistanceFromProbMatrix) { newSREWait=this.maxDistanceFromProbMatrix ;}
		//System.out.println("chosen/new\t"+chosenSREWait+"\t"+newSREWait);

                }while(newSREWait<=this.minDistanceFromProbMatrix || newSREWait>=this.maxDistanceFromProbMatrix);
            }
	//System.out.println("in adjust BL ; truncate="+truncate+" oldSREWait="+chosenSREWait+", newSREWait="+newSREWait);



        int depth=randomDepth;
        ArrayList<coa_lineage> targetSRENodes=SWPaperAlgOneGenerator.getNodesAtDepth(current,depth);
        for(coa_lineage t:targetSRENodes)
            {
            int k=t.getNumKids();
            for(int d=0;d<k;d++)
                {
                t.setDist(d,newSREWait);
                }
            }

        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        recentCode=1;
        return ret_pack;    
        }
    
    
    public static void main(String[] args)
        {
        System.out.println("Hello");
        coa_lineage root=new coa_lineage("root");
        for(int p=0;p<2;p++)
            {
            coa_lineage ap=null;
            ap=new coa_lineage(""+(p+1)+"");
            for(int i=0;i<3;i++)
                {
                coa_lineage tc=new coa_lineage(""+(p+1)+"_"+(i+1)+"");
                ap.addChild(tc,1);
                }
            root.addChild(ap,1);
            }
        for(int d=(-2);d<=4;d++)
            {
            System.out.println("Now running for depth ="+d);
            ArrayList<coa_lineage> nodesAtDepth=null;
            nodesAtDepth=getNodesAtDepth(root,d);
            int nad=nodesAtDepth.size();
            System.out.println("The number of nodes at depth is "+nad);
            for(int n=0;n<nad;n++)
                {
                System.out.println("One of the nodes is named : "+nodesAtDepth.get(n).getName());
                }
            }
        }
    

    
    public static void installJSONStringToRoot(coa_lineage root)
        {
        int mDepth=returnMaxDepth(root);
        SREvent[] newEvents=new SREvent[mDepth-1];
        int currentDepth=0;
        for(int d=newEvents.length-1;d>=0;d--)
            {
            ArrayList<coa_lineage> nad=getNodesAtDepth(root,currentDepth);
            currentDepth++;
            coa_lineage firstNode=nad.get(0);
            double firstHeight=firstNode.getHeight();
            newEvents[d]=new SREvent(0,0.5,firstHeight);
            }
        String SREJson=SREvent.JSONString(newEvents);
        root.setPosteriorSampledValue("sre_profile",SREJson);
        newPosteriors(root);
        }
    
    //the max depth is the highest recursion such that
    //nodes at the depth have a child and aren't tips
    public static int returnMaxDepth(coa_lineage root)
        {
        int numLeavesUnder=root.getNumLeavesUnder(true);
        for(int i=0;i<numLeavesUnder;i++)
            {
            ArrayList<coa_lineage> nodesAtDepth=SWPaperAlgOneGenerator.getNodesAtDepth(root, i);
            coa_lineage aNode=nodesAtDepth.get(0);
            if(aNode.isTip())
                {
                return i-1;
                }
            }
        throw new RuntimeException("Error, returnMaxDepth got too far!");
        }
    
    public static ArrayList<coa_lineage> getNodesAtDepth(coa_lineage c,int depth)
        {
        ArrayList<coa_lineage> lineages=new ArrayList<coa_lineage>();
        if(depth<0)
            {
            return lineages;
            }
        else if(depth==0)
            {
            lineages.add(c);
            return lineages;
            }
        else if(depth>0)
            {
            int numKids=c.getNumKids();
            if(numKids==0)
                {
                return lineages;
                }
            else
                {
                for(int k=0;k<numKids;k++)
                    {
                    coa_lineage the_child=c.getChild(k);
                    lineages.addAll(getNodesAtDepth(the_child,depth-1));
                    }
                }
            }
        return lineages;
        }
    
    
    public static ArrayList<Double> getSREWaitSpectrumAssingAlg1(coa_lineage c)
        {
        ArrayList<Double> hs=new ArrayList<Double>();
        if(c.getNumKids()==0)
            {
            return hs;
            }
        else
            {
            coa_lineage firstKid=c.getChild(0);
            double a_dist=c.getDist(0);
            hs.add(a_dist);
            ArrayList<Double> recw=getSREWaitSpectrumAssingAlg1(firstKid);
            hs.addAll(recw);
            }
        return hs;
        }
    
    
    
    public static ArrayList<Double> getFullHeightSpectrumAssingAlg1(coa_lineage c)
        {
        ArrayList<Double> hs=new ArrayList<Double>();
        hs.add(c.getHeight());
        for(int k=0;k<c.getNumKids();k++)
            {
            hs.addAll(getFullHeightSpectrumAssingAlg1(c.getChild(k)));
            }
        hs.sort(null);
        return hs;
        }
    
    public static ArrayList<Double> getHeightSpectrumAssingAlg1(coa_lineage c)
        {
        ArrayList<Double> hs=new ArrayList<Double>();
        double this_height=c.getHeight();
        if(this_height!=0)
            {
            hs.add(this_height);
            }
        if(c.getNumKids()==0)
            {
            return hs;
            }
        else
            {
            coa_lineage firstKid=c.getChild(0);
            hs.addAll(getHeightSpectrumAssingAlg1(firstKid));
            }
        return hs;
        }

}
