/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comet;

import java.util.HashMap;

/**
 *
 * @author esalina
 */
public class ProposalLogger 
    {
    HashMap successMap=new HashMap();
    HashMap failureMap=new HashMap();
    
    
    public void logInMap(boolean isSuccess,int code)
        {
        HashMap mapToUse=null;
        if(isSuccess)
            {
            mapToUse=successMap;
            }
        else
            {
            mapToUse=failureMap;
            }
        Integer c=new Integer(code);
        if(mapToUse.containsKey(c))
            {
            Integer val=(Integer)(mapToUse.get(c));
            val=new Integer(val.intValue());
            mapToUse.put(c,new Integer(val+1));
            }
        else
            {
            mapToUse.put(c,new Integer(1));
            }
        }
    
    public HashMap getCountMap(boolean isSuccess)
        {
        if(isSuccess)
            {
            return (HashMap)TreeUtils.cloneMap(successMap);
            }
        else
            {
            return (HashMap)TreeUtils.cloneMap(failureMap);
            }
        }
    
    
    }
