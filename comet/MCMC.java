/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.nevec.rjm.BigDecimalMath;

/**
 *
 * @author esalina
 */
public class MCMC {
    
    
    public static final int TREE_PROPOSAL=0;
    public static final int LIKELIHOOD_PROPOSAL=1;
    
    int proposalStrategy=(-1);
    long maxIter=(-1);
    long burnIter=0;
    long recordingFreq=(-1);
    FastaIO fastaIO=null;
    String logTracePath=null;
    String tracerLogTracePath=null;
    String fastaInput=null;
    String phylipPath=null;
    String nxsPath=null;
    String mlXMLPath=null;
    String xmlTriggerFilePath;
    TreeGenerator mcmcTGen=null;
    TreeLikelihoodCalculator mcmcLC=null;
    ProposalManager proposalManager=null;
    String proposalLine=null;


 
    
    
    
    public void appendToTraceLog(
            HashMap recMap,
            long iterNum,
            boolean firstRecord,
            File logFile,
            boolean tracerReadyOnly,
            boolean cMLOverRide
            ) throws Exception
        {
        //^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$
        Pattern tracerReadyPattern=Pattern.compile("^\\s*[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?\\s*$");
        FileWriter fw=new FileWriter(logFile,!firstRecord);
        Set keySet=recMap.keySet();
        ArrayList<String> keyArrayList=new ArrayList<String>();
        for(Object k:keySet)
            {
            if(!tracerReadyOnly)
                {
                //if not tracer ready only, then add unconditionally
                keyArrayList.add(k.toString());
                }
            else
                {
                //if tracer-ready only, then test for numeric!
                Object val=recMap.get(k);
                String valStr=val.toString();
                Matcher m=tracerReadyPattern.matcher(valStr);
                if(m.find())
                    {
                    keyArrayList.add(k.toString());
                    }
                else
                    {
                    String keyString=k.toString();
                    //if c_ML is the key and the override is on, then log it!
                    if(cMLOverRide && keyString.equalsIgnoreCase("c_ML"))
                        {
                        keyArrayList.add(k.toString());
                        }
                    }
                }
            }
        Collections.sort(keyArrayList);
        int likeIndex=TreeUtils.getIndexOfStringInArrayList(keyArrayList, "Likelihood",true);
        keyArrayList=TreeUtils.swapVals(0, likeIndex, keyArrayList);
        int numItems=keyArrayList.size();
        if(firstRecord)
            {
            String header=new String("");
            header+="# COMET "+Comet.getVersion()+"\n";
            header+="# Generated "+TreeUtils.getNiceDataFormat()+"\n";
            header+="State\t";
            for(int i=0;i<numItems;i++)
                {
                header+=keyArrayList.get(i);
                if(i<numItems-1)
                    {
                    header+="\t";
                    }
                }
            header+="\n";
            fw.write(header);
            }
        String data="";
        //write the state here
        data+=""+(iterNum)+"\t";
        for(int i=0;i<numItems;i++)
                {
                data+=recMap.get(keyArrayList.get(i));
                if(i<numItems-1)
                    {
                    data+="\t";
                    }
                }
        fw.write(data+"\n");
        fw.flush();
        fw.close();
        }
    
    
    
    
    public MCMC(String confPath,String fastaPath,String phyloXMLStartFile)
        {
        File confFile=new File(confPath);
        BufferedReader br=null;
        try
            {
            FileReader myFileReader=new FileReader(confFile);
            br=new BufferedReader(myFileReader);
            }
        catch(Exception e)
            {
            e.printStackTrace();
            System.exit(1);
            }

        int  ln=0;
        double nminpsi=0;
        double nmaxpsi=1.0;
        //double nminkappa=0.01;
        //double nmaxkappa=5.0;
        double nminkappa=0.01;
        double nmaxkappa=6.0;
        String betaStart=null;
        String betaStop=null;
        String betaStep=null;
        BigDecimal bdBetaStart=null;
        BigDecimal bdBetaStop=null;
        BigDecimal bdBetaStep=null;
        String modelName=null;
        
        
        //setup FASTA I/O
        fastaInput=fastaPath;
        File fastaInputFile=new File(fastaInput);
        String numMilliPastEpoch=TreeUtils.getMilliSecondsPastEpoch();
        String pid=TreeUtils.getPID();
        String randNumDouble=""+Math.random()+"";
        String base_digits=numMilliPastEpoch+"."+randNumDouble+"."+pid;
        String outLogPath=fastaPath+"."+base_digits+".out";
        logTracePath=fastaPath+"."+base_digits+".log";
        phylipPath=fastaPath+"."+base_digits+".phylip";
        nxsPath=fastaPath+"."+base_digits+".nxs";
                
        try
            {
            System.out.println("Using input : "+fastaInputFile);                
            System.out.println("To set output log as "+outLogPath);
            System.setOut(new PrintStream(outLogPath));
            fastaIO=new FastaIO(fastaInputFile,true);   
            }
        catch(Exception e)
            {
            e.printStackTrace();
            System.exit(0);
            }
        System.out.println("Starting COMET at "+TreeUtils.getNiceDataFormat());
        System.out.println("INPUT INFORMATION");
	System.out.println("Using conf file "+confPath);
        System.out.println(fastaIO.abbreviatedToString());
        
        //setup LOG and TRACE IO

        LikelihoodUtils.setPhylipPath(phylipPath);
        System.out.println("Set output Phylip path as : "+phylipPath);
        System.out.println("To write nexus file to "+nxsPath);
        //always use PAL, too fast not to use!
        LikelihoodUtils.usePhyML=false;
        LikelihoodUtils.usePAL=true;
        Double maxNumHoursToRun=null;
        Double minESSScoreRequired=null;
        try
            {
            while(br.ready())
                {
                String lineRead=br.readLine();
                lineRead=lineRead.trim();
                boolean isCommentLine=false;
                if(lineRead.startsWith("#"))
                    {
                    isCommentLine=true;
                    }
                if(!isCommentLine)
                    {
                    if(ln==0)
                        {
                        try
                            {
                            Long tempMaxIter=null;
                            maxIter=Long.MAX_VALUE;
                            String[] maxPieces=lineRead.split("_");
                            for(int m=0;m<maxPieces.length;m++)
                                {
                                if(maxPieces[m].startsWith("H") || maxPieces[m].startsWith("h"))
                                    {
                                    //parse for max hours to run
                                    String hourString=maxPieces[m].substring(1);
                                    maxNumHoursToRun=new Double(hourString);
                                    System.out.println("Max Hours to run set as : "+maxNumHoursToRun);
                                    }
                                else if(maxPieces[m].startsWith("E") || maxPieces[m].startsWith("e"))
                                    {
                                    //parse for min ESS values
                                    String minESSStr=maxPieces[m].substring(1);
                                    minESSScoreRequired=new Double(minESSStr);
                                    System.out.println("Min ESS score set as "+minESSScoreRequired);
                                    }
                                else
                                    {
                                    tempMaxIter=Long.parseLong(maxPieces[m]);
                                    maxIter=tempMaxIter.longValue();
                                    }
                                }
                            if(tempMaxIter==null && minESSScoreRequired==null && maxNumHoursToRun==null)
                                {
                                throw new IllegalArgumentException("Error, maxIter not set, minESS not set, and maxHoursToRun not set!  Require at least one!!!");
                                }
                            System.out.println("Max iter set as "+maxIter);
                            }
                        catch(Exception e)
                            {
                            e.printStackTrace();
                            System.err.println("Error in line for max iterations. Should be a '_'-separated values line with"
                                    + "an integer, and/or H<MAX_HOURS> and/or E<MIN_ESS> ; hour and ESS values are floating-point decimal numbers");
                            System.exit(1);
                            }
                        }
                    if(ln==1)
                        {
                        try
                            {
                            recordingFreq=Integer.parseInt(lineRead);
                            System.out.println("Recording freq set as "+recordingFreq);
                            }
                        catch(Exception e)
                            {
                            e.printStackTrace();
                            System.err.println("Error in line for recording frequency.  Should be an integer!");
                            System.exit(1);                        
                            }
                        }
                    if(ln==2)
                        {
                        try
                            {
                            betaStart=lineRead;
                            bdBetaStart=new BigDecimal(betaStart);
                            }
                        catch(Exception e)
                            {
                            e.printStackTrace();
                            System.err.println("Error in line for beta start.  Should be a decimal number!");
                            System.exit(1); 
                            }
                        }
                    if(ln==3)
                        {
                        try
                            {
                            betaStop=lineRead;
                            bdBetaStop=new BigDecimal(betaStop);
                            }
                        catch(Exception e)
                            {
                            e.printStackTrace();
                            System.err.println("Error in line for beta test.  Should be a decimal number!");
                            System.exit(1); 
                            }
                        }
                    if(ln==4)
                        {
                        try
                            {
                            betaStep=lineRead;
                            bdBetaStep=new BigDecimal(betaStep);
                            }
                        catch(Exception e)
                            {
                            e.printStackTrace();
                            System.err.println("Error in line for beta step.  Should be a decimal number!");
                            System.exit(1); 
                            }
                        }
                    if(ln==5)
                        {
                        modelName=lineRead;
                        }
                    if(ln==6)
                        {
                        long longSeed=System.currentTimeMillis();
                        try
                            {
                            String seedStr=lineRead;                            
                            if(seedStr.compareToIgnoreCase("AUTO")==0)
                                {
                                System.out.println("Auto seed!");
                                }
                            else
                                {
                                longSeed=Long.parseLong(seedStr);
                                System.out.println("Read seed value : "+longSeed);
                                }
                            }
                        catch(Exception e)
                            {
                            System.out.println("Failed to read a valid seed value from conf file!");
                            if(e instanceof NumberFormatException)
                                {
                                System.out.println("Failed to parse value as a LONG datatype!");
                                }
                            }
                        System.out.println("Using seed : "+longSeed);
                        TreeUtils.initRandomSeed(longSeed);
                        }
                    if(ln==7)
                        {
                        LikelihoodUtils.COMET_MAX_LL_THREADS=lineRead.trim();
                        }
                    if(ln==8)
                        {
                        String minKappaStr=null;
                        try
                            {
                            minKappaStr=lineRead.trim();
                            double parsedMinKappa=Double.parseDouble(minKappaStr);
                            nminkappa=parsedMinKappa;
                            }
                        catch(NumberFormatException nfe)
                            {
                            System.err.println("Error failed to parse '"+minKappaStr+"' to obain minkappa!");
                            System.err.println("REQUIRE min kappa value on line : 9!");
                            System.exit(1);
                            }
                        }
                    if(ln==9)
                        {
                        String maxKappaStr=null;
                        try
                            {
                            maxKappaStr=lineRead.trim();
                            double parsedMaxKappa=Double.parseDouble(maxKappaStr);
                            nmaxkappa=parsedMaxKappa;
                            }
                        catch(NumberFormatException nfe)
                            {
                            System.err.println("Error failed to parse '"+maxKappaStr+"' to obain minkappa!");
                            System.err.println("REQUIRE min kappa value on line : 9!");
                            System.exit(1);
                            }
                        }
                    ln++;
                    }
                }
            br.close();
            /*String phymlPath=TreeUtils.getPhymlPathFromPathEnv();
            if(phymlPath==null)
                {
                String phymlNoFoundMsg="ERROR, phyml executable NOT found!  Add phyml to your path!\n";
                phymlNoFoundMsg+="Example 'export PATH=/home/esalina/phyml_from_src/bin:$PATH'";
                throw new FileNotFoundException(phymlNoFoundMsg);
                }
            else
                {
                System.out.println("USING phyml at "+phymlPath);
                LikelihoodUtils.setPhymlExecPath(phymlPath);
                }*/
            }
        catch(Exception e)
            {
            System.out.println("Stack trace : ");
            e.printStackTrace();
            if(e.getMessage().contains("phyml"))
                {
                System.err.println(e.getMessage());
                }
            else
                {
                e.printStackTrace();
                System.exit(1);
                }
            }
        
        if(bdBetaStart==null || bdBetaStop==null || bdBetaStep==null)
            {
            System.err.println("Error, beta values (start, test, step) not correctly set!");
            System.exit(1);
            }
        
        
        
        System.out.println("Beta start/test/step to be : "+bdBetaStart+" , "+bdBetaStop+" , and "+bdBetaStep);
        System.out.println("Model loaded : "+modelName);
        System.out.println("Set min kappa value : "+nminkappa);                
        System.out.println("Set max kappa value : "+nmaxkappa);
        
        
        
        
        try
            {
            fastaIO.writePhylipFile(phylipPath,true);
            fastaIO.writeNexusFile(nxsPath, true);
            }
        catch(Exception e)
            {
            e.printStackTrace();
            System.exit(1);
            }
        int num_lineages=fastaIO.getNumSeqs();
        nminpsi=0.05;
        nmaxpsi=0.95;
        HashMap lpMap=new HashMap();
        lpMap.put("kappa",1.0);
        double[] autoFreqs=fastaIO.computeNAFreqs();
        String[] autoFreqsStr=new String[4];
        for(int b=0;b<4;b++)
            {
            autoFreqsStr[b]=""+autoFreqs[b]+"";
            }
        if(!TreeUtils.arePiFreqsAllPositive(autoFreqsStr))
            {
            System.out.println("WARNING, auto pi freqs calculated as : "+Arrays.toString(autoFreqsStr));
            System.out.println("Overwriting with uniform !");
            lpMap.put("pi_freqs","0.25,0.25,0.25,0.25");
            }
        else
            {
            String autoFreqsCommaSep=TreeUtils.join(autoFreqsStr,",");
            System.out.println("Using pi frequencies : "+Arrays.toString(autoFreqs));
            lpMap.put("pi_freqs",autoFreqsCommaSep);
            }
        KHYLikelihoodCalculator myLC=new KHYLikelihoodCalculator(lpMap,nminkappa,nmaxkappa);
        //myLC.getMaxDistUsingMinMaxKappaAndFreqs(double minKappa,double maxKappa,double[] freqs)
        double maxDistDueToTransitionProbabilityMatrix=KHYLikelihoodCalculator.getMaxDistUsingMinMaxKappaAndFreqs(nminkappa,nmaxkappa,TreeUtils.strFreqsToDoubleArray(lpMap.get("pi_freqs").toString()));

        
        try
            {
            mcmcLC=myLC;
            //this.MCMCRun(myAlgOneGenerator, myLC, fastaIO);        
            BigDecimal beta=bdBetaStart;
            String baseLogTracePath=new String(logTracePath);
            //mcmcTGen.setFasta(fastaIO);
            BigDecimal near1=new BigDecimal("0.99");
            System.out.println("Beta will initialized at value "+betaStart);
            System.out.println("An MCMC chain will be run with that value of beta.");
            System.out.println("Next, beta will be incremented at steps of "+bdBetaStep);
            System.out.println("An an MCMCMC will be run with that value of beta.");
            System.out.println("Increments will be done and chains will be run");
            System.out.println("until beta is greater than "+bdBetaStop);
            System.out.println("at which point the process stops.");            
            while(beta.compareTo(bdBetaStop)<=0)
                {
                if(beta.compareTo(near1)>0)
                    {
                    if(near1.compareTo(bdBetaStop)<=0)
                        {
                        beta=new BigDecimal(bdBetaStop.toString());
                        }
                    }
                System.out.println("Now running MCMCMC with BETA = "+beta);
                double[] autoBetas;
                autoBetas=MCMCMC.getAutoBetaFactors(beta.doubleValue());
                System.out.println("From beta="+beta+", the auto betas for MCMCMC are "+Arrays.toString(autoBetas));

                    MCMCMC myMCMCMC=new MCMCMC(autoBetas,
                        modelName,
                        fastaIO,
                        nminkappa,
                        nmaxkappa,
                        maxIter,
                        recordingFreq,
                        baseLogTracePath,
                        phyloXMLStartFile,
                        maxNumHoursToRun,
                        minESSScoreRequired
                        );
                    
                beta=beta.add(bdBetaStep);

                }
            
            System.out.println("COMET finished at "+TreeUtils.getNiceDataFormat());
            }
        catch(Exception e)
            {
            e.printStackTrace();
            System.exit(1);
            }
        

        }
    
    
    
    public coa_lineage getTree(Object[] p)
        {
        return (coa_lineage)(p[0]);
        }
    
    public BigDecimal getTreePrior(Object[] p)
        {
        return (BigDecimal)(p[1]);
        }

    
    public static BigDecimal potentialOneModel(BigDecimal likelihood,BigDecimal prior,boolean areLogs)
        {
        //see eq. #34 from 
        //Computing Bayes Factors Using Thermodynamic Integration
        //Author(s): Nicolas Lartillot and Herve Philippe
        //Source: Systematic Biology, Vol. 55, No. 2 (Apr., 2006), pp. 195-207
        if(areLogs)
            {
            return likelihood.add(prior);
            }
        else
            {
            BigDecimal prod=likelihood.multiply(prior);
            BigDecimal logLike=BigDecimalMath.log(prod);
            return logLike;
            }
            
            
        }
    
    
    public static BigDecimal qBetaOneModel(
            BigDecimal beta,
            BigDecimal tLike,
            BigDecimal priorProdTreeAndLCParams,
            boolean areLogs
    )
        {
        //only marginal likelihood for a single tree (not bayes factor)
        //see eq. #32 from 
        /*Computing Bayes Factors Using Thermodynamic Integration
        Author(s): Nicolas Lartillot and Herve Philippe
        Source: Systematic Biology, Vol. 55, No. 2 (Apr., 2006), pp. 195-207*/
        
        /*NOTE that BETA only affects the likelihood ant NOT the prior.
        thus, when BETA=1 it's like regular MCMC  
        when BETA=0 however, the likelihood is essentially dropped and only the prior is used
        This way BETA represents to some degree the extent 
        to which the likelihood is used in addition to the prior
            
        See also, Fig. 7.15 in Yang, Z., (2014) Molecular Evolution : A Statistical Approach, Oxford Univ. Press
        See also, Fig. 5.8 in Yang, Z., (2006) Computational Molecular Evolution, Oxford Univ. Press
        */
            
        BigDecimal prod=null;
        if(!areLogs)
            {
            //if not logs, just perform the calculations direct
            prod=BigDecimalMath.pow(tLike, beta);
            prod=prod.multiply(priorProdTreeAndLCParams);
            }
        else
            {
            if(beta.compareTo(BigDecimal.ZERO)==0)
                {
                //beta_pow would be 1
                //beta_pow_ln would be zero
                BigDecimal beta_pow_ln=BigDecimal.ZERO;
                prod=beta_pow_ln.add(priorProdTreeAndLCParams); 
                }
            else
                {
                /*
                by properties of logs                    
                */    
                    
                    

                prod=new BigDecimal(tLike.doubleValue()*beta.doubleValue());
                prod=prod.add(priorProdTreeAndLCParams);
                    
                    
                //convert from logSpace to real space 
                /*BigDecimal real=TreeUtils.exp(tLike, tLike.scale());

                //raise to the power in real space
                BigDecimal beta_pow=BigDecimalMath.pow(real,beta);

                //take the natural log to get back to log space
                BigDecimal beta_pow_ln=BigDecimalMath.log(beta_pow);

                //add in log space
                prod=beta_pow_ln.add(priorProdTreeAndLCParams);*/
                //double real=Math.exp(tLike.doubleValue());
                //double beta_pow=Math.pow(real,beta.doubleValue());
                //double beta_pow_ln=Math.log(beta_pow);
                //prod=new BigDecimal(beta_pow_ln+priorProdTreeAndLCParams.doubleValue());
                }

            }
        return prod;            
        }
    

    
    
    
    
    
    public BigDecimal[] MCMCBetaRun(
        TreeGenerator tGen,
        TreeLikelihoodCalculator lc,
        FastaIO fio,
        BigDecimal beta,
        File phyloXMLStartFile
        )
        {

        //mark first recording
        boolean firstRecord=true;
            
        //this is TI for ONE model!
        BigDecimal dSum=new BigDecimal("0.0");
        BigDecimal numDSum=new BigDecimal("0.0");
            


        //initialize!
        lc.proposeNewParams(true);
        System.out.println("Initialized likelihood parameters: ");
        System.out.println(TreeUtils.mapToString(lc.cloneParams()));
        tGen.proposeParams();
        coa_lineage current=null;
        Object[] initialTreeItsLikelihoodAndPrior=new Object[2];
        initialTreeItsLikelihoodAndPrior[1]=new BigDecimal("1");
        if(phyloXMLStartFile!=null)
            {
            //Read XML into a coa_lineage!
            try
                {
                current=coa_lineage.parsePhyloXML(phyloXMLStartFile);
                if(tGen instanceof SWPaperAlgTwoGenerator)
                    {
                    SREvent[] loadedEvents=SWPaperAlgTwoGenerator.generateEvents(current);
                    String waitsStr=SREvent.getWaitsArrAsStr(loadedEvents);
                    String heightsStr=SREvent.getHeightsArrAsStr(loadedEvents);
                    current.setPosteriorSampledValue("sre_profile",SREvent.JSONString(loadedEvents));
                    current.setPosteriorSampledValue("sre_waits",waitsStr);
                    current.setPosteriorSampledValue("sre_heights",heightsStr);
                    int[] mergerCounts=SWPaperAlgTwoGenerator.countMergers(current);
                    current.setPosteriorSampledValue("KingmanMergers",mergerCounts[0]);
                    current.setPosteriorSampledValue("SREMergers",mergerCounts[1]);
                    double c_ML=SWPaperAlgTwoGenerator.mlEstC(current,true);
                    current.setPosteriorSampledValue("c_ML",c_ML);
                    current.setFastaIORef(fio);
                    }
                if(tGen instanceof SWPaperAlgOneGenerator)
                    {
                    int mvd=SWPaperAlgOneGenerator.returnMaxDepth(current);
                    current.setPosteriorSampledValue("max_val_depth", mvd);
                    }
                }
            catch(Exception e)
                {
                e.printStackTrace();
                System.err.println(e.getMessage());
                System.exit(0);
                }
            }
        else
            {
            initialTreeItsLikelihoodAndPrior=tGen.generateTree();
            current=(coa_lineage)(initialTreeItsLikelihoodAndPrior[0]);
            current.setFastaIORef(fio);
            }


        BigDecimal currentLikelihood=(BigDecimal)lc.computeLikelihood(current,fio,true,tGen);     
        BigDecimal currentPrior=(BigDecimal)(initialTreeItsLikelihoodAndPrior[1]);
        //System.out.println("Initial tree "+current.toString());
        //System.out.println("Initial newick : "+current.makeNewickString(true,fio));
        System.out.println("Initial likelihood and prior : "+currentLikelihood.toString()+" & "+currentPrior.toString());
        //HashMap initialPosteriorMap=current.getPosteriorSampleMap();
        
        //System.out.println("Initial posterior map : "+TreeUtils.mapToString(initialPosteriorMap));
        HashMap currentGenParams=tGen.cloneParams();
        HashMap currentLCParams=lc.cloneParams();
        long firstIterVal=1;
        long currentIterationNumber=firstIterVal;
        /*System.out.println("Likelihood of preloop tree is "+currentLikelihood);
        System.out.println("Starting MCMC.");
        System.out.println("Current iteration number is "+currentIterationNumber);
        System.out.println("Burn-in is "+burnIter+"...");*/
        //save ML tree and likelihood values
        coa_lineage mlTree=current.cloneRec();
        HashMap mlPosteriors;
        if(TreeUtils.cloneMap(mlTree.getPosteriorSampleMap())==null)
            {
            mlPosteriors=new HashMap();
            }
        else
            {
            mlPosteriors=new HashMap();
            }
        BigDecimal mlValue=new BigDecimal(currentLikelihood.toString());
        String cometDisablePhyloXMLVar=System.getenv("COMET_DISABLE_PHYLOXML");
        if(cometDisablePhyloXMLVar!=null)
            {
            System.out.println("Environment variable COMET_DISABLE_PHYLOXML found to be set.  Suppressing output of non-ML XML...");
            if(tGen instanceof SWPaperAlgOneGenerator)
                {
                SWPaperAlgOneGenerator.setPosteriors=false;
                }
            }
        while(currentIterationNumber<=this.maxIter)
            {
            /*System.out.println("###############################################");
            System.out.println("At iteration #"+currentIterationNumber+", beta="+beta);
            System.out.println("Current likelihood = "+currentLikelihood);
            System.out.println("Current TGEN params : "+TreeUtils.mapToString(currentGenParams));
            System.out.println("Current LC params : "+TreeUtils.mapToString(currentLCParams));*/
            HashMap lcParamsBeforeProposal=lc.cloneParams();
 
            Object[] proposedTreeItsLikelihoodAndItsPrior=acquireProposalAndLikelihood(
                    current,
                    currentPrior,
                    tGen,
                    lc,
                    fio,
                    currentIterationNumber
                    );
            int pMethod=(int)proposedTreeItsLikelihoodAndItsPrior[3];
            /*System.err.println("LC params before is "+TreeUtils.mapToString(lcParamsBeforeProposal));
            System.err.println("LC params after  is "+TreeUtils.mapToString(lc.cloneParams()));
            System.exit(0);*/
            //System.out.println("Array from apal is "+Arrays.toString(proposedTreeItsLikelihoodAndItsPrior));
            HashMap proposedtGenParams=tGen.cloneParams();
            HashMap proposedLCParams=lc.cloneParams();

            coa_lineage proposedTree=(coa_lineage)(proposedTreeItsLikelihoodAndItsPrior[0]);
            BigDecimal proposedTreeLikelihood=(BigDecimal)(proposedTreeItsLikelihoodAndItsPrior[1]);
            BigDecimal proposedTreePrior=(BigDecimal)(proposedTreeItsLikelihoodAndItsPrior[2]);
            /*System.out.println("Proposed TGEN params " +TreeUtils.mapToString(proposedtGenParams));
            System.out.println("Proposed LC params "+TreeUtils.mapToString(currentLCParams));
            System.out.println("Likelihood of proposed tree is "+proposedTreeLikelihood);*/

            //compute qBETAs
            BigDecimal qBetaCurrent=qBetaOneModel(beta,currentLikelihood,currentPrior,true);
            BigDecimal qBetaProposd=qBetaOneModel(beta,proposedTreeLikelihood,proposedTreePrior,true);

            
            
            int likelihoodsComparisonResult=qBetaCurrent.compareTo(qBetaProposd);
            //System.out.println("The likelihoodsComparisonResult is "+likelihoodsComparisonResult);
            boolean proposalAccepted=false;
            if(likelihoodsComparisonResult==(-1))
                {
                //accept the proposed UNCONDITIONALLY
                //System.out.println("Proposal and params ACCEPTED unconditionally!");
                if(pMethod==MCMC.TREE_PROPOSAL)
                    {
                    proposalManager.logAcceptance(proposalManager.getLastProposalStyleMade(),tGen);
                    }
                else
                    {
                    proposalManager.logLCAcceptance();
                    }
                current=proposedTree;
                currentLikelihood=proposedTreeLikelihood;
                currentPrior=proposedTreePrior;
                //overwrite current params with the proposal params
                currentGenParams=proposedtGenParams;
                currentLCParams=proposedLCParams;
                qBetaCurrent=qBetaProposd;
                proposalAccepted=true;
                }//ACCEPT THE PROPOSAL UNCONDITIONALLY
            else
                {
                //System.out.println("Require a probability....");
                //accept proposal with proportional probability because its
                //likelihood is LESS than the current one
                //see Yang Z., Computational Mol. Evol., eq 5.27, pg 162
                BigDecimal diff=qBetaProposd.subtract(qBetaCurrent);
                /** SEE ALSO
                 *  Mol. Evol. : A Statistical Approach, Yang Z.,
                 * Chap 7, pg 215, eq 7.5
                 */
                //Is better to compute expm1(x)+1 than to compute exp(x) ; 
                //Note that for values of x near 0, the exact sum of expm1(x)+1 
                //is much closer to the true result of e^x than exp(x)
                //https://docs.oracle.com/javase/7/docs/api/java/lang/Math.html#expm1(double)
                double expDiff=Math.expm1(diff.doubleValue())+1.0;
                //The expDiff must be used to convert the log-likelihood to regular probability
                //because the 1 and alpha are in regular probability space
                double alpha=Math.min(1,expDiff);
                double randNum=TreeUtils.getRandNumBet(0, 1, false);                    
                //System.out.println("diff="+diff+", alpha="+alpha+", exp(diff)="+expDiff+" and randNum is "+randNum);
                if(randNum<=alpha)
                    {
                    //accept
                    //System.out.println("Proposal ACCEPTED!");
                    current=proposedTree;
                    currentLikelihood=proposedTreeLikelihood;
                    currentPrior=proposedTreePrior;
                    //overwrite current params with the proposal params
                    currentGenParams=proposedtGenParams;
                    currentLCParams=proposedLCParams;                    
                    qBetaCurrent=qBetaProposd;
                    if(pMethod==MCMC.TREE_PROPOSAL)
                        {
                        proposalManager.logAcceptance(proposalManager.getLastProposalStyleMade(),tGen);
                        }
                    else
                        {
                        proposalManager.logLCAcceptance();
                        }
                    proposalAccepted=true;
                    }
                else
                    {
                    //reject
                    //System.out.println("Proposal rejected!");
                    if(pMethod==MCMC.TREE_PROPOSAL)
                        {
                        proposalManager.logRejection(proposalManager.getLastProposalStyleMade(),tGen);
                        }
                    else
                        {
                        proposalManager.logLCRejection();
                        }
                    lc.setParams(lcParamsBeforeProposal);
                    proposalAccepted=false;
                    }                    
                }//needed to decide whether or not to accept the proposal

            if(currentLikelihood.compareTo(mlValue)==1)
                {
                //current likelihood is bigger, so save it
                mlValue=new BigDecimal(currentLikelihood.toString());
                mlTree=current.cloneRec();
                if(mlTree.getPosteriorSampleMap()!=null)
                    {
                    mlPosteriors=TreeUtils.cloneMap(mlTree.getPosteriorSampleMap());
                    HashMap lcMLCloneWork=lc.cloneParams();
                    for(Object k:lcMLCloneWork.keySet())
                        {
                        mlPosteriors.put(k,lcMLCloneWork.get(k));
                        }
                    }
                }
            

            if(currentIterationNumber==firstIterVal || currentIterationNumber%this.recordingFreq==0)
                {
                BigDecimal thisPotential=MCMC.potentialOneModel(currentLikelihood,currentPrior,true);
                dSum=dSum.add(thisPotential);
                numDSum=numDSum.add(new BigDecimal("1.0"));
                HashMap recMap=new HashMap();
                recMap.put("Proposal_scheme",proposalManager.getLastProposalStyleMade());
                recMap.put("Tree",current.makeNewickString(true,fio));
                recMap.put("TreeHeight",current.getHeight());
                recMap.put("Likelihood",currentLikelihood.toString().substring(
                        0,Math.min(12,currentLikelihood.toString().length())));
                recMap.put("Timestamp",TreeUtils.getNiceDataFormat());
                recMap.put("Proposal_scheme_status",proposalAccepted);

                if(cometDisablePhyloXMLVar==null)
                    {
                    //if the disable flag is absent, then make XML
                    coa_lineage currentTaggedForXML=tGen.tagForPhyloXML(current);
                    recMap.put("phylo_xml",currentTaggedForXML.makePhyloXML(true,0,false,currentLikelihood));                        
                    }
                else
                    {
                    //if the disable flag is present then don't make it!                        
                    recMap.put("phylo_xml","off");
                    }

                recMap=current.addPosteriorSampledToMap(recMap);
                for(Object key:currentLCParams.keySet())
                    {
                    //set likelihood calculation parameters to be logged
                    recMap.put(key,currentLCParams.get(key));
                    }
                try
                    {
                    File rawLogFile=new File(logTracePath);
                    File tracerLogFile=new File(tracerLogTracePath);
                    boolean cMLOverride=false;
                    if(tGen instanceof SWPaperAlgTwoGenerator)
                        {
                        cMLOverride=true;
                        }
                    appendToTraceLog(recMap,currentIterationNumber,firstRecord,rawLogFile,false,cMLOverride);
                    appendToTraceLog(recMap,currentIterationNumber,firstRecord,tracerLogFile,true,cMLOverride);
                    firstRecord=false;
                    }
                catch(Exception e)
                    {
                    e.printStackTrace();
                    System.exit(0);
                    }
                }
            File xmlTriggerFile=new File(xmlTriggerFilePath);
            boolean triggerXML=false;
            if(xmlTriggerFile.exists())
                {
                triggerXML=true;
                xmlTriggerFile.delete();
                }
            if(triggerXML)
                {
                System.out.println("*******************************************");
                System.out.println("Found file "+xmlTriggerFilePath+" and deleted it...");
                System.out.println("To write ML posteriors to stdout and ML XML to "+mlXMLPath);
                System.out.println("ML value = "+mlValue.toString());
                System.out.println("ML newick  = "+mlTree.makeNewickString(true, fio));
                System.out.println("ML posteriors : "+TreeUtils.mapToString(mlPosteriors));                    
                coa_lineage mlTreeForPhyloXML=tGen.tagForPhyloXML(mlTree);
                String thePhyloXML=mlTreeForPhyloXML.makePhyloXML(true,0,false,mlValue);                
                try
                    {
                    TreeUtils.writeStringToFile(thePhyloXML+"\n",mlXMLPath);
                    }
                catch(Exception e)
                    {
                    System.err.println("WARNING ! FAILURE to write ML XML to file "+mlXMLPath);
                    }                
                }
            
            currentIterationNumber++;
            }
        
        System.out.println("*******************************************");
        System.out.println("ML value = "+mlValue.toString());
        System.out.println("ML newick  = "+mlTree.makeNewickString(true, fio));
        System.out.println("ML posteriors : "+TreeUtils.mapToString(mlPosteriors));
        coa_lineage mlTreeForPhyloXML=tGen.tagForPhyloXML(mlTree);
        String thePhyloXML=mlTreeForPhyloXML.makePhyloXML(true,0,false,mlValue);
        System.out.println("PhyloXML written to "+mlXMLPath);
        try
            {
            TreeUtils.writeStringToFile(thePhyloXML+"\n",mlXMLPath);
            }
        catch(Exception e)
            {
            System.err.println("WARNING ! FAILURE to write ML XML to file "+mlXMLPath);
            }
        System.out.println("*******************************************");

        //return the beta package
        return new BigDecimal[]{dSum,numDSum};
        }


       public static BigDecimal MCMCIntegrateArrayList(
               ArrayList<BigDecimal> avg_list,
               ArrayList<BigDecimal> beta_list,
               boolean printInputs
                )
        {
        BigDecimal[] aList=new BigDecimal[avg_list.size()];
        BigDecimal[] bList=new BigDecimal[beta_list.size()];
        for(int i=0;i<aList.length;i++)
            {
            aList[i]=avg_list.get(i);
            bList[i]=beta_list.get(i);
            }
        if(printInputs)
            {
            System.out.println("Avg  list : "+Arrays.toString(aList));
            System.out.println("Beta list : "+Arrays.toString(bList));
            }
        return MCMCIntegrate(aList,bList);
        }
    
    
    
       public static BigDecimal MCMCIntegrate(BigDecimal[] avg_list,BigDecimal[] beta_list)
        {
        BigDecimal area_tot=new BigDecimal("0");
        for(int i=0;i<avg_list.length-1;i++)
            {
            System.out.println("#################################################");
            BigDecimal width=beta_list[i].subtract(beta_list[i+1]);
            System.out.println("betas are "+beta_list[i]+" and "+beta_list[i+1]);
            System.out.println("avgs are "+avg_list[i]+" and "+avg_list[i+1]);
            width=width.abs();
            BigDecimal height=null;
            BigDecimal bigH=null;
            BigDecimal litH=null;
            //-1, 0, 1 if this is <, == , >
            if(avg_list[i].compareTo(avg_list[i+1])==(-1))
                {
                litH=avg_list[i];
                bigH=avg_list[i+1];
                height=avg_list[i];
                }
            else
                {
                litH=avg_list[i+1];
                bigH=avg_list[i];                    
                height=avg_list[i+1];
                }
            System.out.println("width="+width);
            System.out.println("big h="+bigH);
            System.out.println("lit h="+litH);
            System.out.println("height="+height);
            BigDecimal area=width.multiply(height);
            System.out.println("Area is "+area);
            BigDecimal tri_height=bigH.min(litH);
            System.out.println("triHeight is "+tri_height);
            BigDecimal tri=tri_height.multiply(width).divide(new BigDecimal("2.0"));
            System.out.println("tri is "+tri);
            System.out.println("Adding to area_tot "+area.add(tri));
            area_tot=area_tot.add(area.add(tri));
            System.out.println("Running area tot "+area_tot);
            System.out.println("\n\n\n\n\n\n");
            }
        return area_tot;
   
        }
    
    

    
    public Object[] acquireProposalAndLikelihood(
            coa_lineage current,
            BigDecimal currentPrior,
            TreeGenerator tGen,
            TreeLikelihoodCalculator lc,
            FastaIO fio,
            long iterNum
            )
        {
        //init
        BigDecimal acquiredLikelihood=null;
        BigDecimal prior=null;
        coa_lineage proposedTree=null;
        int pMethod=(-1);
        
        //acquire proposal
        
        double randNum=TreeUtils.getRandNumBet(0,1,true);
        //System.out.println("In acquireProposalAndLikelihood");
        //System.out.println("randnum is "+randNum);
        
        
        //half the time IF the likelihood parameters
        //aren't [X,X] (ie set to be a single thing)
        //then the proposal is just on the liklihood end
        //and the tree stays the same
        //System.err.println("The value of randNum in apl is "+randNum);
        //System.err.println("The value of lc.arePriorsAllSingle() is "+lc.arePriorsAllSingle());
        if(randNum>=0.50 && !(lc.arePriorsAllSingle()))
            {
            //draw new likelihood params
            pMethod=MCMC.LIKELIHOOD_PROPOSAL;
            if(proposalManager.doesProposalLineIndicatePureGeneration())
                {
                lc.proposeNewParams(true);
                }
            else
                {
                lc.proposeNewParams(false);
                }
            prior=currentPrior;
            proposedTree=current;
            }
        else
        //other half the time IF the likelihood parameters
        //are [X,X] (ie set to be a single thing)
        //then the proposal is on the tree side
            {
            pMethod=MCMC.TREE_PROPOSAL;
            //draw new tree 1/2 time if lc params have a range
            //NOTE the first thing proposal manager does is clone a tree unless it's a generic proposal
            Object[] ret_pack=proposalManager.proposeTree(current, currentPrior, tGen,iterNum);
            proposedTree=(coa_lineage)(ret_pack[0]);
            prior=(BigDecimal)(ret_pack[1]);
            }
        //System.err.println("The value of pMethod is "+pMethod);

        //compute likelihood
        acquiredLikelihood=lc.computeLikelihood(proposedTree,fio,true,tGen);
        if(acquiredLikelihood==null)
            {
            return acquireProposalAndLikelihood(
                    current,
                    currentPrior,
                    tGen,
                    lc,
                    fio,
                    iterNum
                    );
            }
        Object[] retPackage=new Object[]{proposedTree,acquiredLikelihood,prior,pMethod};
        
        
        return retPackage;
        }
            
            
    public static boolean isValidFof(String fofPath)
        {
        try
            {
            BufferedReader br=new BufferedReader(new FileReader(fofPath));
            int numFilesFound=0;
            boolean foundANonExistingFile=false;
            while(br.ready())
                {
                String aPathFromFof=br.readLine();
                aPathFromFof=aPathFromFof.trim();
                File aFileFromFof=new File(aPathFromFof);
                if(!aFileFromFof.exists())
                    {
                    System.err.println("Warning file path "+aPathFromFof+" from "+fofPath+" not found!");
                    foundANonExistingFile=true;
                    }
                else
                    {
                    numFilesFound++;
                    }
                }
            br.close();
            if(numFilesFound==0 || foundANonExistingFile)
                {
                return false;
                }
            else
                {
                return true;
                }
            }
        catch(Exception e)
            {
            return false;
            }
        }
            
            

    
    
}







