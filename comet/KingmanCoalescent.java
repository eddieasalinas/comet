/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author esalina
 */
public class KingmanCoalescent implements TreeGenerator {

    private Double maxDistanceFromProbMatrix=null;
    private Double minDistanceFromProbMatrix=null;    
    
    public void setMinDistanceFromProbMatrix(double d)
        {
        if(d<=0)
            {
            throw new RuntimeException("Error, trying to set maximum distance for branch that is not positive : "+d);
            }
        minDistanceFromProbMatrix=new Double(d);
        System.out.println("Kingman min branch length set at : "+minDistanceFromProbMatrix);
        }
    
    public void setMaxDistanceFromProbMatrix(double d)
        {
        if(d<=0)
            {
            throw new RuntimeException("Error, trying to set maximum distance for branch that is not positive : "+d);
            }
        maxDistanceFromProbMatrix=new Double(d);
        System.out.println("Kingman max branch length set at : "+maxDistanceFromProbMatrix);
        }
    
    
    
    

    double beta=1;
    
    public double getBeta()
        {
        return beta;
        }

    public void setBeta(double b)
        {
        if(b<0 || b>1)
            {
            throw new IllegalArgumentException("Error, supplied beta is "+b+", but is not within range [0,1]!");
            }
        beta=b;
        }
    
    
    
    public coa_lineage tagForPhyloXML(coa_lineage root)
        {
        //the nodes are already tagged
        coa_lineage rootClone=root.cloneRec();
        return rootClone;
        }    
    

    public static coa_lineage tagForAlg2XML(coa_lineage root)
        {
        coa_lineage rootClone=root.cloneRec();
        ArrayList<coa_lineage> descendants=root.get_descendants(false);
        coa_lineage.sortByHeightInPlace(descendants,false);
        ArrayList<Double> heights=new ArrayList<Double>();
        for(int a=0;a<descendants.size();a++)
            {
            heights.add(descendants.get(a).getHeight());
            }
        System.out.println("Heights is "+heights.toString());
        prepForAlg2(rootClone,heights);
        applyKingTags(rootClone);
        return rootClone;
        }
    
    
    
    private static int applyKingTags(coa_lineage root)
        {

        int numKids=root.getNumKids();
        if(numKids==0)
            {
            //root.setTag("KING_0_0");
            return (-1);
            }
        else if(numKids==1)
            {
            int r1=applyKingTags(root.getChild(0))+1;
            root.setTag("KING_0_"+r1);
            return r1;
            }
        else if(numKids==2)
            {
            int r11=applyKingTags(root.getChild(0))+1;
            int r12=applyKingTags(root.getChild(1))+1;
            if(r11==r12)
                {
                root.setTag("KING_0_"+r11);
                return r11;
                }
            else
                {
                String mme="Error, left int king_coa is "+r11+" but right king_coa is "+r12+" mismatch!";
                throw new RuntimeException(mme);
                }
            }
        else
            {
            String nke="Numkids for a Kingnode node is "+numKids+" ! Error !";
            throw new RuntimeException(nke);
            }            
        }
    
    
    private static void prepForAlg2(coa_lineage root,ArrayList<Double> heights)
        {
        int numKids=root.getNumKids();
        if(numKids==0)
            {
            return;
            }
        else if(numKids==2)
            {
            double rootHeight=root.getHeight();
            coa_lineage ka=root.getChild(0);
            coa_lineage kb=root.getChild(1);
            double ha=ka.getHeight();
            double hb=kb.getHeight();
            double da=(-1);
            double db=(-1);
            for(int i=heights.size()-1;i>=0;i--)
                {
                double hi=heights.get(i);
                if(ha<hi && hi<rootHeight && da!=(-1))
                    {
                    //da is new dist from root to new intermediate node
                    da=rootHeight-hi;
                    }
                if(hb<hi && hi<rootHeight && db!=(-1))
                    {
                    db=rootHeight-hi;
                    }
                }
            if(da!=(-1))
                {
                double dda=rootHeight-da-ha;
                //dda is dist from new intermediate node to previous child removed
                coa_lineage newa=new coa_lineage("");
                newa.addChild(ka,dda);
                root.removeChild(ka);
                root.addChild(newa,da);
                prepForAlg2(newa,heights);
                }
            if(db!=(-1))
                {
                double ddb=rootHeight-db-ha;
                coa_lineage newb=new coa_lineage("");
                newb.addChild(kb,ddb);
                root.removeChild(kb);
                root.addChild(newb,db);
                prepForAlg2(newb,heights);
                }           
            }
        else
            {
            String errStr="numKids is "+numKids+". unexpected value!";
            throw new RuntimeException(errStr);
            }

        }
    
    
    
    FastaIO myFastaIO=null;
    public void setFasta(FastaIO fio)
        {
        this.myFastaIO=fio;
        }
    
    
    public void proposeParams()
        {
        
        }
    
    
        HashMap kingParams=null;

    public double computePriordouble(coa_lineage current)
        {
            return (double)(1.0);
        }
        
    public HashMap getParams()
        {
            return this.kingParams;
        }
        
    public void setParams(HashMap paramMap)
        {
              this.kingParams=paramMap;
        }
    
    
    public KingmanCoalescent(int num_lineages)
        {
        HashMap iMap=new HashMap();
        iMap.put("num_lineages",num_lineages);
        this.setParams(iMap);
        }
    
    
    public KingmanCoalescent(HashMap paramMap)
        {
            this.setParams(paramMap);
        }

    
        
    public static double drawExponentialKing(double c_val,int numLineages)
        {
        //see John Wakeley, Coalescent Theory book, eq 3.10
        //here the c is in the numerator and the k(k-1) is in the denominator
        //because drawExponential expects a mean  and mean=1/rate
        //K(K-1)/c is a rate so that's why there is the swapping here
        double exp_mean_num=c_val;
        double exp_mean_denom=(double)(numLineages)*(numLineages-1.0);
        double exp_mean=exp_mean_num/exp_mean_denom;
        double ret_king=TreeUtils.drawExponentialWithMean(exp_mean);
        return ret_king;        
        }
    
    
    
    public static double drawExponentialKingTruncatedFactor(int numLineages,double existingKing,double factor)
        {
        double low=Math.max(0,existingKing*factor);
        double exp_mean_num=2.0;
        double exp_mean_denom=(double)(numLineages)*(numLineages-1.0);
        double exp_mean=exp_mean_num/exp_mean_denom;
        double lambda=1.0/exp_mean;
        //find prob. mass between existing and low and solve for high so that the existing is the median of the truncated distribution
        double cdfLow=TreeUtils.exponentialDistCDFEval(low,lambda);
        double cdfCur=TreeUtils.exponentialDistCDFEval(existingKing,lambda);
        double massBelow=cdfCur-cdfLow;
        double high=(Math.log(1-massBelow)/((-1)*lambda))-existingKing;
        return drawExponentialKingTruncated(numLineages,low,high);
        }
    
    
    public static double drawExponentialKingTruncated(int numLineages,double tLow,double tHigh)
        {
        double exp_mean_num=2.0;
        double exp_mean_denom=(double)(numLineages)*(numLineages-1.0);
        double exp_mean=exp_mean_num/exp_mean_denom;
        //double ret_king=TreeUtils.drawExponentialWithMean(exp_mean);
        double ret_king=TreeUtils.drawTruncatedExponentialWithMean(exp_mean,tLow,tHigh);
        return ret_king;
        }
    
    
    public static double drawExponentialKing(int numLineages)
        {
        //see John Wakeley, Coalescent Theory book, eq 3.10
        double exp_mean_num=2.0;
        double exp_mean_denom=(double)(numLineages)*(numLineages-1.0);
        double exp_mean=exp_mean_num/exp_mean_denom;
        //the library wants mean, and the mean=1/rate ; 
        double ret_king=TreeUtils.drawExponentialWithMean(exp_mean);
        return ret_king;
        }
    
    public HashMap cloneParams()
        {
        return TreeUtils.cloneMap(kingParams);
        }
    

    
    public Object[] proposeTree(coa_lineage current,BigDecimal cPrior)
        {
        double randNum=TreeUtils.getRandNumBet(0,1,false);
        HashMap curMap=current.getPosteriorSampleMap(); 
        Object l30Ratio=curMap.get("last_30_acc_ratio");
        double acc30=(double)l30Ratio;        
        Object[] ret_pack=this.perturbBinaryHeightNew(current, cPrior);        
        current=(coa_lineage)ret_pack[0];
        cPrior=(BigDecimal)ret_pack[1];   
        if(randNum>=0.60)
            {
            randNum=TreeUtils.getRandNumBet(0,1,false);                
            if(randNum>=0.2)
                {
                return this.swapLineagesNew(current, cPrior);
                }
            else
                {
                return this.transferLineageNew(current, cPrior);
                }
            }
        else
            {
            return ret_pack;        
            }
        
        
        
        }
    
    int recentCode=0;
    
    public int getLastProposalCode()
        {
        return recentCode;
        }
        
    public static String code_0="GENERIC_PROPOSAL";
    public static String code_1="LAMARC_CT_87";
    public static String code_2="BINARY_HEIGHT_PERTURB";
    public static String code_3="SWAP_LINEAGES";
    public static String code_4="TRANSFER_LINEAGE";

    
    
    
    public String getProposalDescription(int code)
        {
        String desc;
        switch(code)
            {
            case 0:
                desc=code_0;
                break;
            case 1:
                desc=code_1;
                break;
            case 2:
                desc=code_2;
                break;
            case 3:
                desc=code_3;
                break;
            case 4:
                desc=code_4;
                break;
            default:
                throw new RuntimeException("Error, unknown proposal code "+code);
            }
        return desc;
        }
    
    
    
    private Object[] perturbBinaryHeight(coa_lineage current,BigDecimal cPrior)
        {
        //STEP 1 Acquire internal nodes formed by coalescent events
        ArrayList<coa_lineage> coals=current.returnNodesHavingExactly2KidsInSubtreeRootedHere();
        //System.out.println("The number of coals is "+coals.size());
        
        //STEP 2 Sort the list by height (in ascending order) so root is at end
        coals=coa_lineage.sortByHeightInPlace(coals, true);
        
        //STEP 3 Compute Kingman values sampled
        ArrayList<Double> kings=new ArrayList<Double>();
        for(int k=0;k<coals.size();k++)
            {
            if(k>0)
                {
                coa_lineage higher_lineage=coals.get(k);
                coa_lineage lower_lineage=coals.get(k-1);
                double diff=higher_lineage.getHeight()-lower_lineage.getHeight();
                kings.add(diff);
                }
            else
                {
                kings.add(coals.get(0).getHeight());
                }
            }
        //System.out.println("The number of kings is "+kings.size());
        
        //STEP 4 create new lineage for use in step 5
        ArrayList<String> coa_names=current.getNamesOfTipsInSubtreeRootedHere();
        ArrayList<coa_lineage> tempLineages=new ArrayList<coa_lineage>();
        for(int i=0;i<coa_names.size();i++)
            {
            coa_lineage temp=new coa_lineage(coa_names.get(i));
            //System.out.println("Just created a lineage with name "+coa_names.get(i));
            tempLineages.add(temp);
            }
        
        //STEP 5 recreate same topology BUT in one branch apply a different KINGMAN sampled value        
        //join lineages until one lineage is left!
        double maxHeight=0;
        int k_i=0;
        int s_i=0;
        int[] newKingMaskIntArr=TreeUtils.chooseRandomSubset(1,kings.size());
        int kingIndex=newKingMaskIntArr[0];
        while(tempLineages.size()>1)
            {
            //see http://pydoc.net/Python/DendroPy/3.8.1/dendropy.coalescent/
                
            //first acquire two lineages per the existing setup
            ArrayList <String> subtree_tip_names=coals.get(s_i).getNamesOfTipsInSubtreeRootedHere();
            s_i++;
            int[] twoLineageIndices=acquireLineageIndicesByNames(
                    tempLineages,
                    subtree_tip_names
                    );
            int firstLineage=twoLineageIndices[0];
            int secondLineage=twoLineageIndices[1];
            String firstName=tempLineages.get(firstLineage).getName();
            String secondName=tempLineages.get(secondLineage).getName();

            
            //compute name of new lineage and create the new lineage
            String newName="("+firstName+" & "+secondName+")";
            coa_lineage newLineage=new coa_lineage(newName);
            
            //obtain heights of selected lineages to acquire delta
            double firstHeight=tempLineages.get(firstLineage).getHeight();
            double secondHeight=tempLineages.get(secondLineage).getHeight();

            //perform the coalescence!
            //compute max height across all lineages to serve as base height
            //to which the drawn kingman exponential time is added to
            double newDistBase=maxHeight;
            double newCoaHeight=kings.get(k_i)+newDistBase;
            if(k_i!=kingIndex)
                {
                //use same value from before
                newCoaHeight=kings.get(k_i)+newDistBase;
                }
            else
                {
                //here use a different value!
                newCoaHeight=KingmanCoalescent.drawExponentialKing(tempLineages.size())+newDistBase;
                }
            k_i++;
            //update maxHeight to be newCoaHeight.  maxHeight always increases because of definition of newCoaHeight
            maxHeight=newCoaHeight;
            newLineage.addChild(tempLineages.get(firstLineage),newCoaHeight-firstHeight);
            newLineage.addChild(tempLineages.get(secondLineage),newCoaHeight-secondHeight);
            tempLineages.add(newLineage);
            tempLineages.remove(firstLineage);
            if(secondLineage>firstLineage)
                {
                //if the index of the second lineage is beyond the first this decrement is needed
                tempLineages.remove(secondLineage-1);
                }
            else
                {
                //if the index of the second lineage is before the first, no decrement needed
                tempLineages.remove(secondLineage);
                }
            }//while there are lineages to join   
        
        /*boolean fv=tempLineages.get(0).verifyHeights(false);
        boolean sv=tempLineages.get(0).verifyKidsAndDistsConsistency();
        System.out.println("fv is "+fv);
        System.out.println("sv is "+sv);
        String cn=current.makeNewickString(false, null);
        String n=tempLineages.get(0).makeNewickString(false, null);
        System.out.println("cn is "+cn);
        System.out.println("n is "+n);
        System.out.println("Cur height is "+current.getHeight());*/
        coa_lineage proposed=tempLineages.get(0);
        Object[] retPackage=new Object[2];
        retPackage[0]=proposed;
        retPackage[1]=cPrior;
        recentCode=2;
        return retPackage;
        }
    
    
    private static int[] acquireLineageIndicesByNames(
            ArrayList<coa_lineage> tempLineages,
            ArrayList<String> subtree_tip_names
            )
        {
        //System.out.println("Subtree tip names are : "+TreeUtils.arrayListStringToString(subtree_tip_names));
        for(int i=0;i<tempLineages.size()-1;i++)
            {
            for(int j=i+1;j<tempLineages.size();j++)
                {
                //System.out.println("I names are : "+TreeUtils.arrayListStringToString(tempLineages.get(i).getNamesOfTipsInSubtreeRootedHere()));
                //System.out.println("J names are : "+TreeUtils.arrayListStringToString(tempLineages.get(j).getNamesOfTipsInSubtreeRootedHere()));
                boolean isPartition=arePartition(
                        subtree_tip_names,
                        tempLineages.get(i).getNamesOfTipsInSubtreeRootedHere(),
                        tempLineages.get(j).getNamesOfTipsInSubtreeRootedHere());
                if(isPartition)
                    {
                    //System.out.println("YES a partition!");
                    int[] rp=new int[2];
                    rp[0]=i;
                    rp[1]=j;
                    return rp;
                    }
                else
                    {
                    //System.out.println("NOT a partition!");
                    }
                }
            }
           
        throw new NullPointerException("Error, didn't find the pair?!?!");
        }
    
    private static boolean arePartition(ArrayList<String> full,ArrayList<String> a,ArrayList<String> b)
        {
        //assume nothing is null
        if(full.size()==a.size()+b.size())
            {
            //maybe
            for(String s:b)
                {
                if(a.contains(s))
                    {
                    //intersection ! bad!
                    return false;
                    }
                }
            for(String s:a)
                {
                if(b.contains(s))
                    {
                    //intersection ! bad!
                    return false;
                    }
                }
            for(String s:full)
                {
                if(!(a.contains(s) || b.contains(s)))
                    {
                    return false;
                    }
                }
            return true;
            }
        else
            {
            return false;
            }
            
        }
    
    
    
    private Object[] LAMARC_CT_87(coa_lineage current,BigDecimal cPrior)
        {
        double pre_height=current.getHeight();
        ArrayList<String> tips_pre=current.getNamesOfTipsInSubtreeRootedHere();
        int num_tips_pre=tips_pre.size();
            
            
        /*perform a tree-rearrangement (very much like local neighborhood rearrangement
        as shown in Coalescent Theory : An Introduction, 2009, by John Wakely, Fig. 8.7 pg 289 )
            
        The essence of the code is to simply choose a different pair of lineages to coalesce within
        the tree, but otherwise leave the geneaology alone.  The branch lengths don't change except
        to preserve the coalsecent property that the distnace to the MRCA (from any tip) before and after the 
        the rearrangement is the same!  This is based on LAMARC  (see reference for more info)*/
            
        /*STEP 1.  Choose a node N at random and then randomly Nr one of its two children.  
        N cannot be the root and N cannot be a tip!*/
        //System.out.println("STEP 1 ... find a random node");
        Object[] randomInternalNodeAndChild=current.chooseRandomNonTipInternalNode();
        coa_lineage randomNode=(coa_lineage)(randomInternalNodeAndChild[0]);
        //this randomNode corresponds to the parent of the * node in Fig 8.7

        //STEP 2.  Find the parent of the randomly chosen node and the parent's height
        //System.out.println("STEP 2 ... get the parent of the random node and the parent's height...");
        coa_lineage parent=current.findTheParentOfGivenNode(randomNode);
        double parent_height=parent.getHeight();
        //System.out.println("The height of the parent of the random node is "+parent_height);
        //this parent corresponds to the parent of the random node above
        int randomNodeIndexFromParent=parent.getKidIndex(randomNode);
        double distBetweenParentAndRandomNode=parent.getDist(randomNodeIndexFromParent);
        //System.out.println("The distance between the parent and the random node : "+distBetweenParentAndRandomNode);
        //since there should be exactly two nodes, otherIndex points to the parent's other child


        /*STEP 3.  Find a node and one of its children such that the edge/branch
        connecting the node and the child 'crosses' over the height value 
        (ie parent height and kid height are on separate sides of the value!**/
        
        /*
        STEP 3a Acquire a list of nodes such that each node in the list has at least
        one child with the branch from the node to the child crossing the height
        */
        /*System.out.println("Step 4a, get a list of nodes with at least one child 
        with a branch leading to the child such that the branch crosses the random node's height");
        System.out.println("Finding nodes that have at least one child such that the "
                + "branch from the node to the child crosses the value of  "
                +parent_height+" which is the random node parent's height.");*/
        ArrayList<coa_lineage> nodesHavingAtLeastOneChildCrossingHeightLine=current.
                findAllNodesWithAtLeastOneChildEdgeCrossingHeightValue(parent_height);
        int numAcceptableNodes=nodesHavingAtLeastOneChildCrossingHeightLine.size();
        if(numAcceptableNodes==0)
            {
            return this.generateTree();
            }        
        
        /*
        STEP 3b  Select one of the nodes at random
        */
        int indexForANodeAndAtLeastOneQualifyingChild=(-1);
        if(nodesHavingAtLeastOneChildCrossingHeightLine.size()==1)
            {
            indexForANodeAndAtLeastOneQualifyingChild=0;
            }
        else
            {
            indexForANodeAndAtLeastOneQualifyingChild=TreeUtils.getRandomInt(0,
                    nodesHavingAtLeastOneChildCrossingHeightLine.size()-1);
            }
        //System.out.println("The index of the randomly chosen acceptable node is "+indexForANodeAndAtLeastOneQualifyingChild);
        coa_lineage selected_node=nodesHavingAtLeastOneChildCrossingHeightLine.get(indexForANodeAndAtLeastOneQualifyingChild);

        /*
        STEP 3c 
        Of the selected node, of the qualifying children, select a child at random
        */
        int insKidIndex=selected_node.selectIndexOfKidBranchCrossingBoundary(parent_height);
        /*System.out.println("Of the selected node, its height is "+selected_node.getHeight());
        System.out.println("Of the selected node its selected kid index is "+insKidIndex);
        System.out.println("Of the selected node its selected kid is height "+selected_node.getChild(insKidIndex).getHeight());
        System.out.println("\n\n\n\n");*/
        
        
        /*
        STEP 4
        Create a new node that will be a parent for the random node!
        */
        coa_lineage newParentForRandomNode=new coa_lineage("");
        //System.out.println("Step 5, create a new node/parent for the insertion point....");
        
        
        /*
        STEP 5
        Add the random node with the same distance that the random nodes current parent has
        Get the height of the new subtree
        */
        newParentForRandomNode.addChild(randomNode,distBetweenParentAndRandomNode);
        double subTreeHeight=newParentForRandomNode.getHeight();
        //System.out.println("Step 6, add to the insertion point the subtree of the random node and get the height there.");
        //System.out.println("The height of the subtree is "+subTreeHeight+"\n\n\n\n");
        
        

        
        
        /*
        STEP 6
        on the acceptable node, replace the child with the new node
        for the dist compute it using the coalescent MRCA height criterion
        */
        double computedHeight=selected_node.getHeight()-subTreeHeight;
        coa_lineage saved_child=selected_node.
                replaceChild(newParentForRandomNode,computedHeight,insKidIndex);
        /*System.out.println("Step 7 On the accepted node, replace the child with the new parent");
        System.out.println("The computed height for insertion is "+computedHeight);
        System.out.println("The insKidIndex is "+insKidIndex);
        System.out.println("The height of the parent, post insertion is "+newParentForRandomNode.getHeight());
        System.out.println("\n\n\n\n");*/
        

        /*
        STEP 7
        Invoke on the parent remove child!
        Here "parent" refers to the parent of the originally chosen random node!
        if(parent and saved child are the same, abort and recurse)
        */
        parent.removeChild(randomNodeIndexFromParent);        
        
        
        
        /*
        STEP 8
        Re-integrate the saved child!
        */
        double saved_child_height=saved_child.getHeight();
        double computed_reint_dist=subTreeHeight-saved_child_height;
        newParentForRandomNode.addChild(saved_child,computed_reint_dist);
        
        
        //necessary to call this because removeChild was called
        current.removeExtraNodes();
        /*String post_newick=current.makeNewickString(false, null);
        System.out.println("POST-NEWICK : "+post_newick);*/
        /*ArrayList<String> tips_post=current.getNamesOfTipsInSubtreeRootedHere();
        int num_tips_post=tips_post.size();
        double post_height=current.getHeight();
        boolean final_v=current.verifyHeights(false);
        if(num_tips_pre!=num_tips_post)
            {
            System.out.println("FAIL on tip-count comparison!");
            System.out.println("PRE="+num_tips_pre+", but post="+num_tips_post);
            }
        else
            {
            System.out.println("Success on tip-count comparison!");
            }
        if(final_v)
            {
            System.out.println("VERIFIED final_v!!!");
            }
        else
            {
            System.out.println("FAIL final_v!!!!!!!");
            }
        if(pre_height==post_height)
            {
            System.out.println("Success in height comparisons!");
            }
        else
            {
            System.out.println("FAIL in height comparisons!");
            System.out.println("Pre height is  : "+pre_height);
            System.out.println("Post height is : "+post_height);
            }*/
        boolean binaryTest=current.isRootOfBinary();
        if(!binaryTest)
            {
            System.out.println("Newick is "+current.makeNewickString(false, null));
            throw new RuntimeException("Fail binary test!");
            }
        else
            {
            //System.out.println("Success on binary test!");
            }
        
        recentCode=1;
        return new Object[]{current,cPrior};
        
        //System.exit(0);
        //return null;
        }
    
    private static ArrayList<String> kingTags=null;
    private static ArrayList<String> kingTagsNoRoot=null;
    private static ArrayList<String> kingTagsSingleAppearanceNoRoot=null;
    private static ArrayList<String> kingTagsSingleAppearanceNoRootAndNotNodesImmediatelyUnderRoot=null;
    private static Pattern kingCoaPattern=Pattern.compile("_(\\d+)$");

    private static void initKingTags(coa_lineage current)
        {
        if(kingTags==null)
            {
            kingTagsNoRoot=new ArrayList<String>();
            kingTags=new ArrayList<String>();
            kingTagsSingleAppearanceNoRoot=new ArrayList<String>();
            kingTagsSingleAppearanceNoRootAndNotNodesImmediatelyUnderRoot=new ArrayList<String>();
            String rootTag=current.getTag();
            int numLeaves=current.getNumLeavesUnder(true);
            Matcher m=kingCoaPattern.matcher(rootTag);
            if(m.find())
                {
                int rootKingCoa=Integer.parseInt(m.group(1));
                for(int i=0;i<=rootKingCoa;i++)
                    {
                    int level=i;
                    int numAppearances=numLeaves-level-1;
                    String t="KING_0_"+level;
                    for(int a=0;a<numAppearances;a++)
                        {
                        if(numAppearances>2)
                            {
                            kingTagsSingleAppearanceNoRootAndNotNodesImmediatelyUnderRoot.add(t);
                            }
                        if(a==0 && numAppearances>1)
                            {
                            kingTagsSingleAppearanceNoRoot.add(t);
                            }
                        kingTags.add(t);
                        if(numAppearances>1)
                            {
                            kingTagsNoRoot.add(t);
                            }
                        }
                    }
                }
            else
                {
                String notFoundEx="Error, didn't find pattern "+kingCoaPattern.toString()+" in '"+rootTag+"'!";
                throw new NullPointerException(notFoundEx);
                }
            }
        }
    
    
    public Object[] transferLineageNew(coa_lineage current,BigDecimal cPrior)
        {
          
        //make tag counts if necessary
        initKingTags(current);
        
        //acquire a random tag
        String randomTag=null;
        randomTag=(String)(TreeUtils.chooseRandom(kingTagsSingleAppearanceNoRootAndNotNodesImmediatelyUnderRoot));

        //get the nodes of that tag
        ArrayList<coa_lineage> nodesOfTag=current.getNodesWithTagMatching(randomTag,false);
        
        
        //find the node with two kids
        int indexOfTwoKidder=(-1);
        for(int i=0;i<nodesOfTag.size();i++)
            {
            if(nodesOfTag.get(i).getNumKids()==2)
                {
                indexOfTwoKidder=i;
                i=nodesOfTag.size()+1;
                }
            }
        coa_lineage sourceNode=nodesOfTag.get(indexOfTwoKidder);
        
        //remove it from the list of nodes and choose a receiver node
        nodesOfTag.remove(indexOfTwoKidder);
        int receiverIndex=TreeUtils.chooseRandomIndex(nodesOfTag.size());
        coa_lineage receiver=nodesOfTag.get(receiverIndex);
        
        //chose a random kid for the source to give up
        int randKidIndex=sourceNode.chooseRandomKidIndex();
        coa_lineage randKid=sourceNode.getChild(randKidIndex);
        sourceNode.removeChild(randKid);
        
        //add the child to the receiver
        double dist=receiver.getDist(0);
        receiver.addChild(randKid,dist);
        recentCode=4;
        Object[] ret_pieces=new Object[]{current,cPrior};
        return ret_pieces;
        }
    
    
    
    public Object[] swapLineagesNew(coa_lineage current,BigDecimal cPrior)
        {
          
        //make tag counts if necessary
        initKingTags(current);
        
        //acquire a random tag
        String randomTag=null;
        randomTag=(String)(TreeUtils.chooseRandom(kingTagsSingleAppearanceNoRootAndNotNodesImmediatelyUnderRoot));

        //get the nodes of that tag
        ArrayList<coa_lineage> nodesOfTag=current.getNodesWithTagMatching(randomTag,false);

        
        //choose two at random, require near shared parentage if low recent acc ratio
        boolean requireShareParents=true;
        HashMap curMap=current.getPosteriorSampleMap(); 
        Object l30Ratio=curMap.get("last_30_acc_ratio");
        double acc30=(double)l30Ratio;           
        if(acc30<=0.20)
            {
            requireShareParents=true;
            }
        int firstLineageIdx;
        int secondLineageIdx;
        coa_lineage firstLineage=null;
        coa_lineage secondLineage=null;
        do
            {
            int[] twoRandomLineages=TreeUtils.chooseRandomSubset(2,nodesOfTag.size());
            firstLineageIdx=Math.min(twoRandomLineages[0],twoRandomLineages[1]);
            secondLineageIdx=Math.max(twoRandomLineages[0],twoRandomLineages[1]);
            firstLineage=nodesOfTag.get(firstLineageIdx);
            secondLineage=nodesOfTag.get(secondLineageIdx);
            }while(
                requireShareParents && 
                coa_lineage.shareAParent(firstLineage,secondLineage) &&
                (firstLineage.getNumKids()>1 || secondLineage.getNumKids()>1));
        
        //choose kids of theirs at random
        int firstRandKidIndex=firstLineage.chooseRandomKidIndex();
        int secondRandKidIndex=secondLineage.chooseRandomKidIndex();
        
        //acquire distance
        double dist=firstLineage.getDist(0);

        //detach
        coa_lineage firstDetached=firstLineage.getChild(firstRandKidIndex);
        firstLineage.removeChild(firstDetached);
        coa_lineage secondDetached=secondLineage.getChild(secondRandKidIndex);
        secondLineage.removeChild(secondDetached);
        
        //swap
        secondLineage.addChild(firstDetached,dist);
        firstLineage.addChild(secondDetached,dist);
        
        
        //return the lineages
        Object[] ret_pieces=new Object[]{current,cPrior};
        recentCode=3;
        return ret_pieces;                 
        }
    
    
    
    public Object[] perturbBinaryHeightNew(coa_lineage current,BigDecimal cPrior)
        {

        //make tag counts if necessary
        initKingTags(current);
        
        //acquire a random tag
        String randomTag=(String)(TreeUtils.chooseRandom(kingTags));

        //get the nodes of that tag
        ArrayList<coa_lineage> nodesOfTag=current.getNodesWithTagMatching(randomTag,false);

        //compute a new king perturbed dist
        int K=nodesOfTag.size()+1; //add 1 because of the merger at this level
        double kingFactor=0;
        HashMap curMap=current.getPosteriorSampleMap(); 
        Object l30Ratio=curMap.get("last_30_acc_ratio");
        double acc30=(double)l30Ratio;
        kingFactor=Math.min(0.5,TreeUtils.drawTruncatedExponentialWithMean(0.999-acc30/2.0,0.5,0.999));
        /*if(TreeUtils.getRandNumBet(0,1,true)>=0.95)
            {
            kingFactor=0.85;
            }*/
        
        double newKingDist=(-1);
        do
            {
            newKingDist=KingmanCoalescent.drawExponentialKingTruncatedFactor(K,nodesOfTag.get(0).getDist(0),kingFactor);
            }while(newKingDist<=this.minDistanceFromProbMatrix || newKingDist>=this.maxDistanceFromProbMatrix);
            

        //set the dist
        for(int l=0;l<nodesOfTag.size();l++)
            {
            int numKids=nodesOfTag.get(l).getNumKids();
            for(int k=0;k<numKids;k++)
                {
                nodesOfTag.get(l).setDist(k, newKingDist);
                }
            }
        
        //compute new Prior
        coa_lineage tempNode=current;
        int numMergeInto=2;
        double sum_log_like=0;
        while(tempNode.getNumKids()>=1)
            {
            double dist=tempNode.getDist(0);
            tempNode=tempNode.getChild(0);
            double chooseFactor=TreeUtils.n_Choose_k(numMergeInto,2);
            double exponent=(-1)*chooseFactor*dist;
            //double val=chooseFactor*Math.exp(exponent);
            double ln_val=Math.log(chooseFactor)+exponent;
            sum_log_like+=ln_val;
            /*System.out.println("Ending loop, numMergeInto is "+numMergeInto+
                    " dist="+dist+" ln_val="+ln_val+",tempNode.numkids=="+
                    tempNode.getNumKids()+", sumll is "+sum_log_like);*/
            
            numMergeInto++;
            }
        //double  sum_log_like=0;
       

        //return the lineages
        BigDecimal prior=new BigDecimal(sum_log_like);
        Object[] ret_pieces=new Object[]{current,prior};
        recentCode=2;
        return ret_pieces;        
        }
    
    
    public double estThetaHatIP278(coa_lineage current)
        {
        int numLeaves=current.getNumLeavesUnder(true);
        //System.err.println("in theta numLeaves="+numLeaves);
        coa_lineage temp=current;
        double numSum=0;
        int k=2;
        do 
            {
            double contribution=k*(k-1)*temp.getDist(0);
            numSum+=contribution;
            //System.err.println("in theta k="+k);
            k++;
            temp=temp.getChild(0);
            }while(k<=numLeaves);
        double ratio=numSum/(numLeaves-1);
        return ratio;
        }
    
    
    public Object[] generateTreeNew()
        {

        //first initialize a LIST of lineages
        int num_lineages_to_get=Integer.parseInt(kingParams.get("num_lineages").toString());
        ArrayList<coa_lineage> tempLineages=new ArrayList<coa_lineage>();
        for(int l=0;l<num_lineages_to_get;l++)
            {
            coa_lineage tempLineage=new coa_lineage(""+(l+1)+"");
            tempLineages.add(tempLineage);
            }
        
        //see http://pydoc.net/Python/DendroPy/3.8.1/dendropy.coalescent/
        //join lineages until one lineage is left!
        int king_coa=0;
        while(tempLineages.size()>1)
            {
            //see http://pydoc.net/Python/DendroPy/3.8.1/dendropy.coalescent/
                
            //first acquire two random lineages and their names
            int[] twoRandomLineages=TreeUtils.chooseRandomSubset(2,tempLineages.size());
            int firstLineage=Math.min(twoRandomLineages[0],twoRandomLineages[1]);
            int secondLineage=Math.max(twoRandomLineages[0],twoRandomLineages[1]);
            String firstName=tempLineages.get(firstLineage).getName();
            String secondName=tempLineages.get(secondLineage).getName();        
            //compute name of new lineage and create the new lineage
            String newName="("+firstName+" & "+secondName+")";

            //draw the new dist!
            double newCoaDist=(-1);
            do
                {
                newCoaDist=drawExponentialKing(tempLineages.size());
                }while(newCoaDist<=this.minDistanceFromProbMatrix || newCoaDist>=this.maxDistanceFromProbMatrix);
                        
            //create the merger
            coa_lineage merger=new coa_lineage(newName);
            merger.setMergerStyle(coa_lineage.KINGMAN_STYLE);
            String thisTag="KING_0_"+king_coa;
            merger.setTag(thisTag);
            merger.addChild(tempLineages.get(firstLineage),newCoaDist);
            merger.addChild(tempLineages.get(secondLineage),newCoaDist);
            
            //remove the merged from the templineages
            tempLineages.remove(firstLineage);
            secondLineage--; //decrement, cause the lower one is removed
            tempLineages.remove(secondLineage);
            
            //extend the lineages and transfer
            ArrayList<coa_lineage> newTempLineages=new ArrayList<coa_lineage>();
            for(int i=0;i<tempLineages.size();i++)
                {
                coa_lineage newLineage=new coa_lineage(tempLineages.get(i).getName());
                newLineage.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                newLineage.setTag(thisTag);
                newLineage.addChild(tempLineages.get(i),newCoaDist);
                newTempLineages.add(newLineage);
                }
            newTempLineages.add(merger);
            tempLineages=newTempLineages;
            king_coa++;
            }
        
            
        //return the lineages
        BigDecimal prior=new BigDecimal("1");
        Object[] ret_pieces=new Object[]{tempLineages.get(0),prior};
        recentCode=0;
        return ret_pieces;
        }
    
    
    
    
    public Object[] generateTree()
        {
        return generateTreeNew();
        /*   
            
        //first initialize a LIST of lineages
        int num_lineages_to_get=Integer.parseInt(kingParams.get("num_lineages").toString());
        ArrayList<coa_lineage> tempLineages=new ArrayList<coa_lineage>();
        for(int l=0;l<num_lineages_to_get;l++)
            {
            coa_lineage tempLineage=new coa_lineage(""+(l+1)+"");
            tempLineages.add(tempLineage);
            }
        
        //join lineages until one lineage is left!
        double maxHeight=0;
        while(tempLineages.size()>1)
            {
            //see http://pydoc.net/Python/DendroPy/3.8.1/dendropy.coalescent/
                
            //first acquire two random lineages and their names
            int[] twoRandomLineages=TreeUtils.chooseRandomSubset(2,tempLineages.size());
            int firstLineage=twoRandomLineages[0];
            int secondLineage=twoRandomLineages[1];
            String firstName=tempLineages.get(firstLineage).getName();
            String secondName=tempLineages.get(secondLineage).getName();

            
            //compute name of new lineage and create the new lineage
            String newName="("+firstName+" & "+secondName+")";
            coa_lineage newLineage=new coa_lineage(newName);
            
            //obtain heights of selected lineages to acquire delta
            double firstHeight=tempLineages.get(firstLineage).getHeight();
            double secondHeight=tempLineages.get(secondLineage).getHeight();


            
            //perform the coalescence!
            //compute max height across all lineages to serve as base height
            //to which the drawn kingman exponential time is added to
            double newDistBase=maxHeight;
            double newCoaHeight=drawExponentialKing(tempLineages.size())+newDistBase;
            //update maxHeight to be newCoaHeight.  maxHeight always increases because of definition of newCoaHeight
            maxHeight=newCoaHeight;
            newLineage.addChild(tempLineages.get(firstLineage),newCoaHeight-firstHeight);
            newLineage.addChild(tempLineages.get(secondLineage),newCoaHeight-secondHeight);
            tempLineages.add(newLineage);
            tempLineages.remove(firstLineage);
            if(secondLineage>firstLineage)
                {
                //if the index of the second lineage is beyond the first this decrement is needed
                tempLineages.remove(secondLineage-1);
                }
            else
                {
                //if the index of the second lineage is before the first, no decrement needed
                tempLineages.remove(secondLineage);
                }
            }//while there are lineages to join
        
        
        //return the lineages
        BigDecimal prior=new BigDecimal("1");
        Object[] ret_pieces=new Object[]{tempLineages.get(0),prior};
        recentCode=0;
        return ret_pieces;*/
        }
        
    
    
    
}
