/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.random.Well19937c;
import org.apache.commons.math3.util.Combinations;


/**
 *
 * @author esalina
 */
public class TreeUtils {

    
    private static int n_Choose_k_hash(int n,int k,String key)
        {
        if(n_choose_k_hash==null)
            {
            n_choose_k_hash=new HashMap();
            }
        Combinations myCombs=new Combinations(n,k);
        Iterator myIterator=myCombs.iterator();
        int count=0;
        while(myIterator.hasNext())
            {
            count++;
            myIterator.next();
            }
        //System.out.println("for ("+n+" | "+k+") n choose k is "+count);
        n_choose_k_hash.put(key,count);
        return count;
        }
    
    private static HashMap n_choose_k_hash=null;
    
    public static int n_Choose_k(int n,int k)
        {
        String key=""+n+":"+k;
        if(n_choose_k_hash!=null &&  n_choose_k_hash.containsKey(key))
            {
             
            return (int)n_choose_k_hash.get(key);
            }
        else
            {
            return n_Choose_k_hash(n,k,key);
            }
        }
   
    public static double[] applyLog(double[] data) {
        if(data==null)
            {
            return null;
            }
        else
            {
            double[] logs=new double[data.length];
            for(int x=0;x<logs.length;x++)
                {
                logs[x]=Math.log(data[x]);
                }
            return logs;
            }
        }
    
    public static int figureIfHyperThreadingIsEnabled()
        {

        try
            {
            String pcu="/proc/cpuinfo";
            BufferedReader br=new BufferedReader(new FileReader(pcu));
            String htRegex="^flags\\s.*\\bht\\b";
            Pattern htPattern=Pattern.compile(htRegex);      
            //System.out.println("now entering while...");
            String tempLine;
            while((tempLine = br.readLine()) != null)
                {
                tempLine=tempLine.trim();
                //System.out.println("Read "+tempLine);                
                if(tempLine.startsWith("flags"))
                    {
                    //System.out.println("swf");
                    Matcher m=htPattern.matcher(tempLine);
                    if(m.find())
                        {
                        return 1;
                        }
                    }
                else
                    {
                    //System.out.println("not swf");
                    }
                }
            br.close();
            //System.out.println("plain exit");
            return 0;
            }
        catch(Exception e)
            {
            System.err.println("WARNING : Unable to automatically figure if hyperthreading is enabled!  User is advised to manually set number of physical cores in conf file for best performance!");
            return 2;
            }
            
            
        }
    
    
    
    public static int getNumIndicates(boolean[] indicators)
        {
        int num=0;
        for(int b=0;b<indicators.length;b++)
            {
            if(indicators[b])
                {
                num++;
                }
            }
        return num;
        }
    
    
    public static boolean[] getNegInfIndicators(double[] probs)
        {
        boolean[] indicators=new boolean[probs.length];
        for(int i=0;i<probs.length;i++)
            {
            if(probs[i]==Double.NEGATIVE_INFINITY)
                {
                indicators[i]=true;
                }
            else
                {
                indicators[i]=false;
                }
            }
        return indicators;
        }
    
    public static String getPID()
        {
	RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();
 
	String jvmName = runtimeBean.getName();
	//System.out.println("JVM Name = " + jvmName);
	long pid = Long.valueOf(jvmName.split("@")[0]);
        return ""+pid+"";

        }
    
    
    
    private final static double lnTwo=Math.log(2.0);
    public static double logBaseTwo(int val)
        {
        if(val<=0)
            {
            throw new IllegalArgumentException("Error, can't take log of a non-positive number!");
            }
        else
            {
            if(val==1)
                {
                return 0;
                }
            else if(val==2)
                {
                return 1;
                }
            else
                {
                //Logb x = Loga x/Loga b
                double numerator=Math.log((double)(val));
                double denominator=lnTwo;
                return numerator/denominator;
                }
            }
        }
    
    private static BinomialDistribution binDist=null;
    public static int binomialDraw(double p_success,int num_trials)
        {
        binDist=new BinomialDistribution(myRandomDataGenerator.getRandomGenerator(),num_trials,p_success);
        return binDist.sample();            
        }
    
    
    public static ArrayList<String> readLines(String filePath,boolean trim) throws Exception
        {
        ArrayList<String> linesRead=new ArrayList<String>();
        BufferedReader br=new BufferedReader(new FileReader(filePath));
        while(br.ready())
            {
            String tempLine=br.readLine();
            if(trim)
                {
                tempLine=tempLine.trim();
                }
            linesRead.add(tempLine);
            }
        return linesRead;
        }
    
    
    public static void writeStringToFile(String s,String filePath) throws Exception
        {
        FileWriter fw=new FileWriter(filePath);
        fw.write(s,0,s.length());
        fw.flush();
        fw.close();
        }

    public static ArrayList<String> extractMatchingPattern(ArrayList<String> l,Pattern p)
        {
        ArrayList<String> matches=new ArrayList<String>();
        for(String s:l)
            {
            if(s!=null)
                {
                Matcher m=p.matcher(s);
                if(m.find())
                    {
                    matches.add(s);
                    }
                }
            }
        return matches;
        }
    
    
    public static ArrayList<String> extractMatchingStart(ArrayList<String> l,String st)
        {
        ArrayList<String> matches=new ArrayList<String>();
        for(String s:l)
            {
            if(s!=null)
                {
                if(s.startsWith(st))
                    {
                    matches.add(s);
                    }
                }
            }
        return matches;
        }
    
    
    
    public static int getIndexOfStringInArrayList(ArrayList<String> als,String target,boolean ignoreCase)
        {
        if(als==null)
            {
            return -1;
            }
        else
            {
            boolean eq=false;
            for(int i=0;i<als.size();i++)
                {
                String alsStr=als.get(i);
                if(target.equalsIgnoreCase(alsStr))
                    {
                    if(!ignoreCase)
                        {
                        if(target.equals(alsStr))
                            {
                            return i;
                            }
                        }
                    else
                        {
                        return i;
                        }
                    }
                }            
            }
        return (-1);
        }
    
    public static ArrayList<String> swapVals(int a,int b,ArrayList<String> als)
        {
        if(a<0 || b<0 || a==b)
            {
            return als;
            }
        if(a>=als.size() || b>=als.size())
            {
            return als;
            }
        else
            {
            String c=new String(als.get(a));
            als.set(a,als.get(b));
            als.set(b, c);
            return als;
            }
            
        }
    
    
    public static String getPhymlPathFromPathEnv()
        {
        try
            {
            String p=System.getenv("PATH");    
            if(p==null)
                {
                return null;
                }
            else
                {
                String[] paths=p.split(":");
                for(int x=0;x<paths.length;x++)
                    {
                    String possiblePhymlPath=paths[x]+"/phyml";
                    File possiblePhymlExecFile=new File(possiblePhymlPath);
                    if(possiblePhymlExecFile.exists() && 
                            possiblePhymlExecFile.canRead() && 
                            possiblePhymlExecFile.canExecute())
                        {
                        return possiblePhymlExecFile.getPath();
                        }
                    }
                }
            //didn't find it!
            return null;
            }
        catch(Exception e)
            {
            return null;
            }      
        
        }
    
    public static double randomDouble(double minD,double maxD,double curD)
        {

        double dSize=(maxD-minD)/10.0;
        double lowerBound=Math.max(curD-dSize,minD);
        double upperBound=Math.min(curD+dSize,maxD);
        return getRandNumBet(lowerBound,upperBound,true);
            
            
        }
    
    
    
    
    
    public static int randomInt(int minI,int maxI,int curI)
        {
        if(maxI==minI)
            {
            return maxI;
            }
        else
            {
            ArrayList<Integer> randomIntsToChoose=new ArrayList<Integer>();
            for(int v=Math.max(curI-1,minI);v<=Math.min(curI+1,maxI);v++)
                {
                //the initialization is so you don't go below bounds
                //the finish test is so you don't go above bounds
                randomIntsToChoose.add(v);
                }
            return TreeUtils.getRandomInt(randomIntsToChoose.get(0),randomIntsToChoose.get(randomIntsToChoose.size()-1));
            }
        }
    
    
    public static boolean sumToOne(double[] pcts)
        {
        if(pcts==null)
            {
            return false;
            }
        else
            {
            double sum=0.0;
            for(int i=0;i<pcts.length;i++)
                {
                sum+=pcts[i];
                }
            if(tolerEq(sum,1.0))
                {
                return true;
                }
            else
                {
                return false;
                }
            }            
        }
    
    
    
    private static Pattern intPattern=Pattern.compile("^\\d+$");
    public static boolean isIntegerString(String i)
        {
        if(i==null)
            {
            return false;
            }
        Matcher m=intPattern.matcher(i);
        return m.find();
        }
    
    
    public static double findBoundaryWhereExpDistMeanOnePDFGoesToZeroAndCDFGoesToOne(boolean atLeastOne,boolean toler)
        {
        double xMax=1000;
        double startX=0;
        double deltaX=0.01;
        for(double x=startX;x<=xMax;x+=deltaX)
            {
            double xCDF=TreeUtils.exponentialDistCDFEval(x,1);
            double xPDF=TreeUtils.exponentialPDF(1,x);
            //System.out.println("x="+x+", f(x)="+xPDF+", and F(x)="+xCDF);
            if(toler)
                {
                if(atLeastOne)
                    {
                    if(tolerEq(xCDF,1) || tolerEq(xPDF,0))
                        {
                        return x;
                        }
                    }
                else
                    {
                    if(tolerEq(xCDF,1) && tolerEq(xPDF,0))
                        {
                        return x;
                        }
                    }
                }//toler
            else
                {
                if(atLeastOne)
                    {
                    if(xCDF==1 || xPDF==0)
                        {
                        return x;
                        }
                    }
                else
                    {
                    if(xCDF==1 && xPDF==0)
                        {
                        return x;
                        }
                    }
                }
            }//for loop
        String eMsg="Error, did not find distnace where precision is insufficient from exploring range ";
            eMsg+=" (in double precision) from start=";
            eMsg+=""+startX+" to stop="+xMax;
        throw new RuntimeException(eMsg);
        
                                
        }

    
    
    public static double[] strFreqsToDoubleArray(String freqs)
        {
        try
            {
            String[] pieces=freqs.split(",");
            if(pieces.length!=4)
                {
                throw new RuntimeException("Error, input should be CSV string of four frequencies!");
                }
            double[] dPieces=new double[4];
            for(int p=0;p<4;p++)
                {
                dPieces[p]=Double.parseDouble(pieces[p]);
                }
            return dPieces;
            }
        catch(Exception e)
            {
            throw new RuntimeException(e);
            }
        }
    
    
    public static String transProbMatrixToString(double[][] tmp)
        {
        if(tmp.length!=4)
            {
            throw new RuntimeException("Error, got transition prob matrix not of dim 4x4 !");
            }
        String s="[\n";
        for(int f=0;f<tmp.length;f++)
            {
            if(tmp[f].length!=4)
                {
                throw new RuntimeException("Error, got transition prob matrix not of dim 4x4 !");
                }
            else
                {
                s+=Arrays.toString(tmp[f]);
                if(f<3)
                    {
                    s+=" ,\n";
                    }
                else
                    {
                    s+="]";
                    }
                }
            }
        return s;
        }
    
    public static boolean freqsEq(double[] a,double[] b,boolean toler)
        {
        if(a==null && b==null)
            {
            //if both null, then equl
            return true;
            }
        else if(a==null || b==null)
            {
            //if either null, but not both, then NOT equal
            return false;
            }
        if(a.length==4 && 4==b.length)
            {
            //if same length, then consider values next
            for(int i=0;i<4;i++)
                {
                if(!toler)
                    {
                    if(a[i]!=b[i])
                        {
                        return false;
                        }
                    }
                else
                    {
                    if(tolerEq(a[i],b[i]))
                        {
                        //so far equal
                        }
                    else
                        {
                        return false;
                        }
                    }
                }
            //never hit inequality
            return true;
            }
        else
            {
            //if not same length then not equal!
            return false;
            }
        }
    
    public static boolean areUni(double[] f)
        {
        if(f.length==4)
            {
            if(f[0]==0.25 && f[1]==0.25 && f[2]==0.25 && f[3]==0.25)
                {
                return true;
                }
            else
                {
                return false;
                }
            }
        else
            {
            return false;
            }
        }

  
    
    public static boolean areValsEq(ArrayList<Double> al,boolean toler)
        {
        if(al==null)
            {
            return true;
            }
        if(al.size()<=1)
            {
            return true;
            }
        
        if(!toler)
            {
            for(int a=0;a<al.size()-1;a++)
                {
                if(!(al.get(a)==al.get(a+1)))
                    {
                    return false;
                    }
                }
            return true;
            }
        else
            {
            for(int a=0;a<al.size()-1;a++)
                {
                if(!(TreeUtils.tolerEq(al.get(a),al.get(a+1))))
                    {
                    return false;
                    }
                }
            return true;
            }
            
        }
    

    
    
    public static boolean arePiFreqsAllPositive(String[] pf)
        {
        try
            {
            if(pf.length!=4)
                {
                throw new IllegalArgumentException("Error, pf length not 4 !");
                }
            for(int i=0;i<4;i++)
                {
                BigDecimal t=new BigDecimal(pf[i]);
                if(t.compareTo(BigDecimal.ZERO)!=1)
                    {
                    //equal to or less than zero!
                    return false;
                    }
                }
            return true;
            }
        catch(Exception e)
            {
            return false;
            }
        
        }
    
    

    
    
    public static String join(String[] ar,String glue)
        {
        if(ar==null)
            {
            return null;
            }
        else if(ar.length==0)
            {
            return "";
            }
        else if(ar.length==1)
            {
            return ar[0];
            }
        else
            {
            String tbr="";
            for(int i=0;i<ar.length;i++)
                {
                tbr+=ar[i];
                if(i<ar.length-1)
                    {
                    tbr+=glue;
                    }
                }
            return tbr;
            }
        
        }
            
   
    //some code from http://freelance-webs.com/junks/websfun/BigFunctions.java
    public static BigDecimal intRoot(BigDecimal x, long index,
                                     int scale)
    {
        // Check that x >= 0.
        if (x.signum() < 0) {
            throw new IllegalArgumentException("x < 0");
        }

        int        sp1 = scale + 1;
        BigDecimal n   = x;
        BigDecimal i   = BigDecimal.valueOf(index);
        BigDecimal im1 = BigDecimal.valueOf(index-1);
        BigDecimal tolerance = BigDecimal.valueOf(5)
                                            .movePointLeft(sp1);
        BigDecimal xPrev;

        // The initial approximation is x/index.
        x = x.divide(i, scale, BigDecimal.ROUND_HALF_EVEN);

        // Loop until the approximations converge
        // (two successive approximations are equal after rounding).
        do {
            // x^(index-1)
            BigDecimal xToIm1 = intPower(x, index-1, sp1);

            // x^index
            BigDecimal xToI =
                    x.multiply(xToIm1)
                        .setScale(sp1, BigDecimal.ROUND_HALF_EVEN);

            // n + (index-1)*(x^index)
            BigDecimal numerator =
                    n.add(im1.multiply(xToI))
                        .setScale(sp1, BigDecimal.ROUND_HALF_EVEN);

            // (index*(x^(index-1))
            BigDecimal denominator =
                    i.multiply(xToIm1)
                        .setScale(sp1, BigDecimal.ROUND_HALF_EVEN);

            // x = (n + (index-1)*(x^index)) / (index*(x^(index-1)))
            xPrev = x;
            x = numerator
                    .divide(denominator, sp1, BigDecimal.ROUND_DOWN);

            Thread.yield();
        } while (x.subtract(xPrev).abs().compareTo(tolerance) > 0);

        return x;
    }

    
    
    
    
    public static BigDecimal intPower(BigDecimal x, long exponent,
                                      int scale)
    {
        // If the exponent is negative, compute 1/(x^-exponent).
        if (exponent < 0) {
            return BigDecimal.valueOf(1)
                        .divide(intPower(x, -exponent, scale), scale,
                                BigDecimal.ROUND_HALF_EVEN);
        }

        BigDecimal power = BigDecimal.valueOf(1);

        // Loop to compute value^exponent.
        while (exponent > 0) {

            // Is the rightmost bit a 1?
            if ((exponent & 1) == 1) {
                power = power.multiply(x)
                          .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
            }

            // Square x and shift exponent 1 bit to the right.
            x = x.multiply(x)
                    .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
            exponent >>= 1;

            Thread.yield();
        }

        return power;
    }

    
        private static BigDecimal expTaylor(BigDecimal x, int scale)
    {
        BigDecimal factorial = BigDecimal.valueOf(1);
        BigDecimal xPower    = x;
        BigDecimal sumPrev;

        // 1 + x
        BigDecimal sum  = x.add(BigDecimal.valueOf(1));

        // Loop until the sums converge
        // (two successive sums are equal after rounding).
        int i = 2;
        do {
            // x^i
            xPower = xPower.multiply(x)
                        .setScale(scale, BigDecimal.ROUND_HALF_EVEN);

            // i!
            factorial = factorial.multiply(BigDecimal.valueOf(i));

            // x^i/i!
            BigDecimal term = xPower
                                .divide(factorial, scale,
                                        BigDecimal.ROUND_HALF_EVEN);

            // sum = sum + x^i/i!
            sumPrev = sum;
            sum = sum.add(term);

            ++i;
            Thread.yield();
        } while (sum.compareTo(sumPrev) != 0);

        return sum;
    }

    
    
        public static BigDecimal exp(BigDecimal x, int scale)
    {
        // e^0 = 1
        if (x.signum() == 0) {
            return BigDecimal.valueOf(1);
        }

        // If x is negative, return 1/(e^-x).
        else if (x.signum() == -1) {
            return BigDecimal.valueOf(1)
                        .divide(exp(x.negate(), scale), scale,
                                BigDecimal.ROUND_HALF_EVEN);
        }

        // Compute the whole part of x.
        BigDecimal xWhole = x.setScale(0, BigDecimal.ROUND_DOWN);

        // If there isn't a whole part, compute and return e^x.
        if (xWhole.signum() == 0) return expTaylor(x, scale);

        // Compute the fraction part of x.
        BigDecimal xFraction = x.subtract(xWhole);

        // z = 1 + fraction/whole
        BigDecimal z = BigDecimal.valueOf(1)
                            .add(xFraction.divide(
                                    xWhole, scale,
                                    BigDecimal.ROUND_HALF_EVEN));

        // t = e^z
        BigDecimal t = expTaylor(z, scale);

        BigDecimal maxLong = BigDecimal.valueOf(Long.MAX_VALUE);
        BigDecimal result  = BigDecimal.valueOf(1);

        // Compute and return t^whole using intPower().
        // If whole > Long.MAX_VALUE, then first compute products
        // of e^Long.MAX_VALUE.
        while (xWhole.compareTo(maxLong) >= 0) {
            result = result.multiply(
                                intPower(t, Long.MAX_VALUE, scale))
                        .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
            xWhole = xWhole.subtract(maxLong);

            Thread.yield();
        }
        return result.multiply(intPower(t, xWhole.longValue(), scale))
                        .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
    }

    
    
        private static BigDecimal lnNewton(BigDecimal x, int scale)
    {
        int        sp1 = scale + 1;
        BigDecimal n   = x;
        BigDecimal term;

        // Convergence tolerance = 5*(10^-(scale+1))
        BigDecimal tolerance = BigDecimal.valueOf(5)
                                            .movePointLeft(sp1);

        // Loop until the approximations converge
        // (two successive approximations are within the tolerance).
        do {

            // e^x
            BigDecimal eToX = exp(x, sp1);

            // (e^x - n)/e^x
            term = eToX.subtract(n)
                        .divide(eToX, sp1, BigDecimal.ROUND_DOWN);

            // x - (e^x - n)/e^x
            x = x.subtract(term);

            Thread.yield();
        } while (term.compareTo(tolerance) > 0);

        return x.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
    }

    
    
    public static BigDecimal ln(BigDecimal x, int scale)
    {
        // Check that x > 0.
        if (x.signum() <= 0) {
            throw new IllegalArgumentException("x <= 0");
        }

        // The number of digits to the left of the decimal point.
        int magnitude = x.toString().length() - x.scale() - 1;

        if (magnitude < 3) {
            return lnNewton(x, scale);
        }

        // Compute magnitude*ln(x^(1/magnitude)).
        else {

            // x^(1/magnitude)
            BigDecimal root = intRoot(x, magnitude, scale);

            // ln(x^(1/magnitude))
            BigDecimal lnRoot = lnNewton(root, scale);

            // magnitude*ln(x^(1/magnitude))
            return BigDecimal.valueOf(magnitude).multiply(lnRoot)
                        .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
        }
    }
    
    
    
    public static String[] abcNames=null;
    
    public static void initABCNames()
        {
        ArrayList<String> tr=new ArrayList<String>();
        for(int i=0;i<26;i++)
            {
            String iChar = new Character((char) (i+65)).toString();
            tr.add(iChar);
            }
        for(int i=0;i<26;i++)
            {
            for(int j=0;j<26;j++)
                {
                //starts at A
                String iChar = new Character((char) (i+65)).toString();
                String jChar=new Character((char) (j+65)).toString();
                String nl=iChar+jChar;
                tr.add(nl);
                }
            }
        abcNames=new String[tr.size()];
        for(int i=0;i<tr.size();i++)
            {
            abcNames[i]=tr.get(i);
            }
        }
    
    
    public static String acquireABCName(int a)
        {
        if(abcNames==null)
            {
            initABCNames();
            }
        if(0<=a && a<abcNames.length)
            {
            return abcNames[a];
            }
        else
            {
            if(a<0)
                {
                throw new RuntimeException("Error, cannot get ABC name for a="+a);
                }
            throw new RuntimeException("Error cannot have "+(a+1)+" or more sequences!");
            }
        }
    
    
    public static String toString(ArrayList<Integer> al)
        {
        if(al==null)
            {
            return "null";
            }
        Object[] cont=new Object[al.size()];
        for(int i=0;i<al.size();i++)
            {
            cont[i]=al.get(i);
            }
        String ret=Arrays.toString(cont);
        return ret;
        }
    
        
    public static  String[] toStringArr(ArrayList<Integer> al)
        {
        if(al==null)
            {
            String[] u=new String[0];
            return u;
            }
        String[] os=new String[al.size()];
        for(int i=0;i<al.size();i++)
            {
            os[i]=new String(al.get(i).toString());
            }
        return os;
        }
    
    public static String getStringButFilterOutIndices(String s,ArrayList<Integer> f)
        {
        String newString=new String("");
        char[] pieces=s.toCharArray();
        for(int c=0;c<pieces.length;c++)
            {
            Integer site=new Integer(c);
            if(f.contains(site))
                {
                //don't accumulate!
                }
            else
                {
                newString=newString+pieces[c];
                }
            }
        return newString;
        }
    

    public static String getMilliSecondsPastEpoch()
        {
        Date now=new Date();
        long time=now.getTime();
        String strTime=""+time+"";
        return strTime;
        }
    
    
    public static String getNiceDataFormat()
        {
        DateFormat df=DateFormat.getDateTimeInstance(DateFormat.FULL,DateFormat.FULL);
        Date now=new Date();
        String niceDate=df.format(now);
        return niceDate;
        }
    
    public static String[] runCmdCaptureSTDOUTSTDERR(String cmd) throws Exception
        {
        Runtime r=Runtime.getRuntime();
        Process p=r.exec(cmd);
        InputStream inStream=p.getInputStream();
        BufferedReader outBR=new BufferedReader(new InputStreamReader(inStream));
        InputStream errStream=p.getErrorStream();
        BufferedReader errBR=new BufferedReader(new InputStreamReader(errStream));
        p.waitFor();
        String out="";
        String err="";
        while(outBR.ready())
            {
            out+=outBR.readLine();
            out+="\n";
            }
        while(errBR.ready())
            {
            err+=errBR.readLine();
            err+="\n";
            }
        outBR.close();
        errBR.close();
        String[] out_and_err=new String[2];
        out_and_err[0]=out;
        out_and_err[1]=err;
        return out_and_err;
        }
    
    
    public static HashMap addCountToMap(HashMap h,Object k)
        {
        if(h==null)
            {
            h=new HashMap();
            h.put(k,1);
            return h;
            }
        else if(h.containsKey(k))
            {
            int e=(int)(h.get(k));
            e++;
            h.put(k,e);
            return h;
            }
        else
            {
            h.put(k,1);
            return h;
            }
        }
    
    public static double[] getDoubleArrFromDoubleArrayList(ArrayList<Double> ald)
        {
        if(ald==null)
            {
            return null;
            }
        else
            {
            double[] tr=new double[ald.size()];
            for(int a=0;a<ald.size();a++)
                {
                tr[a]=ald.get(a);
                }
            return tr;
            }
        }
    
    
    public static Object getKeyWithHighestCount(HashMap countMap)
        {
        if(countMap==null)
            {
            return null;
            }
        else
            {
            int numKeys=countMap.size();
            if(numKeys==0)
                {
                return null;
                }
            int maxCount=Integer.MIN_VALUE;
            Object maxKey=null;
            for(Object key:countMap.keySet())
                {
                if(maxKey==null)
                    {
                    maxKey=key;
                    maxCount=(int)countMap.get(key);
                    }
                else
                    {
                    int existingCount=(int)(countMap.get(key));
                    if(existingCount>maxCount)
                        {
                        maxCount=existingCount;
                        maxKey=key;
                        }
                    }
                }
            return maxKey;
            }
        }
    
    
    
    public static HashMap cloneMap(HashMap a)
        {
        if(a==null)
            {
            return null;
            }
        else
            {
            HashMap b=new HashMap();
            for(Object k:a.keySet())
                {
                Object newKey=cloneObj(k);
                Object newVal=cloneObj(a.get(k));
                b.put(newKey,newVal);
                //Object v=a.get(k);
                //b.put(k, v);
                }
            return b;
            }
        }
    
    
    
    
    public static Object cloneObj(Object o)
        {
        if(o==null)
            {
            return null;
            }
        else if(o instanceof Double)
            {
            Double newDouble=new Double(o.toString());
            return newDouble;
            }
        else if(o instanceof Long)
            {
            return new Long(o.toString());
            }
        else if(o instanceof String)
            {
            return new String(o.toString());
            }
        else if(o instanceof Integer)
            {
            Integer newIntObj=new Integer(o.toString());
            return newIntObj;
            }
        else if(o instanceof BigDecimal)
            {
            BigDecimal bdo=(BigDecimal)(o);
            BigDecimal cbd=new BigDecimal(bdo.toString());
            return cbd;
            }
        else if(o instanceof ArrayList)
            {
            ArrayList alo=(ArrayList)(o);           
            ArrayList alc=new ArrayList();
            for(int i=0;i<alo.size();i++)
                {
                alc.add(cloneObj(alo.get(i)));
                }
            return alc;                
            }
        else if(o instanceof double[])
            {
            double[] od_cast=(double[])(o);
            double[] nd=new double[od_cast.length];
            System.arraycopy(od_cast,0,nd,0,od_cast.length);
            return nd;
            }        
        else if(o instanceof Object[])
            {
            Object[] o_cast=(Object[])(o);
            Object[] noa=new Object[o_cast.length];
            for(int i=0;i<noa.length;i++)
                {
                noa[i]=cloneObj(o_cast[i]);
                }
            return noa;
            }
        else
            {
            String classDesc=o.toString();
            String className=o.getClass().getName();
            throw new RuntimeException("Error bad clone!\ntoString='"+classDesc+"'\nclassName="+className);
            }            
        }
    
    
    
    public static RandomDataGenerator myRandomDataGenerator=null;
    
    
    public static void initRandomSeed(long seedVal)
        {
        Well19937c myWC=new Well19937c(seedVal);
        myRandomDataGenerator=new  RandomDataGenerator(myWC);
        //System.out.println("Init seed with "+seedVal);
        //myRandomDataGenerator=null;
        }
    
    
    public static String mapToString(HashMap h)
        {
        String s="";
        s+="key\t->\tval\n";
        if(h!=null)
            {
            for(Object k:h.keySet())
                {
                s+=k.toString();
                s+="\t->\t";
                s+=h.get(k);
                s+="\n";
                }
            }

        return s;
        }

    public static Object[] sampleWOReplacement(Collection c,int numToSample)
        {
            /*
            nextSample
            public Object[] nextSample(Collection<?> c,
                              int k)
                                throws NumberIsTooLargeException,
                                       NotStrictlyPositiveException
            */
            
            return myRandomDataGenerator.nextSample(c, numToSample);
            
        }
    
    
    public static double getRandNormal(double mean,double stddev)
        {
        double r=myRandomDataGenerator.nextGaussian(mean, stddev);
        return r;
        }
    
    
    public static double getRandomBetaDistSample(double alpha,double beta)
        {
        double b=myRandomDataGenerator.nextBeta(alpha, beta);
        return b;
        }
    
    
    public static double linearMap(double from_min,double from_max,double to_min,double to_max,double x)
        {
        //get slope(m)
        double rise=to_max-to_min;
        double run=from_max-from_min;
        double slope=rise/run;
        
        //get intercept(b)
        /*
        y=mx+b
        b=y-mx
        */
        double b=to_max-slope*from_max;
        
        //compute the mapping
        double y=slope*x+b;
        
        return y;
            
        }
    
    
    
    public static double getRandNumBet(double a,double b,boolean inc)
        {
        //System.out.println("a="+a+" b="+b+" inc="+inc);
        if(a==b && inc)
            {
            return a;
            }
        double r=myRandomDataGenerator.nextUniform(a,b,inc);
        //System.out.println("To return random number "+r);
        return r;
        }
    
    
    
    
    
    public static boolean isBetween(double a,double b,double c,boolean inclusive)
        {
        /*
            return true if a<=b<=c or c<=b<=c
            otherwise, return false
            */
        if(inclusive)
            {
            if(a<=b && b<=c)
                {
                return true;
                }
            else if(c<=b && b<=a)
                {
                return true;
                }
            else
                {
                return false;
                }
            }
        else
            {
            if(a<b && b<c)
                {
                return true;
                }
            else if(c<b && b<a)
                {
                return true;
                }
            else
                {
                return false;
                }
            }

        }
    
    
    public static int getRandomInt(int lower,int upper)
        {
        //Generates a uniformly distributed random integer 
        //between lower and upper (ENDPOINTS INCLUDED).
        if(upper<lower)
            {
            return getRandomInt(upper,lower);
            }
        if(lower==upper)
            {
            return lower;
            }
        if(lower==upper-1)
            {
            double a_rand=myRandomDataGenerator.nextUniform(0,1);
            if(a_rand<=0.5)
                {
                return lower;
                }
            else
                {
                return upper;
                }
            }
        //Generates a uniformly distributed random integer 
        //between lower and upper (ENDPOINTS INCLUDED).
        int x=(int)(myRandomDataGenerator.nextInt(lower, upper));
        return x;
        }
    
    
    public static long readLong(String prompt)
        {
        long x=0;
        try
            {
            String rx=readLine(prompt);
            x=Long.parseLong(rx);
            }
        catch(Exception e)
            {
            return readInt(prompt);
            }
        
        return x;
        }
    
    
    public static int readInt(String prompt)
        {
        int x=0;
        try
            {
            String rx=readLine(prompt);
            x=Integer.parseInt(rx);
            }
        catch(Exception e)
            {
            return readInt(prompt);
            }
        
        return x;
        }
    
    public static String readLine(String prompt)
        {

        if(prompt!=null)
            {
            System.out.print(prompt+" : ");
            }
        try
            {
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            String read=br.readLine();
            return read;
            }
        catch(Exception e)
            {
            e.printStackTrace();
            return "";
            }
        }
    
    public static int[] multinomialVector(int m_sum,double[] p)
        {
        int[] bins=new int[p.length];
        for(int i=0;i<m_sum;i++)
            {
            int binNum=drawMultinomial(p);
            bins[binNum]++;
            }
        return bins;
        }
    
    public static HashMap mergeMaps(HashMap A,HashMap B)
        {
        if(A==null && B==null)
            {
            return null;
            }
        else if(A!=null && B==null)
            {
            return A;
            }
        else if(A==null && B!=null)
            {
            return B;
            }
        else
            {
            HashMap merged=new HashMap();
            for(Object k:A.keySet())
                {
                merged.put(k,A.get(k));
                }
            for(Object k:B.keySet())
                {
                merged.put(k,B.get(k));
                }
            return merged;
            }            
        }
    
    
    public static double[] acquireCDF(double[] p,boolean check)
        {
        if(check)
            {
            if(!TreeUtils.sumToOne(p))
                {
                //bad!
                throw new IllegalArgumentException("Error, pcts : "+Arrays.toString(p)+" don't sum to 1.0!");
                }
            }
        double[] cdf=new double[p.length];
        double sum=0;
        for(int i=0;i<p.length;i++)
            {
            if(i==0)
                {
                cdf[0]=p[0];
                }
            else
                {
                cdf[i]=p[i]+cdf[i-1];
                }
            }
        return cdf;
            
        }
    
    
    public static String arrayListStringToString(ArrayList<String> alo)
        {
        if(alo==null)
            {
            return "null";
            }
        else
            {
            Object[] oa=new Object[alo.size()];
            for(int i=0;i<alo.size();i++)
                {
                oa[i]=alo.get(i);
                }
            return Arrays.toString(oa);
            }
        }
    
    
    public static double acquireSum(double[] p)
        {
        double sum=0;
        for(int i=0;i<p.length;i++)
            {
            sum+=p[i];
            }
        return sum;
        }
    
    public static void testDrawMultinomial()
        {
        for(int lengths=1;lengths<=4;lengths++)
            {
            ArrayList<Double> probs=new ArrayList<Double>();
            double cumSumLeft=1;
            double cumSum=0;
            for(int p=0;p<lengths;p++)
                {
                System.out.println("Round "+p+", cumSum is "+cumSum);
                System.out.println("Round "+p+", cumSumLeft is "+cumSumLeft);
                if(p<lengths-1)
                    {
                    System.out.println("Require random");
                    double newProb=TreeUtils.getRandNumBet(0,cumSumLeft,false);
                    probs.add(newProb);
                    cumSum+=newProb;
                    cumSumLeft-=newProb;
                    }
                else
                    {
                    System.out.println("Just compute from cumSum");
                    double newProb=1-cumSum;
                    probs.add(newProb);
                    cumSum+=newProb;
                    cumSumLeft-=newProb;
                    }
                }
            System.out.println("After rounds, ");
            System.out.println("cumSum="+cumSum);
            System.out.println("cumSumLeft="+cumSumLeft);
            Collections.shuffle(probs);
            double computedSum=0;
            double[] probsDA=new double[probs.size()];
            for(int p=0;p<probs.size();p++)
                {
                computedSum+=probs.get(p);
                probsDA[p]=probs.get(p);
                }
            System.out.println("The probabilities : "+probs.toString());
            System.out.println("The computed sum  : "+computedSum);
            int numSamples=10000;
            int[] selections=new int[probs.size()];
            for(int s=0;s<numSamples;s++)
                {
                int selection=drawMultinomial(probsDA);
                selections[selection]++;
                }
            System.out.println("For length="+lengths);
            System.out.println("Probability\tNum_samples");
            for(int d=0;d<selections.length;d++)
                {
                System.out.println(probsDA[d]+"\t"+selections[d]);
                }
            System.out.println("\n\n");
            }
        }
    
    public static int drawMultinomial(ArrayList<Double> al)
        {
        double[] probs=null;
        if(al==null)
            {
            return drawMultinomial(probs);
            }
        else
            {
            probs=new double[al.size()];
            for(int p=0;p<al.size();p++)
                {
                probs[p]=al.get(p);
                }
            return drawMultinomial(probs);
            }
       
        }
    
    
    public static double[] makeMultinomialVector(int[] counts)
        {
        int sum=0;
        for(int c=0;c<counts.length;c++)
            {
            sum+=counts[c];
            }
        double[] probs=new double[counts.length];
        for(int p=0;p<probs.length;p++)
            {
            probs[p]=(double)(counts[p])/sum;
            }
        return probs;
        }
    
    
    public static int drawMultinomial(double[] p)
        {
        if(p.length==1)
            {
            return 0;
            }
        double r=myRandomDataGenerator.nextUniform(0,1);
        double[] cdf=TreeUtils.acquireCDF(p,false);
        //System.out.println("For dist p="+Arrays.toString(p)+" the cdf is "+Arrays.toString(cdf)+", the r is "+r);
        int index=0;
        while(index<p.length)
            {
            //System.out.println("In while, index="+index);
            if(cdf[index]>=r)
                {
                //return the first index such that cdf[index]>=r
                //System.out.println("to return ri "+index);
                return index;
                }
            index++;
            }
        //System.out.println("to return (at end) ri "+(p.length-1));
        return p.length-1;        
        }
    
    public static double onePercentChangeTruncExpMeanOne(double current,double frac)
        {
        double lower=current-current*frac;
        double upper=current+current*frac;
        return drawTruncatedExponentialMeanOne(lower,upper);
        }
    
    
    public static double drawTruncatedExponentialMeanOneWithHalfWindowLenOneLowerSideOfMedian(double halfWindowLen,double current,double lbFloor)
        {
        double lowerBound=Math.max(current-halfWindowLen,lbFloor);
        double currCDF=TreeUtils.exponentialDistCDFRateAndMeanOneEval(current);
        double lowerCDF=TreeUtils.exponentialDistCDFRateAndMeanOneEval(lowerBound);
        double massBelow=currCDF-lowerCDF;
        double delta=(-current)-Math.log(Math.exp(-current)-massBelow);
        double upperBound=current+delta;
        /*System.out.println("In trun exp u/l bound definition.... lower="+lowerBound+" curr="+current+", upper="+upperBound);
        double calcMassAbove=TreeUtils.exponentialDistCDFRateAndMeanOneEval(upperBound)-currCDF;
        System.out.println("Lower CDF: "+lowerCDF+", currCDF: "+currCDF+", massBelow: "+massBelow+", and massAb: "+calcMassAbove);
        //System.exit(0);*/
        double valToReturn;
        if(lowerBound==current || upperBound==current || upperBound==lowerBound)
            {
            //System.out.println("Get value via pure generation");
            valToReturn=TreeUtils.drawExponentialWithMean(1.0);
            }
        else
            {
            //System.out.println("Get value via window");
            valToReturn=drawTruncatedExponentialMeanOne(lowerBound,upperBound);        
            }
        //System.out.println("Returning the value : "+valToReturn);
        //System.out.println("\n\n\n\n\n");
        return valToReturn;
        }
    
    
    
    public static double drawTruncatedExponentialMeanOneWithHalfWindowLen(double halfWindowLen,double current)
        {
        double lowerBound=Math.max(current-halfWindowLen,0);
        double upperBound=current+halfWindowLen;
        return drawTruncatedExponentialMeanOne(lowerBound,upperBound);
        }
    
    
    /*public static double drawTruncatedExponentialWithRate(double rate,double lowerBound,double upperBound)
        {

        
        }*/

    public static double exponentialPDF(double rate,double x)
        {
        //lambda * e^(-lambda*x)
        return rate*Math.exp((-1)*rate*x);
        }
    
    
    public static double exponentialDistCDFRateAndMeanOneEval(double x)
        {
        //CDF 1-e^(-lambda*x)
        return 1-Math.exp((-1)*x);
        }
    
    
    public static double exponentialDistCDFEval(double x,double lambda)
        {
        //CDF 1-e^(-lambda*x)
        //the mean is 1/lambda=lambda^(-1)
        return 1-Math.exp((-1)*lambda*x);
        }
    

    
    public static int min(int[] n)
        {
        if(n==null)
            {
            throw new NullPointerException("Error, expect array to be non-null!");
            }
        else if(n.length==0)
            {
            throw new IndexOutOfBoundsException("Error, expect array to be at least one item!");
            }
        else
            {
            int m=Integer.MAX_VALUE;
            for(int num:n)
                {
                if(num<m)
                    {
                    m=num;
                    }
                }
            return m;
            }
        }

    public static int max(int[] n)
        {
        if(n==null)
            {
            throw new NullPointerException("Error, expect array to be non-null!");
            }
        else if(n.length==0)
            {
            throw new IndexOutOfBoundsException("Error, expect array to be at least one item!");
            }
        else
            {
            int m=Integer.MIN_VALUE;
            for(int num:n)
                {
                if(num>m)
                    {
                    m=num;
                    }
                }
            return m;
            }
        }    
    
    
    
    public static boolean isValidExpSample(double s)
        {
        if(
                s==Double.NaN ||
                s==Double.NEGATIVE_INFINITY ||
                s==Double.POSITIVE_INFINITY 
                )
            {
            return false;
            }
        else if(s<=0)
            {
            return false;
            }
        else
            {
            return true;
            }
        }
    
    
    
    public static double drawTruncatedExponentialWithRate(double rate,double lowerBound,double upperBound)
        {
        if(!(0<=lowerBound && lowerBound<=upperBound))
            {
            return drawExponentialWithMean(1/rate);
            }
        else
            {
            /*
            We have a fraction
            The numerator is an integral from a to u of the un-truncated distribution (where u is a uniformly distributed value in [0,1])
            The denominator is F(b)-F(a) where F is the CDF of the un-truncated distribution
                and b and a are the upper and lower bounds.
            See https://en.wikipedia.org/wiki/Truncated_distribution
            */
            /*
 R CODE tested seems to work
 19         numerator=log(1-
 20                 aUni*(expCDFEval(upper,lambda)-expCDFEval(lower,lambda))-expCDFEval(lower,lambda))
 21         denominator=(-1)*(lambda)
 22         return(numerator/denominator)
    
            */
            double u=TreeUtils.getRandNumBet(0,1,false);
            double upperCDFEval=exponentialDistCDFEval(upperBound,rate);
            double lowerCDFEval=exponentialDistCDFEval(lowerBound,rate);
            double passedToLog=1.0-u*(upperCDFEval-lowerCDFEval)-lowerCDFEval;
            double num=Math.log(passedToLog);
            double denom=(-1)*rate;
            /*System.err.println("lambda rate is "+rate);
            System.err.println("lower, upper bounds are "+lowerBound+" and "+upperBound);
            System.err.println("u="+u+", up="+upperCDFEval+", low="+lowerCDFEval+", passed="+passedToLog+", num="+num+", dnm="+denom);*/
            double x=num/denom;
            //System.out.println("to return x=num/denom="+x);
            if(Double.isInfinite(x))
                {
                //in infinit case, fall back on uniform dist
                x=TreeUtils.getRandNumBet(lowerBound,upperBound,false);
                //System.err.println("Returning "+x+" with lb="+lowerBound+" and ub="+upperBound);
                }

            return x;
            }        
        }
    
    
    public static double drawTruncatedExponentialWithMean(double mean,double lowerBound,double upperBound)
        {
        if(!(0<=lowerBound && lowerBound<=upperBound))
            {
            return drawExponentialWithMean(mean);
            }
        else
            {
            /*
            We have a fraction
            The numerator is an integral from a to u of the un-truncated distribution (where u is a uniformly distributed value in [0,1])
            The denominator is F(b)-F(a) where F is the CDF of the un-truncated distribution
                and b and a are the upper and lower bounds.
            See https://en.wikipedia.org/wiki/Truncated_distribution
            */
            /*
 R CODE tested seems to work
 19         numerator=log(1-
 20                 aUni*(expCDFEval(upper,lambda)-expCDFEval(lower,lambda))-expCDFEval(lower,lambda))
 21         denominator=(-1)*(lambda)
 22         return(numerator/denominator)
    
            */
            //System.out.println("The mean is "+mean);
            //System.out.println("lower is "+lowerBound);
            //System.out.println("upper is "+upperBound);
            double rate=1.0/mean;
            //System.out.println("rate is "+rate);
            double u=TreeUtils.getRandNumBet(0,1,false);
            double upperCDF=exponentialDistCDFEval(upperBound,rate);
            double lowerCDF=exponentialDistCDFEval(lowerBound,rate);
            //System.out.println("lowerCDF="+lowerCDF+" upper="+upperCDF);
            double toLog=1-u*(upperCDF-lowerCDF)-lowerCDF;
            //System.out.println("tolog is "+toLog);
            double num=Math.log(toLog);
            double denom=(-1)*rate;
            //System.out.println("numerator="+num);
            //System.out.println("denominator="+denom);
            double x=num/denom;            
            //System.out.println("to return x=num/dnm="+x);
            if(Double.isInfinite(x))
                {
                //in infinit case, fall back on uniform dist
                x=TreeUtils.getRandNumBet(lowerBound,upperBound,false);
                //System.err.println("Returning "+x+" with lb="+lowerBound+" and ub="+upperBound);
                }
            
            
            return x;
            }
        }
    
    
    public static double drawTruncatedExponentialMeanOne(double lowerBound,double upperBound)
        {
        if(!(0<=lowerBound && lowerBound<=upperBound))
            {
            return drawExponentialMeanOne();
            }
        else
            {
            //System.out.println("HERE");
            //see https://en.wikipedia.org/wiki/Truncated_distribution
            double u=TreeUtils.getRandNumBet(0,1,false);
            double ena=Math.exp(-lowerBound);
            double enb=Math.exp(-upperBound);
            double x=(-1)*Math.log(ena-u*(ena-enb));
            return x;
               
            }
        }
    
    static ExponentialDistribution edOne=null;
    public static double drawExponentialMeanOne()
        {
        if(edOne==null)
            {
            edOne=new ExponentialDistribution(myRandomDataGenerator.getRandomGenerator(),1.0);
            }
        return edOne.sample();
        }
    
    
    public static double drawExponentialWithRate(double rate)
        {
        double mean=1.0/rate;
        return drawExponentialWithMean(mean);
        }
    
    
    public static double drawExponentialWithMean(double mean)
        {
        ExponentialDistribution ed=new ExponentialDistribution(myRandomDataGenerator.getRandomGenerator(),mean);
        return ed.sample();
        }
    
    
    public static Object chooseRandom(Object[] os)
        {
        if(os==null)
            {
            return null;
            }
        else if(os.length==0)
            {
            return null;
            }
        else
            {
            int randomIndex=chooseRandomIndex(os.length);
            return os[randomIndex];
            }
        }
    
    public static Object chooseRandom(ArrayList al)
        {
        if(al==null)
            {
            return null;
            }
        else if(al.size()==0)
            {
            return null;
            }
        else
            {
            int randomIndex=chooseRandomIndex(al.size());
            return al.get(randomIndex);
            }
        }
    
    public static int chooseRandomIndex(int arrayLen)
        {
        int[] randomIndicesArr=TreeUtils.chooseRandomSubset(1,arrayLen);
        int idx=randomIndicesArr[0];
        return idx;
        }
    
    
    public static int[] chooseRandomIndices(int arrayLen,int numIndices)
        {
        if(numIndices<=0)
            {
            throw new IllegalArgumentException("Error, require numIndices="+numIndices+" to be a postive integer!");
            //return new int[0];
            }
        else
            {
            return chooseRandomSubset(numIndices,arrayLen);
            }            
        }
    
    
    public static int[] chooseRandomSubset(int subSetSize,int setSize)
    {
        /*generates an integer array of length k whose entries are selected randomly, 
                without repetition, from the integers 0, ..., n - 1 (inclusive).*/
        return myRandomDataGenerator.nextPermutation(setSize,subSetSize);
        
    }
    
    public static final double tolerDist=0.00001;
    
    public static boolean tolerEq(double a,double b)
        {
        double dist=Math.abs(a-b);
        if(dist>=tolerDist)
            {
            return false;
            }
        else
            {
            return true;
            }
        }

    
    public static String[] arrayListToStringArray(ArrayList al)
        {
        if(al==null)
            {
            return null;
            }
        else
            {
            String[] tbr=new String[al.size()];
            for(int i=0;i<al.size();i++)
                {
                tbr[i]=new String(al.get(i).toString());
                }
            return tbr;
            }
        }
    
    public static void main(String[] args)
    {

    try
        {
        String phymlPath=getPhymlPathFromPathEnv();
        System.out.println("Got phyml path : "+phymlPath);
            
        }
    catch(Exception e)
        {
        e.printStackTrace();
        }

        
        
    }
    
    
private static Pattern phymlLLPattern=Pattern.compile("\\-\\d+\\.\\d{0,6}");
    
public static String makeLikePhymlLL(String logLikeStr)
    {
    //get up to the first six digits after the decimal
    Matcher m=phymlLLPattern.matcher(logLikeStr);
    if(m.find())
        {
        String result=m.group();
        //System.out.println("To return "+result+" from "+logLikeStr);
        return result;
        }
    else
        {
        throw new RuntimeException("Error in applying "+phymlLLPattern+" to "+logLikeStr);
        }
    }
    
            
}
