/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 *
 * @author esalina
 */
public class InfiniteSitesLikelihoodCalculator  implements TreeLikelihoodCalculator   {
    

    public boolean arePriorsAllSingle()
        {
        //currently no min and max theta, so return true
        return true;
        }
    
    
    public boolean isPruningComputed()
        {
        return false;
        }
    
    
    HashMap pMap=null;
    
    public InfiniteSitesLikelihoodCalculator()
        {

        }

    public void setParams(HashMap pMap)
        {
        this.pMap=pMap;
        }
    
    public void proposeNewParams(boolean generate)
        {
        throw new RuntimeException("Error, not ready!");
        }
    
    public HashMap cloneParams()
        {
        HashMap c=new HashMap();
        c.put("theta",this.pMap.get("theta"));
        return c;
        }
    
    
    public Object getParamValue(String paramName)
        {
        return this.pMap.get(paramName);
        }
    
    public double computeLikelihoodNBD(coa_lineage t,FastaIO fio,boolean isLog)
        {
        throw new RuntimeException("NEED TO IMPLEMENT!");
        }
    
    
    public BigDecimal computeLikelihood(coa_lineage t,FastaIO fio,boolean isLog,TreeGenerator tGen)
        {
        throw new RuntimeException("NEED TO IMPLEMENT!");
            
        //return null;
        }
    
    public static BigDecimal factorialBD(int n)
        {
            BigDecimal f=new BigDecimal(""+1+"");
            for(int x=2;x<=n;x++)
                {
                f=f.multiply(new BigDecimal(""+x+""));
                }
            return f;
        }
    
    
    
    public static BigDecimal unfoldedProbBD
            (
            int num_seg_sites,
            double length_sum,
            double theta,
            boolean debug
            )
        {
        if(num_seg_sites==0 && length_sum==0)
            {
            return new BigDecimal("1.0");
            }
        else
            {
            BigDecimal thetaBD=new BigDecimal(""+theta+"");
            BigDecimal lenSumBD=new BigDecimal(""+length_sum+"");
            BigDecimal numerator_base=thetaBD.multiply(lenSumBD).divide(new BigDecimal(""+2.0+""));
            BigDecimal numerator=numerator_base.pow(num_seg_sites);
            BigDecimal denominator=factorialBD(num_seg_sites);
            BigDecimal ratio=numerator.divide(denominator);
            BigDecimal factor_exp=numerator_base.multiply(new BigDecimal("-1"));
            BigDecimal factor=new BigDecimal(Math.exp(factor_exp.doubleValue()));
            BigDecimal product=ratio.multiply(factor);
            return product;
            }
        }
    
        
    
    
}
