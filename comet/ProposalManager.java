/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author esalina
 */
public class ProposalManager {

    
    int num_proposals=4;
    public static final int GENERIC_PROPOSAL=0;
    double pct_generic;
    public static final int TIP_SWAPPING=1;
    double pct_tip_swap;
    public static final int GUIDED_TIP_SWAP=2;
    double pct_guided_tip_swap;
    public static final int MODEL_SPECIFIC_ROUTINE=3;
    double pct_model_specific_routine;
    
    int[] acceptanceCounter=new int[num_proposals];
    int[] rejectionCounter=new int[num_proposals];

    double[] proposalDistribution=null;
    String[] proposalSettings=null;
    int proposalMode=0;
    FastaIO fio=null;
    ArrayList tipCollection=null;
    
    String proposalSetting=null;
    
        
    double tip_swap_vs_generic_ratio=0;
    int num_tips=0;
    //tip-swap burn in (iteration where tip-swapping becomes possible
    long tip_swap_burn_thresh=0;
    //random number threshold where tip-swapping is used for proposal rather than random
    double tip_swap_prob=0;
    
    int lastProposal=0;
    ProposalLogger myProposalLogger=new ProposalLogger();
    public static final int AccRejQueueSize=50;
    LinkedBlockingQueue accRejQueue=new LinkedBlockingQueue(AccRejQueueSize);
    LinkedBlockingQueue treeAccRejQueue=new LinkedBlockingQueue(AccRejQueueSize);

    
    int lcAcceptances=0;
    int lcRejections=0;
    
    public double getTreeAccRejRatio(int last)
        {
        if(last<=0)
            {
            throw new IllegalArgumentException("Error, trying to get last <=0 acceptances ratio!");
            }
        else if(last>AccRejQueueSize)
            {
            throw new IllegalArgumentException("Error, trying to get acceptance ration of last "+last+" proposals, but log is only up to "+AccRejQueueSize);
            }
        Object[] array=(Object[])treeAccRejQueue.toArray();
        int trueCount=0;
        int totCount=0;
        for(int i=array.length-1;i>=0;i--)
            {
            totCount++;
            if((boolean)array[i])
                {
                trueCount++;
                }
            if(totCount>=last)
                {
                return ((double)(trueCount))/((double)(totCount));
                }
            }
        return ((double)(trueCount))/((double)(totCount));
        }
    
    public void logLCAcceptance()
        {
        lcAcceptances++;
        if(accRejQueue.remainingCapacity()==0)
            {
            accRejQueue.poll();
            }
        accRejQueue.add(Boolean.TRUE);
        }
    
    public void logLCRejection()
        {
        lcRejections++;
        if(accRejQueue.remainingCapacity()==0)
            {
            accRejQueue.poll();
            }        
        accRejQueue.add(Boolean.FALSE);
        }
    
    public void logAcceptance(int method,TreeGenerator tGen)
        {
        if(treeAccRejQueue.remainingCapacity()==0)
            {
            treeAccRejQueue.poll();
            }
        if(accRejQueue.remainingCapacity()==0)
            {
            accRejQueue.poll();
            }
        treeAccRejQueue.add(Boolean.TRUE);
        accRejQueue.add(Boolean.TRUE);
        acceptanceCounter[method]++;
        if(method==MODEL_SPECIFIC_ROUTINE)
            {
            int codeToLog=tGen.getLastProposalCode();
            myProposalLogger.logInMap(true, codeToLog);
            }
        }
    
    public void logRejection(int method,TreeGenerator tGen)
        {
        if(treeAccRejQueue.remainingCapacity()==0)
            {
            treeAccRejQueue.poll();
            }            
        rejectionCounter[method]++;
        if(accRejQueue.remainingCapacity()==0)
            {
            accRejQueue.poll();
            }        
        accRejQueue.add(Boolean.FALSE);
        treeAccRejQueue.add(Boolean.FALSE);        
        if(method==MODEL_SPECIFIC_ROUTINE)
            {
            int codeToLog=tGen.getLastProposalCode();
            myProposalLogger.logInMap(false, codeToLog);
            }        
        }
  
    public String getARTable(TreeGenerator tGen)
        {
        String table="Acceptances :\n";
        int a_sum=0;
        table+="Likelihood Calculator: "+lcAcceptances+"\n";
        a_sum+=lcAcceptances;
        for(int i=0;i<num_proposals;i++)
            {
            table+=ProposalManager.getStrategyDescription(i)+"\t"+acceptanceCounter[i]+"\n";
            a_sum+=acceptanceCounter[i];
            if(i==MODEL_SPECIFIC_ROUTINE)
                {
                HashMap mySuccesses=myProposalLogger.getCountMap(true);
                for(Object k:mySuccesses.keySet())
                    {
                    Object val=mySuccesses.get(k);
                    int code=Integer.parseInt(k.toString());
                    String toPrint="\t"+tGen.getProposalDescription(code)+"\t"+val.toString()+"\n";
                    table+=toPrint;
                    }
                }
            }
        table+="\n";
        table+="Total acceptance sum : "+a_sum+"\n\n";
        table+="Rejections :\n";
        int r_sum=0;
        table+="Likelihood Calculator: "+lcRejections+"\n";
        r_sum+=lcRejections;
        for(int i=0;i<num_proposals;i++)
            {
            table+=ProposalManager.getStrategyDescription(i)+"\t"+rejectionCounter[i]+"\n";
            r_sum+=rejectionCounter[i];
            if(i==MODEL_SPECIFIC_ROUTINE)
                {
                HashMap myFailures=myProposalLogger.getCountMap(false);
                for(Object k:myFailures.keySet())
                    {
                    Object val=myFailures.get(k);
                    int code=Integer.parseInt(k.toString());
                    String toPrint="\t"+tGen.getProposalDescription(code)+"\t"+val.toString()+"\n";
                    table+=toPrint;
                    }
                }            
            }
        table+="\n";
        table+="Total rejections sum : "+r_sum;
        double a_sum_d=(double)(a_sum);
        double r_sum_d=(double)(r_sum);
        double a_ratio_dnm=(a_sum_d+r_sum_d);
        double a_ratio=a_sum_d/a_ratio_dnm;
        double a_ratio_pct=a_ratio*100.0;
        table+="\nAcceptance ratio = "+a_sum_d+"/"+a_ratio_dnm+" = "+a_ratio+" = "+a_ratio_pct+"%";
        table+="\n";
        return table;
        }
    
    
    public static String getStrategyDescription(int v)
        {
        if(v==ProposalManager.GENERIC_PROPOSAL)
            {
            return "GENERIC_PROPOSAL";
            }
        else if(v==ProposalManager.TIP_SWAPPING)
            {
            return "TIP_SWAPPING";
            }
        else if(v==ProposalManager.GUIDED_TIP_SWAP)
            {
            return "GUIDED_TIP_SWAPPING";
            }
        else if(v==ProposalManager.MODEL_SPECIFIC_ROUTINE)
            {
            return "MODEL_SPECIFIC_REARRANGEMENT_ROUTINE";
            }
        else
            {
            throw new RuntimeException("Error, undefined proposal strategy!?  ("+v+")");
            }
        }
    
    
    private void initFastaIO(FastaIO iFio)
        {
        this.fio=iFio;
        }
    
    public int getLastProposalStyleMade()
        {
        return this.lastProposal;
        }

    
    public Object[] proposeTree(coa_lineage current,BigDecimal cPrior,TreeGenerator tGen,long iterNum)
        {
            
        int proposalScheme=TreeUtils.drawMultinomial(proposalDistribution);
        //System.out.println("On iteration number "+iterNum+" the randomly selected proposal is "+getStrategyDescription(proposalScheme));
        this.lastProposal=proposalScheme;
        if(proposalScheme==ProposalManager.GENERIC_PROPOSAL)
            {
            tGen.proposeParams();
            return tGen.generateTree();
            }
        else if(proposalScheme==ProposalManager.TIP_SWAPPING)
            {
            //tip swapping
            HashMap swapMap=this.generateRandomTipSwappingMap();
                
            coa_lineage cloned_tree=current.cloneRec();
            cloned_tree.recTipSwap(swapMap);
            Object[] ret_pack=new Object[2];
            ret_pack[0]=cloned_tree;

            //swapped tips doesn't affect the prior because the branch lenghts aren't affected
            ret_pack[1]=cPrior;
                
            //System.out.println("TS ret_pack : "+Arrays.toString(ret_pack));
                
            return ret_pack;
            }
        else if(proposalScheme==ProposalManager.GUIDED_TIP_SWAP)
            {
            //first acquire a tree and prior
            tGen.proposeParams();
            Object[] proposed_gts=tGen.generateTree();
            coa_lineage proposedTree=(coa_lineage)(proposed_gts[0]);
            BigDecimal proposedPrior=(BigDecimal)(proposed_gts[1]);
            
            //use the trees to make a guided-tip swapping map!
            Object[] currentPairing=current.acquireNameDistSorted();
            Object[] proposedPairing=proposedTree.acquireNameDistSorted();
            ArrayList<String> currentOrderedNames=(ArrayList<String>)(currentPairing[0]);
            ArrayList<String> proposedOrderedNames=(ArrayList<String>)(proposedPairing[0]);
            ArrayList<Double> currentDists=(ArrayList<Double>)(currentPairing[1]);
            ArrayList<Double> proposedDists=(ArrayList<Double>)(proposedPairing[1]); 
            /*System.out.println("Current tree is "+current.toString());
            System.out.println("Propsed is "+proposedTree.toString());
            System.out.println("Current newick is :"+current.makeNewickString(true, fio));
            System.out.println("Proposed newick is "+proposedTree.makeNewickString(true, fio));
            current.resetNewick();
            proposedTree.resetNewick();
            for(int i=0;i<currentOrderedNames.size();i++)
                {
                if(i==0)
                    {
                    System.out.println("ID\tNC\tNP\tDC\tDP");
                    }
                System.out.print((i+1)+"\t"+
                        currentOrderedNames.get(i)+","+TreeUtils.acquireABCName(Integer.parseInt(currentOrderedNames.get(i))-1)+"\t"+
                        proposedOrderedNames.get(i)+","+TreeUtils.acquireABCName(Integer.parseInt(proposedOrderedNames.get(i))-1)+"\t"+
                        currentDists.get(i)+"\t"+
                        proposedDists.get(i)+"\n");
                }*/
            
            HashMap guidedMap=new HashMap();
            for(int i=0;i<proposedOrderedNames.size();i++)
                {
                String from=proposedOrderedNames.get(i);
                String to=currentOrderedNames.get(i);
                guidedMap.put(from,to);
               //System.out.println("IN guided map "+from+" -> "+to);
                }
            proposedTree.recTipSwap(guidedMap);
            //System.out.println("Post guided tree : "+proposedTree.toString());
            //System.out.println("Post guided newick "+proposedTree.makeNewickString(true, fio));
            proposedTree.resetNewick();
            proposed_gts[0]=proposedTree;
            return proposed_gts;
            }
        else if(proposalScheme==ProposalManager.MODEL_SPECIFIC_ROUTINE)
            {
            coa_lineage current_clone=current.cloneRec();
            //System.out.println("call proposeTree from manager");
            return  tGen.proposeTree(current_clone, cPrior);
            }

        //shouldn't get here!!!!
        
        throw new NullPointerException("HUH?");
        //return null;
        }
    
    
    public HashMap generateRandomTipSwappingMap()
        {
        //only swap TWO tips!
        int numTipsToChooseAtRandom=2;
        Object[] randomTips=TreeUtils.sampleWOReplacement(tipCollection,numTipsToChooseAtRandom);
        String first=(String)randomTips[0];
        String second=(String)randomTips[1];
        HashMap swapMap=new HashMap();
        swapMap.put(first.toString(),second.toString());
        swapMap.put(second.toString(),first.toString());
        return swapMap;
        }
    


    private void initTipCollection(FastaIO iFio)
        {
        //acquire and init info for tip-swap map
        this.tipCollection=new ArrayList<String>();
        this.initFastaIO(iFio);
        int numTips=fio.getNumSeqs();
        for(int i=0;i<numTips;i++)
            {
            String tipName=new String(""+(i+1)+"");
            //System.out.println("Adding tip name '"+tipName+"' to tip-swap list (possible selection....)");
            this.tipCollection.add(tipName);
            }        
        }
    
    
    public boolean doesProposalLineIndicatePureGeneration()
        {
        if(this.pct_generic==1)
            {
            return true;
            }
        else
            {
            return false;
            }
        }
    
    
    
    public ProposalManager(String proposalLine,FastaIO iFio)
        {
        
        initTipCollection(iFio);
        String[] pieces=proposalLine.split(",");
        String propsalRTErrMsg="Error, invalid proposal strategy setting line !";
        propsalRTErrMsg+=" Require "+num_proposals+" percents summing to 1.0!";
        propsalRTErrMsg+=" Got : '"+proposalLine+"'";
        if(pieces==null || pieces.length!=num_proposals)
            {
            //require a distribution of proposals
            //propsalRTErrMsg+=" Null or invalid number (!4)!";
            if(pieces==null)
                {
                propsalRTErrMsg+=" Got Null!";
                }
            else
                {
                propsalRTErrMsg+="or invalid number (length="+pieces.length+")";
                }
            
            throw new RuntimeException(propsalRTErrMsg);
            }
        proposalSettings=pieces;
        System.out.println("Initializing proposal manager ...");
        double[] pcts=new double[num_proposals];
        for(int i=0;i<num_proposals;i++)
            {
            pcts[i]=Double.parseDouble(pieces[i]);
            }
        if(!(TreeUtils.sumToOne(pcts)))
            {
            propsalRTErrMsg=propsalRTErrMsg+" with sum "+TreeUtils.acquireSum(pcts);
            //pcts don't sum to 1.0!
            throw new RuntimeException(propsalRTErrMsg);
            }

        //impose the distribution
        pct_generic=pcts[0];
        pct_tip_swap=pcts[1];
        pct_guided_tip_swap=pcts[2];
        pct_model_specific_routine=pcts[3];
        
        this.proposalDistribution=pcts;
        System.out.println("Generic proposal pct set as "+pct_generic);
        //System.out.println("Tip-swapping pct set as "+pct_tip_swap);
        //System.out.println("Guided tip-swapping pct set as "+pct_guided_tip_swap);
        System.out.println("Model-specific rearrangement pct set as "+pct_model_specific_routine);

        
        }
        
    
    
}
