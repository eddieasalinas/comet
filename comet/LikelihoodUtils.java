/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pal.substmodel.HKY;
import pal.substmodel.NucleotideModel;
import pal.substmodel.TransitionProbability;


/**
 *
 * @author esalina
 */
public class LikelihoodUtils {


    
    public static boolean usePAL=false;
    public static boolean usePhyML=false;
    
    
    
    public static class ProbVectorFloat {
        public float[] pieces=new float[4];
        
        ProbVectorFloat()
            {
            pieces[0]=Float.NEGATIVE_INFINITY;
            pieces[1]=Float.NEGATIVE_INFINITY;
            pieces[2]=Float.NEGATIVE_INFINITY;
            pieces[3]=Float.NEGATIVE_INFINITY;
            }
        
        }
        
    
    
    public static class ProbVector {
        public double[] pieces=new double[4];
        
        ProbVector()
            {
            pieces[0]=Double.NEGATIVE_INFINITY;
            pieces[1]=Double.NEGATIVE_INFINITY;
            pieces[2]=Double.NEGATIVE_INFINITY;
            pieces[3]=Double.NEGATIVE_INFINITY;
            }
        
        int getNumNegInfs()
            {
            int numNegInfs=0;
            for(int p=0;p<pieces.length;p++)
                {
                if(pieces[p]==Double.NEGATIVE_INFINITY)
                    {
                    numNegInfs++;
                    }
                }
            return numNegInfs;
            }
        
        }
    

    
    public static double logAdd(double a,double b)
        {
        //see https://en.wikipedia.org/wiki/Log_probability
            
        /*
        ==Addition in log space==

: <math>\log(x + y)</math>
: <math>= \log(x + x \cdot y / x)</math>
: <math>= \log(x + x \cdot \exp(\log(y / x)))</math>
: <math>= \log(x \cdot (1 + \exp(\log(y) - \log(x))))</math>
: <math>= \log(x) + \log(1 + \exp(\log(y) - \log(x)))</math>
: <math>= x' + \log(1 + \exp(y' - x'))</math>

The formula above is more accurate than <math> \log(e^{x'} + e^{y'})</math>, provided one takes advantage of the asymmetry in the addition formula. <math> {x'} </math> should be the larger (least negative) of the two operands. This also produces the correct behavior if one of the operands is floating-point negative infinity, which corresponds to a probability of zero.

: <math>-\infty + \log(1 + \exp(y' - (-\infty))) = -\infty + \infty </math> This quantity is [[Indeterminate_form | indeterminate]], and will result in [[NaN]].
: <math> x' + \log(1 + \exp(-\infty - x')) = x' + 0</math> This is the desired answer.    
        */
        
        if(a==Double.NEGATIVE_INFINITY && b==Double.NEGATIVE_INFINITY)
            {
            return Double.NEGATIVE_INFINITY;
            }
            
            
        double x_prime=Math.max(a,b);
        double y_prime=Math.min(a,b);
        double prime_diff=y_prime-x_prime;
        double sum_to_log=1.0+Math.exp(prime_diff);
        double sum_to_ret=x_prime+Math.log(sum_to_log);
        
            
            
        return sum_to_ret;
        }
    
    public static float logAddFloat(float a,float b)
        {
        //see https://en.wikipedia.org/wiki/Log_probability
            
        /*
        ==Addition in log space==

: <math>\log(x + y)</math>
: <math>= \log(x + x \cdot y / x)</math>
: <math>= \log(x + x \cdot \exp(\log(y / x)))</math>
: <math>= \log(x \cdot (1 + \exp(\log(y) - \log(x))))</math>
: <math>= \log(x) + \log(1 + \exp(\log(y) - \log(x)))</math>
: <math>= x' + \log(1 + \exp(y' - x'))</math>

The formula above is more accurate than <math> \log(e^{x'} + e^{y'})</math>, provided one takes advantage of the asymmetry in the addition formula. <math> {x'} </math> should be the larger (least negative) of the two operands. This also produces the correct behavior if one of the operands is floating-point negative infinity, which corresponds to a probability of zero.

: <math>-\infty + \log(1 + \exp(y' - (-\infty))) = -\infty + \infty </math> This quantity is [[Indeterminate_form | indeterminate]], and will result in [[NaN]].
: <math> x' + \log(1 + \exp(-\infty - x')) = x' + 0</math> This is the desired answer.    
        */
        
        if(a==Float.NEGATIVE_INFINITY && b==Float.NEGATIVE_INFINITY)
            {
            return Float.NEGATIVE_INFINITY;
            }
            
            
        float x_prime=Math.max(a,b);
        float y_prime=Math.min(a,b);
        float prime_diff=y_prime-x_prime;
        float sum_to_log=(float)(Math.exp(prime_diff));
        sum_to_log+=1.0f;
        float sum_to_ret=x_prime+(float)(Math.log(sum_to_log));
        
            
            
        return sum_to_ret;
        }
    
    
    
    
    
        
    public static class siteLikelihoodThread extends Thread
        {

        int tsite;
        float logLikeFloat=0.0f;
        double logLikeDouble=0.0;
        coa_lineage thread_coa;
        FastaIO threadIO;
        NucleotideModel threadNMModel;
        boolean isDone=false;
	int totalNumThreads=0;
        int ttempId=(-1);
        HashMap tsharedDistDict=null;
        ArrayList<Double> tlengthsToCache;
        
        public int getSite()
            {
            return tsite;
            }
        
        public siteLikelihoodThread(
            coa_lineage init_coa,
            FastaIO iFio,
            NucleotideModel nm,
            int site,
	    int numThreadsTotal
            )
            {
            tsite=site;
            threadNMModel=nm;
            thread_coa=init_coa;
            threadIO=iFio;
	    totalNumThreads=numThreadsTotal;
            
            }
        
        
        public void setTIDAndSharedDictAndLengthsToCache(int tempID,HashMap sharedDict,ArrayList<Double> lengthsToCache)
            {
            ttempId=tempID;
            tsharedDistDict=sharedDict;
            tlengthsToCache=lengthsToCache;            
            }
        
        
        public void setTreeAndModel(coa_lineage nt,NucleotideModel nm)
            {
            thread_coa=nt;
            threadNMModel=nm;
            }
        
        
        public void setSiteTreeAndModel(int ns,coa_lineage nt,NucleotideModel nm)
            {
            tsite=ns;
            thread_coa=nt;
            threadNMModel=nm;
            }
        
        public void setIsDone()
            {
            isDone=true;
            }
        
        boolean isFloat=true;
        
        public void setFloat(boolean n)
            {
            isFloat=n;
            }
        
        public void run()
            {
            if(ttempId<totalNumThreads)
                {
                //totalNumThreads is the number of threads in the thread-pool
                //this block occurs when the first threads are submitted.  The first
                //threads submitted pre-compute the transition probability matrices
		NucleotideModel compModel=(NucleotideModel)(threadNMModel.clone());
                //System.out.println("Thread "+ttempId+" of "+totalNumThreads+" running....");
                for(int d=0;d<this.tlengthsToCache.size();d++)
                    {
                    boolean thisThreadShouldComputeForThisDistance=(d%totalNumThreads==ttempId);
                    //System.out.println("For d="+d+" for ttempId="+ttempId+" and totalNumThreads="+totalNumThreads+", compute flag is "+thisThreadShouldComputeForThisDistance);
		    if(thisThreadShouldComputeForThisDistance)
			{
			TransitionProbability tp=compModel.getTransitionProbability();
			double distForCalc=tlengthsToCache.get(d);
			tp.setDistance(distForCalc);
			double[][] arrToHash=new double[4][];
			for(int from=0;from<4;from++)
				{
                                boolean redoForNegInfLog=false;
				arrToHash[from]=new double[4];
				for(int to=0;to<4;to++)
					{
					double prob;
					if(distForCalc==0)
						{
						if(from==to)
							{
							prob=Math.log(1.0);
							}
						else
							{
							prob=Math.log(0.0);
							}
                                                arrToHash[from][to]=prob;
						}//zero distance
					else
						{
						prob=(double)(tp.getTransitionProbability(from,to));
                                                if(prob==0)
                                                    {
                                                    arrToHash[from][to]=Double.NEGATIVE_INFINITY;
                                                    redoForNegInfLog=true;
                                                    }
                                                else
                                                    {
                                                    double log=Math.log(prob);
                                                    arrToHash[from][to]=log;
                                                    }
						}//positive distance
                                        }//to loop 0...3
                                if(redoForNegInfLog)
                                    {
                                    //only handled for when distance is near zero
                                    if(distForCalc>=0.001)
                                        {
                                        //if the distance is not near zero then the case is not handled!
                                        throw new RuntimeException("Error : negative inf. (log prob near zero) is computed for a distance not near zero!  unhandled case!");
                                        }
                                    redoForNegInfLog=false;
                                    boolean[] negInfIndicators=TreeUtils.getNegInfIndicators(arrToHash[from]);
                                    int numNegIndicators=TreeUtils.getNumIndicates(negInfIndicators);
                                    if(numNegIndicators==4)
                                        {
                                        //should never get here
                                        String eMsg="When dist="+distForCalc+", from="+from+" the log transition probabilities are as "+Arrays.toString(arrToHash[from])+" and all are neg infinity?!?!";
                                        throw new RuntimeException(eMsg);
                                        }
                                    else if(numNegIndicators==3 || numNegIndicators==2 || numNegIndicators==1)
                                        {
                                        //assume distance is near zero and store Neg. Inf. at non-diagonal values
                                        for(int to=0;to<4;to++)
                                            {
                                            if(from==to)
                                                {
                                                arrToHash[from][to]=0;
                                                }
                                            else
                                                {
                                                arrToHash[from][to]=Double.NEGATIVE_INFINITY;
                                                }
                                            }
                                        }
                                    else
                                        {
                                        //should never get here!
                                        //num Neg Inf is zero!?!?
                                        String eMsg="When dist="+distForCalc+", from="+from+" the log transition probabilities are as "+Arrays.toString(arrToHash[from])+" and none are neg infinity but at least one got tagged as?!?!";
                                        throw new RuntimeException(eMsg);                                            
                                        }
                                    }//rewrite this row in the matrix cause of underflow                                
				}//from loop 0...3
	                synchronized(tsharedDistDict)
        	            {
			    tsharedDistDict.put(tlengthsToCache.get(d),arrToHash);
        	            }

			}//if work should be done for this distance
                    }//for each distance in the cache list
                }//if this thread will do any work at all

	    //System.out.println("Before : wait : "+TreeUtils.mapToString(tsharedDistDict));
	    while(tsharedDistDict.keySet().size()<tlengthsToCache.size())
		{
		//wait!
		}
	    //System.out.println("After : wait : "+TreeUtils.mapToString(tsharedDistDict));
            tlengthsToCache=null;

            if(isFloat)
                {
                logLikeFloat=computeLikeLikeAtSite(
                        thread_coa,
                        threadIO,
                        true,
                        threadNMModel,
                        tsite
                    );
                }
            else
                {
                //System.out.println("Using double...");
                logLikeDouble=computeLikeAtSiteDouble(
                        thread_coa,
                        threadIO,
                        true,
                        threadNMModel,
                        tsite                        
                    );
                }

            isDone=true;
            }
        
        public double computeLikeAtSiteDouble(
            coa_lineage c,
            FastaIO fio,
            boolean isRoot,
            NucleotideModel nm,
            int site        
            )
            {
            ProbVector aSiteVector=compuateBasicLikeStayLNAllSitesBySite(
                c,
                fio,
                isRoot,
                nm,
                site,
                tsharedDistDict
                );
            double r=aSiteVector.pieces[0];
            return r;
            }
        
        
        public float computeLikeLikeAtSite(
            coa_lineage c,
            FastaIO fio,
            boolean isRoot,
            NucleotideModel nm,
            int site
            )
            {

            ProbVectorFloat pvf=compuateBasicLikeStayLNAllSitesFloatBySite(
                c,fio,isRoot,nm,site
                );
            
            return pvf.pieces[0];
            }
        
        public float getLogLikeFloat()
            {
            return logLikeFloat;
            }
        
        public double getLogLikeDouble()
            {
            return logLikeDouble;
            }
        
        }
    
    
    public static String COMET_MAX_LL_THREADS="AUTO";
    
    public static boolean announcedMT=false;
    static int numAvailProcs;
    static int numThreadsToUseForThreadPool;
    public static ExecutorService ThreadPoolExecService=null;
    public static ArrayList<siteLikelihoodThread> patternThreadArr=null;
    public static Number compuateBasicLikeStayLNAllSitesBySiteWrapperMT(
            coa_lineage c,
            FastaIO fio,
            NucleotideModel nm,
            boolean useFloat,
            ArrayList<Double> lengthsToCache
        )
        {
        ArrayList<Integer> coveringSites=fio.getSitesToCoverAllPatterns();        
        if(!announcedMT)
            {
	    String envCometNumAvailProcs=System.getenv("COMET_NUM_AVAIL_PROCS");
	    if(envCometNumAvailProcs!=null)
		{
		try
			{
			envCometNumAvailProcs=envCometNumAvailProcs.trim();
			numAvailProcs=Integer.parseInt(envCometNumAvailProcs);
			}
		catch(Exception e)
			{
			System.err.println("Error using environment variable COMET_NUM_AVAIL_PROCS !");
			System.err.println("To use Runtime.getRuntime().availableProcessors() to get num procs!");
			numAvailProcs=Runtime.getRuntime().availableProcessors(); 
			}
		}
	    else
		{
		numAvailProcs=Runtime.getRuntime().availableProcessors();
		}
            int htEnabled=TreeUtils.figureIfHyperThreadingIsEnabled();
            if(htEnabled==1)
                {
                System.out.println("Hyperthreading detected !");
                numAvailProcs/=2;
                }
            else if(htEnabled==2)
                {
                System.out.println("Assuming hyperthreading *is* enabled to avoid excessive core utilization.");
                numAvailProcs/=2;
                }
            else
                {
                System.out.println("Hyperthreading NOT detected !");  
                }
            if(numAvailProcs<=0)
                {
                System.out.println("numAvailProcs detected as non-positive, now setting it to one!");
		if(envCometNumAvailProcs==null)
			{
			System.out.println("Consider using COMET_NUM_AVAIL_PROCS to set the number of processor manually."); 
			}
                numAvailProcs=1;
                }
            numThreadsToUseForThreadPool=numAvailProcs;
            String cometMaxThreads=COMET_MAX_LL_THREADS;
            if(cometMaxThreads.trim().equalsIgnoreCase("auto"))
                {
                cometMaxThreads=null;
                }
            System.out.println("Number available processors for computation : "+numAvailProcs);            
            if(cometMaxThreads!=null)
                {
                String exceptionString="ERROR, the valud of the variable COMET_MAX_LL_THREADS was '"+
                        cometMaxThreads+"' but it must be 'AUTO' or a positive integer!";
                if(TreeUtils.isIntegerString(cometMaxThreads))
                    {
                    int cometMaxThreadsInt=Integer.parseInt(cometMaxThreads);
                    if(cometMaxThreadsInt<=0)
                        {
                        //must be positive!
                        throw new RuntimeException(exceptionString);
                        }
                    else
                        {
                        System.out.println("*** NOTE *** : "+
                                "Overriding with variable 'COMET_MAX_LL_THREADS'.");
                        numThreadsToUseForThreadPool=cometMaxThreadsInt;
                        }
                    }
                else
                    {
                    throw new RuntimeException(exceptionString);
                    }
                }
            if(numThreadsToUseForThreadPool>coveringSites.size())
                {
                System.out.println("NOTE : Number of threads set at "+numThreadsToUseForThreadPool+" but number of site-patterns is "+coveringSites.size());
                System.out.println("So will use "+coveringSites.size()+" threads instead.");
                numThreadsToUseForThreadPool=coveringSites.size();
                }
            System.out.println("Number threads to use : "+numThreadsToUseForThreadPool);
            System.out.println("Invoking routines for multithreaded computation of site log-likelihoods!");
            announcedMT=true;
            ThreadPoolExecService=java.util.concurrent.Executors.newFixedThreadPool(numThreadsToUseForThreadPool);
            patternThreadArr=new ArrayList<siteLikelihoodThread>();
            //use tempId for first few threads to compute/cache transition probabilities
            for(Integer sInt:coveringSites)
                {
                siteLikelihoodThread aThread=new siteLikelihoodThread(
                        c,
                        fio,
                        nm,
                        sInt.intValue(),
			numThreadsToUseForThreadPool//used when tempId is less than this so thread pre-computes distances; only the first threads that run will do so
			);
                patternThreadArr.add(aThread);
                }
            }
        ThreadPoolExecService=java.util.concurrent.Executors.newFixedThreadPool(numThreadsToUseForThreadPool);
        //launch threads over covering sites
        int tempId=0;
        HashMap sharedDistDict=new HashMap();       
        for(siteLikelihoodThread aThread:patternThreadArr)
            {
            aThread.setTreeAndModel(c,nm);
            aThread.setFloat(useFloat);
            aThread.setTIDAndSharedDictAndLengthsToCache(tempId,sharedDistDict,lengthsToCache);        
            ThreadPoolExecService.submit(aThread);
            tempId++;            
            }
        //wait for threads to finish
        ThreadPoolExecService.shutdown();
        try
            {
            ThreadPoolExecService.awaitTermination(10,TimeUnit.MINUTES);
            }
        catch(InterruptedException ie)
            {
            ie.printStackTrace();
            throw new RuntimeException("Waiting too long for threads!");
            }   
        //gather thread results
        if(useFloat)
            {
            float logLikeSumOverSites=0;
            for(siteLikelihoodThread aThread:patternThreadArr)
                {
                float patternLike=aThread.getLogLikeFloat();
                int site_num=aThread.getSite();             
                int pid_at_site=(Integer)fio.getSiteToPatternIDMap().get(site_num);
                int num_times_pattern_appears=(Integer)fio.getPatternIDCount(pid_at_site);
                logLikeSumOverSites=(float)(logLikeSumOverSites)+(float)(num_times_pattern_appears)*patternLike;
                }
            return new Float(logLikeSumOverSites);
            }
        else
            {
            //System.out.println("In double block for return ");
            double logLikeSumOverSites=0;
            for(siteLikelihoodThread aThread:patternThreadArr)
                {
                double patternLike=aThread.getLogLikeDouble();
                int site_num=aThread.getSite();
                int pid_at_site=(Integer)fio.getSiteToPatternIDMap().get(site_num);
                int num_times_pattern_appears=(Integer)fio.getPatternIDCount(pid_at_site);
                logLikeSumOverSites=logLikeSumOverSites+(double)(num_times_pattern_appears)*patternLike;
                }
            
            return new Double(logLikeSumOverSites);
            }
      
        }

    
    
    
    
    private static ProbVector pairWisePruning(
            NucleotideModel nm,
            double leftDist,
            double rightDist,
            ProbVector llike,
            ProbVector rlike,
            HashMap distProbCache
            )
            
        {

        
        double[][] leftTPM=(double[][])distProbCache.get(leftDist);
        double[][] rightTPM=(double[][])distProbCache.get(rightDist);
        

        ProbVector like=new ProbVector();
        for(int a=0;a<4;a++)
            {
            //like[a]=Double.NEGATIVE_INFINITY;
            for(int lbase=0;lbase<4;lbase++)
                {
                double tpLeft=leftTPM[a][lbase];
                //double cacheLeft=tpLeft;
                //tpLeft=lp.getTransitionProbability(a, lbase);
                //tpLeft=Math.log(tpLeft);
                for(int rbase=0;rbase<4;rbase++)
                    {
                    double tpRight=rightTPM[a][rbase];
                    //double cacheRight=tpRight;
                    //tpRight=rp.getTransitionProbability(a, rbase);
                    //tpRight=Math.log(tpRight);
                    
                    /*if(cacheLeft!=tpLeft)
                        {
                        System.out.println("Mismatch! ldist="+leftDist+"  Cache left is "+cacheLeft+" and left norm is "+tpLeft+" a="+a+" and lbase is "+lbase);
                        }
                    else if(cacheRight!=tpRight)
                        {
                        System.out.println("Mismatch! rdist="+rightDist+"   Cache right is "+cacheRight+" and right norm is "+tpRight+" a="+a+" and Rbase is "+rbase);
                        }
                    else
                        {
                        System.out.println("MATCHING!");
                        }*/
                    if(rlike.pieces[rbase]!=Double.NEGATIVE_INFINITY &&
                        llike.pieces[lbase]!=Double.NEGATIVE_INFINITY
                        )
                        {
                        like.pieces[a]=logAdd(like.pieces[a],
                            rlike.pieces[rbase]+tpRight+
                            tpLeft+llike.pieces[lbase]);                                    
                        }
                    else
                        {
                        //pass
                        }
                    }//for each rightbase
                }//for each left base
            }//for each of the 4 bases            
        return like;
        }
    
    

    
    public static  ProbVector compuateBasicLikeStayLNAllSitesBySite(
            coa_lineage c,
            FastaIO fio,
            boolean isRoot,
            NucleotideModel nm,
            int site,
            HashMap dictForCache
            )
        {
        if(nm!=null && true)
            {
            HKY mm=(HKY)(nm);
            //System.out.println("MM Eq freqs : "+Arrays.toString(mm.getEqulibriumFrequencies()));
            //System.out.println("MM num params "+mm.getNumParameters());
            //System.out.println("MM kappa : "+mm.getParameter(123));
            }
        //double[] like=new double[4];
        ProbVector like=null;
        Object[] kids_and_dists_arr=c.get_immediate_kids_and_dists_arr();
        coa_lineage[] myImmKids=(coa_lineage[])(kids_and_dists_arr[0]);
        double[] myImmDists=(double[])(kids_and_dists_arr[1]);        
        
        if(myImmKids.length!=0)
            {
            if(myImmKids.length>=2)
                {
                //System.out.println("Using new code....");
                ProbVector[] myLikes=new ProbVector[myImmKids.length];
                for(int k=0;k<myImmKids.length;k++)
                    {
                    myLikes[k]=compuateBasicLikeStayLNAllSitesBySite(myImmKids[k],fio,false,nm,site,dictForCache);
                    }
                like=pairWisePruning(
                            nm,
                            myImmDists[myImmKids.length-2],
                            myImmDists[myImmKids.length-1],
                            myLikes[myImmKids.length-2],
                            myLikes[myImmKids.length-1],
                            dictForCache
                            );
                for(int k=myImmDists.length-3;k>=0;k--)
                    {
                    ProbVector likeTemp=pairWisePruning(
                            nm,
                            myImmDists[k],
                            0,
                            myLikes[k],
                            like,
                            dictForCache
                            );
                    like=likeTemp;
                    }
                }
            else
                {
                throw new RuntimeException("Error, cannot invoke pruning algorithm with one child !");
                }
            }//recursive case
        else
            {
            like=new ProbVector();
            String seqIDStr=c.getName();
            //System.out.println("The seqID Str is "+seqIDStr);
            
            int seqID=fio.convertIntegerNameToID(seqIDStr);
            //System.out.println("Looked up ID is "+seqID);
            //System.out.println("site is "+site+" and seqID is "+seqID);
            
            //String seq=dfm.getSeqByName(seqName);
            String seq=fio.getSeqByID(seqID);
            //System.out.println("The seq retrieved from string '"+seqIDStr+"' is '"+seq+"'");
            
            //System.out.println("Computing base case with "+c.getName());
            char[] bases=seq.toCharArray();
            char base=bases[site];
            int p;
            if(base=='A')
                p=0;
            else if(base=='C')
                p=1;
            else if(base=='G')
                p=2;
            else
                p=3;
            like.pieces[p]=0;

            } //base case
        if(isRoot)
            {
            double sitesum=Double.NEGATIVE_INFINITY;
            double[] freqs=fio.getPiFreqsACGT();
            for(int b=0;b<4;b++)
                {
                sitesum=logAdd(
                        sitesum,
                        like.pieces[b]+
                        Math.log(freqs[b])
                );
                            //like[b]+Math.log(nm.frequency[b]));
                }
            like.pieces[0]=sitesum;
                
            return like;

            }        
        
        return like;
        }
    

    
    public static double getTotalMutationRate(double[][] rateMatrix)
        {
        double[][] relativeRates=rateMatrix;
        int n=relativeRates.length;
        double sum=0;
        for(int row=0;row<n;row++)
            {
            for(int col=0;col<n;col++)
                {
                if(row!=col)
                    {
                    sum+=rateMatrix[row][col];
                    }
                }
            }
        return sum;
        }
    
    
    
    
    public static  ProbVectorFloat compuateBasicLikeStayLNAllSitesFloatBySite(
            coa_lineage c,
            FastaIO fio,
            boolean isRoot,
            NucleotideModel nm,
            int site
            )
        {
        if(nm!=null && true)
            {
            HKY mm=(HKY)(nm);
            //System.out.println("MM Eq freqs : "+Arrays.toString(mm.getEqulibriumFrequencies()));
            //System.out.println("MM num params "+mm.getNumParameters());
            //System.out.println("MM kappa : "+mm.getParameter(123));
            }
        //double[] like=new double[4];
        ProbVectorFloat like=new ProbVectorFloat();
        Object[] kids_and_dists_arr=c.get_immediate_kids_and_dists_arr();
        coa_lineage[] myImmKids=(coa_lineage[])(kids_and_dists_arr[0]);
        double[] myImmDists=(double[])(kids_and_dists_arr[1]);        
        
        if(myImmKids.length!=0)
            {
            if(myImmKids.length>2)
                {
                String c_string=c.toString();
                String errMsg="myImmKids size="+myImmKids.length+"\n";
                errMsg+=c_string+"\n";
                errMsg+="Error, not ready for MM!\n\n\n\n\n\n\n\n\n";
                throw new RuntimeException(errMsg);
                
                }
            else
                {
                //System.out.println("immKids length is "+myImmKids.length);
                coa_lineage leftKid=myImmKids[0];
                coa_lineage rightKid=myImmKids[1];
                ProbVectorFloat llike=compuateBasicLikeStayLNAllSitesFloatBySite(leftKid,fio,false,nm,site);
                ProbVectorFloat rlike=compuateBasicLikeStayLNAllSitesFloatBySite(rightKid,fio,false,nm,site);
                
                float lproblen=(float)myImmDists[0];
                float rproblen=(float)myImmDists[1];

                //HKY leftHKY=new HKY(1.0,freqs);
                TransitionProbability lp=nm.getTransitionProbability();
                lp.setDistance(lproblen);
                //HKY rightHKY=new HKY(1.0,freqs);
                TransitionProbability rp=nm.getTransitionProbability();
                rp.setDistance(rproblen);

                like=new ProbVectorFloat();
                for(int a=0;a<4;a++)
                    {
                    //like[a]=Double.NEGATIVE_INFINITY;
                    for(int lbase=0;lbase<4;lbase++)
                        {
                        for(int rbase=0;rbase<4;rbase++)
                            {
                            float tpLeft=(float)(lp.getTransitionProbability(a,lbase));
                            tpLeft=(float)(Math.log(tpLeft));
                            float tpRight=(float)(rp.getTransitionProbability(a, rbase));
                            tpRight=(float)(Math.log(tpRight));
                            if(rlike.pieces[rbase]!=Float.NEGATIVE_INFINITY &&
                                llike.pieces[lbase]!=Float.NEGATIVE_INFINITY
                                )
                                {
                                like.pieces[a]=logAddFloat(like.pieces[a],
                                    rlike.pieces[rbase]+tpRight+
                                    tpLeft+llike.pieces[lbase]);                                    
                                }
                            }//for each rightbase
                        }//for each left base
                    }//for each of the 4 bases
                }            
            }//recursive case
        else
            {
            String seqIDStr=c.getName();
            //System.out.println("The seqID Str is "+seqIDStr);
            
            int seqID=fio.convertIntegerNameToID(seqIDStr);
            //System.out.println("Looked up ID is "+seqID);
            
            //String seq=dfm.getSeqByName(seqName);
            String seq=fio.getSeqByID(seqID);
            //System.out.println("The seq retrieved from string '"+seqIDStr+"' is '"+seq+"'");
            
            //System.out.println("Computing base case with "+c.getName());
            char[] bases=seq.toCharArray();
            char base=bases[site];
            int p;
            if(base=='A')
                p=0;
            else if(base=='C')
                p=1;
            else if(base=='G')
                p=2;
            else
                p=3;
            like.pieces[p]=0;

            } //base case
        if(isRoot)
            {
            float sitesum=Float.NEGATIVE_INFINITY;
            double[] freqs=fio.getPiFreqsACGT();
            //double[] freqs=FastaIO.getCMEFreqs();
            for(int b=0;b<4;b++)
                {
                sitesum=logAddFloat(
                        sitesum,
                        like.pieces[b]+
                        (float)(Math.log(freqs[b]))
                );
                            //like[b]+Math.log(nm.frequency[b]));
                }
            like.pieces[0]=sitesum;
            
            return like;

            }        
        
        return like;
        }
    

    
    
    
 
    public static Boolean canUseDevShm=null;
    
    
    
    
    static String phylipPath=null;
    
    
    static String phymlExecPath=null;

    public static void setPhylipPath(String pp)
        {
        LikelihoodUtils.phylipPath=pp;
        }

    
    public static boolean canUseDEVShm()
        {
        boolean testResult=false;
        try
            {
            testResult=runDevShmTest();
            return testResult;
            }
        catch(Exception e)
            {
            return false;
            }
        }
    
    
    public static boolean runDevShmTest() throws Exception
        {
        File dsf=new File("/dev/shm/COMET_text.txt");
        FileWriter fw=new FileWriter(dsf);
        BufferedWriter bw=new BufferedWriter(fw);
        String cometWasHere=new String("Comet was here!");
        cometWasHere=cometWasHere.trim();
        bw.write(cometWasHere);
        bw.flush();
        bw.close();
        fw.close();
        FileReader fr=new FileReader(dsf);
        BufferedReader br=new BufferedReader(fr);
        String aLine=br.readLine();
        br.close();
        fr.close();
        dsf.deleteOnExit();
        aLine=aLine.trim();
        if(aLine.equalsIgnoreCase(cometWasHere))
            {
            return true;
            }
        else
            {
            return false;
            }           
        }
    
    
    
    public static void setPhymlExecPath(String npep)
        {
        phymlExecPath=new String(npep);
        }
    
    
    public static String tempFilePrefix="coa_lineage.";
    public static String tempFileSuffix=".newick";
    public static File devShmFile=new File("/dev/shm/");
    
    public static String getPhyMLLike(
            coa_lineage c,
            boolean useUniRates,
            FastaIO fio,
            TreeLikelihoodCalculator lc
            ) throws Exception
        {
        //extract newick data and write it to a file
        File tempNewick=null;
        if(canUseDevShm==null)
            {
            boolean testResult=canUseDEVShm();
            if(testResult)
                {
                System.out.println("USING /dev/shm for temp PhyML files...");
                canUseDevShm=Boolean.TRUE;
                }
            else
                {
                System.out.println("USING standard area for temp PhyML files...");
                canUseDevShm=Boolean.FALSE;
                }
            }
        if(!canUseDevShm)
            {
            tempNewick=File.createTempFile(tempFilePrefix,tempFileSuffix);
            }
        else
            {
            tempNewick=File.createTempFile(tempFilePrefix,
                              tempFileSuffix,
                              devShmFile);
            }
        String newickString=c.makeNewickString(true,fio);
        FileWriter fw=new FileWriter(tempNewick);
        BufferedWriter bw=new BufferedWriter(fw);
        bw.write(newickString,0,newickString.length());
        bw.flush();
        bw.close();
        
        //extract pi RATES
        double[] piRates=new double[]{0.25,0.25,0.25,0.25};
        if(!useUniRates)
            {
            //pass
            piRates=fio.getPiFreqsACGT();
            }
        
        //point to the phylip file
        String phylipFilePath=LikelihoodUtils.phylipPath;
        
        //extract kappa
        double kappa=(Double)(lc.getParamValue("kappa"));
        
        //run PhyML
        String phymlOut=LikelihoodUtils.execPhyML(
            piRates,
            tempNewick.getCanonicalPath(),
            phylipFilePath,
            lc,
            kappa
            );
        
        //extract the likelihood
        String phymlLogLikelihood=extractLogLikeFromPhyMLOutput(phymlOut);

        //delete the temp file
        tempNewick.delete();
        
        return phymlLogLikelihood;
        }
    
    
    private static String execPhyML(double[] piRates,
            String newickFilePath,
            String phylipFilePath,
            TreeLikelihoodCalculator lc,
            double kappa
            ) throws Exception
        {
            //./PhyML-3.1_linux64   
            //-o n 
            // --nclasses 1  
            //-u  bd_tree.newic
            //-m JC69 
            //-d nt
            //-i  Naso_Guam.no_gaps.phylip
            //-a 1 
            //-f 0.25,0.25,0.25,0.25  //-f e, m, or fA,fC,fG,fT
            //--quiet
            //--bootstrap 0
            String phyMLCmd="";
            phyMLCmd+=LikelihoodUtils.phymlExecPath+" ";
            phyMLCmd+=" -o n "; //no optimization, just compute a likelihood
            phyMLCmd+=" --nclasses 1 "; //no classes among-site rate variation
            phyMLCmd+=" -u "+newickFilePath+" "; //file to the newick tree
            phyMLCmd+=" -t "+kappa+" ";
            if(lc instanceof comet.KHYLikelihoodCalculator)
                {
                phyMLCmd+=" -m HKY85 "; //use HKY85 model! :)
                }
            else
                {
                throw new RuntimeException("Error, likelihood calculator interface for PhyML not a valid substitution model!");
                }
            phyMLCmd+=" -i "+phylipFilePath+" ";
            phyMLCmd+=" -a 1 ";//1 alpha parameter
            phyMLCmd+=" -f "+piRates[0]+","+piRates[1]+","+piRates[2]+","+piRates[3]+" ";
            phyMLCmd+=" --quiet ";//minimize outputs
            phyMLCmd+=" --bootstrap 0 "; //conduct no additional tests, just compute the likelihood!!!!
            
            String[] phymlResults=TreeUtils.runCmdCaptureSTDOUTSTDERR(phyMLCmd);
            String phymlOUT=phymlResults[0];
            String phymlERR=phymlResults[1];
            if(phymlERR.length()>0)
                {
                System.err.println("PHYML OUT : "+phymlOUT);
                System.err.println("PHYML ERROR : \n"+phymlERR);
                }
            return phymlOUT;
        }
    
    
    public static String extractLogLikeFromPhyMLOutput(String phymlOut)
        {
        //Log likelihood of the current tree: -35518.968625.
        Pattern p=Pattern.compile("tree:\\s*(\\-\\d+\\.\\d+)[^\\d]",Pattern.CASE_INSENSITIVE);
        Matcher m=p.matcher(phymlOut);
        if(m.find())
            {
            String phyMLLogLikelihood=m.group(1);
            return phyMLLogLikelihood;
            }
        System.err.println("ERROR, failure to obtain phyml likelihood from string \n"+phymlOut);
         
        return null;
        }
        
    
    
    
    
}
