/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.forester.io.parsers.util.ParserUtils.readPhylogenies;
import org.forester.phylogeny.Phylogeny;
import org.forester.phylogeny.PhylogenyNode;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author esalina
 */
public class coa_lineage {
    
    public static boolean shareAGrandParent(coa_lineage a,coa_lineage b)
        {
        if(a==null || b==null)
            {
            return false;
            }
        if(shareAParent(a,b))
            {
            return false;
            }
        else
            {
            coa_lineage a_parent=a.getParentRef();
            coa_lineage b_parent=b.getParentRef();
            if(a_parent==null || b_parent==null)
                {
                return false;
                }
            else
                {
                return shareAParent(a_parent,b_parent);
                }
            }
        }
    
    public static boolean shareAParent(coa_lineage a,coa_lineage b)
        {
        if(a==null || b==null)
            {
            return false;
            }
        coa_lineage a_parent=a.getParentRef();
        coa_lineage b_parent=b.getParentRef();
        if(a_parent==null || b_parent==null)
            {
            return false;
            }
        if(a_parent==b_parent)
            {
            return true;
            }
        else
            {
            return false;
            }
        }
    
    
    
    public HashSet getLengthsOfBranches()
        {
        int nk=this.getNumKids();
        HashSet theseDists=new HashSet();
        HashSet recDists=new HashSet();
        for(int k=0;k<nk;k++)
            {
            double a_dist=this.getDist(k);
            theseDists.add(a_dist);
            HashSet recDist=this.getChild(k).getLengthsOfBranches();
            recDists.addAll(recDist);
            }
        theseDists.addAll(recDists);
        return theseDists;
        }
    
    public void getLengthsDownFirstChildren(ArrayList<Double> existingList)
	{
	int nk=getNumKids();
	if(nk>0)
            {
	    double distToAdd=this.getDist(0);
	    /*if(!(existingList.contains(distToAdd)))
		{
	        existingList.add(distToAdd);
		}*/
	    existingList.add(distToAdd);
	    this.getChild(0).getLengthsDownFirstChildren(existingList);
	    }
	}
    
    public static void parentRefVerify(coa_lineage root,boolean treeRoot)
        {
        if(treeRoot)
            {
            coa_lineage pRef=root.getParentRef();
            if(pRef!=null)
                {
                throw new RuntimeException("Error, tree root should have null parent reference!");
                }
            }
        int numKids=root.getNumKids();
        if(numKids>0)
            {
            for(int k=0;k<numKids;k++)
                {
                coa_lineage kid=root.getChild(k);
                coa_lineage kidsParentRef=kid.getParentRef();
                if(kidsParentRef!=root)
                    {   
                    throw new RuntimeException("Error bad parent ref!");
                    }
                parentRefVerify(kid,false);
                }
            }
        }
    
    
    public void setTagOnThisAnAllNodesUnderThisTree(String newTag)
        {
        this.setTag(newTag);
        if(this.coa_kids!=null)
            {
            for(coa_lineage c:this.coa_kids)
                {
                c.setTagOnThisAnAllNodesUnderThisTree(newTag);
                }
            }
        }
    
    
    public static coa_lineage makeCME()
        {
        coa_lineage one=new coa_lineage("1");
        coa_lineage two=new coa_lineage("2");
        coa_lineage three=new coa_lineage("3");
        coa_lineage four=new coa_lineage("4");
        coa_lineage five=new coa_lineage("5");
        coa_lineage seven=new coa_lineage("7");
        seven.addChild(one,0.2);
        seven.addChild(two,0.2);
        coa_lineage eight=new coa_lineage("8");
        eight.addChild(four,0.2);
        eight.addChild(five,0.2);
        coa_lineage six=new coa_lineage("6");
        six.addChild(seven,0.1);
        six.addChild(three,0.2);
        coa_lineage root=new coa_lineage("0");
        root.addChild(six,0.1);
        root.addChild(eight,0.1);
        return root;
        }
    
    
    public static final String phyloXMLBeginning="<phyloxml xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.phyloxml.org\" xsi:schemaLocation=\"http://www.phyloxml.org http://www.phyloxml.org/1.10/phyloxml.xsd\">";
    
    public String makePhyloXML(boolean isRoot,double length_to_node,boolean useLength,BigDecimal logLikelihood)
        {
        String phyloXML="";
        //header for root case
        if(isRoot)
            {
            phyloXML=new String(phyloXMLBeginning);
            phyloXML+="<phylogeny rooted=\"true\">";
            }
        //open clade
        phyloXML+="<clade>";
        
        
        String thisName=this.getName();
        thisName=thisName.replace("&"," ");
        if(this.getNumKids()!=0)
            {
            thisName="";
            }
        int thisMergerStyle=this.getMergerStyle();
        String thisTag=this.getTag();
        String jsonXMLName="{";
        jsonXMLName+="\"name\":\""+thisName+"\"";
        if(thisMergerStyle>=0)
            {
            jsonXMLName+=",\"merger_style\":\""+thisMergerStyle+"\"";
            }
        if(thisTag!=null)
            {
            jsonXMLName+=",\"coa_tag\":\""+thisTag+"\"";
            }
        ArrayList<Double> thisKidsDists=this.coa_kids_dists;
        if(thisKidsDists!=null)
            {
            jsonXMLName+=",\"coa_kids_dists\":"+thisKidsDists.toString();
            }
        jsonXMLName+="}";
        phyloXML+="<name>"+jsonXMLName+"</name>";
        
        //write branch_length
        if(useLength)
            {
            phyloXML+="<branch_length>"+length_to_node+"</branch_length>";
            //<branch_length>0.06584</branch_length>
            }              
        
        //recurse here
        for(int k=0;k<this.getNumKids();k++)
            {
            phyloXML+=this.getChild(k).makePhyloXML(false,this.getDist(k),true,null);
            }
        
        //close clade
        phyloXML+="</clade>";
        //close root if necessary
        if(isRoot)
            {
            if(logLikelihood!=null)
                {
                phyloXML+="<property datatype=\"xsd:double\" ref=\"phy:Phylogeny\" applies_to=\"phylogeny\">";
                phyloXML+=logLikelihood.toString();
                phyloXML+="</property>";
                }
            phyloXML+="</phylogeny></phyloxml>";
            }
        return phyloXML;
        }
    
    public static coa_lineage parsePhyloXML(File phyloXML) throws Exception
        {
        Phylogeny[] myPhylogenies=readPhylogenies(phyloXML);
        if(myPhylogenies.length!=1)
            {
            throw new RuntimeException("Error, number of phylogenies encountered during XML parsing : "+myPhylogenies.length+", but expected exactly 1!");
            }
        Phylogeny myPhylogeny=myPhylogenies[0];
        PhylogenyNode foresterRoot=myPhylogeny.getRoot();
        coa_lineage root=foresterNodeToCoaLineage(foresterRoot);
        int numLeavesUnder=root.getNumLeavesUnder(true);
        //System.out.println("The num leaves under root is "+numLeavesUnder);
        return root;
        }
    
    
    private static coa_lineage foresterNodeToCoaLineage(PhylogenyNode foresterRoot)
        {
        String nameJson=foresterRoot.getName();
        //System.out.println("Got a forester JSON name : "+nameJson);
        JSONObject myCOMETJson=new JSONObject(nameJson);
        Object cometName=myCOMETJson.getString("name");
        coa_lineage root=new coa_lineage(cometName.toString());
        //do TAG
        if(myCOMETJson.has("coa_tag"))
            {
            String cometTag=myCOMETJson.getString("coa_tag");
            root.setTag(cometTag.toString());
            }

        //do MERGER STYLE
        if(myCOMETJson.has("merger_style"))
            {
            Object mergerStyle=myCOMETJson.optString("merger_style");
            String mergerStyleStr=mergerStyle.toString();
            int merger_style=Integer.parseInt(mergerStyleStr);
            if(merger_style!=coa_lineage.KINGMAN_STYLE && merger_style!=coa_lineage.SRE_STYLE && merger_style!=(-1))
                {
                throw new RuntimeException("Error, invalid merger style : "+
                        merger_style+" retrieved from phyloXML from forester node with JSON "+nameJson);
                }
            else
                {
                root.setMergerStyle(merger_style);
                }
            }
        
        //acquire child data
        JSONArray coa_kids_dists=null;
        if(myCOMETJson.has("coa_kids_dists"))
            {
            coa_kids_dists=myCOMETJson.getJSONArray("coa_kids_dists");
            //System.out.println("the coa_kids_dists IS a JSON array!");
            JSONArray cda=(JSONArray)coa_kids_dists;
            double[] newDists=new double[cda.length()];
            for(int i=0;i<newDists.length;i++)
                {
                newDists[i]=cda.getDouble(i);
                }
            assert(foresterRoot.getNumberOfDescendants()==newDists.length);
            for(int d=0;d<foresterRoot.getNumberOfDescendants();d++)
                {
                PhylogenyNode indexedKid=foresterRoot.getChildNode(d);
                coa_lineage newChild=foresterNodeToCoaLineage(indexedKid);
                root.addChild(
                        newChild,
                        newDists[d]
                    );
                }
            root.setName("");
            }
        else
            {
            //System.out.println("the coa_kids_dists is NOT a JSON array!");
            assert(foresterRoot.getNumberOfDescendants()==0);
            }

        return root;
        }
    
    
    
    public ArrayList<coa_lineage> getNodesWithTagMatching(String s,boolean recurseAfterSuccess)
        {
        Pattern p=Pattern.compile("^"+s+"$");
        return getNodesWithTagMatching(p,recurseAfterSuccess);
        }
    
    
    public coa_lineage getOneNodeMatchingTag(String s)
        {
        String thisTag=this.getTag();
        int numKids=this.getNumKids();
        if(thisTag!=null && numKids>0)
            {
            if(s.equalsIgnoreCase(thisTag))
                {
                return this;
                }
            else
                {
                coa_lineage firstChild=this.getChild(0);
                return firstChild.getOneNodeMatchingTag(s);
                }
            }
        else if( 
                (thisTag==null && numKids>0)
                ||
                (thisTag!=null && numKids==0)
                ) 
            {
            throw new RuntimeException("Error found node with bad combination of tag and kid count!");
            }
        return null;
        }
    
    public ArrayList<coa_lineage> getNodesWithTagMatching(Pattern p,boolean recurseAfterSuccess)
        {
        ArrayList<coa_lineage> matchingNodes=new ArrayList<coa_lineage>();
        String thisTag=this.getTag();
        boolean thisNodeMatched=false;
        if(thisTag!=null)
            {
            Matcher m=p.matcher(thisTag);
            if(m.find())
                {
                matchingNodes.add(this);
                thisNodeMatched=true;
                }
            }

        boolean recurseFlag= (!thisNodeMatched)  || (thisNodeMatched && recurseAfterSuccess);
        if(recurseFlag)
            {
            if(this.coa_kids!=null)
                {
                for(coa_lineage k:this.coa_kids)
                    {
                    if(k!=null)
                        {
                        matchingNodes.addAll(k.getNodesWithTagMatching(p,recurseAfterSuccess));                    
                        }
                    }
                }
            }

        return matchingNodes;
        }

    private String coa_tag;
    private String coa_name;
    private ArrayList<coa_lineage> coa_kids=null;
    private ArrayList<Double> coa_kids_dists=null;
    private HashMap post_generation_sample_map=null;
    
    public int merger_style=(-1);
    public static final int KINGMAN_STYLE=0;
    public static final int SRE_STYLE=1;
    public static int[] validStyles={KINGMAN_STYLE,SRE_STYLE};
    
    public int getMergerStyle()
        {
        return this.merger_style;
        }
    
    public static String getMergerStyleAsString(int s)
        {
        if(s==coa_lineage.KINGMAN_STYLE)
            {
            return "KINGMAN_STYLE";
            }
        else if(s==coa_lineage.SRE_STYLE)
            {
            return "SRE_STYLE";
            }
        else
            {
            return "UNDEFINED";
            }
        }
    
    public void setMergerStyle(int newStyle)
        {
        if(newStyle!=KINGMAN_STYLE && newStyle!=SRE_STYLE)
            {
            throw new RuntimeException("ERROR, invalid merger style : "+newStyle);
            }
        this.merger_style=newStyle;
        }    
    
    public coa_lineage replaceChild(coa_lineage replacement,double replacmentDist,int kidIndex)
        {
        if(kidIndex<0)
            {
            throw new ArrayIndexOutOfBoundsException("Error, no negative indices ("+kidIndex+") allowed !");
            }
        if(this.coa_kids==null)
            {
            throw new NullPointerException("Error, can't replace a non-existent child when coa_kids is null!");
            }
        else if(kidIndex>=this.coa_kids.size())
            {
            throw new ArrayIndexOutOfBoundsException("Error, index ("+
                    kidIndex+") when size is "+this.coa_kids.size()+" not allowed!");
            }
        else
            {
            coa_lineage kidToReturn=this.coa_kids.get(kidIndex);
            this.coa_kids.set(kidIndex, replacement);
            this.coa_kids_dists.set(kidIndex, replacmentDist);
            return kidToReturn;
            }
            
        }
    
    
    public void dropAllChildren()
        {
        this.coa_kids=null;
        this.coa_kids_dists=null;
        this.resetNewick();
        }
    
    
    public ArrayList<String> getNamesOfTipsInSubtreeRootedHere()
        {
        ArrayList<String> tbr=new ArrayList<String>();
        if(this.coa_kids==null)
            {
            if(this.coa_name==null)
                {
                throw new NullPointerException("Error, tip names should not be null!");
                }
            String tipName=new String(this.coa_name);
            tbr.add(tipName);
            return tbr;
            }
        else
            {
            for(int k=0;k<this.coa_kids.size();k++)
                {
                ArrayList<String> recList=null;
                recList=this.coa_kids.get(k).getNamesOfTipsInSubtreeRootedHere();
                tbr.addAll(recList);
                }
            Collections.sort(tbr);
            return tbr;
            }
        }
    
    
    public int getKidIndex(coa_lineage kid)
        {
        if(this.coa_kids==null)
            {
            //return -1 if no kids
            return (-1);
            }
        else
            {
            for(int k=0;k<this.coa_kids.size();k++)
                {
                if(this.coa_kids.get(k)==kid)
                    {
                    return k;
                    }
                }
            //if never found the kid return -1
            return (-1);
            }
            
        }
    
    

    
    
    public boolean doesEdgeToKidTraverseHeight(double h,int index)
        {
        if(this.coa_kids_dists==null)
            {
            return false;
            }
        double kid_height=this.coa_kids.get(index).getHeight();
        double c_h=kid_height+this.coa_kids_dists.get(index);
        if(kid_height<=h && h<=c_h)
            {
            return true;
            }
        else
            {
            return false;
            }        
        }
    
    
    public int selectIndexOfKidBranchCrossingBoundary(double heightVal)
        {
        if(this.coa_kids!=null)
            {
            ArrayList<Integer> validIndices=new ArrayList<Integer>();
            double thisHeight=this.getHeight();
            for(int k=0;k<this.coa_kids.size();k++)
                {
                double kid_height=this.coa_kids.get(k).getHeight();
                if(TreeUtils.isBetween(thisHeight,heightVal,kid_height,false))
                    {
                    validIndices.add(k);
                    }
                }
            int i_index=TreeUtils.getRandomInt(0,validIndices.size()-1);
            return validIndices.get(i_index);
            }
        else
            {
            throw new RuntimeException("Error 2, a node should have at least one qualifying child!");
            }
        }
    
    public ArrayList<coa_lineage> findAllNodesWithAtLeastOneChildEdgeCrossingHeightValue(double heightVal)
        {
        ArrayList<coa_lineage> nodesOfInterest=new ArrayList<coa_lineage>();
        if(this.coa_kids!=null)
            {
            double thisHeight=this.getHeight();
            if(thisHeight<heightVal)
                {
                return nodesOfInterest;
                }
            boolean thisNodeAdded=false;
            for(int i=0;i<this.coa_kids.size();i++)
                {
                double kid_height=this.coa_kids.get(i).getHeight();
                if(!thisNodeAdded && TreeUtils.isBetween(thisHeight,heightVal,kid_height,false))
                    {
                    nodesOfInterest.add(this);
                    thisNodeAdded=true;
                    }
                }
            for(int i=0;i<this.coa_kids.size();i++)
                {
                ArrayList<coa_lineage> rec_list=new ArrayList<coa_lineage>();
                rec_list=this.coa_kids.get(i).findAllNodesWithAtLeastOneChildEdgeCrossingHeightValue(heightVal);
                nodesOfInterest.addAll(rec_list);
                }
            return nodesOfInterest;
            }
        else
            {
            return nodesOfInterest;
            }            
        }
    
    public coa_lineage findTheParentOfGivenNode(coa_lineage given)
        {
        return given.getParentRef();
        /* 
        //start from the root and recurse looking if a child
        //is "given"
        if(this==given)
            {
            //must be descendant!
            return null;
            }
        else
            {
            if(this.coa_kids!=null)
                {
                for(int k=0;k<this.coa_kids.size();k++)
                    {
                    if(this.coa_kids.get(k)==given)
                        {
                        //if this has given as a kid, return it
                        return this;
                        }
                    coa_lineage temp=this.coa_kids.get(k).findTheParentOfGivenNode(given);
                    if(temp!=null)
                        {
                        //if a recursrive call is non-null, return it
                        return temp;
                        }
                    }
                return null;                    
                }
            else
                {
                return null;
                }
            }*/         
        }
    
    
    public Object[] chooseRandomNonTipInternalNode()
        {
        //step 1, get internal nodes
        ArrayList<coa_lineage> int_nodes=this.returnInternalNodes();
        
        //choose a random index to select a random internal node
        int random_index=TreeUtils.getRandomInt(0,int_nodes.size()-1);
        coa_lineage randomInternalNode=int_nodes.get(random_index);
        
        //choose a random child
        int random_kid_index=TreeUtils.getRandomInt(0,1);

        //package up the values for return!
        Object[] retPackage=new Object[2];
        retPackage[0]=randomInternalNode;
        retPackage[1]=random_kid_index;
        return retPackage;
        }
    
    private ArrayList<coa_lineage> returnInternalNodes()
        {
            
        ArrayList<coa_lineage> int_nodes=new ArrayList<coa_lineage>();
        
        if(this.coa_kids!=null)
            {
            for(int i=0;i<this.coa_kids.size();i++)
                {
                if(coa_kids.get(i).getNumKids()>0)
                    {
                    int_nodes.add(coa_kids.get(i));
                    ArrayList<coa_lineage> int_nodes_rec=coa_kids.get(i).returnInternalNodes();
                    int_nodes.addAll(int_nodes_rec);
                    }
                }
            }
        return int_nodes;
        }

    public coa_lineage getChild(int child_index)
        {
        if(child_index<0)
            {
            return null;
            }
        else if(this.coa_kids==null)
            {
            return null;
            }
        else
            {
            if(child_index<this.coa_kids.size())
                {
                //in range
                return this.coa_kids.get(child_index);
                }
            else
                {
                return null;
                }
            }
        }
    
    
    public coa_lineage cloneRec()
        {
        //first clone non-rec
        String clone_coa_name=null;
        if(this.coa_name!=null)
            {
            clone_coa_name=new String(this.coa_name);
            }
        HashMap clone_post_gen_sample_map=TreeUtils.cloneMap(this.post_generation_sample_map);
        
        //clone rec dists
        ArrayList<Double> cloned_dists=null;
        if(this.coa_kids_dists!=null)
            {
            cloned_dists=new ArrayList<Double>();
            for(int i=0;i<this.coa_kids_dists.size();i++)
                {
                Double cloned_double=(Double)(TreeUtils.cloneObj(this.coa_kids_dists.get(i)));
                cloned_dists.add(cloned_double);
                }
            }
        
        //clone the kids
        ArrayList<coa_lineage> cloned_coa_kids=null;
        if(this.coa_kids!=null)
            {
            cloned_coa_kids=new ArrayList<coa_lineage>();
            for(int k=0;k<this.coa_kids.size();k++)
                {
                coa_lineage cloned_lineage=this.coa_kids.get(k).cloneRec();
                cloned_coa_kids.add(cloned_lineage);
                }
            }
        
        
        //return 'this' clone
        coa_lineage cloned_base=new coa_lineage(clone_coa_name);
        cloned_base.setFastaIORef(this.fioRef);
        if(this.getMergerStyle()!=(-1))
            {
            cloned_base.setMergerStyle(this.getMergerStyle());
            }
        cloned_base.setTag(this.getTag());
        cloned_base.post_generation_sample_map=clone_post_gen_sample_map;
        if(cloned_coa_kids!=null)
            {
            for(int ck=0;ck<cloned_coa_kids.size();ck++)
                {
                cloned_base.addChild(cloned_coa_kids.get(ck),
                        cloned_dists.get(ck));
                }
            }

        
        
        
        return cloned_base;
        }
    
    
    
    public HashMap getPosteriorSampleMap()
        {
        return post_generation_sample_map;
        }
    
    public void setPosteriorSampledValue(Object key,Object val)
        {
        if(this.post_generation_sample_map==null)
            {
            this.post_generation_sample_map=new HashMap();
            }
        this.post_generation_sample_map.put(key, val);
        }
    
    
    public int chooseRandomKidIndex()
        {
        if(this.coa_kids==null)
            {
            throw new IllegalArgumentException("Error, trying to choose random kid index with list being null!");
            }
        else if(this.coa_kids.isEmpty())
            {
            throw new IllegalArgumentException("Error, trying to choose random kid index with list being empty!");
            }
        else
            {
            int numKids=this.coa_kids.size();
            return TreeUtils.chooseRandomIndex(numKids);
            }
        }
    
    public void resetPosteriorSampledValues()
        {
        this.post_generation_sample_map=null;
        }
    
    
    /*
    given a map, merge the poster key->value pairs
    to the given map and return the given map after
    this merge has been done
    */
    public HashMap addPosteriorSampledToMap(HashMap toAddTo)
        {
        HashMap msm=this.post_generation_sample_map;
        if(msm==null)
            {
            return toAddTo;
            }
        else
            {
            for(Object key:msm.keySet())
                {
                Object value=msm.get(key);
                toAddTo.put(key, value);
                }
            return toAddTo;
            }
        }
    
    
    public int getNumKids()
        {
        if(coa_kids==null && coa_kids_dists==null)
            {
            return 0;
            }
        if(
                (coa_kids==null && coa_kids_dists!=null) ||
                (coa_kids!=null && coa_kids_dists==null)
                )
            {
            throw new RuntimeException("Error, kids==null and dists!=null or vice-versa!?");
            }
                
        int nd=this.coa_kids.size();
        int nk=this.coa_kids_dists.size();
        if(nd==nk)
            {
            return nd;
            }
        else
            {
            throw new RuntimeException("Error, nd="+nd+" and nk="+nk+", MISMATCH!");
            }
        }
    
    
    public void resetNewick()
        {
        this.newickRepresentation=null;
        }
    
    
    public boolean equalInTermsOfLeaves(coa_lineage other)
        {
        ArrayList<String> theseTips=this.getNamesOfTipsInSubtreeRootedHere();
        ArrayList<String> thoseTips=other.getNamesOfTipsInSubtreeRootedHere();
        theseTips.sort(null);
        thoseTips.sort(null);
        if(theseTips.size()!=thoseTips.size())
            {
            return false;
            }
        for(int i=0;i<theseTips.size();i++)
            {
            if(theseTips.get(i).compareToIgnoreCase(thoseTips.get(i))!=0)
                {
                return false;
                }
            }
        return true;
        }
    
    public boolean hasExtraNodes(boolean isRoot)
        {
        if(this.isThisExtraNode(isRoot))
            {
            return true;
            }
        else
            {
            if(this.coa_kids==null)
                {
                return false;
                }
            for(coa_lineage k:this.coa_kids)
                {
                if(k.isThisExtraNode(false))
                    {
                    return true;
                    }
                if(k.hasExtraNodes(false))
                    {
                    return true;
                    }
                }
            }
        return false;
        }
    
    
    public void setFastaIORef(FastaIO fio)
        {
        fioRef=fio;
        }
    
    public String makeNewickString(boolean useSeqNames,FastaIO fio)
        {
        if(fioRef==null)
            {
            fioRef=fio;
            }
        coa_lineage a_clone=this.cloneRec();
        while(a_clone.hasExtraNodes(true))
            {
            a_clone.removeExtraNodes();
            }
        a_clone.makeBinary();
        return a_clone.makeNewickStringRec(true, useSeqNames, fio);
        /*if(newickRepresentation==null)
            {
            newickRepresentation=this.makeNewickStringRec(true,useSeqNames,fio);    
            }
        return newickRepresentation;*/        
        }
    

    private String newickRepresentation=null;

    private FastaIO fioRef=null;
    
    private String makeNewickStringRec(boolean isRoot,boolean useSeqNames,FastaIO fio)
        {
        if(fioRef==null)
            {
            fioRef=fio;
            }

        if(this.getNumKids()==0)
            {
            if(!useSeqNames)
                {
                return this.getName();
                }
            else
                {
                return fio.getSeqNameByID(Integer.parseInt(this.getName())-1);
                }
            }
        else
            {
            String newick="(";
            for(int k=0;k<this.getNumKids();k++)
                {
                newick+=this.coa_kids.get(k).makeNewickStringRec(false,useSeqNames,fio);
                newick+=":"+this.coa_kids_dists.get(k).toString();
                if(k<this.getNumKids()-1)
                    {
                    newick+=",";
                    }
                }
            newick+=")";
            if(isRoot)
                {
                newick+=";";
                }
            return newick;
            }
            
        
        }
    

    
    
    
    
    public int getNumInternalBranches()
        {
        int count=0;

        if(this.coa_kids!=null)
            {
            for(int k=0;k<this.coa_kids.size();k++)
                {
                count++;
                count+=this.coa_kids.get(k).getNumInternalBranches();
                }                
            }
        return count;            
        }
    
    
    public boolean isRootOfBinary()
        {
        //require NO Extra NODES!
        if(this.coa_kids==null)
            {
            return true;
            }
        if(this.coa_kids.size()==0)
            {
            return true;
            }
        else if(this.coa_kids.size()!=2)
            {
            System.out.println("Returning false in irob because kids!=2!");
            System.out.println("Kids="+this.coa_kids.size());
            return false;
            }
        boolean firstIsBinary=this.coa_kids.get(0).isRootOfBinary();
        boolean secondIsBinary=this.coa_kids.get(1).isRootOfBinary();
        if(firstIsBinary && secondIsBinary)
            {
            return true;
            }
        else
            {
            System.out.println("Returning false in irob because recursive call.");
            return false;
            }

        }
    
    
    public void makeBinary()
        {
        if(this.coa_kids==null)
            {
            return;
            }
           
        if(this.coa_kids.size()==2)
            {
            for(coa_lineage c_l:coa_kids)
                {
                c_l.makeBinary();
                }
            return;
            }
        else if(this.coa_kids.size()==1)
            {
            //System.out.println("Have a coa_lineage : ");
            //System.out.println(this.toString());
            throw new RuntimeException("Error, number kids==1  in call to makeBinary!");
            }
        else  if(this.coa_kids.size()==0)
            {
            return ;
            }

        
        
        //add internal 'holder' node
        ArrayList<coa_lineage> new_kids=new ArrayList<coa_lineage>();
        ArrayList<Double> new_kids_dists=new ArrayList<Double>();
        new_kids.add(coa_kids.get(0));
        new_kids_dists.add(coa_kids_dists.get(0));
        coa_lineage holder=new coa_lineage("holder_under_"+this.getName());
        for(int k=1;k<coa_kids.size();k++)
            {
            holder.addChild(coa_kids.get(k),coa_kids_dists.get(k));
            }
        new_kids.add(holder);
        new_kids_dists.add(0.0);
        coa_kids=new_kids;
        coa_kids_dists=new_kids_dists;
        
        coa_kids.get(0).makeBinary();
        coa_kids.get(1).makeBinary();
        
        
        }
    
    public double getDistToKid(int kidIndex)
        {
        return this.coa_kids_dists.get(kidIndex);
        }
    
    public double getDistToKid(coa_lineage kid)
        {
        int kidIndex=this.getKidIndex(kid);
        return getDistToKid(kidIndex);
        }
    
    
    public Object[]  get_immediate_kids_and_dists()
        {
        Object[] p=new Object[]{this.coa_kids,this.coa_kids_dists};
        return p;
        }
    
    public Object[] get_immediate_kids_and_dists_arr()
        {
        if(this.coa_kids==null || this.coa_kids_dists==null)
            {
            //throw new NullPointerException("Erro, NPE kids=="+this.coa_kids+", dists="+this.coa_kids_dists);
            coa_lineage[] cl_arr=new coa_lineage[0];
            double[] d_arr=new double[0];
            Object[] rp=new Object[2];
            rp[0]=cl_arr;
            rp[1]=d_arr;
            return rp;                
            }
        else
            {
            coa_lineage[] cl_arr=new coa_lineage[this.coa_kids.size()];
            double[] d_arr=new double[this.coa_kids.size()];
            for(int i=0;i<d_arr.length;i++)
                {
                cl_arr[i]=this.coa_kids.get(i);
                d_arr[i]=this.coa_kids_dists.get(i);
                }
            Object[] rp=new Object[2];
            rp[0]=cl_arr;
            rp[1]=d_arr;
            return rp;
            }
        }
    
    
    public boolean changeNameRec(String oldName,String newName)
        {
        boolean flag=false;
        if(this.coa_name.compareTo(oldName)==0)
            {
            flag=true;
            this.coa_name=newName;
            }
        if(coa_kids==null)
            {
            return flag;
            }
        for(int k=0;k<coa_kids.size();k++)
            {
            boolean reck=coa_kids.get(k).changeNameRec(oldName, newName);
            if(reck||flag)
                {
                flag=true;
                }
            }
            
        return flag;
        }
    
    
    
    private boolean isIthKidExtraNode(int i)
        {
            if(this.coa_kids==null)
                {
                return false;
                }
            if(this.coa_kids.size()<=i)
                {
                return false;
                }
            coa_lineage ithKid=this.coa_kids.get(i);
            if(ithKid.coa_kids==null)
                {
                return false;
                }
            if(ithKid.coa_kids.size()==1)
                {
                    return true;
                }
            else
                {
                    return false;
                }            
        }

    
    public boolean testRecIfExtraNodes()
        {
        if(this.coa_kids==null)
            {
            return false;
            }
        else if(this.coa_kids.size()==1)
            {
            return true;
            }
        if(this.coa_kids.size()==0)
            {
            return false;
            }
        else
            {
            for(coa_lineage c:this.coa_kids)
                {
                boolean rr=c.testRecIfExtraNodes();
                if(rr)
                    {
                    return true;
                    }
                }
            }
        return false;        
        }
    
    public ArrayList<coa_lineage> removeAllKids()
        {
        ArrayList<coa_lineage> the_kids=this.coa_kids;
        this.coa_kids=null;
        this.coa_kids_dists=null;
        return the_kids;        
        }
    
    
    public void removeChild(coa_lineage c)
        {
        if(this.coa_kids==null)
            {
            return;
            }
        else 
            {
            for(int i=0;i<coa_kids.size();i++)
                {
                if(this.coa_kids.get(i)==c)
                    {
                    this.removeChild(i);
                    return;
                    }
                }
            }
        }
    
    
    public void removeChild(int kid_index_to_remove)
        {
        String oob_err_msg="Error, trying to remove kid at index="+kid_index_to_remove;
        if(this.coa_kids!=null)
            {
            if(0<=kid_index_to_remove && kid_index_to_remove<this.coa_kids.size())
                {
                //in bounds
                ArrayList<coa_lineage> newKids=new ArrayList<coa_lineage>();
                ArrayList<Double> newDists=new ArrayList<Double>();
                for(int i=0;i<this.coa_kids.size();i++)
                    {
                    if(i!=kid_index_to_remove)
                        {
                        newKids.add(this.coa_kids.get(i));
                        newDists.add(this.coa_kids_dists.get(i));
                        }
                    }
                this.coa_kids=newKids;
                this.coa_kids_dists=newDists;
                if(this.coa_kids.size()==0 && this.coa_kids_dists.size()==0)
                    {
                    this.coa_kids=null;
                    this.coa_kids_dists=null;
                    }
                }
            else
                {
                oob_err_msg+="\nBut size of coa_kids is "+this.coa_kids.size();
                throw new ArrayIndexOutOfBoundsException(oob_err_msg);
                }
            }
        else
            {
            if(kid_index_to_remove>=0)
                {
                oob_err_msg+="\nBut coa_kids is null!";
                throw new ArrayIndexOutOfBoundsException(oob_err_msg);
                }
            }
  
            
        }
    
    
    public void removeExtraNodes()
        {
        this.removeExtraNodes(true);
        this.removeExtraNodes(true);
        this.removeExtraNodes(true);
        this.removeExtraNodes(true);
        }
    
    
    private void removeExtraNodes(boolean isRoot)
        {
        if(this.coa_kids==null && this.coa_kids_dists==null)
            {
            //nothing to do!
            return;
            }
        else if(
                (this.coa_kids!=null && this.coa_kids_dists==null)
                ||
                (this.coa_kids==null && this.coa_kids_dists!=null)
                )
            {
            //should both be null together or be non-null togeter!
            throw new RuntimeException("should both be null together or be non-null togeter!");
            }
        ArrayList<coa_lineage> newKids=new ArrayList<coa_lineage>();
        ArrayList<Double> newKidsDists=new ArrayList<Double>();
        
        for(int i=0;i<this.coa_kids.size();i++)
            {
            if(this.isIthKidExtraNode(i))
                {
                double nearDist=this.coa_kids_dists.get(i);
                double farDist=this.coa_kids.get(i).coa_kids_dists.get(0);
                double newDist=nearDist+farDist;
                coa_lineage grandKid=this.coa_kids.get(i).coa_kids.get(0);
                coa_lineage newKid=grandKid;
                newKids.add(newKid);
                newKidsDists.add(newDist);
                }
            else
                {
                newKids.add(coa_kids.get(i));
                newKidsDists.add(coa_kids_dists.get(i).doubleValue());
                }
            }
        this.coa_kids=newKids;
        this.coa_kids_dists=newKidsDists;
        for(int i=0;i<coa_kids.size();i++)
            {
            coa_kids.get(i).removeExtraNodes(false);
            }
            
            
        }
    
    
    
    public boolean isThisExtraNode(boolean isRoot)
        {
            if(isRoot)
                {
                    return false;
                }
            if(this.coa_kids==null)
                {
                return false;
                }
            if(this.coa_kids.size()==1)
                {
                return true;
                }
            else
                {
                return false;
                }
        }
    
    public ArrayList<coa_lineage> returnNodesHavingExactly2KidsInSubtreeRootedHere()
        {
        ArrayList<coa_lineage> tbr=new ArrayList<coa_lineage>();
        if(this.getNumKids()==2)
            {
            tbr.add(this);
            for(int k=0;k<2;k++)
                {
                ArrayList<coa_lineage> rec=this.coa_kids.get(k).returnNodesHavingExactly2KidsInSubtreeRootedHere();
                tbr.addAll(rec);
                }
            }
        
        return tbr;
        }
    
    public static ArrayList<coa_lineage> sortByHeightInPlace(ArrayList<coa_lineage> input,boolean asc)
        {
        if(input==null)
            {
            return null;
            }
        if(input.size()<=1)
            {
            return input;
            }
        else
            {
            for(int i=0;i<input.size()-1;i++)
                {
                for(int j=i+1;j<input.size();j++)
                    {
                    boolean requireSwap=false;
                    double ih,jh;
                    ih=input.get(i).getHeight();
                    jh=input.get(j).getHeight();
                    if(asc)
                        {
                        if(ih>jh)
                            {
                            requireSwap=true;
                            }
                        }
                    else
                        {
                        if(ih<jh)
                            {
                            requireSwap=true;
                            }
                        }
                    if(requireSwap)
                        {
                        coa_lineage temp=input.get(i);
                        input.set(i,input.get(j));
                        input.set(j,temp);
                        }
                    }//j
                }//i
            return input;
            }//>2 kids
        }
    
        
    public ArrayList<coa_lineage> get_descendants(boolean andRoot)
        {

            ArrayList<coa_lineage> myDescendants=new ArrayList<coa_lineage>();
            if(andRoot)
                {
                myDescendants.add(this);
                }
            
            if(this.coa_kids==null)
                {
                //if no kids return an empty list
                }
            else
                {
                //if there are kids, add them to the descendant list, next add their descendants
                myDescendants.addAll(this.coa_kids);
                for(int k=0;k<this.coa_kids.size();k++)
                    {
                    myDescendants.addAll(coa_kids.get(k).get_descendants(false));
                    }
                }
        return myDescendants;
        }
    
    
    public String toString()
        {
            double h=this.getHeight();
            if(coa_kids==null)
                {
                return coa_name+":"+h;
                }
            else if(coa_kids.size()==0)
                {
                return coa_name+":"+h;
                }
            else
                {
                    String n="Internal node '"+this.getName()+"' : (";
                    for(int i=0;i<coa_kids.size();i++)
                        {
                            n+=coa_kids.get(i).toString();
                            if(i<coa_kids.size()-1)
                                {
                                    n+=" , ";
                                }
                        }
                    n+="):"+h;
                    return n;
                }
        }

    
    
    //includes the leaf of self/this
    public int getNumLeavesUnder(boolean assertToBeRoot)
        {
        if(assertToBeRoot)
            {
            if(this.fioRef!=null)
                {
                return this.fioRef.getNumSeqs();
                }
            }

        if(coa_kids==null)
            {
            //return 1 for self
            return 1;
            }
        else
            {
            int sum=0;
            for(int k=0;k<coa_kids.size();k++)
                {
                int tempKids=coa_kids.get(k).getNumLeavesUnder(false);
                sum+=tempKids;
                }
            return sum;
            }
        }
    
    public void recTipSwap(HashMap swapMap)
        {
        //reset newick so that post-swapping accesses a new newick!
        this.resetNewick();
        if(swapMap.containsKey(this.getName()))
            {
            //use the mapping too change the name from->to with key->value
            this.coa_name=swapMap.get(this.getName()).toString();
            }
        if(this.coa_kids!=null)
            {
            for(coa_lineage k:this.coa_kids)
                {
                if(k!=null)
                    {
                    k.recTipSwap(swapMap);
                    }
                }
            }            
        }
    
    
    private coa_lineage parentRef=null;
    
    public void resetParentRef()
        {
        this.parentRef=null;
        }
       
    public coa_lineage getParentRef()
        {
        return this.parentRef;
        }
    
    public void addChild(coa_lineage child,double childDist)
        {
        child.parentRef=this;
        if(coa_kids==null)
            {
              coa_kids=new ArrayList<coa_lineage>();
              coa_kids_dists=new ArrayList<Double>();
            }
        coa_kids.add(child);
        coa_kids_dists.add(new Double(childDist));
        }
    
    public void setAllDists(double newDist)
        {
        if(this.coa_kids_dists!=null)
            {
            for(int d=0;d<coa_kids_dists.size();d++)
                {
                this.coa_kids_dists.set(d, newDist);
                }
            }
        }
    
    
    public void setDist(int kidNum,double newDist)
        {
        this.coa_kids_dists.set(kidNum,newDist);
        }
    
    public double getDist(int kidNum)
        {
        return this.coa_kids_dists.get(kidNum).doubleValue();
        }
    

    
    
    
    public boolean verifyKidsAndDistsConsistency()
        {
        if(this.coa_kids==null && this.coa_kids_dists==null)
            {
            return true;
            }
        else if(this.coa_kids!=null && this.coa_kids_dists==null)
            {
            System.out.println("kids is non-null but dists is null");
            return false;
            }
        else if(this.coa_kids==null && this.coa_kids_dists!=null)
            {
            System.out.println("kids is null but dists is non-null");
            return false;
            }
        else
            {
            if(this.coa_kids.size()!=this.coa_kids_dists.size())
                {
                System.out.println("kids size is "+this.coa_kids.size()+" but dists size is "+this.coa_kids_dists.size());
                return false;
                }
            if(this.coa_kids.size()==0 || this.coa_kids_dists.size()==0)
                {
                System.out.println("kids size is "+this.coa_kids.size()+" and dists size is "+this.coa_kids_dists.size());    
                return false;
                }
            return true;
            }
          
        }
    
    
    
    public boolean verifyHeights(boolean debug)
        {
        boolean kdC=this.verifyKidsAndDistsConsistency();
        if(!kdC)
            {
            throw new RuntimeException("Error, kids dist consistency!");
            }
        if(debug)
            System.out.println("Verifying height for node named "+
                    this.getName()+
                    " which has "+this.getNumLeavesUnder(false)+" leaves");


        if(!(this.coa_kids_dists==null))
            {
            for(int d=0;d<coa_kids_dists.size();d++)
                {
                double a_dist=coa_kids_dists.get(d);
                if(a_dist<0)
                    {
                    System.out.println("Found a negative distance : d="+d+", val="+a_dist);
                    }
                }
            }


        if(this.coa_kids==null)
            {
            if(debug)
                System.out.println(this.getName()+" has no kids so it's height is zero verified!");
            return true;
            }//null kids
        else
            {
            double h=0;
            double firstHeight=getDist(0)+coa_kids.get(0).getHeight();
            for(int k=0;k<this.coa_kids.size();k++)
                {
                System.out.println("The tag of 'this' node is "+this.getTag());
                System.out.println("The merger style of this node is "+this.getMergerStyleAsString(this.getMergerStyle()));
                double thisHeight=getDist(k)+coa_kids.get(k).getHeight();
                if(thisHeight<0)
                    {
                    throw new RuntimeException("Error, detected a negative height!!!!");
                    }
                if(debug)
                    System.out.println("The dist from node='"+
                        this.getName()+"' to its (0-based "+k+"th ) child named '"+
                        coa_kids.get(k).getName()+"' is "+getDist(k));
                if(Math.abs(thisHeight-firstHeight)>=0.00001)
                    {
                    String hm="computed height with k'th kid "+thisHeight+" and computed with first is "+firstHeight+" and dist to First="+getDist(0)+". ";
                    hm+="\n height inconsistency!\n";
                    throw new RuntimeException(hm);
                    //return false;
                    }
                if(!coa_kids.get(k).verifyHeights(debug))
                    {
                    throw new RuntimeException("Error, recursive fail!");
                    }
                }//for each coa_kid
            }//has kids
        return true;
        }

    
    public Object[] acquireNameDistSorted()
        {

        //first acquire map
        HashMap distMap=this.obtainCoalLeafHeightMap();
        
        //use map to acquire sorted name/dist pairing
        Object[] pairing=coa_lineage.acquireNameDistSortedFromDistMap(distMap);
        
        
        //return the pairing
        return pairing;            
        }
    
    
    
    public static Object[] acquireNameDistSortedFromDistMap(HashMap clhMap)
        {
        ArrayList<String> names=new ArrayList<String>();
        ArrayList<Double> dists=new ArrayList<Double>();
        //insert into lists
        for(Object k:clhMap.keySet())
            {
            names.add(k.toString());
            dists.add((Double)(clhMap.get(k)));
            }
        //sort with bubble sort
        for(int i=0;i<names.size()-1;i++)
            {
            for(int j=i+1;j<names.size();j++)
                {
                double i_dist=dists.get(i);
                double j_dist=dists.get(j);
                if(i_dist>j_dist)
                    {
                    //swap

                    //temp data
                    //acquire temp data from i index
                    double temp_dist=i_dist;
                    String temp_name=names.get(i).toString();

                    //overwrite i data with j data
                    names.set(i,names.get(j));
                    dists.set(i,j_dist);
                    
                    //overwrite j data with temp data acquired from i
                    names.set(j, temp_name);
                    dists.set(j,temp_dist);
                    }
                }
            }
        
        Object[] retPack=new Object[2];
        retPack[0]=names;
        retPack[1]=dists;
        return retPack;        
        }
    
    
    
    public boolean isTip()
        {
        if(this.coa_kids==null)
            {
            return true;
            }
        else if(this.coa_kids.size()==0)
            {
            return true;
            }
        else
            {
            return false;
            }
        }
    
        
    
    
    public HashMap obtainCoalLeafHeightMap()
        {
        if(this.coa_kids==null)
            {
            return null;
            }
        else
            {
            HashMap clhMap=new HashMap();
            double this_height=this.getHeight();
            for(coa_lineage kid:this.coa_kids)
                {
                String kid_name=kid.getName();
                if(kid.isTip())
                    {
                    clhMap.put(kid_name,this_height);
                    }
                else
                    {
                    HashMap kidMap=kid.obtainCoalLeafHeightMap();
                    clhMap=TreeUtils.mergeMaps(kidMap, clhMap);
                    }                
                }
            return clhMap;
            }
        }
    
    
    
    public coa_lineage(String name)
        {
        this.coa_tag=null;
        this.merger_style=(-1);
        coa_name=name;
        }
    
    
    public String getName(){
        return this.coa_name;
    }

    public String getTag()
        {
        return this.coa_tag;
        }

    public void setTag(String newTag)
        {
        this.coa_tag=newTag;
        }
    
    public void setName(String newName)
        {
        this.coa_name=newName;
        }
    
    
    public double getHeight()
        {

            if(coa_kids==null)
                {
                    return 0;
                }
            else if(coa_kids.size()==0)
                {
                    return 0;
                }
            else
                {
                double d_1=coa_kids_dists.get(0);
                double rec_height=this.coa_kids.get(0).getHeight();
                return d_1+rec_height;
                }

        }
    
}
