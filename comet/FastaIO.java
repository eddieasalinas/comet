/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.biojava3.core.sequence.ProteinSequence;
import org.biojava3.core.sequence.io.FastaReaderHelper;

/**
 *
 * @author esalina
 */
public class FastaIO {

    String[] seqNames;
    String[] seqs;
    HashMap nameToIDMap=null;
    HashMap segSiteFreqAtPosMap=null;
    HashMap numSegSitesWithGivenFreqMap=null;
    int min_freq=0;
    int max_freq=0;
    File sourceFasta=null;
    double[] piFreqs=null;

    ArrayList<Integer>[] acgtSites=null;
    
    public String getInvBase(int site)
        {
        if(site<0 || site>=seqs[0].length())
            {
            throw new ArrayIndexOutOfBoundsException("");
            }
        if(acgtSites==null)
            {
            acgtSites=getInvariantACGTSites();
            }
        ArrayList<Integer> aSites=acgtSites[0];
        ArrayList<Integer> cSites=acgtSites[1];
        ArrayList<Integer> gSites=acgtSites[2];
        ArrayList<Integer> tSites=acgtSites[3];
        
        if(aSites.contains(site))
            {
            return "A";
            }
        if(cSites.contains(site))
            {
            return "C";
            }
        if(gSites.contains(site))
            {
            return "G";
            }        
        if(tSites.contains(site))
            {
            return "T";
            }
        return null;
        }
    
    
    HashMap patternIDToPatternIDCountMap=null;
    public int getPatternIDCount(int pid)
        {
        if(patternIDToPatternIDCountMap!=null)
            {
            return (Integer)(patternIDToPatternIDCountMap.get(pid));
            }
        else
            {
            patternIDToPatternIDCountMap=new HashMap();
            if(siteToPatternIDMap==null)
                {
                getSiteToPatternIDMap();
                }            
            for(int site=0;site<this.getNumSites();site++)
                {
                int pid_at_site=(Integer)siteToPatternIDMap.get(site);
                if(patternIDToPatternIDCountMap.containsKey(pid_at_site))
                    {
                    //increment if it's there
                    int existing_count=(Integer)patternIDToPatternIDCountMap.get(pid_at_site);
                    int new_count=existing_count+1;
                    patternIDToPatternIDCountMap.put(pid_at_site,new_count);
                    }
                else
                    {
                    //set at 1
                    patternIDToPatternIDCountMap.put(pid_at_site,1);
                    }
                }
            return (Integer)(patternIDToPatternIDCountMap.get(pid));
            }
        }
    
    
    public String getPatternAtSite(int s)
        {
        String sitePattern="";
        for(int seq=0;seq<this.getNumSeqs();seq++)
            {
            String base=""+this.seqs[seq].charAt(s);
            sitePattern=sitePattern+base;
            }
        return sitePattern;
        }

    private HashMap siteToPatternMap=null;
    public HashMap getSiteToPatternMap()
        {
        if(siteToPatternMap!=null)
            {
            return siteToPatternMap;
            }
        else
            {
            siteToPatternMap=new HashMap();
            for(int site=0;site<this.getNumSites();site++)
                {
                String p=getPatternAtSite(site);
                siteToPatternMap.put(site,p);
                }
            return siteToPatternMap;
            }
        }

    
    private HashMap patternToPatternIDMap=null;
    public HashMap getPatternToPatternIdMap()
        {
        if(patternToPatternIDMap!=null)
            {
            return patternToPatternIDMap;
            }
        else
            {
            HashSet hs=new HashSet();
            if(siteToPatternMap==null)
                {
                siteToPatternMap=getSiteToPatternMap();
                }
            for(Object v:siteToPatternMap.values())
                {
                hs.add(v);
                }
            ArrayList<String> myPatterns=new ArrayList<String>();
            Iterator i=hs.iterator();
            while(i.hasNext())
                {
                Object aPattern=i.next();
                myPatterns.add(aPattern.toString());
                }
            Collections.sort(myPatterns);
            patternToPatternIDMap=new HashMap();
            for(int pi=0;pi<myPatterns.size();pi++)
                {
                int the_pattern_id=pi;
                patternToPatternIDMap.put(myPatterns.get(pi),the_pattern_id);
                }
            return patternToPatternIDMap;
            }
        }
    
    public int getNumSitePatterns()
        {
        if(patternToPatternIDMap==null)
            {
            getPatternToPatternIdMap();
            }
        int numSitePatterns=patternToPatternIDMap.values().size();
        return numSitePatterns;
        }

    ArrayList<Integer> sitesCoveringPatternsArr=null;
    public ArrayList<Integer> getSitesToCoverAllPatterns()
        {
        if(sitesCoveringPatternsArr!=null)
            {
            return sitesCoveringPatternsArr;
            }
        else
            {
            if(siteToPatternIDMap==null)
                {
                getSiteToPatternIDMap();
                }
            HashSet coveredSet=new HashSet();
            sitesCoveringPatternsArr=new ArrayList<Integer>();
            for(int site=0;site<getNumSites();site++)
                {
                int pid_at_site=(Integer)siteToPatternIDMap.get(site);
                if(coveredSet.contains(pid_at_site))
                    {
                    //okay it's already covered!
                    }
                else
                    {
                    //not yet covered!
                    coveredSet.add(pid_at_site);
                    sitesCoveringPatternsArr.add(site);
                    }
                }
            return sitesCoveringPatternsArr;
            }

        }
    
    
    private HashMap siteToPatternIDMap=null;
    public HashMap getSiteToPatternIDMap()
        {
        if(siteToPatternIDMap!=null)
            {
            return siteToPatternIDMap;
            }
        else
            {
            if(patternToPatternIDMap==null)
                {
                getPatternToPatternIdMap();
                }
            if(siteToPatternMap==null)
                {
                getSiteToPatternMap();
                }
            siteToPatternIDMap=new HashMap();
            for(Object s:siteToPatternMap.keySet())
                {
                int aSite=(Integer)(s);
                String aPattern=siteToPatternMap.get(s).toString();
                int thePatternID=(Integer)(patternToPatternIDMap.get(aPattern));
                siteToPatternIDMap.put(aSite,thePatternID);
                }
            return siteToPatternIDMap;
            }
        }
    
    
    public boolean isInvariantBase(int site)
        {
        String base=getInvBase(site);
        if(base!=null)
            {
            return true;
            }
        else
            {
            return false;
            }
        }
    
    public ArrayList<Integer>[] getInvariantACGTSites()
        {
        if(acgtSites!=null)
            {
            return acgtSites;
            }
        ArrayList<Integer> aSites=new ArrayList<Integer>();
        ArrayList<Integer> cSites=new ArrayList<Integer>();
        ArrayList<Integer> gSites=new ArrayList<Integer>();
        ArrayList<Integer> tSites=new ArrayList<Integer>();
        
        for(int site=0;site<seqs[0].length();site++)
            {
            int aCount=0;
            int cCount=0;
            int gCount=0;
            int tCount=0;
            for(int s=0;s<seqs.length;s++)
                {
                String thisSeq=seqs[s];
                char thisBase=thisSeq.charAt(site);
                String thisBaseStr=new String(""+thisBase+"");
                if(thisBaseStr.equalsIgnoreCase("A"))
                    {
                    aCount++;
                    }
                if(thisBaseStr.equalsIgnoreCase("C"))
                    {
                    cCount++;
                    }
                if(thisBaseStr.equalsIgnoreCase("G"))
                    {
                    gCount++;
                    }
                if(thisBaseStr.equalsIgnoreCase("T"))
                    {
                    tCount++;
                    }                
                }
            if(aCount!=0 && cCount==0 && gCount==0 && tCount==0)
                {
                aSites.add(site);
                }
            if(aCount==0 && cCount!=0 && gCount==0 && tCount==0)
                {
                cSites.add(site);
                }
            if(aCount==0 && cCount==0 && gCount!=0 && tCount==0)
                {
                gSites.add(site);
                }
            if(aCount==0 && cCount==0 && gCount==0 && tCount!=0)
                {
                tSites.add(site);
                }
            }

        ArrayList<Integer>[] myLists=new ArrayList[4];
        myLists[0]=aSites;
        myLists[1]=cSites;
        myLists[2]=gSites;
        myLists[3]=tSites;
        acgtSites=myLists;
        return acgtSites;
        }
    


    
    public int pruningBurden()
        {
        int prod=this.getNumSeqs()*this.getNumSites();
        return prod;
        }
    
    
    public static double[] getCMEFreqs()
        {
        return new double[]{0.25,0.25,0.25,0.25};
        }
    
    
    
    public double[] getPiFreqsACGT()
        {
        double[] tr=new double[4];
        System.arraycopy(this.piFreqs,0,tr,0,this.piFreqs.length);
        return tr;
        }
    

    
        
    public double[] computeNAFreqs()
        {
            /*
            if(base=='A')
                p=0;
            else if(base=='C')
                p=1;
            else if(base=='G')
                p=2;
            else
                p=3;
            */
        int[] counts=new int[4];
        double[] tFreq=new double[4];
        int totCount=0;
        int s=this.getNumSeqs();
        for(int i=0;i<s;i++)
            {
            String aSeq=this.seqs[i];
            char[] bases=aSeq.toUpperCase().toCharArray();
            for(int b=0;b<bases.length;b++)
                {
                int p=0;
                char base=bases[b];
                if(base=='A')
                    p=0;
                else if(base=='C')
                    p=1;
                else if(base=='G')
                    p=2;
                else if(base=='T')
                    p=3;
                else
                    {
                    /*
                    Seems consistent with PAL
                        
                        IN FILE GTR.java
		out.println("                                   A  C  G  T");
		out.println("Corresponding rate matrix      ----------------");
		out.println("(shown without frequencies):     A    a  b  c");
		out.println("                                 C       d  e");
		out.println("                                      
                        
                    */
                        
                    //if unable to find ACGT, just increment all! this to get near uniform freqs
                    counts[0]++;
                    counts[1]++;
                    counts[2]++;
                    counts[3]++;
                    totCount+=4;
                    }
                if(p!=(-1))
                    {
                    counts[p]++;
                    totCount++;
                    }
                }
            }

        for(int p=0;p<counts.length;p++)
            {
            tFreq[p]=(double)(counts[p])/(double)(totCount);                    
            }
        
        return tFreq;
        }
    
    
    
    
    private void setPiFreqsACGT()
        {
        double[] tFreq=this.computeNAFreqs();
        this.piFreqs=new double[4];
        System.arraycopy(tFreq,0,this.piFreqs,0,4);
            
        }
    
    
    
    public ArrayList<Integer> getZeroBasedArrayListOfSitesThatAreSegregating()
        {
        ArrayList<Integer> indicesOfSegSites=new ArrayList<Integer>();
        int numSites=this.getNumSites();
        int numSeqs=this.getNumSeqs();
        for(int s=0;s<numSites;s++)
            {
            boolean isThisSiteSegregating=false;
            String aBase=null;
            for(int seq=0;seq<numSeqs;seq++)
                {
                String aSeq=this.getSeqByID(s).toUpperCase();
                char[] bases=aSeq.toCharArray();
                String compBase=new String(""+bases[s]+"");
                if(aBase==null)
                    {
                    //get the first base at this site
                    aBase=new String(compBase);
                    }
                else
                    {
                    //if the first base isn't the same as this base,
                    //then the site is segregating
                    if(aBase.compareToIgnoreCase(compBase)!=0)
                        {
                        isThisSiteSegregating=true;                        
                        }
                    }
                }//for each seq
            if(isThisSiteSegregating)
                {
                indicesOfSegSites.add(s);
                }
            }//for each site
        return  indicesOfSegSites;
        }
    
    public  Object[]  getZeroBasedArrayListOfSitesInSegClassesNonThenSeg()
        {
        ArrayList<Integer> nonSegSiteAL=new ArrayList<Integer>();
        ArrayList<Integer> segSiteAL=new ArrayList<Integer>();
        for(int pos=0;pos<seqs[0].length();pos++)
            {
            int f=this.getSegSiteFreqAtPos(pos);
            if(f==0)
                {
                //it's a non-segregating site!
                nonSegSiteAL.add(pos);
                }
            else
                {
                //it's a segregating site
                segSiteAL.add(pos);
                }
            }
        Object[] ret_package=new Object[2];
        ret_package[0]=nonSegSiteAL;
        ret_package[1]=segSiteAL;
        return  ret_package;
        }
    
    
    public static FastaIO filterOutSites(FastaIO fio,boolean filterSegregating)
        {
        ArrayList<Integer> sitesToBeFiltered=null;
        Object[] ret_pack=fio.getZeroBasedArrayListOfSitesInSegClassesNonThenSeg();
        if(filterSegregating)
            {
            //set the sites to be filtered to be the segregating sites
            sitesToBeFiltered=(ArrayList<Integer>)(ret_pack[1]);
            System.out.println("To filter out the following ("+sitesToBeFiltered.size()+") SEGREGATING sites : "+TreeUtils.toString(sitesToBeFiltered)+" of "+fio.getNumSites()+" total.");
            }
        else
            {
            //set the sites to be filtered to be the non-segregating sites
            sitesToBeFiltered=(ArrayList<Integer>)(ret_pack[0]);
            System.out.println("To filter out the following ("+sitesToBeFiltered.size()+") NON-SEGREGATING sites : "+TreeUtils.toString(sitesToBeFiltered)+" of "+fio.getNumSites()+" total.");
            }
        ArrayList<String> newSeqs=new ArrayList<String>();
        ArrayList<String> newNames=new ArrayList<String>();
        int numSeqs=fio.getNumSeqs();
        for(int seq=0;seq<numSeqs;seq++)
            {
            String newSeq=TreeUtils.getStringButFilterOutIndices(fio.getSeqByID(seq), sitesToBeFiltered);
            String newName=fio.getSeqNameByID(seq);
            newSeqs.add(newSeq);
            newNames.add(newName);
            }
        String[] newNamesArr=new String[newNames.size()];
        String[] newSeqsArr=new String[newSeqs.size()];
        for(int i=0;i<newNamesArr.length;i++)
            {
            newNamesArr[i]=newNames.get(i);
            newSeqsArr[i]=newSeqs.get(i);
            }
        FastaIO newFIO=new FastaIO(newNamesArr,newSeqsArr);
        newFIO.sourceFasta=fio.sourceFasta;
        return newFIO;
        }
    
    
    
    public void writeNexusFile(String nexusPath,boolean useABC) throws Exception
        {
        File nexusFile=new File(nexusPath);
        FileWriter fw=new FileWriter(nexusFile);
        BufferedWriter bw=new BufferedWriter(fw);
        String nxsString=this.generateNexusString(useABC);
        bw.write(nxsString,0,nxsString.length());
        bw.newLine();
        bw.flush();
        bw.close();        
        }
    
    public String generateNexusString(boolean useABC)
        {
        int numSeqs=this.getNumSeqs();
        int numBases=this.getNumSites();            
        String nxs="";
        nxs+="#NEXUS\n";
        nxs+="begin taxa;\n";
        nxs+="dimensions ntax="+numSeqs+";\n";
        nxs+="taxlabels\n";
        for(int s=0;s<numSeqs;s++)
            {
            if(!useABC)
                {
                nxs+=""+(s+1)+"\n";
                }
            else
                {
                nxs+=""+TreeUtils.acquireABCName(s)+"\n";
                }
            }        
        nxs+=";\n";
        nxs+="end;\n";
        nxs+="\n";
        nxs+="begin characters;\n";
        nxs+="dimensions nchar="+numBases+";\n";
        nxs+="format datatype=dna gap=-;\n";
        nxs+="matrix\n";
           for(int s=0;s<numSeqs;s++)
            {
            if(!useABC)
                {
                nxs+=""+(s+1)+" "+this.getSeqByID(s)+"\n";
                }
            else
                {
                nxs+=""+TreeUtils.acquireABCName(s)+" "+this.getSeqByID(s)+"\n";
                }
            }
        nxs+=";\n";
        nxs+="end;";
        return nxs;
            
/*#NEXUS

begin taxa;
dimensions ntax=2;
taxlabels
CPZANT
U455
;
end;

begin characters;
dimensions nchar=250;
format datatype=dna gap=-;
matrix
CPZANT ATGGGAGCGGGGGCGTCTGTTTTGAGGGGAGAGAAGCTAGATACATGGGAAAGTATCAGGCTTCGGCCCGGTGGCAAGAAAAAGTACATGATAAAACATCTGGTTTGGGCAAGATCGGAGCTGCAGCGTTTTGCGCTCAGCTCCTCCCTTCTAGAAACATCAGAAGGTTGTGAAAAGGCTATCCATCAATTGAGCCCTTCCATAGAAATAAGATCCCCTGAAATAATATCTTTGTTTAACACCATTTGTG
U455   ATGGGTGCGAGAGCGTCAGTATTAAGCGGGAAAAAATTAGATTCATGGGAGAAAATTCGGTTAAGGCCAGGGGGAAACAAAAAATATAGACTGAAACATTTAGTATGGGCAAGCAGGGAGCTGGAAAAATTCACACTTAACCCTGGCCTTTTAGAAACAGCAGAAGGATGTCAGCAAATACTGGGACAATTACAACCAGCTCTCCAGACAGGAACAGAAGAACTTAGATCATTATATAATACAGTAGCAG
;
end;*/
        }
    
    
    public void writePhylipFile(String phylipPath,boolean useABC) throws Exception
        {
        File phylipFile=new File(phylipPath);
        FileWriter fw=new FileWriter(phylipFile);
        BufferedWriter bw=new BufferedWriter(fw);
        String phylipString=this.generatePhylipString(useABC);
        bw.write(phylipString,0,phylipString.length());
        bw.newLine();
        bw.flush();
        bw.close();
        }
    
    public String generatePhylipString(boolean useABC)
        {
        /*4 6
        MAN ATTGCC
        MKY ACGATC
        WHL CCATTC
        BRD GTTACT*/
        int numSeqs=this.getNumSeqs();
        int numBases=this.getNumSites();
        String phylipString=""+numSeqs+" "+numBases+"\n";
        for(int s=0;s<numSeqs;s++)
            {
            if(!useABC)
                {
                phylipString+=""+(s+1)+" "+this.getSeqByID(s);
                }
            else
                {
                phylipString+=""+TreeUtils.acquireABCName(s)+" "+this.getSeqByID(s);
                }
            if(s<numSeqs-1)
                {
                phylipString+="\n";
                }
            }
        return phylipString;
        }
    

    
    public static int convertIntegerNameToID(String iName)
        {
        int x=Integer.parseInt(iName);
        return (x-1);
        }
    
    public String toSring()
        {
        return toString(true);
        }
    
    public String abbreviatedToString()
        {
        String tr;
        tr="FastaIO : \n";
        tr+="Num seqs : "+this.seqs.length+"\n";
        if(sourceFasta!=null)
            {
            tr+="File : "+sourceFasta.getAbsolutePath()+"\n";
            }
        return tr;
        }
    
    
    public String toString(boolean showAligned)
        {
            
            String tr;
            tr="FastaIO : \n";
            tr+="Num seqs : "+this.seqs.length+"\n";
            if(sourceFasta!=null)
                {
                tr+="File : "+sourceFasta.getAbsolutePath()+"\n";
                }
            tr+="Names : "+Arrays.toString(this.seqNames)+"\n";
            if(showAligned)
                {
                tr+="Aligned :\n";
                for(int a=0;a<this.seqs.length;a++)
                    {
                    //System.out.println("For a="+a+" got ABC : "+TreeUtils.acquireABCName(a));
                    tr+=seqs[a]+"\n";
                    }
                }
            tr+="Pos Frequency Data\n";
            tr+="Pos\tFreq(!=0)\n";
            for(int p=0;p<seqs[0].length();p++)
                {
                if(getSegSiteFreqAtPos(p)!=0 || p==0 || p==seqs[0].length()-1  )
                    {
                    tr+=(p+1)+"\t"+getSegSiteFreqAtPos(p)+"\n";                    
                    }
                }
            tr+="Freq Spectrum Data\n";
            tr+="Freq\tSiteCount (!=0)\n";
            for(int f=0;f<seqs.length;f++)
                {
                int numSegSitesWithGivenFreq=getNumSegSitesWithGivenFreq(f);
                if(numSegSitesWithGivenFreq!=0)
                    {
                    tr+=f+"\t"+getNumSegSitesWithGivenFreq(f)+"\n";
                    }                
                }
            tr+="NameToIDMap\n";
            tr+=TreeUtils.mapToString(nameToIDMap);
            
            return tr;

        }
    
    
    
    
    
    
    public String getSeqNameByID(int id)
        {
        return seqNames[id];
        }

    public String getSeqByID(int id)
        {
        return seqs[id];
        }
    
    String getSeqByName(String n)
        {
        //System.out.println("Using name="+n);
        int id=(Integer)(nameToIDMap.get(n));
        //System.out.println("id gives "+id);
        return this.getSeqByID(id);
        }
    
    String getSeqByOneBasedStringName(String n)
        {
        int i=Integer.parseInt(n);
        i--;
        return getSeqByID(i);
        }
        
    public char getBaseByNameAndSite(String seqName,int siteNum)
        {
        String retrievedSeq=this.getSeqByName(seqName);
        char[] bases=retrievedSeq.toCharArray();
        return bases[siteNum];
        }
    
        
    public int getNumSeqs()
        {
            return this.seqs.length;
        }
    
    

    public int getNumSegSitesWithGivenFreq(int given_freq)
        {
        if(numSegSitesWithGivenFreqMap!=null)
            {
            if(numSegSitesWithGivenFreqMap.containsKey(given_freq))
                {
                return (Integer)(numSegSitesWithGivenFreqMap.get(given_freq));
                }
            }
        String[] seqs_arr=this.seqs;
        int  freq_count=0;
        for(int site=0;site<seqs_arr[0].length();site++)
            {
            int freq=this.getSegSiteFreqAtPos(site);
            if(freq==given_freq)
                {
                freq_count++;  
                }
            }
        return freq_count;
        }

        
    
    public int getSegSiteFreqAtPos(int sspos)
        {
        if(segSiteFreqAtPosMap!=null)
            {
            if(segSiteFreqAtPosMap.containsKey(sspos))
                {
                return (Integer)(segSiteFreqAtPosMap.get(sspos));
                }
            }
            
            
        String[] seqs_arr=this.seqs;
        //at the position, count the number of times that the various 4 bases appear
        HashMap b_dict=new HashMap();
        for(String seq:seqs_arr)
            {
                String base=seq.substring(sspos, sspos+1);
                Integer prev_count=new Integer(0);
                if(b_dict.containsKey(base))
                    {
                        prev_count=(Integer)(b_dict.get(base));
                    }
                prev_count=new Integer(prev_count.intValue()+1);
                b_dict.put(base,prev_count);
            }
        if(b_dict.keySet().size()==1)
            {
                //if the number of keys (bases) found is one, return 0
                return 0;
            }
        else
            {
                ArrayList<Integer> counts=new ArrayList<Integer>();
                counts.addAll(b_dict.values());
                //highest frequency appears at end of list
                Collections.sort(counts);
                int sum=0;
                for(int a=0;a<counts.size()-1;a++)
                    {
                        //System.out.println("counts["+a+"]="+counts.get(a));
                        sum+=counts.get(a);
                    }
                return sum;
            }
        }    

    int getNumSites()
        {
        return this.seqs[0].length();
        }
    
    private void initWithSeqsAndNames(String[] iSeqNames,String[] iSeqs)
        {

            
        //verify lengths match
        if(iSeqNames==null || iSeqs==null)
            {
            throw new NullPointerException("Error, require non-null data to init IO!");
            }
        if(iSeqNames.length==0 || iSeqs.length==0)
            {
            throw new RuntimeException("Error, require non-empty data to init IO!");
            }
        //here, get columns with gaps
        ArrayList gapColumns=new ArrayList<Integer>();
        for(int i=0;i<iSeqs.length;i++)
            {
            char[] cols=iSeqs[i].toUpperCase().toCharArray();
            for(int c=0;c<cols.length;c++)
                {
                if(!gapColumns.contains(c))
                    {
                    if(cols[c]!='A' && cols[c]!='C' && cols[c]!='G' && cols[c]!='T')
                        {
                        gapColumns.add(c);
                        }
                    }
                }
            }
        //rebuild seqs without the gapped columns
        System.out.println("Number columns in alignment found with non-ACGT : "+gapColumns.size());
        if(!gapColumns.isEmpty())
            {
            for(int i=0;i<iSeqs.length;i++)
                {
                String newSeq="";
                char[] bases=iSeqs[i].toCharArray();
                for(int c=0;c<bases.length;c++)
                    {
                    if(!gapColumns.contains(c))
                        {
                        newSeq=newSeq+bases[c];
                        }
                    }
                iSeqs[i]=newSeq;
                }
            Collections.sort(gapColumns);
            System.out.println("The following (0-based) columns were removed :"+gapColumns.toString());
            }
        int firstSeqLen=0;
        for(int i=0;i<iSeqs.length;i++)
            {
            if(iSeqs[i]==null)
                {
                throw new NullPointerException("Error, sequence # "+(i+1)+" is null!!!");
                }
            else
                {
                iSeqs[i]=iSeqs[i].toUpperCase();
                }
            if(seqs[i].length()<=0)
                {
                throw new IllegalArgumentException("Error, sequence number "+(i+1)+" is empty ! ");
                }
            if(isNonEmptyACGT(iSeqs[i]))
                {
                //good
                }
            else
                {
                throw new IllegalArgumentException("Error, sequence number "+(i+1)+" contains non-DNA! ('"+iSeqs[i]+"')");
                //bad
                }
            if(i==0)
                {
                firstSeqLen=iSeqs[i].length();
                }
            else
                {
                if(firstSeqLen!=iSeqs[i].length())
                    {
                    String eMsg="Error, seq length mismatch! first seq("+
                            iSeqNames[0]+" has length "+firstSeqLen+" and seq("+
                            iSeqNames[i]+") has length="+iSeqs[i].length();
                    throw new RuntimeException(eMsg);                           
                    }
                }
            }//make sure seqs are all of the same length
        if(seqNames==null || seqs==null || nameToIDMap==null)
            {
            seqNames=new String[iSeqNames.length];
            seqs=new String[iSeqNames.length];
            nameToIDMap=new HashMap();
            }
        
        for(int d=0;d<iSeqNames.length;d++)
            {
            seqNames[d]=new String(iSeqNames[d]);
            seqs[d]=new String(iSeqs[d]);
            nameToIDMap.put(seqNames[d],d);
            }
        if(nameToIDMap.keySet().size()!=iSeqNames.length)
            {
            throw new RuntimeException("Error,seq names not unique!");
            }
        segSiteFreqAtPosMap=new HashMap();
        for(int pos=0;pos<seqs[0].length();pos++)
            {
            int f=this.getSegSiteFreqAtPos(pos);
            segSiteFreqAtPosMap.put(pos,f);
            }
        numSegSitesWithGivenFreqMap=new HashMap();
        for(int a_freq=0;a_freq<this.seqs.length;a_freq++)
            {
            int numSitesAtThisFreq=this.getNumSegSitesWithGivenFreq(a_freq);
            numSegSitesWithGivenFreqMap.put(a_freq,numSitesAtThisFreq);
            }
        this.setPiFreqsACGT();
        }
    
    
    public FastaIO(String[] IseqNames,String[] Iseqs)
        {
        this.initWithSeqsAndNames(IseqNames,Iseqs);
        }
    
    public static boolean isNonEmptyACGT(String dna)
        {
        Pattern dnaPattern=Pattern.compile("^[ACGT]+$",Pattern.CASE_INSENSITIVE);
        Matcher m=dnaPattern.matcher(dna);
        if(m.find())
            {
            //good!
            return true;
            }
        else
            {
            return false;
            }
        }
    
    
    
    public FastaIO(File fastaFile,boolean useABCNames) throws Exception
        {
        sourceFasta=fastaFile;
        ArrayList<String> fastaNameList=new ArrayList<String>();
        ArrayList<String> fastaStringList=new ArrayList<String>();
        try
            {
            LinkedHashMap<String, ProteinSequence> a = FastaReaderHelper.readFastaProteinSequence(fastaFile);
            for (  Map.Entry<String, ProteinSequence> entry : a.entrySet() )
                {
                //System.out.println("To add to name list "+entry.getKey());
                fastaNameList.add(entry.getKey().toString());
                fastaStringList.add(entry.getValue().getSequenceAsString());
                }
            }
        catch(Exception e)
            {
            e.printStackTrace();
            String errStr="\n\n\n\nError reading/loading FASTA file "+fastaFile.getAbsolutePath()+":"+e.getMessage();
            errStr+="\n"+Comet.getUsageString();
            System.err.println(errStr);
            System.exit(1);
            }
        catch(Error e)
            {
            e.printStackTrace();
            String errStr="\n\n\n\nError reading/loading FASTA file "+fastaFile.getAbsolutePath()+":"+e.getMessage();
            errStr+="\n"+Comet.getUsageString();
            System.err.println(errStr);
            System.exit(1);
            }


        //init the maps/caches
        nameToIDMap=new HashMap();
        seqNames=new String[fastaNameList.size()];
        seqs=new String[fastaNameList.size()];
        
        for(int d=0;d<seqNames.length;d++)
            {
            if(!useABCNames)
                {
                seqNames[d]=fastaNameList.get(d);
                } 
            else
                {
                seqNames[d]=TreeUtils.acquireABCName(d);
                }
            seqs[d]=fastaStringList.get(d);
            }

        
        this.initWithSeqsAndNames(seqNames,seqs);
        }
    
    
    
    
}
