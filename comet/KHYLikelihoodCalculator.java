/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import pal.substmodel.HKY;
import pal.substmodel.NucleotideModel;
import pal.substmodel.TransitionProbability;


/**
 *
 * @author esalina
 */
public class KHYLikelihoodCalculator  implements TreeLikelihoodCalculator   {
    
    
    
    public static double getTransProb(double kappa,int from,int to,double t,double[] f,boolean recCheck)
        {
        //see Computational Mol. Evol., pg 13, eq 1.20
        //see Mol. Evol : A Statistical Approach, pg 10, eq 1.21
        //freqs are ACGT
            
        /*if(from==to && t==0)
            {
            return 1.0;
            }
        else if(t==0)
            {
            return 0.0;
            }*/
            
            
        double pi_a=f[0];
        double pi_c=f[1];
        double pi_g=f[2];
        double pi_t=f[3];
        double pi_y=pi_t+pi_c;
        double pi_r=pi_a+pi_g;
        //double lambda_1=0;
        double lambda_2=(-1.0);
        double lambda_3=-(pi_r*kappa+pi_y);
        double lambda_4=-(pi_y*kappa+pi_r);
        double e2=Math.exp(lambda_2*t);
        double e3=Math.exp(lambda_3*t);
        double e4=Math.exp(lambda_4*t);
        double val=(-1);
        if(from==0)
            {
            if(to==0)
                //A->A
                val=pi_a+(pi_a*pi_y/pi_r)*e2+(pi_g/pi_r)*e3;
            else if(to==1)
                //A->C
                val=pi_c*(1-e2);
            else if(to==2)
                //A->G
                val=pi_g+(pi_g*pi_y/pi_r)*e2-(pi_g/pi_r)*e3;
            else
                //A->T
                val=pi_t*(1-e2);
            }
        else if (from==1)
            {
            if(to==0)
                //C->A
                val=pi_a*(1-e2);
            else if(to==1)
                //C->C
                val=pi_c+(pi_c*pi_r/pi_y)*e2+(pi_t/pi_y)*e4;
            else if(to==2)
                //C->G
                val=pi_g*(1-e2);
            else
                //C->T
                val=pi_t+(pi_t*pi_r/pi_y)*e2-(pi_t/pi_y)*e4;
            }
        else if(from==2)
            {
            if(to==0)
                //G->A
                val=pi_a+(pi_a*pi_y/pi_r)*e2-(pi_a/pi_r)*e3;
            else if(to==1)
                //G->C
                val=pi_c*(1-e2);
            else if(to==2)
                //G->G
                val=pi_g+(pi_g*pi_y/pi_r)*e2+(pi_a/pi_r)*e3;
            else
                //G->T
                val=pi_t*(1-e2);
            }
        else if(from==3)
            {
            if(to==0)
                //T->A
                val=pi_a*(1-e2);
            else if(to==1)
                //T->C
                val=pi_c+(pi_c*pi_r/pi_y)*e2-(pi_c/pi_y)*e4;
            else if(to==2)
                //T->G
                val=pi_g*(1-e2);
            else
                //T->T
                val=pi_t+(pi_t*pi_r/pi_y)*e2+(pi_c/pi_y)*e4;
            }

        if(recCheck)
            {
            double[] test=new double[4];
            for(int to_i=0;to_i<4;to_i++)
                {
                test[to_i]=getTransProb(kappa,from,to_i,t,f,false);
                }
            boolean stof=TreeUtils.sumToOne(test);
            System.out.println("If  from ="+from+" and t="+t+" then possible array is "+Arrays.toString(test)+" and sum_to_one is "+stof);
            
            }
        
        
        return val;
        }
    
    double kappa;
    double[] freqs=null;
    HashMap pMap=null;
    double minKappa;
    double maxKappa;
    HKY myHKY=null;
    
    public static NucleotideModel getCMEK80()
        {
        pal.substmodel.HKY myCME=new pal.substmodel.HKY(2.0,FastaIO.getCMEFreqs());
        return myCME;
        }
    
    
    public double getTotalMutationRate()
        {
        double[][] rateMatrix=this.myHKY.rate;
        double kappa=this.myHKY.getParameter(0);
        return LikelihoodUtils.getTotalMutationRate(rateMatrix);
        }
    
    
    public double computeLikelihoodNBD(coa_lineage t,FastaIO fio,boolean isLog)
        {
        BigDecimal compedVal=computeLikelihood(t,fio,isLog,null);
        if(compedVal==null)
            {
            return Double.NEGATIVE_INFINITY;
            }
        else
            {
            return compedVal.doubleValue();
            }
        }
        
    
    public boolean arePriorsAllSingle()
        {
        if(minKappa==maxKappa)
            {
            return true;
            }
        else
            {
            return false;
            }
        }

    private double[][] getDerivativeArray(double dist,boolean abs)
        {
/*
            if(base=='A')
                p=0;
            else if(base=='C')
                p=1;
            else if(base=='G')
                p=2;
            else
                p=3;            
*/      //kappa=alpha/beta
        double pi_A=this.freqs[0];
        double pi_C=this.freqs[1];
        double pi_G=this.freqs[2];
        double pi_T=this.freqs[3];
        double pi_Y=pi_T+pi_C;
        double pi_R=pi_A+pi_G;
        double lambda_2=-1/kappa;
        double lambda_3=-(pi_R*kappa+pi_Y/kappa);
        double lambda_4=-(pi_Y*kappa+pi_R/kappa);
        double[][] deriv=new double[4][];
        for(int from=0;from<4;from++)
            {
            deriv[from]=new double[4];
            for(int to=0;to<4;to++)
                {
                double this_deriv=0;
                switch(from)
                    {
                    case 0:
                        switch(to)
                            {
                            case 0:
                                //A->A
                                //value=pi_A+(pi_A*pi_Y/pi_R)*exp(-t/kappa)+(pi_G/pi_R)*exp(-(pi_R*kappa+pi_Y)*t)
                                //pi_A+f1*exp(-t/kappa)+f2*exp(-f3*t)
                                //deriv of first term is zero , cause t doesn't appear
                                //deriv of second term is
                                double second_term_deriv=(pi_A*pi_Y)/(pi_R)*lambda_2*Math.exp(lambda_2*dist);
                                double third_term_deriv=(pi_G/pi_R)*lambda_3*Math.exp(lambda_3*dist);
                                this_deriv=second_term_deriv+third_term_deriv;                                
                                break;
                            case 1:
                                //A->C
                                //pi_C(1-exp(lambda_2*t))=pi_C-pi_C*exp(lambda_2*t)
                                this_deriv=-pi_C*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            case 2:
                                //A->G
                                double second_term=(pi_G*pi_Y/pi_R)*lambda_2*Math.exp(lambda_2*dist);
                                double third_term=(pi_G/pi_R)*lambda_3*Math.exp(lambda_3*dist);
                                this_deriv=second_term-third_term;
                                break;
                            case 3:
                                //A->T
                                //prob=pi_T(1-e2)=pi_T-pi_T*Math.exp(lambda_2*t)
                                this_deriv=-pi_T*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            }//A->?
                        break;
                    case 1://C->?
                        switch(to)
                            {
                            case 0:
                                //C->A
                                //value=pi_A*(1-e2)=pi_A-pi_A*Math.exp(lambda_2*dist)
                                this_deriv=-pi_A*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            case 1:
                                //C->C
                                this_deriv=((pi_C*pi_R)/pi_Y)*lambda_2*Math.exp(lambda_2*dist);
                                this_deriv+=(pi_T/pi_Y)*lambda_4*Math.exp(lambda_4*dist);
                                break;
                            case 2:
                                //C->G
                                this_deriv=-pi_G*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            case 3:
                                //C->T
                                this_deriv=((pi_T*pi_R)/pi_Y)*lambda_2*Math.exp(lambda_2*dist);
                                this_deriv-=(pi_T/pi_Y)*lambda_4*Math.exp(lambda_4*dist);
                                break;
                            }//C->?
                        break;
                    case 2:
                        //G->?
                        switch(to)
                            {
                            case 0:
                                //G->A
                                this_deriv=(pi_A*pi_Y/pi_R)*lambda_2*Math.exp(lambda_2*dist);
                                this_deriv-=(pi_A/pi_R)*lambda_3*Math.exp(lambda_3*dist);
                                break;
                            case 1:
                                //G->C
                                this_deriv=-pi_C*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            case 2:
                                //G->G
                                this_deriv=(pi_G*pi_Y/pi_R)*lambda_2*Math.exp(lambda_2*dist);
                                this_deriv+=(pi_A/pi_R)*lambda_3*Math.exp(lambda_3*dist);
                                break;
                            case 3:
                                //G->T
                                this_deriv=-pi_T*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            }//G->?                        
                        break;
                    case 3:
                        switch(to)
                            {
                            //T->?
                            case 0:
                                //T->A
                                this_deriv=-pi_A*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            case 1:
                                //T->C
                                this_deriv=(pi_C*pi_R/pi_Y)*lambda_2*Math.exp(lambda_2*dist);
                                this_deriv-=(pi_C/pi_Y)*lambda_4*Math.exp(lambda_4*dist);
                                break;
                            case 2:
                                //T->G
                                this_deriv=-pi_G*lambda_2*Math.exp(lambda_2*dist);
                                break;
                            case 3:
                                //T->T
                                this_deriv=(pi_T*pi_R/pi_Y)*lambda_2*Math.exp(lambda_2*dist);
                                this_deriv+=(pi_C/pi_Y)*lambda_4*Math.exp(lambda_4*dist);
                                break;                                
                            }                        
                        break;
                    }//switch from
                deriv[from][to]=this_deriv;
                if(abs)
                    {
                    deriv[from][to]=Math.abs(deriv[from][to]);
                    }
                }//to loop
            }//from loop
        /*double diag_sum=0;
        double off_diag_sum=0;
        double all_sum=0;
        double freq_sum=0;
        for(int from=0;from<4;from++)
            {
            for(int to=0;to<4;to++)
                {
                if(from==to)
                    {
                    diag_sum+=deriv[from][to];
                    }
                else
                    {
                    off_diag_sum+=deriv[from][to];
                    }
                all_sum+=deriv[from][to];
                freq_sum+=deriv[from][to]*this.freqs[from]*freqs[to];
                }
            }*/
        return deriv;
        }
    
    
    
    public void proposeNewParams(boolean generate)
        {
        /*System.err.println("Proposing new kappa...");
        System.err.println("Half win length is "+kappaWindowHalfWidth);
        System.err.println("minkappa is "+minKappa);
        System.err.println("max kapps is "+maxKappa);*/
        if(!generate)
            {
            double dynUpperWindow=Math.min(kappa+kappaWindowHalfWidth,maxKappa);
            double dynLowerWindow=Math.max(kappa-kappaWindowHalfWidth,minKappa);
            //System.err.println("Current kappa is "+kappa);
            this.kappa=TreeUtils.getRandNumBet(dynLowerWindow,dynUpperWindow,true);
            }
        else
            {
            this.kappa=TreeUtils.getRandNumBet(minKappa,maxKappa,true);
            }
        /*System.err.println("Dyn lower/upper window are : "+dynLowerWindow+" and "+dynUpperWindow);
        System.err.println("Proposed kappa is "+kappa);*/
        this.pMap.put("kappa", kappa);
        }
    
    public HashMap cloneParams()
        {
        HashMap c=new HashMap();
        c.put("kappa",this.pMap.get("kappa"));
        c.put("pi_freqs",this.pMap.get("pi_freqs"));
        return c;
        }

     public void setParams(HashMap pMap)
        {
        this.pMap=pMap;
        Object kappaObj=pMap.get("kappa");
        kappa=Double.parseDouble(kappaObj.toString());
        Object freqsObj=pMap.get("pi_freqs");
        String freqsStr=freqsObj.toString();
        String[] freqsPieces=freqsStr.split(",");
        freqs=new double[freqsPieces.length];
        if(freqs.length!=4)
            {
            throw new IllegalArgumentException("Error, freqs passed as "+freqsStr+" but should be string of 4 parsable doubles adding to 100%!");
            }
        double sum=0;
        for(int i=0;i<4;i++)
            {
            freqs[i]=Double.parseDouble(freqsPieces[i]);
            sum+=freqs[i];
            }
        if(!TreeUtils.tolerEq(sum,1.0))
            {
            throw new IllegalArgumentException("Error, argument "+freqsStr+" sums to "+sum+", not 1.0!");
            }
        this.myHKY=new HKY(kappa,freqs);
        //throw new RuntimeException("in setparams");
        }
    
    
    public boolean isPruningComputed()
        {
        return true;
        }
    

    public double[] getFreqs()
        {
        return freqs;
        }
    
    public double getKappa()
        {
        return kappa;
        }
    
    public static double getMinDistUsingMinMaxKappaAndFreqs(double minKappa,double maxKappa,double[] freqs)
        {
            
            
        String distBase="1E-";
        int maxBaseTen=Integer.MAX_VALUE;
        ArrayList<Double> kappaValuesAL=new ArrayList<Double>();
        double kappaDelta=Math.abs(minKappa-maxKappa)/10.0;
        if(Math.abs(minKappa-maxKappa)<=0.00001 || minKappa==maxKappa)
            {
            kappaDelta=Double.MAX_VALUE;
            }        
        for(double tempKappa=minKappa;tempKappa<=maxKappa;tempKappa+=kappaDelta)
            {
            kappaValuesAL.add(tempKappa);
            }
        double[] kappaValues=new double[kappaValuesAL.size()];        
        for(int k=0;k<kappaValuesAL.size();k++)
            {
            kappaValues[k]=kappaValuesAL.get(k);
            }
            
        
        //System.out.println("*******************************************************\nAnalyzing for kappas : "+Arrays.toString(kappaValues));
        for(int k=0;k<kappaValues.length;k++)
            {
            double kappa=kappaValues[k];
            //System.out.println("*******************************************************");
            //System.out.println("Current kappa="+kappa);
            HKY myHKYTestModel=new HKY(kappa,freqs);
            //System.out.println("Dist base is "+distBase);
            int highestPower=20;
            for(int negTenPower=1;negTenPower<=highestPower;negTenPower++)
                {
                String thisDist=distBase+negTenPower;
                double dist=Double.parseDouble(thisDist);
                //System.out.println("Neg 10 power is "+negTenPower);
                //System.out.println("The distance is "+dist);
                TransitionProbability mytpm=myHKYTestModel.getTransitionProbability();
                mytpm.setDistance(dist);
                double[][] probs=new double[4][];
                for(int from=0;from<4;from++)
                    {
                    probs[from]=new double[4];
                    for(int to=0;to<4;to++)
                        {
                        probs[from][to]=mytpm.getTransitionProbability(from,to);
                        }
                    }
                boolean isAnyDiagonalOne=false;
                boolean anyOffDiagonalIsZero=false;
                //System.out.print("[");
                for(int from=0;from<4;from++)
                    {
                    //System.out.print(Arrays.toString(probs[from]));
                    //if(from<3) { System.out.println(","); }
                    for(int to=0;to<4;to++)
                        {
                        if(from==to)
                            {
                            if(probs[from][to]==1)
                                {
                                isAnyDiagonalOne=true;
                                }
                            }//on-diagonal
                        else
                            {
                            if(probs[from][to]==0)
                                {
                                anyOffDiagonalIsZero=true;
                                }
                            }//off-diagonal
                        }//to
                    }//from
                //System.out.println("]");
                //System.out.println("Log version : \n [ ");
                for(int from=0;from<4;from++)
                    {
                    double[] logs=TreeUtils.applyLog(probs[from]);
                    //System.out.print(Arrays.toString(logs));
                    if(from<=2)
                        {
                        //System.out.println(",");
                        }
                    else
                        {
                        //System.out.println("]");
                        }
                    }
                /*System.out.println("Any diagonal 1 : "+isAnyDiagonalOne);
                System.out.println("Any off-diagonal zero : "+anyOffDiagonalIsZero);*/
                //System.out.println("All rows sum to 1 : "+doAllRowsSumToOne);                
                if(isAnyDiagonalOne || anyOffDiagonalIsZero )
                    {
                    //System.out.println("Trigger!");
                    //System.out.println("Old max bas is "+maxBaseTen);
                    maxBaseTen=Math.min(maxBaseTen,negTenPower);
                    //System.out.println("New max bas is "+maxBaseTen);
                    negTenPower=highestPower+1;
                    }
                }//neg to powers
            }//iterate over kappas
        maxBaseTen-=1;
        String toParse=distBase+maxBaseTen;
        double toReturn=Double.parseDouble(toParse);
        System.out.flush();
        //throw new RuntimeException("To return as min dist : "+ toReturn);
        return toReturn;
        }
    
    
    public static double getMaxDistUsingMinMaxKappaAndFreqs(double minKappa,double maxKappa,double[] freqs)
        {
        double maxReturnableDist=Double.POSITIVE_INFINITY;            
        //System.out.println("Testing with freqs="+Arrays.toString(freqs));
        ArrayList<Double> kappaValuesAL=new ArrayList<Double>();
        double kappaDelta=Math.abs(minKappa-maxKappa)/10.0;
        if(Math.abs(minKappa-maxKappa)<=0.00001 || minKappa==maxKappa)
            {
            kappaDelta=Double.MAX_VALUE;
            }
        for(double tempKappa=minKappa;tempKappa<=maxKappa;tempKappa+=kappaDelta)
            {
            kappaValuesAL.add(tempKappa);
            }
        double[] kappaValues=new double[kappaValuesAL.size()];
        for(int k=0;k<kappaValuesAL.size();k++)
            {
            kappaValues[k]=kappaValuesAL.get(k);
            }
        for(int k=0;k<kappaValues.length;k++)
            {
            double kappa=kappaValues[k];
            HKY myHKYTestModel=new HKY(kappa,freqs);
            int maxDist=100000;
            for(int dist=1;dist<=maxDist;dist+=100)
                {
                //System.out.println("Dist="+dist+" kappa="+kappa);
                //System.out.println("Passed freqs : "+Arrays.toString(freqs));
                TransitionProbability mytpm=myHKYTestModel.getTransitionProbability();
                mytpm.setDistance(dist);
                double[][] probs=new double[4][];
                for(int from=0;from<4;from++)
                    {
                    probs[from]=new double[4];
                    for(int to=0;to<4;to++)
                        {
                        probs[from][to]=mytpm.getTransitionProbability(from,to);
                        }
                    }
                boolean allFourTolerEq=true;
                for(int from=0;from<4;from++)
                    {
                    //System.out.println(Arrays.toString(probs[from]));
                    boolean eq=TreeUtils.freqsEq(probs[from],freqs,false);
                    boolean tolerEq=TreeUtils.freqsEq(probs[from],freqs,true);
                    //System.out.println("Eq Flag : "+eq);
                    //System.out.println("TolerEq flag : "+tolerEq);
                    if(!tolerEq)
                        {
                        allFourTolerEq=false;
                        }
                    }//testing for eq
                //System.out.println("All four tolerEQ flag : "+allFourTolerEq);
                if(allFourTolerEq && dist>=10)
                    {
                    /*System.out.println("Returning max distance of "+dist+" based on probability matrix.");
                    System.out.println("Using frequencies : "+Arrays.toString(freqs)+" and kappa="+kappa);
                    System.out.println("At this distnace, the transition probability matrix is : ");
                    System.out.println(TreeUtils.transProbMatrixToString(probs));*/
                    maxReturnableDist=Math.min(dist,maxReturnableDist);
                    dist=maxDist+1;
                    //return dist;
                    }
                }//dist loop
            }//kappa iteration
        if(maxReturnableDist==Double.POSITIVE_INFINITY)
            {
            //never found a good distance to go?!?!
            throw new RuntimeException("Error, analyzed various kappa values ("+Arrays.toString(kappaValues)+") and varioud distances, and no explored distances were long/great enough so that probabilities were sufficiently near to equilibirum values!");
            }
        else
            {
            //System.out.println("From transition probability matrices exploration, max distance to return  is : "+(maxReturnableDist-1));
            return maxReturnableDist-1;
            }
        }
    
    
    double kappaWindowWidth;
    double kappaWindowHalfWidth;
    public KHYLikelihoodCalculator(HashMap iMap,double iminKappa,double imaxKappa)
        {
        this.setParams(iMap);
        this.minKappa=iminKappa;
        this.maxKappa=imaxKappa;
        if(this.minKappa>this.maxKappa)
            {
            throw new IllegalArgumentException("Error, minKappa>maxKappa?!?!");
            }
        if(Math.abs(this.maxKappa-this.minKappa)<=0.1)
            {
            kappaWindowWidth=this.maxKappa-this.minKappa;
            }
        else
            {
            kappaWindowWidth=Math.abs(maxKappa-minKappa)/10.0;
            }
        kappaWindowHalfWidth=kappaWindowWidth/2.0;
        }
    
   
    public Object getParamValue(String paramName)
        {
        return pMap.get(paramName);
        }
    

    public void derivExplore()
        {
        double minDist=KHYLikelihoodCalculator.getMinDistUsingMinMaxKappaAndFreqs(minKappa, maxKappa, freqs);
        double maxDist=KHYLikelihoodCalculator.getMaxDistUsingMinMaxKappaAndFreqs(minKappa, maxKappa, freqs);
        //for(double dist=KHYLikelihoodCalculator.getMinDistUsingMinMaxKappaAndFreqs(minKappa, maxKappa, freqs);
        String bases="ACGT";
            String header="dist\t";
            for(int from=0;from<4;from++)
                {
                char f=bases.toCharArray()[from];
                for(int to=0;to<4;to++)
                    {
                    char t=bases.toCharArray()[to];
                    String way=f+"->"+t;
                    header+=way;
                    if(!(to==3 && from==3))
                        {
                        //pass
                        header+="\t";
                        }
                    }
                }
        System.out.println("kappa="+this.kappa);
        System.out.println(header);
        for(double dist=minDist;dist<=10;dist+=0.0001)
            {
            double[][] da=this.getDerivativeArray(dist,true);
            System.out.print(dist+"\t");
            for(int from=0;from<4;from++)
                {
                for(int to=0;to<4;to++)
                    {
                    System.out.print(da[from][to]);
                    if(!(from==3 && to==3))
                        {
                        System.out.print("\t");
                        }
                    else
                        {
                        System.out.println("");
                        }
                    }
                }
            }
        }

    
    public BigDecimal computeLikelihood(coa_lineage t,FastaIO fio,boolean isLog,TreeGenerator tGen)
        {
        
        if(
                fio.getNumSeqs()>=5 && (this instanceof KHYLikelihoodCalculator )
                )
            {
                

               
            BigDecimal logLikelihood=null;
            Number mtLL=null;
            try
                {
                    
                    
                if(LikelihoodUtils.usePhyML)
                    {
                    //try PHYML!!!!                     
                    String phyMLLogLikelihoodString=LikelihoodUtils.getPhyMLLike(t,false,fio,this);
                    String niceDecStr=String.format("%.1f",Double.parseDouble(phyMLLogLikelihoodString));
                    logLikelihood=new BigDecimal(niceDecStr);
                    }
                else
                    {
                    //if the tree is used in the MCMC it must be unmodified
                    //that's why this cloning is necesasry
                    coa_lineage t_clone=t.cloneRec();
                    while(t_clone.hasExtraNodes(true))
                        {
                        t_clone.removeExtraNodes();    
                        }
                    //t_clone.makeBinary();
                    //t_clone=coa_lineage.makeCME();
                    HashSet lengthsToCacheHS=t_clone.getLengthsOfBranches();
		    ArrayList<Double> lengthsToCache=new ArrayList<Double>();
                    lengthsToCache.add(0.0);
                    for(Object o:lengthsToCacheHS)
                        {
                        lengthsToCache.add((double)(o));
                        }
                    boolean useFloat=false;
                    this.myHKY=new HKY(this.getKappa(),this.getFreqs());
                    mtLL=LikelihoodUtils.compuateBasicLikeStayLNAllSitesBySiteWrapperMT(
                            t_clone,
                            fio,
                            this.myHKY,
                            //KHYLikelihoodCalculator.getCMEK80(),
                            useFloat,
                            lengthsToCache
                            );

                    
                    logLikelihood=new BigDecimal(mtLL+"");          
                    //throw new RuntimeException("");
                    }
                return logLikelihood;
                }
            catch(Exception e)
                {
                System.err.println("Retrieved newick string : "+t.makeNewickString(true,fio));
                System.err.println("Message : "+e.getMessage());
                System.err.println("Tried to parse likelihood : "+logLikelihood);
                System.err.println("mtLL value : "+mtLL);
                e.printStackTrace();
                coa_lineage taggedForXML=tGen.tagForPhyloXML(t);
                String phyloXML=taggedForXML.makePhyloXML(true,0,false,new BigDecimal(0));             
                System.err.println("\nThe phyloXML : \n"+phyloXML+"\n");
                System.exit(0);
                }
            
            
            }//5 or more seqs
        else
            {
            throw new RuntimeException("Error, required : 5 or more sequences!");
            }
        return null;
        }
    

}
