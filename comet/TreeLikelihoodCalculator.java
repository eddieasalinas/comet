/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 *
 * @author esalina
 */
public interface TreeLikelihoodCalculator {

    
    public void proposeNewParams(boolean generate);
    public void setParams(HashMap pMap);
    public Object getParamValue(String paramName);
    public BigDecimal computeLikelihood(coa_lineage t,FastaIO fio,boolean isLog,TreeGenerator tGen);
    public double computeLikelihoodNBD(coa_lineage t,FastaIO fio,boolean isLog);
    public boolean isPruningComputed();
    public HashMap cloneParams();
    public boolean arePriorsAllSingle();
 
}
