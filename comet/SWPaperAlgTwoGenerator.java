/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import static comet.KingmanCoalescent.drawExponentialKing;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author esalina
 */
public class SWPaperAlgTwoGenerator implements TreeGenerator  {

    double beta=1;
    
    public double getBeta()
        {
        return beta;
        }

    public void setBeta(double b)
        {
        if(b<0 || b>1)
            {
            throw new IllegalArgumentException("Error, supplied beta is "+b+", but is not within range [0,1]!");
            }
        beta=b;
        }
    
        
    
    
    FastaIO myFastaIO=null;
    public void setFasta(FastaIO fio)
        {
        this.myFastaIO=fio;
        }    
    
    public double estThetaHatIP278(coa_lineage current)
        {
        Object[] tnh=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
        ArrayList<String> tags=(ArrayList<String>)tnh[0];
        int numLeaves=current.getNumLeavesUnder(true);
        double sum=0;
        for(String tag:tags)
            {
            if(tag!=null)
                {
                ArrayList<coa_lineage> nodesOfTag=current.getNodesWithTagMatching(tag,false);
                int k=0;
                for(coa_lineage nodeOfTag:nodesOfTag)
                    {
                    k+=nodeOfTag.getNumKids();
                    }
                double contribution=k*(k-1)*nodesOfTag.get(0).getDist(0);
                //System.err.println("At tag="+tag+" we have k="+k+" and contribution="+contribution);
                sum+=contribution;
                }
            }
        double ratio=sum/(numLeaves-1);
        return ratio;
        }
    
    HashMap params=null;
    double min_psi,max_psi;
    double c_val=2.0;
    
    public SWPaperAlgTwoGenerator(int num_lineages,double nminpsi,double nmaxpsi)
        {
        params=new HashMap();
        params.put("num_lineages",num_lineages);
        this.c_val=2.0;
        this.min_psi=nminpsi;
        this.max_psi=nmaxpsi;
        params=new HashMap();
        params.put("num_lineages",num_lineages);
        proposeParams();   
        }
    
    
    public HashMap getParams()
        {
        return this.params;
        }

    
    public HashMap cloneParams()
        {
        return TreeUtils.cloneMap(params);
        }
    
    
    public void setParams(HashMap paramMap)
        {
        params=paramMap;
        }

    
    int recentCode=0;
    
    public int getLastProposalCode()
        {
        return recentCode;
        }


    public String getProposalDescription(int code)
        {
        try
            {
            if(!(0<=code && code<=(proposalDescriptions.length-1)))
                {
                return "UNDEFINED";
                }
            else
                {
                return proposalDescriptions[code];    
                }            
            }
        catch(Exception e)
            {
            throw new RuntimeException("Error, unknown proposal code "+code);
            }
        }
    
    
    public static boolean veriftyNodesAtTagSameByHeight(String t,coa_lineage root)
        {
        ArrayList<coa_lineage> taggedNodes=root.getNodesWithTagMatching(t,true);
        
        if(taggedNodes.isEmpty())
            {
            throw new RuntimeException("Could not find nodes tagged with :"+t+"!?!?!");
            }
        double h=taggedNodes.get(0).getHeight();
        for(int i=1;i<taggedNodes.size();i++)
            {
            double th=taggedNodes.get(i).getHeight();
            if(th!=h)
                {
                if(TreeUtils.tolerEq(h,th))
                    {
                    System.out.println("WARNING tagged nodes with tag="+t+" have heights "+h+" and "+th+" but are 'tolerEq'!");
                    }
                else
                    {
                    throw new RuntimeException("Tagged nodes with tag="+t+" have heights "+h+" and "+th+" but are NOT 'tolerEq'!");
                    }
                }
            }
        return true;
        }
    
    
    public static boolean verifyOkayDistUnderNodesOfATag(String t,coa_lineage root)
        {
        boolean flag=true;
        ArrayList<coa_lineage> nodesOfATag=root.getNodesWithTagMatching(t,true);
        for(coa_lineage taggedNode:nodesOfATag)
            {
            int n=taggedNode.getNumKids();
            for(int i=0;i<n;i++)
                {
                double a_dist=taggedNode.getDist(i);
                if(!TreeUtils.isValidExpSample(a_dist))
                    {
                    System.out.println("Node tagged as "+t+" found to have dist "+a_dist+"!");
                    flag=false;
                    }
                }
            }
        return flag;
        }
    
    
    public static boolean verifyAGivenKingTagHasExactlyOneNodeWithTwoKidsAndAllOthersWithOneKid(String t,coa_lineage root)
        {
        ArrayList<coa_lineage> kingsOfATag=root.getNodesWithTagMatching(t,true);
        HashMap kidCount=new HashMap();
        for(coa_lineage k:kingsOfATag)
            {
            int n=k.getNumKids();
            if(kidCount.containsKey(n))
                {
                int existing=(Integer)kidCount.get(n);
                existing++;
                kidCount.put(n,existing);
                }
            else
                {
                kidCount.put(n,1);
                }
            }
        boolean isOkay=false;
        System.out.println("For tag : "+t);
        System.out.println("Num kids\tcount");
        int num2=0;
        int num1=0;
        int numOther=0;
        for(Object k:kidCount.keySet())
            {
            int c=(Integer)(kidCount.get(k));
            System.out.println(k+"\t"+c);
            if((Integer)k==2)
                {
                num2+=c;
                }
            else if((Integer)k==1)
                {
                num1+=c;
                }
            else
                {
                numOther+=c;
                }
            }
        if(num2==1 && numOther==0)
            {
            isOkay=true;
            }
        else
            {
            isOkay=false;
            }
        return isOkay;
        }
    
    
    
    
    public static void describeTree(coa_lineage root,int retCode,SWPaperAlgTwoGenerator myGen)
        {
        double h=root.getHeight();
        System.out.println("\n\n\n\nDESCRIBING TREE:");
        System.out.println("recent code="+myGen.getProposalDescription(retCode));
        System.out.println("Height\t"+h);
        HashMap pMap=root.getPosteriorSampleMap();
        String profile=pMap.get("sre_profile").toString();
        SREvent[] srEvents=SREvent.parseJSON(profile);
        String pWaits=pMap.get("sre_waits").toString();
        String pHeights=pMap.get("sre_heights").toString();
        System.out.println("Profile\t"+profile);
        double[] compHeights=SREvent.getSREHeights(srEvents);
        double[] compWaits=SREvent.getSREWaits(srEvents);
        System.out.println(" Waits  \t"+pWaits);
        System.out.println("cWaits  \t"+Arrays.toString(compWaits));        
        System.out.println(" Heights\t"+pHeights);
        System.out.println("cHeights\t"+Arrays.toString(compHeights));
        ArrayList<coa_lineage> allNodes=root.get_descendants(true);
        coa_lineage.sortByHeightInPlace(allNodes, true);
        boolean stop=false;
        for(int n=0;n<allNodes.size();n++)
            {
            coa_lineage x=allNodes.get(n);
            if(n==0)
                {
                System.out.println("ID\tH\tTG\tMS\tNK");
                }
            System.out.print(n+"\t"+x.getHeight()+"\t"+x.getTag()+"\t"+x.getMergerStyle()+"\t"+x.getNumKids()+"\n");
            if(x.getHeight()==Double.POSITIVE_INFINITY   || x.getHeight()==Double.NEGATIVE_INFINITY)
                {
                System.out.println("recent code="+retCode);
                stop=true;
                }
            }
        Object[] tnh=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(root);
        ArrayList<String> tags=(ArrayList<String>)tnh[0];
        for(int t=0;t<tags.size();t++)
            {
            String tag=tags.get(t);
            if(tag!=null)
                {
                veriftyNodesAtTagSameByHeight(tag,root);
                if(tag.startsWith("KING"))
                    {
                    boolean kingV=verifyAGivenKingTagHasExactlyOneNodeWithTwoKidsAndAllOthersWithOneKid(tag,root);
                    if(!kingV)
                        {
                        throw new RuntimeException("Error, fail kingman kid validation (nodes of a king tag not exactly 1 with two kids) with recentCode=("+
                                retCode+
                                ")("+myGen.getProposalDescription(retCode)+")!");
                        }
                    }
                boolean nanOkay=verifyOkayDistUnderNodesOfATag(tag,root);
                if(!nanOkay)
                    {
                    throw new RuntimeException("Error, fail NAN check ! with recentCode=("+
                                retCode+
                                ")("+myGen.getProposalDescription(retCode)+")!");
                    }
                }
            }
        boolean vk=root.verifyKidsAndDistsConsistency();
        boolean vh=root.verifyHeights(false);
        if(!vk)
            {
            System.out.println("FAIL VD!");
            }
        if(!vh)
            {
            System.out.println("FAIL VH!");
            }
        if(!vh || !vk)
            {
            throw new RuntimeException("FAIL VD OR VH!");
            }        
        if(stop)
            {
            throw new RuntimeException("inf");
            }
        System.out.println("\n\n\n");
        }
    
        
    public Object[] proposeTree(coa_lineage current,BigDecimal cPrior)
        {
        Object[] ret_pack;
        //getRandomInt is INCLUSIVE
        if(recentCode!=3 && recentCode!=1)
            {
            double whichHeightToAdjust=TreeUtils.getRandNumBet(0,1,false);
            if(whichHeightToAdjust<=0.5)
                {
                recentCode=1;                    
                ret_pack=adjustSREHeight(current,cPrior);
                }
            else
                {
                recentCode=3;
                ret_pack=kingDistPerturb(current,cPrior);
                }
            //System.out.println("recentCode is "+recentCode);
            double c_ML=mlEstC(
                    (coa_lineage)ret_pack[0],
                    true);
            ((coa_lineage)ret_pack[0]).setPosteriorSampledValue("c_ML",c_ML);
            //System.err.println("first block rpt, recentCode="+recentCode);
            return ret_pack;
            }
        
        int randNum=TreeUtils.getRandomInt(0,11);
        recentCode=randNum+1;
        if(randNum==0)
            {
            ret_pack=adjustSREHeight(current,cPrior);
            }
        else if(randNum==1)
            {
            ret_pack=sreSwap(current,cPrior);
            }
        else if(randNum==2)
            {
            ret_pack=kingDistPerturb(current,cPrior);
            }
        else if(randNum==3)
            {
            ret_pack=kingSwap(current,cPrior);
            }
        else if(randNum==4)
            {
            ret_pack=overlapExtension(current,cPrior);
            }
        else if(randNum==5)
            {
            ret_pack=overlapRetraction(current,cPrior);
            }
        else if(randNum==6)
            {
            ret_pack=sreParentReSelection(current,cPrior);
            }
        else if(randNum==7)
            {
            ret_pack=kingParentReSelection(current,cPrior);
            }
        else if(randNum==8)
            {
            ret_pack=sreCap(current,cPrior);
            }
        else if(randNum==9)
            {
            ret_pack=sreUnCap(current,cPrior);
            }
        else if(randNum==10)
            {
            ret_pack=kingmanDescension(current,cPrior);
            }
        else
            {
            ret_pack=kingmanAscension(current,cPrior);
            }

            
        //System.out.println("The recentCode is ("+recentCode+") : "+getProposalDescription(recentCode)+".");
        /*describeTree((coa_lineage)(ret_pack[0]),recentCode,this);
        boolean vd=((coa_lineage)(ret_pack[0])).verifyKidsAndDistsConsistency();
        if(!vd)
            {
            throw new RuntimeException("Failed verifyKidsAndDistsConsistency");
            }*/
        double c_ML=mlEstC(
                    (coa_lineage)ret_pack[0],
                    true);
        ((coa_lineage)ret_pack[0]).setPosteriorSampledValue("c_ML",c_ML);
        //System.err.println("second block rpt, recentCode="+recentCode);
        return ret_pack;
        }
        
    public static final String[] proposalDescriptions={
        "GENERIC_PROPOSAL",             //0
        "SRE_LEN_ADJUST",               //1
        "SRE_SWAP",                     //2
        "KINGMANG_HEIGHT_PERTURB",      //3
        "KING_SWAP",                    //4
        "OVERLAP_EXTENSION",            //5
        "OVERLAP_RETRACTION",           //6
        "SRE_PARENT_RESELECTION",       //7
        "KING_PARENT_RESELECTION",      //8
        "SRE_CAP",                      //9
        "SRE_UN_CAP",                   //10
        "KINGMAN_DESCENSION",           //11
        "KINGMAN_ASCENSION"             //12    
        };
    
    
    public Object[] sreUnCap(coa_lineage current,BigDecimal cPrior)
        {
        /*
        If the root is an SRE node with exactly two children,
            this code proposes turning it into a KINGMAN node.
        */
            
            
        String rootTag=current.getTag();
        int numKids=current.getNumKids();
        int rootMergerStyle=current.getMergerStyle();
        if( numKids==2 &&
                rootMergerStyle==coa_lineage.SRE_STYLE
                && rootTag.startsWith("SRE"))
            {
            ArrayList<Integer> srePieces=SWPaperAlgTwoGenerator.getTagIntArray(rootTag);
            int sreNum=srePieces.get(0);
            int prevSreNum=sreNum-1;
            double rootHeight=current.getHeight();
            double distToPrevSRE=rootHeight;
            if(prevSreNum>=0)
                {
                //not at the first SRE
                String targetSRETag="SRE_"+prevSreNum;
                coa_lineage anSRENode=current.getOneNodeMatchingTag(targetSRETag);
                double prevSreHeight=anSRENode.getHeight();
                distToPrevSRE=rootHeight-prevSreHeight;
                }
            else
                {
                //root is the first and only SRE!
                }
            double randSREVal=TreeUtils.drawExponentialMeanOne();
            if(randSREVal<distToPrevSRE)
                {
                //perform the uncapping!
                String tagBelow=current.getChild(0).getTag();
                int king_coa=(-1);
                if(tagBelow.startsWith("KING"))
                    {
                    //get king_coa as increment from previous tag
                    ArrayList<Integer> kingPieces=SWPaperAlgTwoGenerator.getTagIntArray(tagBelow);
                    king_coa=kingPieces.get(1)+1;
                    }
                else
                    {
                    //king coa_to become 0
                    king_coa=0;
                    }
                //Object[] tnh=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
                //System.out.println("Tags before : "+tnh[0].toString());                
                //String sreJSONBefore=current.getPosteriorSampleMap().get("sre_profile").toString();
                String targetTag="KING_"+Math.max(0,prevSreNum+1)+"_"+king_coa;
                //System.out.println("Pre tag : "+current.getTag());
                //System.out.println("Pre merger style : "+current.getMergerStyle());
                current.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                current.setTag(targetTag);
                double currentC=mlEstC(current,false);
                double rate_num=2; //K=2 here because the root has 2 kids
                double rate_dnm=currentC;
                double kingDraw=TreeUtils.drawExponentialWithRate(rate_num/rate_dnm);
                current.setAllDists(kingDraw);
                current=applySREPosteriorMapped(current);
                Object[] ret_pack=new Object[]{current,cPrior};
                return ret_pack;
                }
            else
                {
                return proposeTree(current,cPrior);
                }            
            }
        else 
            {      
            return proposeTree(current,cPrior);
            }           
        }
    
    
    
    private static coa_lineage applySREPosteriorMapped(coa_lineage current)
        {
        SREvent[] newEvents=generateEvents(current);
        String waitsStr=SREvent.getWaitsArrAsStr(newEvents);
        String heightsStr=SREvent.getHeightsArrAsStr(newEvents);
        current.setPosteriorSampledValue("sre_profile",SREvent.JSONString(newEvents));
        String sreJSONAfter=current.getPosteriorSampleMap().get("sre_profile").toString();
        current.setPosteriorSampledValue("sre_waits",waitsStr);
        current.setPosteriorSampledValue("sre_heights",heightsStr);
        int[] mergerCounts=countMergers(current);
        current.setPosteriorSampledValue("KingmanMergers",mergerCounts[0]);
        current.setPosteriorSampledValue("SREMergers",mergerCounts[1]);          
        return current;
        }
    
    
    public Object[] sreCap(coa_lineage current,BigDecimal cPrior)
        {
        //The purpose of SRECAP is to transform the root node from
        //KINGMAN style/tag to SRE style/tag
        String rootTag=current.getTag();
        if(rootTag==null)
            {
            //System.out.println("root tag should never be null!");
            throw new RuntimeException("Error, root tag should never be null!");
            }
        int rootMergerStyle=current.getMergerStyle();
        if(rootMergerStyle==coa_lineage.KINGMAN_STYLE
                && rootTag.startsWith("KING"))
                
            {
            //test for dist from SRE first
            ArrayList<Integer> kingPieces=SWPaperAlgTwoGenerator.getTagIntArray(rootTag);
            int srePiece=kingPieces.get(0);
            boolean proceedWithCap=false;
            double distToNearestSRE;
            double distUnder=current.getDist(0);
            double randomSREWait=TreeUtils.drawExponentialMeanOne();
            double rootHeight=current.getHeight();            
            if(srePiece==0)
                {
                //there's no SRE tagged node so we can go aheads
                proceedWithCap=true;
                }
            else
                {
                double sreHeight=0;
                int targetSREVal=srePiece-1;
                String targetSRETag="SRE_"+targetSREVal;
                Object[] tnh=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
                ArrayList<String> tags=(ArrayList<String>)(tnh[0]);
                ArrayList<Double> heights=(ArrayList<Double>)(tnh[2]);
                for(int i=0;i<tags.size();i++)
                    {
                    if(tags.get(i)!=null)
                        {
                        if(tags.get(i).compareTo(targetSRETag)==0)
                            {
                            sreHeight=heights.get(i);
                            i=tags.size();
                            }
                        }
                    }                
                distToNearestSRE=rootHeight-sreHeight;
                double lowerBound=rootHeight-distUnder*0.1;
                randomSREWait=TreeUtils.drawTruncatedExponentialMeanOne(lowerBound,100);
                if(randomSREWait<distToNearestSRE)
                    {
                    proceedWithCap=true;
                    }                
                }
            if(proceedWithCap)
                {
                /*String sreJSONBefore=current.getPosteriorSampleMap().get("sre_profile").toString();
                System.out.println("Pre tag : "+current.getTag());
                System.out.println("Pre MS : "+current.getMergerStyle());
                Object[] tnh=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
                System.out.println("Tags before : "+tnh[0].toString());*/
                current.setTag("SRE_"+srePiece);
                current.setMergerStyle(coa_lineage.SRE_STYLE);
                if(srePiece!=0)
                    {
                    //in this case, randomSREWait<distToNearestSRE
                    //because of the lowerbound of the truncated distribution
                    //the dist can be modified like so
                    double newDistUnder=rootHeight-randomSREWait;
                    current.setAllDists(newDistUnder);
                    }
                current=applySREPosteriorMapped(current);         
                Object[] ret_pack=new Object[]{current,cPrior};
                return ret_pack;
                }
            else
                {
                return proposeTree(current,cPrior);
                }
            }
        else 
            {      
            return proposeTree(current,cPrior);
            }        
        }
    
    
    
    public Object[] kingParentReSelection(coa_lineage current,BigDecimal cPrior)
        {
        /*
        1)  Find KING nodes with exactly two children such that at least one other king node with one child exists with the same tag
            If no such node exists, then recurse to proposeTree.
        2)  Remove at random one of the two lineages and have another node of the same tag adopt it.
        */
        Pattern kingPattern=Pattern.compile("^KING_");
        HashMap pCount=new HashMap();
        ArrayList<coa_lineage> kingNodes=current.getNodesWithTagMatching(kingPattern, true);
        ArrayList<coa_lineage> nodesWithTwoKids=new ArrayList<coa_lineage>();
        for(coa_lineage kingNode:kingNodes)
            {
            int numKids=kingNode.getNumKids();
            String tag=kingNode.getTag();
            if(pCount.containsKey(tag))
                {
                int count=(Integer)pCount.get(tag);
                count++;
                pCount.put(tag,count);
                }
            else
                {
                pCount.put(tag,1);
                }
            if(numKids==2)
                {
                nodesWithTwoKids.add(kingNode);
                }
            }
        if(nodesWithTwoKids.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        ArrayList<coa_lineage> availableNodes=new ArrayList<coa_lineage>();
        for(coa_lineage nodeWithTwoKids:nodesWithTwoKids)
            {
            String tag=nodeWithTwoKids.getTag();
            int tagCount=(Integer)(pCount.get(tag));
            if(tagCount>=2)
                {
                availableNodes.add(nodeWithTwoKids);
                }
            }
        if(availableNodes.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        //choose a node at random and one of its kids at random
        coa_lineage nodeToGiveUpAChild=(coa_lineage)TreeUtils.chooseRandom(availableNodes);
        int randomKidIndex=nodeToGiveUpAChild.chooseRandomKidIndex();
        //now find a node to adopt
        ArrayList<coa_lineage> prospectiveParents=new ArrayList<coa_lineage>();
        String theTag=nodeToGiveUpAChild.getTag();
        ArrayList<coa_lineage> nodesWithSameTag=current.getNodesWithTagMatching(theTag,false);
        for(coa_lineage taggedNode:nodesWithSameTag)
            {
            int tagNumKids=taggedNode.getNumKids();
            if(tagNumKids==1 && taggedNode!=nodeToGiveUpAChild)
                {
                prospectiveParents.add(taggedNode);
                }
            }
        if(prospectiveParents.isEmpty())
            {
            String emptyPPString="Error, prospectiveParents is empty, but shoudln't be because ";
            emptyPPString+="pCount should have already verified a prospective parent would be found!";
            throw new RuntimeException(emptyPPString);
            }
        coa_lineage adoptiveParent=(coa_lineage)TreeUtils.chooseRandom(prospectiveParents);
        //now perform the adoption
        coa_lineage detached=nodeToGiveUpAChild.getChild(randomKidIndex);
        double distToAdd=nodeToGiveUpAChild.getDist(randomKidIndex);
        nodeToGiveUpAChild.removeChild(detached);
        adoptiveParent.addChild(detached,distToAdd);
        //since no lengths nor SREs are changes no prior's must be updated
        Object[] ret_pack=new Object[]{current,cPrior};
        return ret_pack;
        }
    
    
    
    public Object[] sreParentReSelection(coa_lineage current,BigDecimal cPrior)
        {
        /*
        1)  From the SRE nodes with at least 2 in an SRE, choose one at random with n>=2 kids
        2)  Choose a random child, a random parent (at the same level)
        3)  process the adoption!
        */
        Pattern srePattern=Pattern.compile("^SRE_");
        HashMap pCount=new HashMap();
        ArrayList<coa_lineage> sreNodes=current.getNodesWithTagMatching(srePattern, true);
        ArrayList<coa_lineage> availableNodes=new ArrayList<coa_lineage>();
        for(coa_lineage sreNode:sreNodes)
            {
            int numKids=sreNode.getNumKids();
            if(numKids>=2)
                {
                String tag=sreNode.getTag();
                if(pCount.containsKey(tag))
                    {
                    int count=(Integer)pCount.get(tag);
                    count++;
                    pCount.put(tag,count);
                    for(int i=0;i<numKids;i++)
                        {
                        availableNodes.add(sreNode);
                        }
                    }
                else
                    {
                    pCount.put(tag,1);
                    }
                }
            }
        if(availableNodes.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        //  Choose on at random
        coa_lineage nodeToGiveUpAChild=(coa_lineage)TreeUtils.chooseRandom(availableNodes);
        String sreTag=nodeToGiveUpAChild.getTag();
        ArrayList<coa_lineage> levelNodes=current.getNodesWithTagMatching(sreTag,false);
        ArrayList<coa_lineage> possibleAdoptiveParents=new ArrayList<coa_lineage>();
        for(coa_lineage levelNode:levelNodes)
            {
            if(levelNode!=nodeToGiveUpAChild)
                {
                possibleAdoptiveParents.add(levelNode);
                }
            }
        if(possibleAdoptiveParents.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        //perform the adoption transfer
        coa_lineage adoptiveParent=(coa_lineage)TreeUtils.chooseRandom(possibleAdoptiveParents);
        double distUnderSRE=adoptiveParent.getDist(0);
        int randomKidIndex=nodeToGiveUpAChild.chooseRandomKidIndex();
        coa_lineage detached=nodeToGiveUpAChild.getChild(randomKidIndex);
        nodeToGiveUpAChild.removeChild(detached);
        adoptiveParent.addChild(detached,distUnderSRE);
        
        
        current=applySREPosteriorMapped(current);        
        Object[] ret_pack=new Object[]{current,cPrior};             
     
        return ret_pack;
        }
    

    public Object[] overlapRetraction(coa_lineage current,BigDecimal cPrior)
        {
        /*
        1)  a node N is eligible for retraction if it is SRE node and has exactly one child
            and there is another node at the SRE level to accept the retraction.  
            Additionally the parent of N must have 2 or more children.
            Pick such a node at random.  If none is available return proposeTree.
        2)  Take N's only child and set it to be a child of a node A at the SRE level
            such that A is a child of N's parent.
        3)  Special logic is used because a Kingman layer becomes dropped and to
            compensate at SRE above if there is one
        */
        Pattern srePattern=Pattern.compile("^SRE_");
        ArrayList<coa_lineage> sreNodes=current.getNodesWithTagMatching(srePattern,true);
        ArrayList<coa_lineage> acceptableNodes=new ArrayList<coa_lineage>();
        ArrayList<coa_lineage> acceptableNodesParents=new ArrayList<coa_lineage>();
        ArrayList<coa_lineage> acceptableNodesParentsParents=new ArrayList<coa_lineage>();
        HashMap pCount=new HashMap();
        double distUnderRemovedKingmandLayer=0.0;
        //use this first loop for counting occurences of SREnodes
        for(coa_lineage sreNode:sreNodes)
            {
            String t=sreNode.getTag();
            if(pCount.containsKey(t))
                {
                int c=(Integer)(pCount.get(t));
                c++;
                pCount.put(t,c);
                }
            else
                {
                pCount.put(t,1);
                }
            }
        for(coa_lineage sreNode:sreNodes)
            {
            String t=sreNode.getTag();
            int numKids=sreNode.getNumKids();            
            int count=(Integer)(pCount.get(t));
            if(count>=2 && numKids==1)
                {
                //require count>=2 so that there's another node at the SRE level to accept the child
                coa_lineage itsParent=current.findTheParentOfGivenNode(sreNode);
                coa_lineage grandParent=current.findTheParentOfGivenNode(itsParent);                
                int numKidsOfParent=itsParent.getNumKids();
                if(numKidsOfParent==2)
                    {
                    if(itsParent==current)
                        {
                        //its root, require more nodes at this SRE first
                        //for now keep this code block on the simple side...
                        acceptableNodes.add(sreNode);
                        acceptableNodesParents.add(itsParent);
                        acceptableNodesParentsParents.add(null);
                        }
                    else
                        {
                        acceptableNodes.add(sreNode);
                        acceptableNodesParents.add(itsParent);
                        acceptableNodesParentsParents.add(grandParent);
                        }
                    }
                else if(numKidsOfParent>2)
                    {
                    acceptableNodes.add(sreNode);
                    acceptableNodesParents.add(itsParent);
                    acceptableNodesParentsParents.add(grandParent);
                    }
                }
            }
        if(acceptableNodes.isEmpty())
            {
            return proposeTree(current,cPrior);
            }

        //find a random acceptable node
        int accIdx=TreeUtils.chooseRandomIndex(acceptableNodes.size());
        coa_lineage randomAcceptableNode=acceptableNodes.get(accIdx);
        String randomAcceptableNodeTag=randomAcceptableNode.getTag();
        ArrayList<Integer> srePieces=getTagIntArray(randomAcceptableNodeTag);
        int sreNum=srePieces.get(0);
        coa_lineage randomAcceptableNodeParent=acceptableNodesParents.get(accIdx);
        //find a random acceptable adoptive parent for the acceptable nodes only child
        //Use the parent of the acceptable node and choose one of the acceptable node's siblings
        ArrayList<coa_lineage> prospectiveAdoptiveParents=new ArrayList<coa_lineage>();
        for(int k=0;k<randomAcceptableNodeParent.getNumKids();k++)
            {
            coa_lineage sibling=randomAcceptableNodeParent.getChild(k);
            if(sibling!=randomAcceptableNode)
                {
                //node won't adopt back it's own child!
                prospectiveAdoptiveParents.add(sibling);
                }
            }
        if(prospectiveAdoptiveParents.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        //now perform the adoptions
        double distUnderSRE=randomAcceptableNode.getDist(0);
        coa_lineage theParentToAdopt=(coa_lineage)TreeUtils.chooseRandom(prospectiveAdoptiveParents);
        theParentToAdopt.addChild(randomAcceptableNode.getChild(0),distUnderSRE);
        randomAcceptableNodeParent.removeChild(randomAcceptableNode);
        
        if(randomAcceptableNodeParent==current && randomAcceptableNodeParent.getNumKids()==1)
            {
            coa_lineage newRoot=randomAcceptableNodeParent.getChild(0);
            //the new root cannot point back to its previous parent (old root)
            newRoot.resetParentRef();
            current=newRoot;
            current=applySREPosteriorMapped(current);       
            Object[] ret_pack=new Object[]{current,cPrior};                   
            return ret_pack;
            }

        
        if(acceptableNodesParents.get(0).getTag().startsWith("KING"))
            {
            distUnderRemovedKingmandLayer=acceptableNodesParents.get(0).getDist(0);
            }        
        
        
        if(randomAcceptableNodeParent.getTag().startsWith("KING"))
            {
            //if the parent layer is KINGMAN it'll now have one less incoming lineage.
            //this block helps to compensate for that.
            
            
            coa_lineage grandParent=acceptableNodesParentsParents.get(accIdx);
            String grandParentTag=grandParent.getTag();
            ArrayList<coa_lineage> grandLevelNodes=current.getNodesWithTagMatching(grandParentTag,false);
            double distToAdd=grandParent.getDist(0);
            for(coa_lineage gNode:grandLevelNodes)
                {
                int n=gNode.getNumKids();
                ArrayList<coa_lineage> toDetach=new ArrayList<coa_lineage>();
                for(int k=0;k<n;k++)
                    {
                    coa_lineage theChildToDetach=gNode.getChild(k);
                    if(theChildToDetach.getNumKids()!=1)
                        {
                        throw new RuntimeException("Error, kingman assumption broken!");
                        }
                    else
                        {
                        gNode.addChild(theChildToDetach.getChild(0),distToAdd);
                        toDetach.add(theChildToDetach);
                        }
                    }
                for(coa_lineage nodeToDetach:toDetach)
                    {
                    gNode.removeChild(nodeToDetach);
                    }
                }
            }
        Object [] tnhExamineForMod=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
        ArrayList<String> tagsToExamineForMod=(ArrayList<String>)tnhExamineForMod[0];
        ArrayList<String> tagsToRename=new ArrayList<String>();
        for(String tAfter:tagsToExamineForMod)
            {
            if(tAfter!=null)
                {
                if(tAfter.startsWith("KING"))
                    {
                    ArrayList<Integer> kingPieces=getTagIntArray(tAfter);
                    int kingSREPiece=kingPieces.get(0);
                    if(kingSREPiece==sreNum+1)
                        {
                        tagsToRename.add(tAfter);
                        }
                    }
                }
            }

        for(int i=tagsToRename.size()-1;i>=0;i--)
            {
            //System.out.println("#"+i+"\t"+tagsToRename.get(i));
            String oldTag=tagsToRename.get(i);
            ArrayList<Integer> kingPieces=getTagIntArray(oldTag);
            int kingSREPiece=kingPieces.get(0);            
            int kingCOAPiece=kingPieces.get(1)-1;
            ArrayList<coa_lineage> toRetag=current.getNodesWithTagMatching(oldTag,false);
            for(coa_lineage nodeToRetag:toRetag)
                {
                nodeToRetag.setTag("KING_"+kingSREPiece+"_"+kingCOAPiece);
                }
            }
       
        //compensate at the next SRE if necessary
        if(distUnderRemovedKingmandLayer!=0.0)
            {
            int nextSRENum=sreNum+1;
            String nextSRE="SRE_"+nextSRENum;
            ArrayList<coa_lineage> nextSRENodes=current.getNodesWithTagMatching(nextSRE,false);
            if(!nextSRENodes.isEmpty())
                {
                double existingDist=nextSRENodes.get(0).getDist(0);
                double newDist=existingDist+distUnderRemovedKingmandLayer;
                for(coa_lineage sreNodeToCompensate:nextSRENodes)
                    {
                    sreNodeToCompensate.setAllDists(newDist);
                    }
                }

            }
        
        
        current=applySREPosteriorMapped(current);       
        Object[] ret_pack=new Object[]{current,cPrior};             
     
        return ret_pack;
        }
    
    
    public static int getSRENum(String sreTag)
        {
        Pattern p=Pattern.compile("SRE_(\\d+)");
        Matcher m=p.matcher(sreTag);
        if(m.find())
            {
            String d=m.group(1);
            int sreNum=Integer.parseInt(d);
            return sreNum;
            }
        throw new IllegalArgumentException("Error, expcted SRE tag with pattern "+p.pattern()+" but got tag "+sreTag);
        }
    
    
    public static SREvent[] generateEvents(coa_lineage root)
        {
        Object[] tagsNodesHeights=getTagsNodesHeightsNameRepresented(root);
        ArrayList<String> tags=(ArrayList<String>)tagsNodesHeights[0];
        ArrayList<String> names=(ArrayList<String>)tagsNodesHeights[3];
        //System.out.println("In generateEvents, TNH is ");
        //showTagsNodesHeights(getTagsNodesHeightsNameRepresented(root));
        ArrayList<String> sreTags=new ArrayList<String>();
        //System.out.println("TAGS : "+tags.toString());
        for(int i=0;i<tags.size();i++)
            {
            String tag=tags.get(i);
            if(tag==null)
                {
                //System.out.println("THE NAME OF A NULL TAG IS "+names.get(i));
                }
            else
                {
                if(tag.startsWith("SRE"))
                    {
                    sreTags.add(tag);
                    } 
                }
            }
        ArrayList<SREvent> newSREvents=new ArrayList<SREvent>();
        for(String sreTag:sreTags)
            {
            int newY=0;
            int mergerCount=0;
            int overlapCount=0;            
            ArrayList<coa_lineage> nodesOfTag=root.getNodesWithTagMatching(sreTag,false);
            for(coa_lineage node:nodesOfTag)
                {
                int m_kids=node.getNumKids();
                if(m_kids>=2)
                    {
                    newY++;
                    }
                if(m_kids>1)
                    {
                    mergerCount+=m_kids;
                    }
                else if(m_kids==1)
                    {
                    overlapCount++;
                    }
                else
                    {
                    System.out.println("For a node m, its num kids is : "+node.getNumKids());
                    throw new RuntimeException("Error, invalid number of kids at depth!");
                    }                
                }
            double pctOverLap=(double)(overlapCount)/(double)(mergerCount+overlapCount);            
            double psiValue=1.0-pctOverLap;
            double heightValue=nodesOfTag.get(0).getHeight();
            if(newY==0)
                {
                newY=1;
                }
            SREvent aNewEvent=new SREvent(newY,psiValue,heightValue);
            newSREvents.add(aNewEvent);
            }
        SREvent[] newSortedEventsArr=SREvent.sortByHeight(newSREvents);
        return  newSortedEventsArr;
        }
    
    
    public Object[] kingmanAscension(coa_lineage current,BigDecimal cPrior)
        {
        /*
            1)  Enumerate SRE nodes such that the layer below them is KINGMAN
                AND at least one child of the SRE has two kids (THE merger at the kingman layer)
            2)  choose one such SRE node at random
            3)  absorbe the KINGMAN coalescents *into* the kingman layer
                a)  for the SRE nodes having the kingman under them, they get two more children for that
                lineage they merge to
                b)  other SRE lineages just add plain 
            
        */
            
        
            
        Pattern srePattern=Pattern.compile("^SRE_");
        HashMap pCount=new HashMap();
        ArrayList<coa_lineage> sreNodes=current.getNodesWithTagMatching(srePattern, true);
        ArrayList<coa_lineage> acceptableNodes=new ArrayList<coa_lineage>();
        for(coa_lineage sreNode:sreNodes)
            {
            int numKids=sreNode.getNumKids();
            String tag=sreNode.getTag();
            if(pCount.containsKey(tag))
                {
                int count=(Integer)pCount.get(tag);
                count++;
                pCount.put(tag,count);
                }
            else
                {
                pCount.put(tag,1);
                }
            boolean addThisNode=false;
            for(int k=0;k<numKids;k++)
                {
                coa_lineage kid=sreNode.getChild(k);
                String kidTag=kid.getTag();
                if(kidTag!=null)
                    {
                    if(kidTag.startsWith("KING"))
                        {
                        int numGKids=kid.getNumKids();
                        if(numGKids>=2)
                            {
                            addThisNode=true;
                            }
                        }
                    }
                }
            if(addThisNode)
                {
                acceptableNodes.add(sreNode);
                }
            }
        if(acceptableNodes.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        coa_lineage acceptableNode=(coa_lineage)TreeUtils.chooseRandom(acceptableNodes);
        String acceptableTag=acceptableNode.getTag();
        ArrayList<coa_lineage> sreNodesAtLevel=current.getNodesWithTagMatching(acceptableTag,false);
        double newDist=acceptableNode.getDist(0);
        newDist+=acceptableNode.getChild(0).getDist(0);
        for(coa_lineage sreNodeAtLevel:sreNodesAtLevel)
            {
            int numKids=sreNodeAtLevel.getNumKids();
            ArrayList<coa_lineage> toAdd=new ArrayList<coa_lineage>();
            for(int k=0;k<numKids;k++)
                {
                coa_lineage child=sreNodeAtLevel.getChild(k);
                int numGKidsHere=child.getNumKids();
                for(int g=0;g<numGKidsHere;g++)
                    {
                    toAdd.add(child.getChild(g));
                    }
                }
            sreNodeAtLevel.dropAllChildren();
            for(coa_lineage nodeToAdd:toAdd)
                {
                sreNodeAtLevel.addChild(nodeToAdd,newDist);
                }
            }
        
        current=applySREPosteriorMapped(current);
        Object[] ret_pack=new Object[]{current,cPrior};        
        return ret_pack;
        }
                           
    public coa_lineage tagForPhyloXML(coa_lineage root)
        {
        //the nodes are already tagged
        coa_lineage rootClone=root.cloneRec();
        return rootClone;
        }
    
    public Object[] kingmanDescension(coa_lineage current,BigDecimal cPrior)
        {
        /*
        1)  find a random SRE node with n>=2 input lineages merging
            give extra weight to a node for each n>2.
        2)  Determine if there is a Kingman, SRE, or leaves below it and
            the K value to compute the kingman rate.  As well as its new tag.
        3)  Draw from a truncated Kingman putting appropriate bounds
        4)  add the layer and new coalescent event in it
        */
            
        Pattern srePattern=Pattern.compile("^SRE");
        ArrayList<coa_lineage> sreNodes=current.getNodesWithTagMatching(srePattern,true);
        ArrayList<coa_lineage> acceptableNodes=new ArrayList<coa_lineage>();
        for(coa_lineage sreNode:sreNodes)
            {
            int numKids=sreNode.getNumKids();
            if(numKids>=2)
                {
                if(!(sreNode==current && numKids<=2))
                    {
                    for(int i=1;i<=sreNode.getNumKids();i++)
                        {
                        //this loop adds extra weight to nodes with n>2 kids
                        acceptableNodes.add(sreNode);
                        //System.out.println("Using pattern "+srePattern+", a node with tag="+sreNode.getTag()+
                        //        " and "+sreNode.getNumKids()+" kids was made possible to choose!");
                        }
                    }
                }
            }
        if(acceptableNodes.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        coa_lineage randomAcceptableNode=(coa_lineage)TreeUtils.chooseRandom(acceptableNodes);
        int sreNum=getSRENum(randomAcceptableNode.getTag());        
        String origTagUnderSRE=randomAcceptableNode.getChild(0).getTag();
        //FIRST determine the KING_COA value for the new node
        int newKingLayerCOA=0;
        if(origTagUnderSRE==null)
            {
            //it's a leaf
            newKingLayerCOA=0;
            }
        else if(origTagUnderSRE.startsWith("KING"))
            {
            ArrayList<Integer> kingPieces=getTagIntArray(origTagUnderSRE);
            int kingSRE=kingPieces.get(0);
            int kingCoa=kingPieces.get(1);
            newKingLayerCOA=kingCoa+1;
            if(kingSRE!=sreNum)
                {
                throw new IllegalStateException("Error, king coa "+kingPieces.toString()+" sreval of "+kingSRE+" does not match SRE value of "+sreNum);
                }
            }
        else
            {
            //it's an SRE
            newKingLayerCOA=0;
            }

        int numIncomingToSRE=0;
        String acceptableNodeSRETag=randomAcceptableNode.getTag();
        ArrayList<coa_lineage> nodesAtSRELevel=current.getNodesWithTagMatching(acceptableNodeSRETag,false);
        for(coa_lineage sreNode:nodesAtSRELevel)
            {
            numIncomingToSRE+=sreNode.getNumKids();
            }
        double K=(double)(numIncomingToSRE);
        double kingRateNum=K*(K-1);
        double kingRateDnm=mlEstC(current,false);
        double kingRate=kingRateNum/kingRateDnm;
        double origDistUnderSRE=randomAcceptableNode.getDist(0);
        double upperBound=origDistUnderSRE;
        double lowerBound=upperBound*0.01;
        double kingDraw=TreeUtils.drawTruncatedExponentialWithRate(kingRate,lowerBound,upperBound);
        double modDist=Math.abs(origDistUnderSRE-kingDraw);
        
        //Add a layer of KINGMAN indiscriminately
        String newKingLayerTag="KING_"+sreNum+"_"+newKingLayerCOA+"";


        
        for(coa_lineage sreNodeAtLevel:nodesAtSRELevel)
            {
            ArrayList<coa_lineage> detachedLineages=new ArrayList<coa_lineage>();
            if(sreNodeAtLevel!=randomAcceptableNode)
                {
                //if this isn't the chosen node, just add a layer
                while(sreNodeAtLevel.getNumKids()>0)
                    {
                    coa_lineage detached=sreNodeAtLevel.getChild(0);
                    sreNodeAtLevel.removeChild(detached);
                    detachedLineages.add(detached);
                    }
                for(int d=0;d<detachedLineages.size();d++)
                    {
                    coa_lineage newKingNode=new coa_lineage(newKingLayerTag);
                    newKingNode.setTag(newKingLayerTag);
                    newKingNode.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                    newKingNode.addChild(detachedLineages.get(d),kingDraw);
                    sreNodeAtLevel.addChild(newKingNode,modDist);
                    }   
                }
            else
                {
                //if this is the node, then add a layer and two kids to it
                int[] randomKids=TreeUtils.chooseRandomIndices(randomAcceptableNode.getNumKids(),2);
                int secondIndex=TreeUtils.max(randomKids);
                int firstIndex=TreeUtils.min(randomKids);
                while(sreNodeAtLevel.getNumKids()>0)
                    {
                    coa_lineage detached=sreNodeAtLevel.getChild(0);
                    sreNodeAtLevel.removeChild(detached);
                    detachedLineages.add(detached);
                    }
                for(int d=0;d<detachedLineages.size();d++)
                    {
                    if(d!=firstIndex && d!=secondIndex)
                        {
                        coa_lineage newKingNode=new coa_lineage(newKingLayerTag);
                        newKingNode.setTag(newKingLayerTag);
                        newKingNode.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                        newKingNode.addChild(detachedLineages.get(d),kingDraw);
                        sreNodeAtLevel.addChild(newKingNode,modDist);
                        }
                    }
                coa_lineage newKingCoalescentEvent=new coa_lineage(newKingLayerTag);
                newKingCoalescentEvent.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                newKingCoalescentEvent.setTag(newKingLayerTag);
                newKingCoalescentEvent.addChild(detachedLineages.get(firstIndex),kingDraw);
                newKingCoalescentEvent.addChild(detachedLineages.get(secondIndex),kingDraw);
                randomAcceptableNode.addChild(newKingCoalescentEvent, modDist);
                }
         
            }


        
        current=applySREPosteriorMapped(current);         
        Object[] ret_pack=new Object[]{current,cPrior};        
        return ret_pack;
        }
    
    

    public Object[] overlapExtension(coa_lineage current,BigDecimal cPrior)
        {
        /*
        1)  find a random SRE node with n>=2 input lineages merging
            give extra weight to a node for each n>2.
        2)  choose one of its kids at random.  It will be detached and start a new overlap
        3)  if the node is the root, create new root (KING or SRE depending on random variables and their comparison
            and finish
        4)  if the node is NOT the root, then "push" up the KINGMAN lineages up and the highest KINGMAN
            coalescent events either (one of a, b, c)
            a) merges into an SRE (adding two incoming nodes)
            b) gets a new tag KING_X_Y (where Y increases by 1 but X stays the 
            c) gets a new tag SRE_Y (where Y is the SRE value from the random node tag plus 1
            in c case a new SRE is created
        */

        Pattern srePattern=Pattern.compile("^SRE");
        ArrayList<coa_lineage> sreNodes=current.getNodesWithTagMatching(srePattern,true);
        ArrayList<coa_lineage> acceptableNodes=new ArrayList<coa_lineage>();
        for(coa_lineage sreNode:sreNodes)
            {
            if(sreNode.getNumKids()>=2)
                {
                for(int i=1;i<=sreNode.getNumKids();i++)
                    {
                    //this loop adds extra weight to nodes with n>2 kids
                    acceptableNodes.add(sreNode);
                    //System.out.println("Using pattern "+srePattern+", a node with tag="+sreNode.getTag()+
                    //        " and "+sreNode.getNumKids()+" kids was made possible to choose!");
                    }
                }
            }
        if(acceptableNodes.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        coa_lineage randomAcceptableNode=(coa_lineage)TreeUtils.chooseRandom(acceptableNodes);       
        int sreNum=getSRENum(randomAcceptableNode.getTag());
        int nextSRENum=sreNum+1;        
        
        //choose a kid at random
        int kidIndex=randomAcceptableNode.chooseRandomKidIndex();
        double distUnderSRE=randomAcceptableNode.getDist(0);
        if(randomAcceptableNode==current)
            {
            //System.err.println("root case...");
            //this is the root!  
            //A new root is needed to allow for overlap extension
            double randSREDist=TreeUtils.drawExponentialMeanOne();

            //K=2 here because we're at the root
            //double rate=2.0*(2.0-1.0)/currentC;
            double currentC=mlEstC(current,false);
            double rate=2.0/currentC;
            double randKing=TreeUtils.drawExponentialWithMean(1/rate);
            String newRootTag=null;
            double newRootDist;
            int newRootMergerStyle=(-1);
            if(randKing<randSREDist)
                {
                //new root is kingman
                newRootTag="KING_"+nextSRENum+"_0";
                newRootDist=randKing;
                newRootMergerStyle=coa_lineage.KINGMAN_STYLE;
                }
            else
                {
                //new root is SRE
                newRootTag="SRE_"+nextSRENum;
                newRootDist=randSREDist;
                newRootMergerStyle=coa_lineage.SRE_STYLE;
                }

            
            coa_lineage detached=randomAcceptableNode.getChild(kidIndex);
            randomAcceptableNode.removeChild(kidIndex);
            coa_lineage newSREOverlapNode=new coa_lineage(randomAcceptableNode.getTag());
            newSREOverlapNode.setTag(randomAcceptableNode.getTag());
            newSREOverlapNode.addChild(detached,distUnderSRE);
            newSREOverlapNode.setMergerStyle(coa_lineage.SRE_STYLE);
            coa_lineage newRoot=new coa_lineage(newRootTag);
            newRoot.setTag(newRootTag);
            newRoot.setMergerStyle(newRootMergerStyle);
            newRoot.addChild(newSREOverlapNode,newRootDist);
            newRoot.addChild(randomAcceptableNode,newRootDist);
            current.resetPosteriorSampledValues();
            current=newRoot;
            }
        else
            {
            //System.err.println("non-root case!");
            //randomly chosen node is NOT the root
            //First insert temp Nodes that have the SREs as their children
            double currentC=mlEstC(current,false);
            //System.err.println("mlc is "+currentC);
            String targetTag=randomAcceptableNode.getTag();
            //System.err.println("TargetTag is "+targetTag);
            sreNodes=current.getNodesWithTagMatching(Pattern.compile(targetTag+"$"),false);
            String tempNodeTag="TEMP";
            //this loop adds a layer of kingman nodes "TEMP"
            for(int n=0;n<sreNodes.size();n++)
                {
                coa_lineage sreNode=sreNodes.get(n);
                //System.err.println("TargetTag is "+targetTag+" and the tag of a retrieved node is "+sreNode.getTag());
                coa_lineage tempNode=new coa_lineage("");
                tempNode.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                tempNode.setTag(tempNodeTag);
                coa_lineage sreParent=current.findTheParentOfGivenNode(sreNode);
                double distToParent=sreParent.getDistToKid(sreNode);
                sreParent.removeChild(sreNode);
                sreParent.addChild(tempNode,distToParent);
                tempNode.addChild(sreNode,0);
                }            
            String nextSRETagPString="SRE_"+nextSRENum; 
            Pattern nextSRENodePattern=Pattern.compile(nextSRETagPString);
            ArrayList<coa_lineage> nextSRENodes=current.getNodesWithTagMatching(nextSRENodePattern,false);
            //use the count of nextSRE nodes to determine truncation point for new distance
            //for kingman dist for TEMP nodes
            double truncUpperBound=0.0;
            if(nextSRENodes.isEmpty())
                {
                //just use 10 in this case
                truncUpperBound=10.0;
                }
            else
                {
                truncUpperBound=nextSRENodes.get(0).getDist(0);
                }
            double kingK=(double)(sreNodes.size());
            double kingRateNum=kingK*(kingK-1);
            double kingRateDnm=currentC;
            double kingRate=kingRateNum/kingRateDnm;
            double truncLowerBound=truncUpperBound*0.01;
            //System.out.println("\n\n\n\n\n");
            double kingExp=TreeUtils.drawTruncatedExponentialWithRate(kingRate, truncLowerBound, truncUpperBound);
            //System.err.println("The king exp is "+kingExp+" and rate and lower/upper are : "+kingRate+" and "+truncLowerBound+"/"+truncUpperBound);
            ArrayList<coa_lineage> tempNodes=current.getNodesWithTagMatching(tempNodeTag,false);
            for(coa_lineage tempNode:tempNodes)
                {
                tempNode.setAllDists(kingExp);
                }
            coa_lineage newSREOverlapNode=new coa_lineage(randomAcceptableNode.getTag());
            newSREOverlapNode.setTag(randomAcceptableNode.getTag());
            newSREOverlapNode.setMergerStyle(coa_lineage.SRE_STYLE);
            coa_lineage detached=randomAcceptableNode.getChild(kidIndex);
            randomAcceptableNode.removeChild(detached);
            coa_lineage randomTempParent=current.findTheParentOfGivenNode(randomAcceptableNode);
            randomTempParent.addChild(newSREOverlapNode,kingExp);
            newSREOverlapNode.addChild(detached,distUnderSRE);
            
            Object[] tagsNodesHeights=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
            ArrayList<String> tags=(ArrayList<String>)tagsNodesHeights[0];
            ArrayList<String> tagsToBeTransformed=new ArrayList<String>();
            HashMap fromTo=new HashMap();
            /*System.out.println("\n\n\nBEFORE RETAGGING:");
            showTagsNodesHeights(SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current));*/
            for(int i=0;i<tags.size();i++)
                {
                String t=tags.get(i);
                if(t!=null)
                    {
                    if(t.startsWith("KING"))
                        {
                        ArrayList<Integer> kingPieces=getTagIntArray(t);
                        int kingSRE=kingPieces.get(0);
                        int kingCoa=kingPieces.get(1);
                        if(kingSRE==nextSRENum)
                            {
                            String to="KING_"+kingSRE+"_"+(kingCoa+1);
                            fromTo.put(t,to);
                            tagsToBeTransformed.add(t);
                            }                    
                        }
                    else if(t.equalsIgnoreCase(tempNodeTag))
                        {
                        tagsToBeTransformed.add(t);
                        String to="KING_"+nextSRENum+"_0";
                        fromTo.put(t,to);
                        }
                    }
                }
            for(int i=tagsToBeTransformed.size()-1;i>=0;i--)
                {
                ArrayList<coa_lineage> taggedNodes=current.getNodesWithTagMatching(tagsToBeTransformed.get(i),false);
                for(coa_lineage taggedNode:taggedNodes)
                    {
                    String f=taggedNode.getTag();
                    String t=fromTo.get(f).toString();
                    taggedNode.setTag(t);
                    }
                }
            /*System.out.println("\n\n\nAFTER RETAGGING:");
            showTagsNodesHeights(SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current));*/
            if(!nextSRENodes.isEmpty())
                {
                double curNextSREDist=nextSRENodes.get(0).getDist(0);
                double modNextSREDist=curNextSREDist-kingExp;
                for(coa_lineage higherSRENode:nextSRENodes)
                    {
                    higherSRENode.setAllDists(modNextSREDist);
                    }
                }
            else
                {
                //no need to compensate in this case
                }
            
            
            }
        
        current=applySREPosteriorMapped(current);    
        Object[] ret_pack=new Object[]{current,cPrior};
        

        return ret_pack;
        }
    
    public static ArrayList<String> getTagsFromRootToAndIncludingThisTag(ArrayList<String> tags,String t)
        {
        ArrayList<String> desiredTags=new ArrayList<String>();
        for(int i=0;i<tags.size();i++)
            {
            desiredTags.add(tags.get(i));
            if(t.equalsIgnoreCase(tags.get(i)))
                {
                i=tags.size();
                }
            }
        return desiredTags;
        }
    
    public static final Pattern intPattern=Pattern.compile("(\\-*[0-9]+)");
    
    public static int getSRENumFromSRETag(String sreTagString)
        {
        Pattern p=Pattern.compile(("^SRE_(\\d+)$"));
        Matcher m=p.matcher(sreTagString);
        if(m.find())
            {
            String g=m.group(1);
            int sreNum=Integer.parseInt(g);
            return sreNum;
            }
        else
            {
            throw new IllegalArgumentException("Error, expected SRE tag to match "+p+" but got "+sreTagString+" and didn't match!");
            }                
        }
    
    
    public static ArrayList<Integer> getTagIntArray(String tagStr)
        {
        String[] pieces=tagStr.split("_");
        ArrayList<Integer> alI=new ArrayList<Integer>();
        for(int p=0;p<pieces.length;p++)
            {
            Matcher m=intPattern.matcher(pieces[p]);
            if(m.find())
                {
                int value=Integer.parseInt(pieces[p]);
                if(value<0)
                    {
                    throw new RuntimeException("Error coalescent tag appears to be negative : "+tagStr);
                    }
                alI.add(value);
                }
            }
        return alI;  
        }
    
    public static ArrayList<Integer> getTagIntArray(coa_lineage node)
        {
        //String kingTag="KING_"+iter_num+"_"+king_coa;
        String tagString=node.getTag();
        return getTagIntArray(tagString);
        }
    
    
    public static double mlEstC(coa_lineage root,boolean allowPosInf)
        {
        String defaultCMLValEnf=System.getenv("NON_INF_C_ML");
        if(defaultCMLValEnf!=null)
            {
            try
                {
                double defaultCMLValIfInf=Double.parseDouble(defaultCMLValEnf);
                return mlEstC(root,false,defaultCMLValIfInf);
                }
            catch(NumberFormatException nfe)
                {
                System.err.println(nfe.getMessage());
                nfe.printStackTrace();
                String eMsg="\n\nIf the 'NON_INF_C_ML' environment variable is to be used, then\n";
                eMsg+="it must be set to a parseable double value!";
                System.err.println(eMsg);
                System.exit(1);
                return 0;
                }
            }
        else
            {
            //if it is null, then pass along what's passed already and use 2.0 
            return mlEstC(root,allowPosInf,2.0);
            }
        }
    
    
    
    public static double mlEstC(coa_lineage root,boolean allowPosInf,double defaultC)
        {
        Object[] tagsNodesHeights=getTagsNodesHeightsNameRepresented(root);
        ArrayList<String> tags=(ArrayList<String>)tagsNodesHeights[0];
        ArrayList<coa_lineage> nodes=(ArrayList<coa_lineage>)tagsNodesHeights[1];
        HashMap tagToIncomingLineageCountMap=new HashMap();
        HashMap tagToDistUnderMap=new HashMap();
        int prevLCount=0;
        int incomingLineageCount=root.getNumKids();
        double distUnder=root.getDist(0);
        double mu=0;
        int T=0;
        for(int t=0;t<tags.size()-1;t++)
            {
            if(t==0)
                {
                incomingLineageCount=root.getNumKids();
                }
            else
                {
                if(tags.get(t).startsWith("SRE"))
                    {
                    //count manually
                    ArrayList<coa_lineage> nodesOfTag=root.getNodesWithTagMatching(tags.get(t),false);
                    incomingLineageCount=0;
                    for(coa_lineage anSRENode:nodesOfTag)
                        {
                        incomingLineageCount+=anSRENode.getNumKids();
                        }
                    }
                else if(tags.get(t).startsWith("KING"))
                    {
                    T++;
                    //1 more than before because we're moving *from* the root
                    incomingLineageCount=prevLCount+1;
                    }
                else
                    {
                    throw new RuntimeException("Unrecognized tag : "+tags.get(t));
                    }
                }
            distUnder=nodes.get(t).getDist(0);
            prevLCount=incomingLineageCount;
            tagToIncomingLineageCountMap.put(tags.get(t),incomingLineageCount);
            tagToDistUnderMap.put(tags.get(t),distUnder);
            double mu_add=(incomingLineageCount*incomingLineageCount-incomingLineageCount)*distUnder;
            mu+=mu_add;
            }
        if(T==0)
            {
            if(allowPosInf)
                {
                return Double.POSITIVE_INFINITY;
                }
            else
                {
                return defaultC;
                }
            }
        else
            {
            return mu/(double)(T);
            }
        
        }
    
    public static Pattern getSREPattern(int sre_iter_num)
        {
        String pStr="SRE_"+sre_iter_num+"";
        return Pattern.compile(pStr);
        }
    
    
    public Object[] kingSwap(coa_lineage current,BigDecimal cPrior)
        {
        /*
        1)  Choose a random Kingman coalescent node
        2)  Select either a node with two or with one child
        3)  Swap the children with another random node!
        */
        Object[] tagsNodesHeights=getTagsNodesHeightsNameRepresented(current);
        ArrayList<String> representedTags=(ArrayList<String>)tagsNodesHeights[0];
        ArrayList<String> kingTags=TreeUtils.extractMatchingStart(representedTags,"KING");
        if(kingTags.size()==0)
            {
            return proposeTree(current,cPrior);
            }
        else if(kingTags.size()==1 && current.getTag().startsWith("KING"))
            {
            return proposeTree(current,cPrior);
            }
        int randomTagIndex=TreeUtils.chooseRandomIndex(kingTags.size());
        String randomTag=kingTags.get(randomTagIndex);
        ArrayList<coa_lineage> kingLevelNodes=current.getNodesWithTagMatching(Pattern.compile(randomTag),false);
        while(kingLevelNodes.size()<=1)
            {
            randomTagIndex=TreeUtils.chooseRandomIndex(kingTags.size());
            randomTag=kingTags.get(randomTagIndex);
            kingLevelNodes=current.getNodesWithTagMatching(Pattern.compile(randomTag),false);
            }
        int firstNodeIndex=TreeUtils.chooseRandomIndex(kingLevelNodes.size());
        int secondNodeIndex=TreeUtils.chooseRandomIndex(kingLevelNodes.size());
        while(firstNodeIndex==secondNodeIndex)
            {
            secondNodeIndex=TreeUtils.chooseRandomIndex(kingLevelNodes.size());
            }
        coa_lineage firstNode=kingLevelNodes.get(firstNodeIndex);
        coa_lineage secondNode=kingLevelNodes.get(secondNodeIndex);
        int firstNodeRandomKidIndex=TreeUtils.chooseRandomIndex(firstNode.getNumKids());
        int secondNodeRandomKidIndex=TreeUtils.chooseRandomIndex(secondNode.getNumKids());
        double dist=firstNode.getDist(0);
        coa_lineage firstNodeKid=firstNode.getChild(firstNodeRandomKidIndex);
        firstNode.removeChild(firstNodeRandomKidIndex);
        coa_lineage secondNodeKid=secondNode.getChild(secondNodeRandomKidIndex);
        secondNode.removeChild(secondNodeRandomKidIndex);
        firstNode.addChild(secondNodeKid,dist);
        secondNode.addChild(firstNodeKid,dist);

        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        return ret_pack;
        }
    
    
  public static void showTagsNodesHeights(Object[] tnh)
    {
    ArrayList<String> tags=(ArrayList<String>)tnh[0];
    ArrayList<coa_lineage> nodes=(ArrayList<coa_lineage>)tnh[1];
    ArrayList<Double> heights=(ArrayList<Double>)tnh[2];
    for(int i=0;i<heights.size();i++)
        {
        if(i==0)
            {
            System.out.println("ID\tTag\tHeight");
            }
        System.out.println(i+"\t"+tags.get(i)+"\t"+heights.get(i));
        }
    
    }
    
    
  public static Object[] getTagsNodesHeightsNameRepresented(coa_lineage root)
    {
    coa_lineage temp=root;
    boolean flag=true;
    if(root==null)
        {
        flag=false;
        }      
    ArrayList<String> tags=new ArrayList<String>();
    ArrayList<coa_lineage> nodes=new ArrayList<coa_lineage>();
    ArrayList<Double> heights=new ArrayList<Double>();
    ArrayList<String> names=new ArrayList<String>();
    while(flag)
        {
        String curr_tag=temp.getTag();
        //record while iterating
        tags.add(curr_tag);
        nodes.add(temp);
        heights.add(0.0);          
        Object[] kids_dists=temp.get_immediate_kids_and_dists();         
        ArrayList<coa_lineage> kids=(ArrayList<coa_lineage>)kids_dists[0];              
        names.add(temp.getName());
        if(kids!=null)
            {
            if(kids.size()>=1)
                {
                temp=kids.get(0);
                }
            else
                {
                flag=false;
                }
            }
        else
            {
            flag=false;
            }
        }     
    //tags and nodes have been logged
    //heights array dimensioned with the same number but with zeros
    //now record the proper heights!
    for(int n=nodes.size()-1;n>=0;n--)
        {
        if(n==nodes.size()-1)
            {
            //base height
            heights.set(n,nodes.get(n).getHeight());
            }
        else
            {
            //use delta
            double prevHeight=heights.get(n+1);
            double thisDist=nodes.get(n).getDist(0);
            double thisHeight=prevHeight+thisDist;
            heights.set(n,thisHeight);
            }
        }
    Object[] ret_pack=new Object[]{
        tags,
        nodes,
        heights,
        names
        };
    return ret_pack;
    }
    
    
    public static double getHeightOfTag(Object[] tagsNodesHeights,String t)
        {
        ArrayList<String> representedTags=(ArrayList<String>)tagsNodesHeights[0];
        for(int n=0;n<representedTags.size();n++)
            {
            if(representedTags.get(n).equalsIgnoreCase(t))
                {
                ArrayList<Double> heights=(ArrayList<Double>)tagsNodesHeights[2];
                return heights.get(n);
                }
            }
        return (-1);
        }
    
    
    public static String getTagOfLowestSREAboveThisKingTag(String kingTag,Object[] tagsNodesHeights)
        {
        if(!(kingTag.startsWith("KING")))
            {
            throw new IllegalArgumentException("Error, expected kingman tag but got : "+kingTag);
            }
        else
            {
            String anSRETag=null;
            ArrayList<String> tags=(ArrayList<String>)tagsNodesHeights[0];
            for(int n=0;n<tags.size();n++)
                {
                if(tags.get(n).startsWith(("SRE")))
                    {
                    anSRETag=tags.get(n);
                    }
                else if(tags.get(n).equalsIgnoreCase(kingTag))
                    {
                    return anSRETag;
                    }
                }
            throw new IllegalArgumentException("Error, never found kingtag "+kingTag+"in "+tags.toString());
            }
        }
    
    
    public Object[] kingDistPerturb(coa_lineage current,BigDecimal cPrior)
        {
        
        /*
        1)  Choose a random Kingman coalescent node
        2)  Compute the Exponential distribution with rate that is a function
            of both c AND the numbe of uncoalesced lineages below it.
        3)  Perturb the height with a truncated distribution
        4)  Adjust height of nodes that need it to preserve the coalescent property
        */


            
            
            
       //1 Choose a random Kingman coalescent node        
        Object[] tagsNodesHeights=getTagsNodesHeightsNameRepresented(current);
        ArrayList<String> representedTags=(ArrayList<String>)tagsNodesHeights[0];
        ArrayList<String> kingTags=new ArrayList<String>();
        for(String a:representedTags)
            {
            if(a!=null)
                {
                if(a.startsWith("KING"))
                    {
                    kingTags.add(a);
                    }
                }
            }
        if(kingTags.isEmpty())
            {
            return proposeTree(current,cPrior);
            }
        String randomKingTag=(TreeUtils.chooseRandom(kingTags)).toString();
        Pattern randKingPattern=Pattern.compile(randomKingTag);
        ArrayList<coa_lineage> kingNodes=current.getNodesWithTagMatching(randKingPattern,false);
        double K_king_wait=0.0;
        double dist_under_king=0;
        for(coa_lineage kingNode:kingNodes)
            {
            K_king_wait+=kingNode.getNumKids();
            dist_under_king=kingNode.getDist(0);
            }
        //2)  Compute the Exponential distribution with rate that is a function
        //    of both c AND the number of uncoalesced lineages below it.
        double c_val=mlEstC(current,false);
        double rate_num=K_king_wait*(K_king_wait-1.0);
        double rate_dnm=c_val;
        double rate=rate_num/rate_dnm;
        double currentKingWait=dist_under_king;
        //double aKingNodeHeight=aKingNode.getHeight();
        String nearestSRETag=getTagOfLowestSREAboveThisKingTag(randomKingTag,tagsNodesHeights);
        ArrayList<coa_lineage> theSRENodes=null;
        double lowerBound;
        double upperBound;
        if(nearestSRETag!=null)
            {
            theSRENodes=current.getNodesWithTagMatching(Pattern.compile(nearestSRETag),false);
            double distImmediatelyUnderSRE=theSRENodes.get(0).getDist(0);
            upperBound=distImmediatelyUnderSRE;
            lowerBound=upperBound*0.01;
            }
        else
            {
            lowerBound=currentKingWait*0.9;
            upperBound=currentKingWait*1.1;
            }
        //3)  Perturb the height with a truncated distribution
        double newKingWait=TreeUtils.drawTruncatedExponentialWithRate(rate, lowerBound, upperBound);

        
        //4)  Adjust height of nodes 
        //    the KING nodes to reflect the perturbation and all of them to preserve the coalescent property
        //    the lowest SRE nodes above the perturbed level to maintain the SRE exponential distribution (mean 1)
        for(coa_lineage k:kingNodes)
            {
            for(int d=0;d<k.getNumKids();d++)
                {
                k.setDist(d, newKingWait);
                }
            }
        if(nearestSRETag!=null)
            {
            double waitDiff=Math.abs(newKingWait-currentKingWait);
            if(newKingWait>currentKingWait)
                {
                //subtract dist from below SRE
                for(coa_lineage s:theSRENodes)
                    {
                    double existingDist=s.getDist(0);
                    for(int kd=0;kd<s.getNumKids();kd++)
                        {
                        s.setDist(kd,existingDist-waitDiff);
                        }
                    }
                }
            else if(newKingWait<currentKingWait)
                {
                //add dist to below SRE
                for(coa_lineage s:theSRENodes)
                    {
                    double existingDist=s.getDist(0);
                    for(int kd=0;kd<s.getNumKids();kd++)
                        {
                        s.setDist(kd,existingDist+waitDiff);
                        }
                    }                
                }
            }
    

        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        return ret_pack;        
        }
    
    
    
    
    
    public Object[] sreSwap(coa_lineage current,BigDecimal cPrior)
        {
            
            
        /*
        1) pick an SRE at random (in proportion)
        2) pick two nodes at the level
            recurse if only one node is at the level
        */
        Object[] tnh=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
        ArrayList<String> tags=(ArrayList<String>)tnh[0];
        ArrayList<String> sreTags=new ArrayList<String>();
        for(String t:tags)
            {
            if(t!=null && t.startsWith(("SRE")))
                {
                sreTags.add(t);
                }
            }
        if(sreTags.size()==0)
            {
            return proposeTree(current,cPrior);
            }
        String randomSRETag=(String)TreeUtils.chooseRandom(sreTags);
        /*System.out.println("An SRE Profile is "+SREvent.JSONString(sreProfile));
        System.out.println("Using the rand_index the event is "+sreProfile[randIndex].toJSONObject().toString());
        System.out.println("The rand_index is "+randIndex+" and computed depth is "+depth);*/
        ArrayList<coa_lineage> theSRENodes=current.getNodesWithTagMatching(randomSRETag,false);
        if(theSRENodes.size()<=1)
            {
            //can't swap if there's only one node!
            return proposeTree(current,cPrior);
            }
        else
            {
            //choose two random nodes
            int[] randomSRENodes=TreeUtils.chooseRandomIndices(theSRENodes.size(),2);
            coa_lineage firstRandomNode=theSRENodes.get(randomSRENodes[0]);
            coa_lineage secondRandomNode=theSRENodes.get(randomSRENodes[1]);

            //from each of those get random kids (and their dists and indices)
            int firstNodeRandomKidIndex=TreeUtils.chooseRandomIndex(firstRandomNode.getNumKids());
            double firstNodeRandomKidDist=firstRandomNode.getDist(firstNodeRandomKidIndex);
            int secondNodeRandomKidIndex=TreeUtils.chooseRandomIndex(secondRandomNode.getNumKids());
            double secondNodeRandomKidDist=secondRandomNode.getDist(secondNodeRandomKidIndex);
            coa_lineage firstRandomNodeKid=firstRandomNode.getChild(firstNodeRandomKidIndex);
            coa_lineage secondRandomNodeKid=secondRandomNode.getChild(secondNodeRandomKidIndex);

            //remove the kids
            firstRandomNode.removeChild(firstRandomNodeKid);
            secondRandomNode.removeChild(secondRandomNodeKid);
            
            //add them back but with swap!
            firstRandomNode.addChild(secondRandomNodeKid,secondNodeRandomKidDist);
            secondRandomNode.addChild(firstRandomNodeKid,firstNodeRandomKidDist);
            
            Object[] ret_pack=new Object[2];
            ret_pack[0]=current;
            ret_pack[1]=cPrior;
            return ret_pack;
            }        
        }
    
    
    public static double getDistBetweenThisAndNearestSREUnderIt(coa_lineage node)
        {
        if(node.getNumKids()==0)
            {
            throw new RuntimeException("No sre node under node with tag "+node.getTag());
            }
        else
            {
            double distUnder=node.getDist(0);
            coa_lineage child=node.getChild(0);
            String childTag=child.getTag();
            if(childTag==null)
                {
                throw new RuntimeException("NULL TAG : No sre node under node with tag "+node.getTag());
                }
            else if(childTag.startsWith("SRE"))
                {
                return distUnder;
                }
            else
                {
                return distUnder+getDistBetweenThisAndNearestSREUnderIt(child);
                }
            }
        }
    
    public Object[] adjustSREHeight(coa_lineage current,BigDecimal cPrior)
        {
        Object[] tnh=SWPaperAlgTwoGenerator.getTagsNodesHeightsNameRepresented(current);
        ArrayList<String> tags=(ArrayList<String>)tnh[0];
        ArrayList<String> sreTags=new ArrayList<String>();
        for(String t:tags)
            {
            if(t!=null && t.startsWith("SRE"))
                {
                sreTags.add(t);
                }
            }
        if(sreTags.size()==0)
            {
            return proposeTree(current,cPrior);
            }
        String randomSRETag=(String)TreeUtils.chooseRandom(sreTags);
        ArrayList<coa_lineage> theSRENodes=current.getNodesWithTagMatching(randomSRETag, false);
        double existingWait=0;
        if(randomSRETag.equalsIgnoreCase("SRE_0"))
            {
            existingWait=theSRENodes.get(0).getHeight();
            }
        else
            {
            existingWait=getDistBetweenThisAndNearestSREUnderIt(theSRENodes.get(0));
            }
        double distUnder=theSRENodes.get(0).getDist(0);
        double lowerBound=existingWait-(distUnder*0.9);
        double upperBound=existingWait+10;
        double newWait=TreeUtils.drawTruncatedExponentialMeanOne(
                //lower bound of truncated is based on height of nodes immediately below
                lowerBound,
                //upper bound is to be very much higher
                upperBound);
        if(Double.isInfinite(newWait))
            {
            //use a value closer to lower-bound to avoid inf. value errors
            newWait=lowerBound+(upperBound-lowerBound)*0.1;
            }
        double waitDiff=Math.abs(newWait-existingWait);
        double newDistUnderNodes=0;
        if(newWait>existingWait)
            {
            newDistUnderNodes=distUnder+waitDiff;
            }
        else
            {
            newDistUnderNodes=distUnder-waitDiff;
            }
        for(int n=0;n<theSRENodes.size();n++)
            {
            coa_lineage theNode=theSRENodes.get(n);
            double theHeight=theNode.getHeight();
            int numKids=theNode.getNumKids();
            for(int k=0;k<numKids;k++)
                {
                theNode.setDist(k,newDistUnderNodes);
                }
            }
        
        current=applySREPosteriorMapped(current);
        Object[] ret_pack=new Object[2];
        ret_pack[0]=current;
        ret_pack[1]=cPrior;
        return ret_pack;    
        }
    
    
    
    public static int[] countMergers(coa_lineage root)
        {
        int[] styleCounts=new int[2];
        int numKids=root.getNumKids();        
        int mergerStyle=root.getMergerStyle();
        if(mergerStyle>=0)
            {
            styleCounts[mergerStyle]+=(numKids-1);    
            }        
        for(int k=0;k<numKids;k++)
            {
            int[] recCounts=countMergers(root.getChild(k));
            styleCounts[coa_lineage.KINGMAN_STYLE]+=recCounts[coa_lineage.KINGMAN_STYLE];
            styleCounts[coa_lineage.SRE_STYLE]+=recCounts[coa_lineage.SRE_STYLE];
            }
        return styleCounts;
        }
    
    
    public static boolean verifyMergerStyles(coa_lineage root)
        {
        int numKids=root.getNumKids();
        if(numKids>0)
            {
            int mergerStyle=root.getMergerStyle();
            if(mergerStyle!=coa_lineage.KINGMAN_STYLE && mergerStyle!=coa_lineage.SRE_STYLE)
                {
                //if it's not Kingman and it's not SRE return false!
                return false;
                }
            else
                {
                for(int k=0;k<numKids;k++)
                    {
                    boolean rec=verifyMergerStyles(root.getChild(k));
                    if(!rec)
                        {
                        //propagate up false
                        return false;
                        }
                    }
                }
            return true;
            }
        else
            {
            return true;
            }
        }

    
    private BigDecimal[] piJointDist(int num_lineages)
        {
        BigDecimal psi=null;
        psi=new BigDecimal(TreeUtils.getRandNumBet(min_psi, max_psi,true));
        int logBTwo=(int)(TreeUtils.logBaseTwo(num_lineages));
        if(logBTwo==0)
            {
            logBTwo+=1;
            }
        //BigDecimal Y=(BigDecimal)(params.get("Y"));
        int binY=TreeUtils.binomialDraw(0.5,logBTwo*2);
        binY++;
        BigDecimal Y=new BigDecimal(binY);
        BigDecimal[] return_pack=new BigDecimal[2];
        return_pack[0]=psi;
        return_pack[1]=Y;
        return return_pack;
        }
    
    public static double getCForGenTree(int numLeaves)
        {
        if(TreeUtils.getRandNumBet(0,1,false)<=0.1)
            {
            return TreeUtils.getRandNumBet(0,1,false);
            }
        else
            {
            double low=1;
            double high=numLeaves*10;
            return TreeUtils.getRandNumBet(low,high,false);
            }        
        }
    
    
    private Object[] generateTree(int numLeaves)
        {       

        double c_val=getCForGenTree(numLeaves);
        //System.err.println("c_val is "+c_val);
        SREvent[] srEvents=new SREvent[0];            

        ////////////////////////////////////////////////////
        // (1) Start with K = n.
        int num_lineages_to_get=numLeaves;
        //initialize maxHeight
        ArrayList<coa_lineage> tempLineages=new ArrayList<coa_lineage>();
        for(int l=0;l<num_lineages_to_get;l++)
            {
            //this little for loop initializes!
            coa_lineage tempLineage=new coa_lineage(""+(l+1)+"");
            tempLineages.add(tempLineage);
            }
            


        int iter_num=0;//keeps track of how many overall iterations        
        while(tempLineages.size()>1)
            {
            ////////////////////////////////////////////////////
            //(2) Sample (phi, Y) from a joint distribution pi (.,.).
            //BigDecimal[] ret_pack=SWPaperAlgOneGenerator.p
            BigDecimal[] psiY=piJointDist(tempLineages.size());
            BigDecimal psi=psiY[0];
            BigDecimal Y=psiY[1];
        
            ////////////////////////////////////////////////////
            //(3) Generate the waiting time t to the first SRE (looking backwards
            //in time): It is an exponential random variable with mean one.
            double sreWait=TreeUtils.drawExponentialWithMean(1.0);
            double currentBase=tempLineages.get(0).getHeight();
            sreWait+=currentBase;


            ////////////////////////////////////////////////////
            /*(4) Up to time t, run the usual binary, Kingman coalescent with
            rate K(K - 1)/c whlie there are K lineages.  Form pairwise
            common ancestors as coalescent events occur, and decrement
            K by one at each event. Exit the algorithm if K = 1 before time
            t.*/
            boolean kingFlag=true;
                //kingFlag tells when to stop kingman style coalescent events and proceed
                //to any SRE-style events
            int king_coa=0;//keeps track of how many kingman coalescent events in a single iteration
            while(kingFlag)
                {
                //Exit the algorithm if K = 1 before time t.
                if(tempLineages.size()==1)
                    {
                    double[] sreWaits=SREvent.getSREWaits(srEvents);
                    String sreWaitsAsStr=Arrays.toString(sreWaits);
                    double[] sreHeights=SREvent.getSREHeights(srEvents);
                    String sreHeightsAsStr=Arrays.toString(sreHeights);
                    tempLineages.get(0).setPosteriorSampledValue("sre_heights",sreHeightsAsStr);
                    tempLineages.get(0).setPosteriorSampledValue("sre_waits",sreWaitsAsStr);
                    tempLineages.get(0).setPosteriorSampledValue("sre_profile",SREvent.JSONString(srEvents));        
                    int[] mergerCounts=countMergers(tempLineages.get(0));
                    tempLineages.get(0).setPosteriorSampledValue("KingmanMergers",mergerCounts[0]);
                    tempLineages.get(0).setPosteriorSampledValue("SREMergers",mergerCounts[1]);
                    if(!verifyMergerStyles(tempLineages.get(0)))
                        {
                        throw new RuntimeException("Error, failed to verify mergerTypes!");
                        }                    
                    Object[] kretPackage=new Object[2];
                    kretPackage[0]=tempLineages.get(0);
                    kretPackage[1]=new BigDecimal("1");
                    return kretPackage;
                    }


                //first acquire two random lineages and their names
                int[] twoRandomLineages=TreeUtils.chooseRandomSubset(2,tempLineages.size());
                int firstLineage=twoRandomLineages[0];
                int secondLineage=twoRandomLineages[1];
                String firstName=tempLineages.get(firstLineage).getName();
                String secondName=tempLineages.get(secondLineage).getName();


                //compute name of new lineage and create the new lineage
                String newName="("+firstName+" & "+secondName+")";

                //obtain heights of selected lineages to acquire delta
                double firstHeight=currentBase;
                double secondHeight=currentBase;                
                
                
                //drawn kingman exponential time
                double newDistBase=tempLineages.get(0).getHeight();
                double kingWait=drawExponentialKing(c_val,tempLineages.size());
                double newCoaHeight=kingWait+newDistBase;

                if(newCoaHeight<sreWait)
                    {
                    //doesn't exceed SREwait...so is okay to create a merged lineage
                    coa_lineage newLineage=new coa_lineage(newName);
                    String kingTag="KING_"+iter_num+"_"+king_coa;
                    king_coa++;
                    newLineage.setTag(kingTag);
                    //here the two lineages are coalesced
                    newLineage.addChild(tempLineages.get(firstLineage),newCoaHeight-firstHeight);
                    newLineage.addChild(tempLineages.get(secondLineage),newCoaHeight-secondHeight);
                    newLineage.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                    //the new lineage is added to the list
                    currentBase=newLineage.getHeight();
                    //the coalsed lineages get removed here
                    tempLineages.remove(firstLineage);
                    if(secondLineage>firstLineage)
                        {
                        //if the index of the second lineage is beyond the first this decrement is needed
                        tempLineages.remove(secondLineage-1);
                        }
                    else
                        {
                        //if the index of the second lineage is before the first, no decrement needed
                        tempLineages.remove(secondLineage);
                        }
                    
                    //here, extend all the lineages and add a node.
                    //this is to help eliminate branch-length arithmetic
                    //in other code
                    ArrayList<coa_lineage> newTempLineages=new ArrayList<coa_lineage>();
                    newTempLineages.add(newLineage);
                    for(int i=0;i<tempLineages.size();i++)
                        {
                        coa_lineage temp=new coa_lineage("");
                        temp.setMergerStyle(coa_lineage.KINGMAN_STYLE);
                        temp.setTag(kingTag);
                        temp.addChild(tempLineages.get(i), kingWait);
                        newTempLineages.add(temp);
                        }
                    tempLineages=newTempLineages;                    
                    }
                else
                    {
                    //would exceed SREWait, do DONT coalesce!
                    //and go to next step
                    kingFlag=false;
                    }
                }//kingLoop

            ////////////////////////////////////////////////////
            /*(5) Using the value of K (> 1) from Step 4, sample (k0, k1,... , kY )
            from Mn(K, p0, p1,....,pY) where p0=1-phi and pi=
            phi/Y, i = 1, . . . , Y.*/
            //define the p_i values in terms of Y
            double[] p=new double[Y.intValue()+1];
            p[0]=1.0-psi.doubleValue();
            for(int pi=1;pi<p.length;pi++)
                {
                p[pi]=psi.doubleValue()/Y.doubleValue();
                }

            ////////////////////////////////////////////////////
            /*(6) If ki 6= 0, i = 1, . . . , Y then randomly choose ki
            individuals from K without replacement and coalesce them together.*/
            int n_sum=tempLineages.size();
            int[] bins=TreeUtils.multinomialVector(n_sum, p);
            //the ArrayList of newLineages represents the new lineages in the SRE
            ArrayList<coa_lineage> newLineages=new ArrayList<>();
            boolean sreAdded=false;
            for(int b=0;b<bins.length;b++)
                {
                //iterate through the bins and merge the number found
                int numToPick=bins[b];
                //System.out.println("At bin="+b+", num to pick is "+numToPick);
                if(b>0 && numToPick>0)
                    {
                    //create a new lineage for the merger
                    coa_lineage aNewLineage=new coa_lineage("");
                    aNewLineage.setMergerStyle(coa_lineage.SRE_STYLE);
                    int[] randomLineages=TreeUtils.chooseRandomSubset(numToPick,tempLineages.size());
                    //System.out.println("The randomly picked lineages : "+Arrays.toString(randomLineages));
                    for(int r=0;r<randomLineages.length;r++)
                        {
                        /*System.out.println(
                                "Adding a child ("+tempLineages.get(randomLineages[r]).getName()+
                                        ") to the new lineage...");*/
                        aNewLineage.addChild(tempLineages.get(randomLineages[r]),
                                sreWait-tempLineages.get(randomLineages[r]).getHeight());
                        
                        if(randomLineages.length==1)
                            {
                            aNewLineage.setName(tempLineages.get(randomLineages[r]).getName());
                            }
                        else if(randomLineages.length>1)
                            {
                            if(r<randomLineages.length-1)
                                {
                                aNewLineage.setName(aNewLineage.getName()+" & "+
                                        tempLineages.get(randomLineages[r]).getName());
                                }
                            else
                                {
                                aNewLineage.setName(aNewLineage.getName()+
                                        tempLineages.get(randomLineages[r]).getName());
                                }
                            }
                        String sreTag="SRE_"+iter_num+"";
                        aNewLineage.setTag(sreTag);
                        }
                    aNewLineage.setName(aNewLineage.getName()+")");

                    //remove from tempLineages the ones that were merged in
                    //the sorting is performed so that lineages are removed
                    //highest index first.
                    Arrays.sort(randomLineages);
                    for(int r=randomLineages.length-1;r>=0;r--)
                        {
                        tempLineages.remove(randomLineages[r]);
                        }
                    //add to the new lineages the new lineage
                    newLineages.add(aNewLineage);
                    if(!sreAdded)
                        {
                        sreAdded=true;
                        //add an SRE because height is known
                        SREvent anSREvent=new SREvent(Y.intValue(),psi.doubleValue(),aNewLineage.getHeight());
                        srEvents=SREvent.appendEvent(srEvents,anSREvent);                        
                        }                    
                    }//for bin b>=1, merging bins
                else if(b==0 && numToPick>0)
                    {
                    //bin is zero
                    //zero is the bin for "generational overlap"
                    //don't do any merging but add a node tagged as being SRE level
                    int[] randomLineages=TreeUtils.chooseRandomSubset(numToPick,tempLineages.size());
                    for(int r=0;r<randomLineages.length;r++)
                        {
                        coa_lineage aNewLineage=new coa_lineage("");
                        aNewLineage.addChild(tempLineages.get(randomLineages[r]), 
                                sreWait-tempLineages.get(randomLineages[r]).getHeight());
                        aNewLineage.setMergerStyle(coa_lineage.SRE_STYLE);
                        String sreTag="SRE_"+iter_num+"";
                        aNewLineage.setTag(sreTag);
                        newLineages.add(aNewLineage);    
                        if(!sreAdded)
                            {
                            sreAdded=true;
                            //add an SRE because height is known
                            SREvent anSREvent=new SREvent(Y.intValue(),psi.doubleValue(),aNewLineage.getHeight());
                            srEvents=SREvent.appendEvent(srEvents,anSREvent);                                                    
                            }                        
                        }
                    Arrays.sort(randomLineages);
                    for(int r=randomLineages.length-1;r>=0;r--)
                        {
                        //have this in a separate loop to preserve index consistency
                        tempLineages.remove(randomLineages[r]);
                        }
                    }//for bin zero, overlap bin
                }//for each bin      
            
            

            //to the new lineages for the bins add in the lineages not merged 
            //to the end of the list
            for(int l=0;l<tempLineages.size();l++)
                {
                newLineages.add(tempLineages.get(l));
                }
            //set the lineages for the next round after any merging
            tempLineages=newLineages;
            iter_num++;
            }//while(tempLineages.size()>1)
        
        //return tree and a prior
        double[] sreWaits=SREvent.getSREWaits(srEvents);
        String sreWaitsAsStr=Arrays.toString(sreWaits);
        double[] sreHeights=SREvent.getSREHeights(srEvents);
        String sreHeightsAsStr=Arrays.toString(sreHeights);
        tempLineages.get(0).setPosteriorSampledValue("sre_heights",sreHeightsAsStr);
        tempLineages.get(0).setPosteriorSampledValue("sre_waits",sreWaitsAsStr);
        tempLineages.get(0).setPosteriorSampledValue("sre_profile",SREvent.JSONString(srEvents));        
        int[] mergerCounts=countMergers(tempLineages.get(0));
        tempLineages.get(0).setPosteriorSampledValue("KingmanMergers",mergerCounts[0]);
        tempLineages.get(0).setPosteriorSampledValue("SREMergers",mergerCounts[1]);
        Object[] swa2retPackage=new Object[2];
        if(!verifyMergerStyles(tempLineages.get(0)))
            {
            throw new RuntimeException("Error, failed to verify mergerTypes!");
            }
        swa2retPackage[0]=tempLineages.get(0);
        swa2retPackage[1]=new BigDecimal("1");
        recentCode=0;
        return swa2retPackage;               

        }
    
    
    
    private ArrayList<coa_lineage> getSRENodesAtDepth(coa_lineage root,int depth,SREvent[] profile)
        {
        ArrayList<coa_lineage> temp=new ArrayList<coa_lineage>();
        if(profile==null)
            {
            return temp;
            }
        if(profile.length==0)
            {
            return temp;
            }
        int tagInt=profile.length-1-depth;
        String computedTag="SRE_"+tagInt;
        temp=root.getNodesWithTagMatching(computedTag,false);
        return temp;
        }
    
    
    
    
    public Object[] generateTree()
        {
        int numLineages=Integer.parseInt(params.get("num_lineages").toString());
        Object[] ret_pack=generateTree(numLineages);
        coa_lineage root=(coa_lineage)(ret_pack[0]);
        double c_ML=mlEstC(
                    root,
                    true);
        root.setPosteriorSampledValue("c_ML",c_ML);
        ret_pack[0]=root;
        return ret_pack;        
        }

    

    public void proposeParams()
        {
        if(params==null)
            {
            params=new HashMap();
            }
        }
    
}
