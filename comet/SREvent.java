/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author esalina
 */
public class SREvent {

    
    
    public static int getDepthFromSREIndex(int i,SREvent[] SREvents)
        {
        int depth=SREvents.length-i-1;
        return depth;
        }
    
    public static int getSREIndexFromDepth(int depth,SREvent[] SREvents)
        {
        int index=SREvents.length-1-depth;
        return index;
        }
            
    
    
    private int y;
    private double phi;
    private double height;
    
    public int getY() {return this.y;}
    public double getPhi() {return this.phi;}
    public double getHeight() {return this.height;}
    public void setY(int ny){this.y=ny;}
    public void setPhi(double np){this.phi=np;}
    public void setHeight(double nh){this.height=nh;}

    
    public String JSONString()
        {
        String j=
          "{\"y\":"+this.y+",";
        j+="\"phi\":"+this.phi+",";
        j+="\"height\":"+this.height ;
        j+="}";
        return j;
        }

    public static double[] getSREHeights(SREvent[] events)
        {
        Arrays.sort(events,myHeightComparator);
        double[] heights=new double[events.length];
        for(int w=0;w<events.length;w++)
            {
            heights[w]=events[w].getHeight();
            }
        return heights;        
        }
    
    
    public static double[] getSREWaitsFromHeights(ArrayList<Double> heights)
        {
        double[] waits=new double[heights.size()];
        for(int w=0;w<heights.size();w++)
            {
            double wait=0;
            if(w==0)
                {
                wait=heights.get(0);
                }
            else
                {
                wait=heights.get(w)-heights.get(w-1);
                }
            waits[w]=wait;
            }
        return waits;
        }
    
    
    public static SREvent[] setNewHeights(SREvent[] existing,ArrayList<Double> newHeights)
        {
        SREvent[] newEvents=new SREvent[existing.length];
        if(newHeights.size()!=existing.length)
            {
            throw new RuntimeException("Error, length mismatch! Event length is "+existing.length+" and newHeights size is "+newHeights.size());
            }
        for(int s=0;s<newEvents.length;s++)
            {
            newEvents[s]=new SREvent(existing[s]);
            newEvents[s].setHeight(newHeights.get(s));
            }
        return newEvents;        
        }
    
    
    public double[] asDoubleArr(ArrayList<Double> al)
        {
        if(al==null)
            {
            return null;
            }
        double[] a=new double[al.size()];
        for(int i=0;i<al.size();i++)
            {
            a[i]=al.get(i);
            }
        return a;
        }
    
    
    public static SREvent[] sortByHeight(SREvent[] events)
        {
        Arrays.sort(events,myHeightComparator);
        return events;
        }
    
    
    public static SREvent[] sortByHeight(ArrayList<SREvent> events)
        {
        SREvent[] eventsArr=new SREvent[events.size()];
        for(int e=0;e<events.size();e++)
            {
            eventsArr[e]=new SREvent(events.get(e));
            }
        return sortByHeight(eventsArr);
        }
    

    public static String getWaitsArrAsStr(SREvent[] sortedEvents)
        {
        double[] sreWaits=SREvent.getSREWaits(sortedEvents);
        String sreWaitsAsStr=Arrays.toString(sreWaits);        
        return sreWaitsAsStr;
        }
    
    public static String getHeightsArrAsStr(SREvent[] sortedEvents)
        {
        double[] sreHeights=SREvent.getSREHeights(sortedEvents);
        String sreHeightsAsStr=Arrays.toString(sreHeights);
        return  sreHeightsAsStr;
        }
    
        
    
    
    public static double[] getSREWaits(SREvent[] events)
        {
        Arrays.sort(events,myHeightComparator);
        double[] waits=new double[events.length];
        for(int w=0;w<events.length;w++)
            {
            if(w==0)
                {
                waits[0]=events[0].getHeight();
                }
            else
                {
                waits[w]=events[w].getHeight()-events[w-1].getHeight();
                }
            }
        return waits;
        }
    
    public static class HeightComparator implements Comparator<SREvent>
        {
        /*Returns a negative integer, zero, or a positive integer as the 
        first argument is less than, equal to, or greater than the second.*/
        public int compare(SREvent e1,SREvent e2)
            {
            double h1=e1.getHeight();
            double h2=e2.getHeight();
            if(h1<h2)
                {
                return (-1);
                }
            else if(h1==h2)
                {
                return 0;
                }
            else
                {
                return 1;
                }
            }
        }

    private static HeightComparator myHeightComparator=new HeightComparator();
    
    
    public JSONObject toJSONObject()
        {
        JSONObject jver=new JSONObject();
        jver.put("phi",this.phi);
        jver.put("height",this.height);
        jver.put("y",this.y);
        return jver;
        }
    
    public String toString()
        {
        JSONObject jso=this.toJSONObject();
        return jso.toString();
        }
    
    
    public static SREvent[] removeEvent(SREvent[] existing,int eventIndex)
        {
        if(existing==null)
            {
            return null;
            }
        else
            {
            if(eventIndex<0 || eventIndex>=existing.length)
                {
                return existing;
                }
            else
                {
                ArrayList<SREvent> sreList=new ArrayList<SREvent>();
                for(int i=0;i<existing.length;i++)
                    {
                    if(i!=eventIndex)
                        {
                        sreList.add(existing[i]);
                        }
                    }
                SREvent[] newArr=new SREvent[sreList.size()];
                for(int i=0;i<sreList.size();i++)
                    {
                    newArr[i]=new SREvent(sreList.get(i));
                    }
                return newArr;
                }
            }
        }
    

    public static SREvent[] appendEvent(SREvent[] existing,SREvent newEvent)
        {
        SREvent[] newEvents=null;
        if(existing==null)
            {
            newEvents=new SREvent[1];
            }
        else
            {
            newEvents=new SREvent[existing.length+1];
            }
        for(int e=0;e<newEvents.length-1;e++)
            {
            newEvents[e]=new SREvent(existing[e]);
            }
        newEvents[newEvents.length-1]=new SREvent(newEvent);
        return newEvents;
        }
    
    public static SREvent[] makeArray(ArrayList<SREvent> eventAL)
        {
        if(eventAL==null)
            {
            return null;
            }
        else
            {
            SREvent[] ret_arr=new SREvent[eventAL.size()];
            for(int e=0;e<eventAL.size();e++)
                {
                ret_arr[e]=eventAL.get(e);
                }
            return ret_arr;
            }
        }
    
    public static String JSONString(ArrayList<SREvent> eventAL)
        {
        SREvent[] events=makeArray(eventAL);
        String json=JSONString(events);
        return json;
        }
    
    
    public static String JSONString(SREvent[] events)
        {
        String ha="{\"sres\":[";
        if(events!=null && events.length>0)
            {
            for(int i=0;i<events.length;i++)
                {
                ha+=events[i].JSONString();
                if(i<events.length-1)
                    {
                    ha+=",";
                    }
                }
            }
        ha+="]}";
        return ha;        
        }
    
     public static SREvent[] parseJSON(String json)
        {
        JSONObject jso=new JSONObject(json);
        JSONArray sres=jso.optJSONArray("sres");
        SREvent[] the_events=null;
        if(sres==null)
            {
            //throw new RuntimeException("Error, failure to find \"sres\" key in JSON "+json);
            return the_events;
            }
        else
            {
            int num_events=sres.length();
            the_events=new SREvent[num_events];
            for(int e=0;e<num_events;e++)
                {
                JSONObject an_event=(JSONObject)sres.get(e);
                SREvent temp_event=new SREvent(
                    an_event.getInt("y"),
                    an_event.getDouble("phi"),
                    an_event.getDouble("height")
                    );
                the_events[e]=temp_event;
                }
            Arrays.sort(the_events,myHeightComparator);
            return the_events;
            }
        }
    
    
    public SREvent(SREvent existing)
        {
        this.height=existing.getHeight();
        this.y=existing.getY();
        this.phi=existing.getPhi();
        }
    
    public SREvent(int y,double phi)
        {
        this.y=y;
        this.phi=phi;
        this.height=0;        
        }
    
    public SREvent(int y,double phi,double h)
        {
        this.y=y;
        this.phi=phi;
        this.height=h;
        }
    
}
