/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author esalina
 */
public class CombineLogs {

    public static long getNumDataRows(File logFile) throws Exception
        {
        BufferedReader br=new BufferedReader(new FileReader(logFile));
        boolean passedState=false;
        long numDataRecs=0;
        while(br.ready())
            {
            String tempLine=br.readLine();
            if(!passedState)
                {
                if(tempLine.startsWith("State"))
                    {
                    passedState=true;
                    }
                }
            else
                {
                String[] pieces=tempLine.split("\\t");
                long thisState=Long.parseLong(pieces[0]);
                if(thisState>=0)
                    {
                    numDataRecs++;
                    }
                }                
            }
        return numDataRecs;
        }
    
    public static String[] getHashHeaders(File logFile) throws Exception
        {
        ArrayList<String> hashHeaders=new ArrayList<String>();
        BufferedReader br=new BufferedReader(new FileReader(logFile));
        boolean passedState=false;
        while(br.ready())
            {
            String tempLine=br.readLine();
            if(!passedState)
                {
                if(tempLine.startsWith("State"))
                    {
                    passedState=true;
                    }
                else if(tempLine.startsWith("#"))
                    {
                    hashHeaders.add(tempLine);
                    }
                }
            else
                {
                String[] hhsa=new String[hashHeaders.size()];
                for(int s=0;s<hhsa.length;s++)
                    {
                    hhsa[s]=hashHeaders.get(s);
                    }
                return hhsa;
                }
            }
        String[] hhsa=new String[hashHeaders.size()];
        for(int s=0;s<hhsa.length;s++)
            {
            hhsa[s]=hashHeaders.get(s);
            }
        return hhsa;            
        }
    
    private static void dropBlankLines(File logFile) throws Exception
        {
        BufferedReader br=new BufferedReader(new FileReader(logFile));
        File tmpFile=File.createTempFile(".log_tmp.",".log");
        BufferedWriter bw=new BufferedWriter(new FileWriter(tmpFile));
        while(br.ready())
            {
            String tempLine=br.readLine();
            String tempLineTrimmed=tempLine.trim()+"\n";
            if(tempLineTrimmed.length()>0)
                {
                //non-empty
                bw.write(tempLineTrimmed,0,tempLineTrimmed.length());
                }
            else
                {
                System.out.println("Removed a blank line from "+logFile);
                //skip cause is blank line
                }
            }
        bw.close();
        br.close();
        logFile.delete();
        tmpFile.renameTo(logFile);
        }
    
    private static long getLastStateFromLog(File logFile) throws Exception
        {
        return  getPctBIVal(logFile,true);
        }
    
    
    private static boolean append10BI(File outLog,File logFile,boolean requireEQ10p) throws Exception
        {
        boolean outLogExists=outLog.exists();
        boolean addHeader=true;
        long recFreq=obtainFreqUsingLastTwoStates(logFile,false);
        long stateVal=0;
        if(outLogExists)
            {
            //System.out.println("outLog is determined to exist");
            long outLogSize=outLog.length();
            if(outLogSize>=10)
                {
                //don't add header
                addHeader=false;
                //if log is found obtain last state from it
                stateVal=getLastStateFromLog(outLog)+recFreq;
                }
            }

        long tenPBIval=getPctBIVal(logFile,false);
        System.out.println("Ten-pct burn-in from "+logFile+" is "+tenPBIval);
        BufferedWriter bw=new BufferedWriter(new FileWriter(outLog,true));
        BufferedReader br=new BufferedReader(new FileReader(logFile));
        boolean passedState=false;
        while(br.ready())
            {
            String tempLine=br.readLine();
            if((tempLine.startsWith("#") || tempLine.startsWith("State")) && addHeader)
                {
                bw.write(tempLine,0,tempLine.length());
                bw.newLine();
                }
            if(tempLine.startsWith("State"))
                {
                passedState=true;
                }
            else if(passedState)
                {
                String[] pieces=tempLine.split("\\t");
                long thisState=Long.parseLong(pieces[0]);
                if(thisState>tenPBIval || (requireEQ10p && thisState==tenPBIval))
                    {
                    pieces[0]=""+stateVal;
                    String toWrite=String.join("\t",pieces);
                    bw.write(toWrite,0,toWrite.length());
                    bw.newLine();
                    bw.flush();
                    stateVal+=recFreq;
                    }
                else
                    {
                    //skip! it's in the burn-in
                    }
                }
            }
        bw.close();
            
        return false;
        }
    
    public static boolean combine10PBI(File outLog,File[] logFiles) throws Exception
        {
        boolean sameFreq=areRecFreqsSame(logFiles,true);
        boolean sameHeaders=compareAllHeadersEqual(logFiles,true);
        if(sameFreq && sameHeaders)
            {
            for(int l=0;l<logFiles.length;l++)
                {
                System.out.println("Now to append file "+logFiles[l]+" to "+outLog+"...");
                append10BI(outLog,logFiles[l],false);
                }
            return true;
            }
        else
            {
            if(!sameFreq)
                {
                System.err.println("Error : un-shared recording frequency!");
                }
            if(!sameHeaders)
                {
                System.err.println("Error : headers aren't all the same!");
                }
            return false;
            }
        }
    
    public static long getPctBIVal(File logFile,boolean returnLastState) throws Exception
        {
        BufferedReader br=new BufferedReader(new FileReader(logFile));
        boolean passedState=false;
        long maxState=0;
        while(br.ready())
            {
            String tempLine=br.readLine();
            if(!passedState)
                {
                if(tempLine.startsWith("State"))
                    {
                    passedState=true;
                    }
                }
            else
                {
                String[] pieces=tempLine.split("\\t");
                long stateVal=Long.parseLong(pieces[0]);
                maxState=stateVal;
                }
            }
        if(returnLastState)
            {
            return maxState;
            }
        else
            {
            long ten_p_bi=maxState/10L;
            return ten_p_bi;
            }
        }
    
    
    public static boolean areRecFreqsSame(File[] logFiles,boolean tellStatus) throws Exception
        {
        ArrayList<Long> rfs=new ArrayList<Long>();
        for(int f=0;f<logFiles.length;f++)
            {
            long rf=obtainFreqUsingLastTwoStates(logFiles[f],tellStatus);
            rfs.add(rf);
            }
        for(int i=0;i<rfs.size()-1;i++)
            {
            for(int j=i+1;j<rfs.size();j++)
                {
                long ri=rfs.get(i);
                long rj=rfs.get(j);
                if(ri!=rj)
                    {
                    String cMsg="Recording freq of file "+logFiles[i]+" is "+ri+" "+
                            "Recording freq of file "+logFiles[j]+" is "+rj+".  Cannot combine!";
                    System.err.println(cMsg);
                    return false;
                    }
                }
            }
        return true;
        }
    
    public static Long obtainFreqUsingLastTwoStates(File logFile,boolean tellStatus) throws Exception
        {
        BufferedReader br=new BufferedReader(new FileReader(logFile));
        ArrayList<Long> all=new ArrayList<Long>();
        boolean passedState=false;
        while(br.ready())
            {
            String tempLine=br.readLine();
            if(passedState)
                {
                String[] pieces=tempLine.split("\\t");
                long tempState=Long.parseLong(pieces[0]);
                all.add(tempState);
                }
            if(tempLine.startsWith("State"))
                {
                passedState=true;
                }
            }
        if(all.size()<2)
            {
            throw new RuntimeException("Error.  From file "+logFile+" only obtained "+all.size()+" state(s)!  Cannot determine frequency!");
            }
        else
            {
            long lastState=all.get(all.size()-1);
            long nextToLastState=all.get(all.size()-2);
            long diff=lastState-nextToLastState;
            if(tellStatus)
                {
                System.out.println("For log file "+logFile+" obtained recording frequency "+diff);
                }
            return diff;
            }
        }
    
    public static boolean compareAllHeadersEqual(File[] logFiles,boolean tellStatus) throws Exception
        {
        ArrayList<Object> headerArrs=new ArrayList<Object>();
        for(int l=0;l<logFiles.length;l++)
            {
            String[] temp_hdrs=obtainColHeaders(logFiles[l],tellStatus);
            if(temp_hdrs==null)
                {
                throw new NullPointerException("Error, got null headers from file "+logFiles[l]);
                }
            headerArrs.add(temp_hdrs);
            }
        for(int i=0;i<headerArrs.size()-1;i++)
            {
            for(int j=i+1;j<headerArrs.size();j++)
                {
                String[] headers_i=(String[])(headerArrs.get(i));
                String[] headers_j=(String[])(headerArrs.get(j));
                boolean headerEqFlag=areHeadersEqusl(headers_i,headers_j);
                if(!headerEqFlag)
                    {
                    String cMsg="For file \n"+logFiles[i]+"\n and file \n"+logFiles[j]+
                            "\n the headers are \n"+Arrays.toString(headers_i)+"\n and \n"+
                            Arrays.toString(headers_j)+"\n respectively.  The headers are *NOT* equal "+
                            "so log-combining cannot proceed!";
                    //throw new RuntimeException(cMsg);
                    System.err.println(cMsg);
                    return false;
                    }
                }
            }
        return true;
        }
    
    public static boolean areHeadersEqusl(String[] h1,String[] h2)
        {

        if(h1==null && h2==null)
            {
            return true;
            }
        else if(h1==null || h2==null)
            {
            return false;
            }
        else
            {
            if(h1.length!=h2.length)
                {
                return false;
                }
            else
                {
                for(int i=0;i<h1.length;i++)
                    {
                    int compareResult=h1[i].compareTo(h2[i]);
                    if(compareResult!=0)
                        {
                        return false;
                        }
                    }
                return true;
                }
            }
            
        }
    
    public static String[] obtainColHeaders(File f,boolean tellHeaders) throws Exception
        {
        BufferedReader br=new BufferedReader(new FileReader(f));
        while(br.ready())
            {
            String tempLine=br.readLine();
            if(tempLine.startsWith("State"))
                {
                String[] pieces=tempLine.split("\\t");
                if(tellHeaders)
                    {
                    System.out.println("From file "+f+" obtained headers "+Arrays.toString(pieces));
                    }
                return pieces;
                }
            }
        throw new RuntimeException("Error: could not find line starting with 'State' for obtaining headers!");            
        }
    
    
}
