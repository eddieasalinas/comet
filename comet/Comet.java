/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author esalina
 */
public class Comet {

    public static String getVersion()
        {
        return "v0.9.951";
        }
    
    public static String getUsageString()
        {
        String usageString="";
        usageString="USAGE : java -jar comet.jar CONF_FILE FASTA_FILE [FILE_OF_PHYLO_XML_FILES]";
        usageString+="\n"+"USAGE : java -jar comet.jar ESS (read state<tab>value\\n from STDIN for ESS-calculation)";
        usageString+="\n"+"USAGE : java -jar comet.jar LOG_COMBINE <OUT_LOG> <IN_LOG> [<IN_LOG>....]";
        return usageString;
        }
    /**
     * @param args the command line arguments
     * COMET - COalescence with Multiple-mergers Employing Thermodynamic integration
     */
    public static void main(String[] args)
        {
        if(args.length>=1)
            {
            if(args[0].trim().equalsIgnoreCase("ESS"))
                {
                //just read data via STDIN for ESS computation
                //first col should be state num ; second col is the value to compute ESS of
                double ess=MCMCUtils.readInputForESSFromSTDIN();
                System.out.println(ess);
                return;
                }
            else if(args[0].trim().equalsIgnoreCase("LOG_COMBINE"))
                {
                if(args.length<3)
                    {
                    String lcMsg="LOG_COMBINE usage : LOG_COMBINE <OUT_LOG> <IN_LOG> [<IN_LOG>....]";
                    throw new RuntimeException(lcMsg);
                    }
                try
                    {
                    File outLog=new File(args[1]);
                    ArrayList<File> inLogs=new ArrayList<File>();
                    //File[] logFiles=new File[args.length-2];
                    for(int l=2;l<args.length;l++)
                        {
                        File tempFile=new File(args[l]);
                        long nRecs=CombineLogs.getNumDataRows(tempFile);
                        if(nRecs>=2)
                            {
                            //logFiles[l-2]=tempFile;
                            inLogs.add(tempFile);
                            }
                        else
                            {
                            System.out.println("Skipping over "+tempFile+"...to few records!");
                            }
                        }
                    if(inLogs.size()<=0)
                        {
                        throw new RuntimeException("Error, no log files to combine!");
                        }
                    File[] logFiles=new File[inLogs.size()];
                    for(int l=0;l<inLogs.size();l++)
                        {
                        logFiles[l]=inLogs.get(l);
                        }
                    CombineLogs.combine10PBI(outLog,logFiles);
                    }
                catch(Exception e)
                    {
                    e.printStackTrace();
                    }
                return;
                }
            }
        System.out.println("COMET "+Comet.getVersion());
        String confFileName=null;
        String fastaFileName=null;
        try
            {
            File confFile=null;
            File fastaFile=null;
            if(args.length>=2)
                {
                confFileName=args[0];
                fastaFileName=args[1];
                fastaFile=new File(fastaFileName);
                confFile=new File(confFileName);
                if(confFile.exists())
                    {
                    System.out.println("Found configuration file "+confFile.getCanonicalPath());
                    }
                else
                    {
                    throw new FileNotFoundException("Error, configuration file "+confFileName+" not found! ABORT!");
                    }
                if(!fastaFile.exists())
                    {
                    String fnfE="ERROR, Input FASTA file "+fastaFileName+" not found! ABORT !";
                    throw new FileNotFoundException(fnfE);
                    }
                }
            else
                {
                System.out.println(Comet.getUsageString());
                System.exit(0);
                }
            String phyloXMLStartFile=null;
            if(args.length==3)
                {
                phyloXMLStartFile=args[2];
                }
                
            MCMC myMCMC=new MCMC(confFileName,fastaFileName,phyloXMLStartFile);
            }
        catch(Exception e)
            {
            System.err.println(Comet.getUsageString());
            e.printStackTrace();
            }
        
        //testAlg1(10);

        //testSuite(true,true,true);
        
        
       
        /*for(int i=0;i<12000;i++)
            {
            testBinarification();
            System.out.println("SUPER TEST DONE! r="+i);
            }*/
        }//main

    public static void testSuite(boolean tf,boolean ta,boolean tLike)
        {
        if(tf)
            {
            System.out.println("Testing FASTAIO:");
            testFastaIO();
            }
        if(ta)
            {
            System.out.println("Test ALG 1");
            testAlg1(10);
            }
        if(tLike)
            {
            singleTreeLikelihoodTest();
            //two short seqs, no mutation
            /*double l1=0;
            l1=testSimpleLikelihood("A","A");
            System.out.println("First like : "+l1);
            double l2=testSimpleLikelihood("A","T");
            System.out.println("Second like : "+l2);
            double l3=testSimpleLikelihood("T","T");
            System.out.println("Third like : "+l3);
            double l4=testSimpleLikelihood("C","G");
            System.out.println("Fourth like : "+l4);*/
            }
        
        }

    
    
    public static boolean singleTreeLikelihoodTestOne()
        {
        String pi_freqs="0.25,0.25,0.25,0.25";
        double kappa=1.0;    
        String[] seqNames={"MAN","MKY","WHL","BRD"};
        String[] seqs={"A","A","C","G"};
        FastaIO fio=new FastaIO(seqNames,seqs);
        System.out.println(fio.toString());
        HashMap lMap=new HashMap();
        lMap.put("kappa", kappa);
        lMap.put("pi_freqs",pi_freqs);
        KHYLikelihoodCalculator myLikeCalc=new KHYLikelihoodCalculator(lMap,1,1);
        coa_lineage man_leaf=new coa_lineage("1");
        coa_lineage mky_leaf=new coa_lineage("2");
        coa_lineage mm_clade=new coa_lineage("");
        mm_clade.addChild(man_leaf,1);
        mm_clade.addChild(mky_leaf,1);
        coa_lineage whl_leaf=new coa_lineage("3");
        coa_lineage brd_leaf=new coa_lineage("4");
        coa_lineage wb_clade=new coa_lineage("");
        wb_clade.addChild(whl_leaf,1);
        wb_clade.addChild(brd_leaf,1);
        coa_lineage root=new coa_lineage("plain_root");
        root.addChild(wb_clade,1);
        root.addChild(mm_clade,1);
        
        double logLike=myLikeCalc.computeLikelihoodNBD(root,fio,true);
        System.out.println("Ret log-like : "+logLike);
        double correctAnswer=(-5.443964);
        System.out.println("Correct log-like : "+correctAnswer);
        double diff=Math.abs(correctAnswer-logLike);
        System.out.println("Diff : "+diff);
        if(diff>=0.00001)
            {
            System.out.println("Error, expected likelihood "+correctAnswer+" but got "+logLike);
            return false;
            }
        else
            {
            return true;
            }
        }
    
    
    public static boolean singleTreeLikelihoodTestTwo()
        {
        String pi_freqs="0.25,0.25,0.25,0.25";
        double kappa=1.0;    
        String[] seqNames={"MAN","MKY","WHL","BRD"};
        String[] seqs={"ATTGCC","ACGATC","CCATTC","GTTACT"};
        FastaIO fio=new FastaIO(seqNames,seqs);
        System.out.println(fio.toString());
        HashMap lMap=new HashMap();
        lMap.put("kappa", kappa);
        lMap.put("pi_freqs",pi_freqs);
        KHYLikelihoodCalculator myLikeCalc=new KHYLikelihoodCalculator(lMap,1,1);
        coa_lineage man_leaf=new coa_lineage("1");
        coa_lineage mky_leaf=new coa_lineage("2");
        coa_lineage mm_clade=new coa_lineage("");
        mm_clade.addChild(man_leaf,1);
        mm_clade.addChild(mky_leaf,1);
        coa_lineage whl_leaf=new coa_lineage("3");
        coa_lineage brd_leaf=new coa_lineage("4");
        coa_lineage wb_clade=new coa_lineage("");
        wb_clade.addChild(whl_leaf,1);
        wb_clade.addChild(brd_leaf,1);
        coa_lineage root=new coa_lineage("plain_root");
        root.addChild(wb_clade,1);
        root.addChild(mm_clade,1);
        
        double logLike=myLikeCalc.computeLikelihoodNBD(root,fio,true);
        System.out.println("Ret log-like : "+logLike);
        double correctAnswer=(-33.588752);
        System.out.println("Correct log-like : "+correctAnswer);
        double diff=Math.abs(correctAnswer-logLike);
        System.out.println("Diff : "+diff);
        if(diff>=0.00001)
            {
            System.out.println("Error, expected likelihood "+correctAnswer+" but got "+logLike);
            return false;
            }
        else
            {
            return true;
            }
        }
    
        
    
    public static boolean singleTreeLikelihoodTestThree()
        {
        String pi_freqs="0.25,0.25,0.25,0.25";
        double kappa=1.0;    
        String[] seqNames={"MAN","MKY","WHL","BRD"};
        String[] seqs={"ATTGCC","ACGATC","CCATTC","GTTACT"};
        FastaIO fio=new FastaIO(seqNames,seqs);
        System.out.println(fio.toString());
        HashMap lMap=new HashMap();
        lMap.put("kappa", kappa);
        lMap.put("pi_freqs",pi_freqs);
        KHYLikelihoodCalculator myLikeCalc=new KHYLikelihoodCalculator(lMap,1,1);
        ArrayList<String> myArrayList=new ArrayList<String>();
        for(int i=1;i<=4;i++)
            {
            myArrayList.add(""+i+"");
            }
        int num_shuffles=100;
        for(int i=0;i<num_shuffles;i++)
            {
            Collections.shuffle(myArrayList);
            //System.out.println("Shuffled : "+Arrays.toString(myArrayList.toArray()));
            coa_lineage man_leaf=new coa_lineage(myArrayList.get(0));
            coa_lineage mky_leaf=new coa_lineage(myArrayList.get(1));
            coa_lineage mm_clade=new coa_lineage("");
            mm_clade.addChild(man_leaf,1);
            mm_clade.addChild(mky_leaf,1);
            coa_lineage whl_leaf=new coa_lineage(myArrayList.get(2));
            coa_lineage brd_leaf=new coa_lineage(myArrayList.get(3));
            coa_lineage wb_clade=new coa_lineage("");
            wb_clade.addChild(whl_leaf,1);
            wb_clade.addChild(brd_leaf,1);
            coa_lineage root=new coa_lineage("plain_root");
            root.addChild(wb_clade,0);
            root.addChild(mm_clade,0);

            double logLike=myLikeCalc.computeLikelihoodNBD(root,fio,true);
            System.out.println("Ret log-like : "+logLike);
            double correctAnswer=(-33.345935);
            System.out.println("Correct log-like : "+correctAnswer);
            double diff=Math.abs(correctAnswer-logLike);
            System.out.println("Diff : "+diff);
            if(diff>=0.00001)
                {
                System.out.println("Error, expected likelihood "+correctAnswer+" but got "+logLike);
                return false;
                }
            else
                {
                //return true;
                }
            
            //now try different branching pattern
            coa_lineage db_man=new coa_lineage(myArrayList.get(0));
            coa_lineage db_mky=new coa_lineage(myArrayList.get(1));
            coa_lineage db_whl=new coa_lineage(myArrayList.get(2));
            coa_lineage db_brd=new coa_lineage(myArrayList.get(3));
            coa_lineage r1=new coa_lineage("r1");
            r1.addChild(db_man,1.0);
            r1.addChild(db_mky,1.0);
            coa_lineage r2=new coa_lineage("r2");
            r2.addChild(r1,0.0);
            r2.addChild(db_whl,1.0);
            coa_lineage mr=new coa_lineage("root");
            mr.addChild(r2,0.0);
            mr.addChild(db_brd,1.0);
            logLike=myLikeCalc.computeLikelihoodNBD(mr,fio,true);
            System.out.println("Correct log-like : "+correctAnswer);
            diff=Math.abs(correctAnswer-logLike);
            System.out.println("Diff : "+diff);
            if(diff>=0.00001)
                {
                System.out.println("Error, expected likelihood "+correctAnswer+" but got "+logLike);
                return false;
                }
            else
                {
                //return true;
                }            
            
            
            
            }
        return true;
        }
    
        
        
    
    
    
    public static void singleTreeLikelihoodTest()
        {
        singleTreeLikelihoodTestOne();
        System.out.println("******************************************");
        singleTreeLikelihoodTestTwo();   
        System.out.println("******************************************");
        singleTreeLikelihoodTestThree();
            

        
        
        }
    
    
    
    
    
    public static double testSimpleLikelihood(String s1,String s2)
        {
        String[] names={"seq1","seq2"};
        String[] seqs={s1,s2};
        FastaIO fio=new FastaIO(names,seqs);
        String pi_freqs="0.25,0.25,0.25,0.25";
        double kappa=1.0;
        HashMap lMap=new HashMap();
        lMap.put("kappa", kappa);
        lMap.put("pi_freqs",pi_freqs);
        KHYLikelihoodCalculator myLikeCalc=new KHYLikelihoodCalculator(lMap,1,1);
        HashMap tMap=new HashMap();
        tMap.put("Y",1);
        tMap.put("psi",1.0);
        int numTrees=100000;
        double probSum=0;
        SWPaperAlgOneGenerator myGen=new SWPaperAlgOneGenerator(2,true);
        for(int t=0;t<numTrees;t++)
            {
            Object[] treeAndPrior=myGen.generateTree();
            coa_lineage randTree=(coa_lineage)(treeAndPrior[0]);
            double logLikelihood=myLikeCalc.computeLikelihoodNBD(randTree,fio,true);
            double logLikeDouble=logLikelihood;
            probSum+=Math.exp(logLikeDouble);
            if(t%10000==0)
                {
                System.out.println("Tree #"+t+" of "+numTrees);
                System.out.println("The log likelihood for tree="+randTree.toString()+" is "+logLikeDouble);
                System.out.println("The data are : "+fio.toString());
                }
            }
        double avg=probSum/(double)(numTrees);
        return avg;
        }
    
    
    public static void testFastaIO()
        {
        
        try
            {
            int x=0;
            for(x=1;x<=5;x++)
                {
                System.out.println("\n\n\n\n\n============================================");
                String path=new String("/home/esalina/NetBeansProjects/coats/test_"+x+".fasta");
                File ff=new File(path);
                FastaIO fio=new FastaIO(ff,false);
                System.out.println(fio.toString());
                System.out.println("\n\n\n\n\n============================================");
                }
            String bluePath="/home/esalina/Downloads/JohnHorne/NexData/BlueCOI.fasta";
            FastaIO fio=new FastaIO(new File(bluePath),false);
            //System.out.println(fio.toString());
            System.out.println("\n\n\n\n\nRegular : ");
            String[] sp1={"A","A"};
            String[] sp1_n={"seq1","seq2"};
            FastaIO init_1=new FastaIO(sp1_n,sp1);
            System.out.println(init_1.toString());
            String[] sp2={"G","C"};
            String[] sp2_n={"t1","t2"};
            FastaIO init_2=new FastaIO(sp2_n,sp2);
            System.out.println(init_2.toString());
                   
            
            }
        catch(Exception e)
            {
            e.printStackTrace();
            }
                            
            
        }
    
    
    
    public static void testAlg1(int numTrees)
        {
        for(double psi=1.0;psi<=1.0;psi+=0.1)
            {
            if(psi>1.0)
                {
                psi=1.0;
                }
            for(int Y=2;Y<=10;Y++)
                {
                for(int num_lin=2;num_lin<=100;num_lin+=30)
                    {
                    SWPaperAlgOneGenerator m=new SWPaperAlgOneGenerator(num_lin,true);
                    HashMap pMap=new HashMap();
                    pMap.put("psi",new BigDecimal(psi));
                    pMap.put("Y",new BigDecimal(Y));
                    pMap.put("num_lineages",num_lin);        
                    m.setParams(pMap);
                    for(int t=0;t<numTrees;t++)
                        {
                        System.out.println("GENERATING TREE #"+t+" with psi="+psi+", Y="+Y+" and num_lineages="+num_lin);
                        Object[] rp=m.generateTree();
                        coa_lineage l=(coa_lineage)(rp[0]);
                        boolean v1=l.verifyHeights(false);
                        if(!v1)
                            {
                            throw new RuntimeException("Error, fail on v1");
                            }
                        String bj=l.toString();
                        double bh=l.getHeight();
                        while(l.testRecIfExtraNodes())
                            {
                            l.removeExtraNodes();
                            }            
                        double ah=l.getHeight();
                        String aj=l.toString();
                        boolean v2=l.verifyHeights(false);
                        if(!v2)
                            {
                            throw new RuntimeException("Error, fail on v2");
                            }
                        System.out.println("##############");
                        System.out.println("BJ (height "+bh+") : "+bj);
                        System.out.println("AJ (height "+ah+") : "+aj);
                        }
                    }
                }
            }
        }
   
    public static void testBinarificationBig()
        {
        int num_lineages=10;
        String[] names=new String[num_lineages];
        String[] seqs=new String[num_lineages];
        for(int i=1;i<=num_lineages;i++)
            {
            names[i-1]=new String(""+i+"");
            seqs[i-1]=new String("G");
            }
        FastaIO fio=new FastaIO(names,seqs);
        HashMap lMap=new HashMap();
        lMap.put("kappa", 1.0);
        lMap.put("pi_freqs","0.25,0.25,0.25,0.25");
        KHYLikelihoodCalculator myLikeCalc=new KHYLikelihoodCalculator(lMap,1,1);
        
        }
    
    
    
    
    public static void testBinarification()
        {
        int numTrees=2;
        SWPaperAlgOneGenerator m=new SWPaperAlgOneGenerator(3,true);
        HashMap pMap=new HashMap();
        pMap.put("psi",new BigDecimal(1.0));
        pMap.put("Y",new BigDecimal(1));
        pMap.put("num_lineages",3);
        m.setParams(pMap);
        coa_lineage firstTree=new coa_lineage("first");
        coa_lineage secondTree=new coa_lineage("second");
        
        for(int t=0;t<numTrees;t++)
            {
            Object[] rp=m.generateTree();
            coa_lineage l=(coa_lineage)(rp[0]);

            if(t==0)
                {
                System.out.println("FIRST");
                System.out.println("Obtained:"+l.toString());
                System.out.println("Ob height : "+l.getHeight());
                firstTree=l;
                }
            else
                {
                System.out.println("SECOND");
                System.out.println("Obtained:"+l.toString());
                System.out.println("Ob height : "+l.getHeight());
                secondTree=l;
                }
            }
        
        firstTree.changeNameRec("1","1_first");
        firstTree.changeNameRec("2","2_first");
        firstTree.changeNameRec("3","3_first");
        System.out.println("First after name changes : ");
        System.out.println(firstTree.toString());
        boolean v1=firstTree.verifyHeights(false);
        System.out.println("v1="+v1);
        secondTree.changeNameRec("1","1_second");
        secondTree.changeNameRec("2","2_second");
        secondTree.changeNameRec("3","3_second");
        System.out.println("Second tree after name changes : ");
        System.out.println(secondTree.toString());
        boolean v2=secondTree.verifyHeights(false);
        System.out.println("v2="+v2);
        System.out.println("First height="+firstTree.getHeight());
        System.out.println("Second height="+secondTree.getHeight());
        if(firstTree.getHeight()>secondTree.getHeight())
            {
            double diff=firstTree.getHeight()-secondTree.getHeight();
            System.out.println("Ok to add!");
            firstTree.addChild(secondTree, diff);
            boolean v3=firstTree.verifyHeights(false);
            System.out.println("v3="+v3);
            System.out.println("POST add is ");
            System.out.println("Height="+firstTree.getHeight());
            System.out.println(firstTree.toString());
            firstTree.makeBinary();
            System.out.println("BINARIFIED");
            System.out.println(firstTree.toString());
            System.out.println("Binarified height : "+firstTree.getHeight());
            boolean v4=firstTree.verifyHeights(false);
            System.out.println("v4="+v4);
            }
        else
            {
            System.out.println("NOT Ok to add!");
            }

        }

}
