/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comet;

import java.time.Clock;
import java.time.Instant;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ArrayBlockingQueue;


/**
 *
 * @author esalina
 */
public class MCMCMC {

    Instant startMCMCMCTime=null;
    Instant endMCMCMCTime=null;
    Double numHoursToRun=null;
    Double minESSValue=null;
    ArrayList<Long> stateList=new ArrayList<Long>();
    private Object[] getStatesAndValFromSuperDict(String key,long burnin)
        {
        ArrayList<Long> states=new ArrayList<Long>();
        ArrayList<Double> vals=new ArrayList<Double>();
        for(int i=0;i<stateList.size();i++)
            {
            long state=(long)(stateList.get(i));
            HashMap specificDict=(HashMap)superDict.get(state);
            String val=(String)specificDict.get(key);
            //System.out.println("For key="+key+" got val="+val);
            if(state>=burnin)
                {
                states.add(state);
                vals.add(Double.parseDouble(val));
                }
            }
        Object[] retPack=new Object[2];
        retPack[0]=states;
        retPack[1]=vals;
        return retPack;        
        }
    
    private double[] getValsFromSuperDict(String key,long burnin)
        {
        Object[] retPack=getStatesAndValFromSuperDict(key,burnin);
        ArrayList<Double> vals=(ArrayList<Double>)retPack[1];
        double[] v=new double[vals.size()];
        for(int i=0;i<v.length;i++)
            {
            v[i]=vals.get(i);
            }
        return v;
        }
    
    
    private HashMap superDict=new HashMap();
    private void superDictTraceLog(String key,String val,long state)
        {
        if(superDict.containsKey(state))
            {
            HashMap specificDict=(HashMap)superDict.get(state);
            specificDict.put(key, val);
            superDict.put(state,specificDict);
            }
        else
            {
            HashMap specificDict=new HashMap();
            specificDict.put(key, val);
            superDict.put(state,specificDict);
            }
        }
    
    
    private HashMap swapAcceptCounter=new HashMap();
    private HashMap swapRejectCounter=new HashMap();
    private HashMap chainSwapCountMap=new HashMap();
    
    private void logSwap(String k)
        {
        int count=0;
        if(chainSwapCountMap.containsKey(k))
            {
            count=(int)(chainSwapCountMap.get(k));
            }
        count++;
        chainSwapCountMap.put(k, count);
        }
    
    private void logAddition(String k,HashMap h)
        {
        int existing=0;
        if(h.containsKey(k))
            {
            existing=(int)h.get(k);
            }
        existing++;
        h.put(k,existing);
        }
    
    public void logMCMCMCAcceptance(int i,int j)
        {
        if(i>j)
            {
            int k=i;
            i=j;
            j=k;
            }
        String key=i+","+j;
        logAddition(key,swapAcceptCounter);
        }
    
    public static String[] generatePossibleMCMCMCSwapKeys(int numChains)
        {
        ArrayList<String> alKeys=new ArrayList<String>();
        for(int i=0;i<numChains;i++)
            {
            for(int j=i+1;j<numChains;j++)
                {
                String k=i+","+j;
                alKeys.add(k);
                }
            }
        String[] mcmcmcKeys=new String[alKeys.size()];
        for(int i=0;i<mcmcmcKeys.length;i++)
            {
            mcmcmcKeys[i]=alKeys.get(i);
            }
        return mcmcmcKeys;
        }
    
    public void logMCMCMCRejection(int i,int j)
        {
        if(i>j)
            {
            int k=i;
            i=j;
            j=k;
            }
        String key=i+","+j;
        logAddition(key,swapRejectCounter);
        }

    
    public static void swapArrayItems(Object[] arr,int idxA,int idxB)
        {
        if(arr==null)
            {
            //return;
            throw new NullPointerException("Trying to swap items of null array?!?!");
            }
        else if(arr.length<=1)
            {
            throw new IllegalArgumentException("Trying to swap items in array of length <=1?!?!");
            //return;
            }
        else
            {
            if(0<=idxA && idxA<arr.length && 0<=idxB && idxB<arr.length)
                {
                if(idxA==idxB)
                    {
                    throw new IllegalArgumentException("Trying to swap an element with iteself?!?!");
                    //return;
                    }
                else
                    {
                    Object temp=arr[idxA];
                    arr[idxA]=arr[idxB];
                    arr[idxB]=temp;
                    }
                }
            }            
        }
    
    
    
    public boolean appendToTraceLog(
            HashMap recMap,
            long iterNum,
            boolean firstRecord,
            File logFile,
            boolean tracerReadyOnly,
            boolean cMLOverRide,
            long recordingFreq
            ) throws Exception
        {
        //^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$
        Pattern tracerReadyPattern=Pattern.compile("^\\s*[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?\\s*$");
        FileWriter fw=new FileWriter(logFile,!firstRecord);
        Set keySet=recMap.keySet();
        ArrayList<String> keyArrayList=new ArrayList<String>();
	ArrayList<String> colsToCalcESSFor=new ArrayList<String>();
        ArrayList<String> blacklistedVals=new ArrayList<>();
        String[] blacklistedArr={"Proposal_scheme","max_val_depth","last_30_acc_ratio","iter_num"};
        for(String bl:blacklistedArr)
            {
            blacklistedVals.add(bl);
            }
        for(Object k:keySet)
            {
            if(!tracerReadyOnly)
                {
                //if not tracer ready only, then add unconditionally
                keyArrayList.add(k.toString());
                }
            else
                {
                //if tracer-ready only, then test for numeric!
                if(!stateList.contains(iterNum))
                    {
                    stateList.add(iterNum);
                    }
                Object val=recMap.get(k);
                String valStr=val.toString();
                Matcher m=tracerReadyPattern.matcher(valStr);
                if(m.find() && !blacklistedVals.contains(k))
                    {
                    keyArrayList.add(k.toString());
		    superDictTraceLog(k.toString(),recMap.get(k.toString()).toString(),iterNum);
		    colsToCalcESSFor.add(k.toString());
                    }
                else
                    {
                    String keyString=k.toString();
                    //if c_ML is the key and the override is on, then log it!
                    if(cMLOverRide && keyString.equalsIgnoreCase("c_ML"))
                        {
                        keyArrayList.add(k.toString());
			superDictTraceLog(k.toString(),recMap.get(k.toString()).toString(),iterNum);
			colsToCalcESSFor.add(k.toString());
                        }
                    }
                }
            }
        Collections.sort(keyArrayList);
        int likeIndex=TreeUtils.getIndexOfStringInArrayList(keyArrayList, "Likelihood",true);
        keyArrayList=TreeUtils.swapVals(0, likeIndex, keyArrayList);
        //in this block add the ESS for tracer-ready log
        boolean stopForReasonSufficientESS=false;
        if(minESSValue!=null && tracerReadyOnly)
            {
            stopForReasonSufficientESS=true;
            }
        if(tracerReadyOnly)
            {
            Collections.sort(colsToCalcESSFor);
            //remove proposalscheme
            //calc ESS for all of the columns indicated here
            for(int c=0;c<colsToCalcESSFor.size();c++)
                {
                String essCol=colsToCalcESSFor.get(c)+"_ESS";
                String essColTP=colsToCalcESSFor.get(c)+"_ESS_10%bi";
                keyArrayList.add(essCol);
                keyArrayList.add(essColTP);
                if(stateList.size()<=10)
                    {
                    recMap.put(essCol,"-1");
                    recMap.put(essColTP,"-1");
                    stopForReasonSufficientESS=false;
                    }
                else
                    {
                    //ESS for whole run for this column
                    double[] vals=this.getValsFromSuperDict(colsToCalcESSFor.get(c),0);
                    HashMap statMap=MCMCUtils.analyseCorrelationContinuous(vals,(int)recordingFreq);
		    double thisESS=(double)(statMap.get("ESS"));
		    if(Double.isNaN(thisESS))
			{
			thisESS=(-1);
			}
                    recMap.put(essCol,String.format("%.2f",thisESS));
                    //10pct burnin for this column
                    long ten_pct=iterNum/10;
                    vals=this.getValsFromSuperDict(colsToCalcESSFor.get(c),ten_pct);
                    statMap=MCMCUtils.analyseCorrelationContinuous(vals,(int)recordingFreq);
                    thisESS=(double)(statMap.get("ESS"));
		    if(Double.isNaN(thisESS))
			{
			thisESS=(-1);
			}
                    recMap.put(essColTP,String.format("%.2f",thisESS));
                    if(minESSValue!=null)
                        {
			//if the value is non-null, there there is a min-required value
                        if(thisESS<minESSValue.doubleValue() && thisESS>0 )
                            {
                            //if at least one of the ESS values is less than the min required value or after burn-in is negative then
                            //set the stop flag to false
                            stopForReasonSufficientESS=false;
                            }
                        }
                    }
                }
            }
            int numItems=keyArrayList.size();

            
        
        
        if(firstRecord)
            {
            String header=new String("");
            header+="# COMET "+Comet.getVersion()+"\n";
            header+="# Generated "+TreeUtils.getNiceDataFormat()+"\n";
            header+="State\t";
            for(int i=0;i<numItems;i++)
                {
                header+=keyArrayList.get(i);
                if(i<numItems-1)
                    {
                    header+="\t";
                    }
                }
            header+="\n";
            fw.write(header);
            }
        String data="";
        //write the state here
        data+=""+(iterNum)+"\t";      
        for(int i=0;i<numItems;i++)
                {
                data+=recMap.get(keyArrayList.get(i));
                if(i<numItems-1)
                    {
                    data+="\t";
                    }
                }
        fw.write(data+"\n");
        fw.flush();
        fw.close();
        return stopForReasonSufficientESS;
        }    
    
    public Object[] acquireProposalAndLikelihood(
            coa_lineage current,
            BigDecimal currentPrior,
            TreeGenerator tGen,
            TreeLikelihoodCalculator lc,
            FastaIO fio,
            ProposalManager proposalManager,
            long iterNum
            )
        {
        //init
        BigDecimal acquiredLikelihood=null;
        BigDecimal prior=null;
        coa_lineage proposedTree=null;
        int pMethod=(-1);
        
        //acquire proposal
        
        double randNum=TreeUtils.getRandNumBet(0,1,true);
        //System.out.println("In acquireProposalAndLikelihood");
        //System.out.println("randnum is "+randNum);
        
        
        //half the time IF the likelihood parameters
        //aren't [X,X] (ie set to be a single thing)
        //then the proposal is just on the liklihood end
        //and the tree stays the same
        //System.err.println("The value of randNum in apl is "+randNum);
        //System.err.println("The value of lc.arePriorsAllSingle() is "+lc.arePriorsAllSingle());
        if(randNum>=0.50 && !(lc.arePriorsAllSingle()))
            {
            //draw new likelihood params
            pMethod=MCMC.LIKELIHOOD_PROPOSAL;
            if(proposalManager.doesProposalLineIndicatePureGeneration())
                {
                lc.proposeNewParams(true);
                }
            else
                {
                lc.proposeNewParams(false);
                }
            prior=currentPrior;
            proposedTree=current;
            }
        else
        //other half the time IF the likelihood parameters
        //are [X,X] (ie set to be a single thing)
        //then the proposal is on the tree side
            {
            pMethod=MCMC.TREE_PROPOSAL;
            //draw new tree 1/2 time if lc params have a range
            //NOTE the first thing proposal manager does is clone a tree unless it's a generic proposal
            Object[] ret_pack=proposalManager.proposeTree(current, currentPrior, tGen,iterNum);
            proposedTree=(coa_lineage)(ret_pack[0]);
            prior=(BigDecimal)(ret_pack[1]);
            }
        //compute likelihood
        acquiredLikelihood=lc.computeLikelihood(proposedTree,fio,true,tGen);
        if(acquiredLikelihood==null)
            {
            return acquireProposalAndLikelihood(
                    current,
                    currentPrior,
                    tGen,
                    lc,
                    fio,
                    proposalManager,
                    iterNum
                    );
            }
        Object[] retPackage=new Object[]{proposedTree,acquiredLikelihood,prior,pMethod};
        
        
        return retPackage;        
        }
    
    
    
    public static HashMap makeInitialLPMap(FastaIO fastaIO)
        {
        HashMap lpMap=new HashMap();
        lpMap.put("kappa",1.0);
        double[] autoFreqs=fastaIO.computeNAFreqs();
        String[] autoFreqsStr=new String[4];
        for(int b=0;b<4;b++)
            {
            autoFreqsStr[b]=""+autoFreqs[b]+"";
            }
        if(!TreeUtils.arePiFreqsAllPositive(autoFreqsStr))
            {
            System.out.println("WARNING, auto pi freqs calculated as : "+Arrays.toString(autoFreqsStr));
            System.out.println("Overwriting with uniform !");
            lpMap.put("pi_freqs","0.25,0.25,0.25,0.25");
            }
        else
            {
            String autoFreqsCommaSep=TreeUtils.join(autoFreqsStr,",");
            System.out.println("Using pi frequencies : "+Arrays.toString(autoFreqs));
            lpMap.put("pi_freqs",autoFreqsCommaSep);
            }
        return lpMap;
        }
    
    
    public void prepMCMCMC(
        TreeGenerator[] mcmcTGens,        
        KHYLikelihoodCalculator[] myLCs,
        ProposalManager[] proposalManagers,
        String[] logTracePaths,
        String[] tracerLogPaths,
        String phyloXMLStartFileOfFiles,
        double[] descendingBetas,
        BigDecimal[] bdDescendingBetas,
        FastaIO fio,
        long maxIter,
        long recordingFreq,
        String mlXMLFilePath
        )
        {
        //initialize corresponding arrays of trees and likelihoods and priors and such
        ArrayList<String> phyloXMLStartTreePaths=new ArrayList<String>();
        if(phyloXMLStartFileOfFiles!=null)
            {
            if(MCMC.isValidFof(phyloXMLStartFileOfFiles))
                {
                try
                    {
                    ArrayList<String> readLines=TreeUtils.readLines(phyloXMLStartFileOfFiles,true);
                    if(readLines.size()>=bdDescendingBetas.length)
                        {
                        //each chain can have its own distinct unique starting tree
                        for(int r=0;r<readLines.size();r++)
                            {
                            String aPathToExamine=readLines.get(r);
                            File aTempFile=new File(aPathToExamine);
                            if(aTempFile.exists())
                                {
                                phyloXMLStartTreePaths.add(readLines.get(r));    
                                }
                            else
                                {
                                System.err.println("WARNING : line "+(r+1)+" from "+phyloXMLStartFileOfFiles+" : "+
                                        aPathToExamine+" file not found! Skipping!"
                                        );
                                }                            
                            }
                        if(readLines.size()>bdDescendingBetas.length)
                            {
                            System.out.println("Warning, there are "+
                                    bdDescendingBetas.length+" chains and "+readLines.size()+
                                    " starting trees.  Ignoring extra starting trees.");
                            }
                        }//sufficient or excessive number of starting trees
                    else
                        {
                        //fewer starting trees than chains
                        System.out.println("NOTE : Fewer starting trees supplied than chains....");
                        for(int b=0;b<bdDescendingBetas.length;b++)
                            {
                            int fofIndex=(b%readLines.size());
                            phyloXMLStartTreePaths.add(readLines.get(fofIndex));
                            System.out.println("Starting tree "+readLines.get(fofIndex)+" assigned to chain "+(b+1));
                            }                        
                        }//insufficient number of trees means round-robin style useage , from modulus
                    }
                catch(Exception e)
                    {
                    e.printStackTrace();
                    System.err.println("Error processing FOF "+phyloXMLStartFileOfFiles+" so ignoring the XML FOF and giving each chain its own random starting tree.");
                    phyloXMLStartFileOfFiles=null;
                    }                
                }//is a valid XML FOF
            else
                {
                System.err.println("Warning, invalid FOF "+phyloXMLStartFileOfFiles+" detected so ignoring it and giving each chain its own random starting tree.");
                //all chains to use different starting trees
                phyloXMLStartFileOfFiles=null;
                }//not a valid FOF
            }//non-null XML FOF
        
        //setup for trees and priors
        coa_lineage[] currents=new coa_lineage[descendingBetas.length];
        BigDecimal[] currentLikelihoods=new BigDecimal[descendingBetas.length];
        BigDecimal[] priors=new BigDecimal[descendingBetas.length];
        System.out.println("Running MCMCMC !");
        HashMap[] currentGenParams=new HashMap[descendingBetas.length];
        HashMap[] currentLCParams=new HashMap[descendingBetas.length];
        coa_lineage[] mlTrees=new coa_lineage[descendingBetas.length];
        BigDecimal[] mlValues=new BigDecimal[descendingBetas.length];
        HashMap[] mlPosteriors=new HashMap[descendingBetas.length];
        for(int b=0;b<descendingBetas.length;b++)
            {

            //Capture start instant in case we want to stop before nax iterations
            if(numHoursToRun!=null)
                {
                BigDecimal numSecsToRunBD=new BigDecimal(numHoursToRun.doubleValue()*3600.0);
                startMCMCMCTime=Clock.systemUTC().instant();
                endMCMCMCTime=startMCMCMCTime.plus(numSecsToRunBD.longValue(),ChronoUnit.SECONDS);
                System.out.println("MCMCMC timer reset at "+startMCMCMCTime.toString()+" for beta="+descendingBetas[b]+". "+
                        " If MCMC is still running at "+endMCMCMCTime.toString()+" then it will be stopped.");
                }                
                
                
            if(phyloXMLStartFileOfFiles!=null)
                {
                File startingTreeFile=new File(phyloXMLStartTreePaths.get(b));
                try
                    {
                    currents[b]=coa_lineage.parsePhyloXML(startingTreeFile);
                    System.out.println("For chain with beta = "+
                            descendingBetas[b]+" loaded starting tree from "+phyloXMLStartTreePaths.get(b));
                    if(mcmcTGens[b] instanceof SWPaperAlgTwoGenerator)
                        {
                        SREvent[] loadedEvents=SWPaperAlgTwoGenerator.generateEvents(currents[b]);
                        String waitsStr=SREvent.getWaitsArrAsStr(loadedEvents);
                        String heightsStr=SREvent.getHeightsArrAsStr(loadedEvents);
                        currents[b].setPosteriorSampledValue("sre_profile",SREvent.JSONString(loadedEvents));
                        currents[b].setPosteriorSampledValue("sre_waits",waitsStr);
                        currents[b].setPosteriorSampledValue("sre_heights",heightsStr);
                        int[] mergerCounts=SWPaperAlgTwoGenerator.countMergers(currents[b]);
                        currents[b].setPosteriorSampledValue("KingmanMergers",mergerCounts[0]);
                        currents[b].setPosteriorSampledValue("SREMergers",mergerCounts[1]);
                        double c_ML=SWPaperAlgTwoGenerator.mlEstC(currents[b],true);
                        currents[b].setPosteriorSampledValue("c_ML",c_ML);
                        currents[b].setFastaIORef(fio);
                        }
                    if(mcmcTGens[b] instanceof SWPaperAlgOneGenerator)
                        {
                        int mvd=SWPaperAlgOneGenerator.returnMaxDepth(currents[b]);
                        currents[b].setPosteriorSampledValue("max_val_depth", mvd);
                        }
                    }
                catch(Exception e)
                    {
                    e.printStackTrace();
                    }                
                }//use starting tree(s)
            else
                {
                Object[] initialTreeAndPrior=mcmcTGens[b].generateTree();
                currents[b]=(coa_lineage)(initialTreeAndPrior[0]);
                currents[b].setFastaIORef(fio);                
                }//generate tree            
            priors[b]=new BigDecimal("0");
            BigDecimal like=(BigDecimal)myLCs[b].computeLikelihood(currents[b],fio,true,mcmcTGens[b]);
            currentLikelihoods[b]=like;
            mlTrees[b]=currents[b].cloneRec();
            mlValues[b]=new BigDecimal(like.toString());
            mlPosteriors[b]=TreeUtils.cloneMap(mlTrees[b].getPosteriorSampleMap());
            System.out.println("For chain with beta="+descendingBetas[b]);
            System.out.println("\tInitial likelihood is : "+like);
            currentGenParams[b]=mcmcTGens[b].cloneParams();
            currentLCParams[b]=myLCs[b].cloneParams();                
            }
        

        //call MCMCMC
        runMCMCMC(
            mcmcTGens,        
            myLCs,
            proposalManagers,
            logTracePaths,
            tracerLogPaths,
            descendingBetas,
            bdDescendingBetas,
            currents,
            currentLikelihoods,
            priors,
            currentGenParams,
            currentLCParams,
            mlTrees,
            mlValues,
            mlPosteriors,
            maxIter,
            recordingFreq,
            fio,
            mlXMLFilePath
        );

        
        
        
        }
    
    public void runMCMCMC(
        TreeGenerator[] mcmcTGens,        
        KHYLikelihoodCalculator[] myLCs,
        ProposalManager[] proposalManagers,
        String[] logTracePaths,
        String[] tracerLogPaths,
        double[] descendingBetas,
        BigDecimal[] bdDescendingBetas,
        coa_lineage[] currents,
        BigDecimal[] currentLikelihoods,
        BigDecimal[] priors,
        HashMap[] currentGenParams,
        HashMap[] currentLCParams,
        coa_lineage[] mlTrees,
        BigDecimal[] mlValues,
        HashMap[] mlPosteriors,
        long maxIter,
        long recordingFreq,
        FastaIO fio,
        String mlXMLPath
        )
        {

        long firstIterationNumber=1;
        long currentIterationNumber=firstIterationNumber;
        boolean[] proposalsAccepted=new boolean[mlPosteriors.length];
        boolean[] firstRecords=new boolean[mcmcTGens.length];        
        for(int b=0;b<proposalsAccepted.length;b++)
            {
            proposalsAccepted[b]=true;
            firstRecords[b]=true;
            }
        String cometDisablePhyloXMLVar=System.getenv("COMET_DISABLE_PHYLOXML");
        if(cometDisablePhyloXMLVar!=null)
            {
            System.out.println("Environment variable COMET_DISABLE_PHYLOXML found to be set.  Suppressing output of non-ML XML...");
            for(int b=0;b<mcmcTGens.length;b++)
                {
                if(mcmcTGens[b] instanceof SWPaperAlgOneGenerator)
                    {
                    SWPaperAlgOneGenerator.setPosteriors=false;
                    }
                }
            }
        
        String triggerXMLPath=mlXMLPath+".trigger";
        System.out.println("Trigger XML path is "+triggerXMLPath);
        System.out.println("Issue 'touch "+triggerXMLPath+"' to trigger write out of current ML XML data and current tree XML data");

        while(currentIterationNumber<=maxIter)
            {
                
            if(currentIterationNumber>=2)
                {
                File triggerFile=new File(triggerXMLPath);
                if(triggerFile.exists())
                    {
                    System.out.println("Found trigger file "+triggerXMLPath);
                    coa_lineage mlTreeForPhyloXML=mcmcTGens[0].tagForPhyloXML(mlTrees[0]);
                    String thePhyloXML=mlTreeForPhyloXML.makePhyloXML(true,0,false,mlValues[0]);
		    String thisIterMLXMLPath=mlXMLPath+".ml.iter_"+currentIterationNumber+".xml";
                    try
                        {
                        TreeUtils.writeStringToFile(thePhyloXML+"\n",thisIterMLXMLPath);
                        System.out.println("PhyloXML for ML tree written to "+thisIterMLXMLPath);
                        triggerFile.delete();
                        System.out.println("Deleted trigger file.");
                        }
                    catch(Exception e)
                        {
                        e.printStackTrace();
                        System.err.println("WARNING ! FAILURE to write ML XML to file "+thisIterMLXMLPath);
                        }
                    coa_lineage currentPhyloXMLCoa_Lineage=mcmcTGens[0].tagForPhyloXML(currents[0]);
                    String currentPhyloXML=currentPhyloXMLCoa_Lineage.makePhyloXML(true,0,false,currentLikelihoods[0]);
                    String thisIterXMLPath=mlXMLPath+".iter_"+currentIterationNumber+".xml";
                    try
                        {
                        TreeUtils.writeStringToFile(currentPhyloXML+"\n",thisIterXMLPath);
                        System.out.println("PhyloXML for current tree written to "+thisIterXMLPath);
                        }
                    catch(Exception e)
                        {
                        e.printStackTrace();
                        System.err.println("WARNING ! FAILURE to write ML XML to file "+thisIterMLXMLPath);
                        }
                    }
                }
                
            
            if(descendingBetas.length>1 && mcmcTGens[0] instanceof SWPaperAlgOneGenerator)
                {
                SWPaperAlgOneGenerator first=(SWPaperAlgOneGenerator)(mcmcTGens[0]);
                first.setTopBeta(true);
                HashMap firstMap=first.getAdaptiveMap();
                for(int b=1;b<descendingBetas.length;b++)
                    {
                    SWPaperAlgOneGenerator other=(SWPaperAlgOneGenerator)(mcmcTGens[b]);
                    other.setAdaptiveMap(firstMap);
                    }
                }
            else if(mcmcTGens[0] instanceof SWPaperAlgOneGenerator)
                {
                SWPaperAlgOneGenerator first=(SWPaperAlgOneGenerator)(mcmcTGens[0]);
                first.setTopBeta(true);        
                }
                
            /*
                this for loop for recording samples in logs
                and for making/accepting proposals
            */    
            for(int b=0;b<descendingBetas.length;b++)
                {
                    
                mcmcTGens[b].setBeta(descendingBetas[b]);
                currents[b].setPosteriorSampledValue("iter_num",currentIterationNumber);
                currents[b].setPosteriorSampledValue("last_30_acc_ratio",proposalManagers[b].getTreeAccRejRatio(30));
                //do logging to trace files
                if(b==0 && 
                        (
                        currentIterationNumber==firstIterationNumber  
                            ||  
                            (currentIterationNumber % recordingFreq)==0)
                        )
                    {
                    //the purpose of this block is to LOG data to the trace output file!
                    HashMap recMap=new HashMap();
                    recMap.put("Proposal_scheme",""+proposalManagers[b].getLastProposalStyleMade()+"");
                    recMap.put("Tree",currents[b].makeNewickString(true,fio));
                    recMap.put("TreeHeight",currents[b].getHeight());
                    recMap.put("Likelihood",currentLikelihoods[b].toString().substring(
                            0,Math.min(12,currentLikelihoods[b].toString().length())));
                    recMap.put("Timestamp",TreeUtils.getNiceDataFormat());
                    recMap.put("Proposal_scheme_status",proposalsAccepted);
                    if(cometDisablePhyloXMLVar==null)
                        {
                        //if the disable flag is absent, then make XML
                        coa_lineage currentTaggedForXML=mcmcTGens[b].tagForPhyloXML(currents[b]);
                        recMap.put("phylo_xml",currentTaggedForXML.makePhyloXML(true,0,false,currentLikelihoods[b]));
                        }
                    else
                        {
                        //if the disable flag is present then don't make it!                        
                        recMap.put("phylo_xml","off");
                        }
                    recMap=currents[b].addPosteriorSampledToMap(recMap);
                    //here are the code for retrieving input for and computing effective population size
                    double thetaHat=mcmcTGens[b].estThetaHatIP278(currents[b]);
                    double mu=myLCs[b].getTotalMutationRate();
                    double N_e=(thetaHat/4.0)/mu;
                    recMap.put("N_e",N_e);
                    for(Object key:currentLCParams[b].keySet())
                        {
                        //set likelihood calculation parameters to be logged
                        recMap.put(key,currentLCParams[b].get(key));
                        }
                    try
                        {
                        File rawLogFile=new File(logTracePaths[b]);
                        File tracerLogFile=new File(tracerLogPaths[b]);
                        boolean cMLOverride=false;
                        if(mcmcTGens[b] instanceof SWPaperAlgTwoGenerator)
                            {
                            cMLOverride=true;
                            }
                        appendToTraceLog(recMap,currentIterationNumber,firstRecords[b],rawLogFile,false,cMLOverride,recordingFreq);
                        boolean stopForESS=appendToTraceLog(recMap,currentIterationNumber,firstRecords[b],tracerLogFile,true,cMLOverride,recordingFreq);
                        firstRecords[b]=false;
                        if(numHoursToRun!=null || stopForESS )
                            {
                            if(numHoursToRun!=null)
                                {
                                Instant now=Clock.systemUTC().instant();
                                if(now.isAfter(this.endMCMCMCTime))
                                    {
                                    System.out.println("The time is now "+TreeUtils.getNiceDataFormat());
                                    System.out.println("STOP because MCMCMC has been running for "+numHoursToRun+" hours!");
                                    maxIter=currentIterationNumber;
                                    currentIterationNumber=maxIter;
                                    }
                                }
                            if(stopForESS)
                                {
                                System.out.println("Stop for ESS is "+stopForESS);
                                System.out.println("STOP for sufficient ESS!");
                                maxIter=currentIterationNumber;
                                currentIterationNumber=maxIter;
                                }
                            }

                        }
                    catch(Exception e)
                        {
                        e.printStackTrace();
                        System.exit(0);
                        }                    
                    }
                
                //proposal-making and likelihood calculations
                HashMap lcParamsBeforeProposal=myLCs[b].cloneParams();
                Object[] proposedTreeItsLikelihoodAndItsPrior=acquireProposalAndLikelihood(
                    currents[b],
                    priors[b],
                    mcmcTGens[b],
                    myLCs[b],
                    fio,
                    proposalManagers[b],
                    currentIterationNumber
                );
                int pMethod=(int)proposedTreeItsLikelihoodAndItsPrior[3];
                HashMap proposedtGenParams=mcmcTGens[b].cloneParams();
                HashMap proposedLCParams=myLCs[b].cloneParams();                
                coa_lineage proposedTree=(coa_lineage)(proposedTreeItsLikelihoodAndItsPrior[0]);
                BigDecimal proposedTreeLikelihood=(BigDecimal)(proposedTreeItsLikelihoodAndItsPrior[1]);
                BigDecimal proposedTreePrior=(BigDecimal)(proposedTreeItsLikelihoodAndItsPrior[2]);
                //compute qBETAs
                /*System.out.println("b="+b);
                System.out.println("priors[b]="+priors[b]);
                System.out.println("proposedprior = "+proposedTreePrior);*/
                BigDecimal qBetaCurrent=MCMC.qBetaOneModel(bdDescendingBetas[b],currentLikelihoods[b],priors[b],true);
                BigDecimal qBetaProposd=MCMC.qBetaOneModel(bdDescendingBetas[b],proposedTreeLikelihood,proposedTreePrior,true);                
                int likelihoodsComparisonResult=qBetaCurrent.compareTo(qBetaProposd);
                if(bdDescendingBetas[b].compareTo(BigDecimal.ZERO)==0)
                    {
                    //trigger always accept if beta is zero
                    likelihoodsComparisonResult=(-1);
                    }                
                boolean proposalAccepted=false;       
                if(likelihoodsComparisonResult==(-1))
                    {
                    //proposal is better : accept unconditionally
                    if(pMethod==MCMC.TREE_PROPOSAL)
                        {
                        proposalManagers[b].logAcceptance(proposalManagers[b].getLastProposalStyleMade(),mcmcTGens[b]);
                        }
                    else
                        {
                        proposalManagers[b].logLCAcceptance();
                        }
                    currents[b]=proposedTree;
                    currentLikelihoods[b]=proposedTreeLikelihood;
                    priors[b]=proposedTreePrior;
                    //overwrite current params with the proposal params
                    currentGenParams[b]=proposedtGenParams;
                    currentLCParams[b]=proposedLCParams;
                    qBetaCurrent=qBetaProposd;
                    proposalAccepted=true;                    
                    }//accept unconditionally
                else
                    {
                    BigDecimal diff=qBetaProposd.subtract(qBetaCurrent);
                    double expDiff=Math.expm1(diff.doubleValue())+1.0;
                    double alpha=Math.min(1,expDiff);
                    double randNum=TreeUtils.getRandNumBet(0, 1, false);
                    if(randNum<=alpha)
                        {
                        if(pMethod==MCMC.TREE_PROPOSAL)
                            {
                            proposalManagers[b].logAcceptance(proposalManagers[b].getLastProposalStyleMade(),mcmcTGens[b]);
                            }
                        else
                            {
                            proposalManagers[b].logLCAcceptance();
                            }
                        currents[b]=proposedTree;
                        currentLikelihoods[b]=proposedTreeLikelihood;
                        priors[b]=proposedTreePrior;
                        //overwrite current params with the proposal params
                        currentGenParams[b]=proposedtGenParams;
                        currentLCParams[b]=proposedLCParams;
                        qBetaCurrent=qBetaProposd;
                        proposalAccepted=true;   
                        }//accept in MH ratio
                    else
                        {
                        if(pMethod==MCMC.TREE_PROPOSAL)
                            {
                            proposalManagers[b].logRejection(proposalManagers[b].getLastProposalStyleMade(),mcmcTGens[b]);
                            }
                        else
                            {
                            proposalManagers[b].logLCRejection();
                            }
                        myLCs[b].setParams(lcParamsBeforeProposal);
                        proposalAccepted=false;
                        }//reject from MH ratio
                    }
                
                //log ML data if acceptance
                if(b==0 && proposalAccepted && currentLikelihoods[b].compareTo(mlValues[b])==1)
                    {
                    //current likelihood is bigger, so save it
                    mlValues[b]=new BigDecimal(currentLikelihoods[b].toString());
                    mlTrees[b]=currents[b].cloneRec();
                    if(mlTrees[b].getPosteriorSampleMap()!=null)
                        {
                        mlPosteriors[b]=TreeUtils.cloneMap(mlTrees[b].getPosteriorSampleMap());
                        HashMap lcMLCloneWork=myLCs[b].cloneParams();
                        for(Object k:lcMLCloneWork.keySet())
                            {
                            mlPosteriors[b].put(k,lcMLCloneWork.get(k));
                            }
                        }
                    }                
                }//for each descending beta
                

            /*
            Once proposals have been made, possibly accepted,
            mlValues recorded, it's time for metropolis coupling
            */
            if(descendingBetas.length>1)
                {
                int randomIndex=TreeUtils.chooseRandomIndex(descendingBetas.length-1);
                int i=randomIndex;
                int j=i+1;
                boolean mcmcmcTest=MCMCMCTest(
                        currentLikelihoods[i],bdDescendingBetas[i],
                        currentLikelihoods[j],bdDescendingBetas[j]);
                if(mcmcmcTest)
                    {
                    //swap trees
                    coa_lineage mcmcmctemptree=currents[i];
                    currents[i]=currents[j];
                    currents[j]=mcmcmctemptree;
                    //swap likelihood calculators/models
                    KHYLikelihoodCalculator mcmcmctempLC=myLCs[i];
                    myLCs[i]=myLCs[j];
                    myLCs[j]=mcmcmctempLC;
                    //swap likelihoods
                    BigDecimal mcmcmcCurrentLikelihoods=currentLikelihoods[i];
                    currentLikelihoods[i]=currentLikelihoods[j];
                    currentLikelihoods[j]=mcmcmcCurrentLikelihoods;
                    logMCMCMCAcceptance(i, j);
                    String swapKey=i+","+j;
                    logSwap(swapKey);
                    }
                else
                    {
                    //chains stay the same!
                    logMCMCMCRejection(i, j);
                    }
                }
            else
                {
                //if there's not more than one chain, then there's no coupling!
                }
            
                
            currentIterationNumber++;  
            }//main MCMCMC iteration loop
         

        //write ML data
        System.out.println("*******************************************");

        //write current data
        System.out.println("Current newick : "+currents[0].makeNewickString(true,fio));
        String currentXMLFile=mlXMLPath+".last.xml";
        try
            {
            coa_lineage lastXMLcoa_lineage=mcmcTGens[0].tagForPhyloXML(currents[0]);
            String lastXML=lastXMLcoa_lineage.makePhyloXML(true,0,false,currentLikelihoods[0]);
            System.out.println("Current XML : "+currentXMLFile);
            TreeUtils.writeStringToFile(lastXML+"\n",currentXMLFile);
            }
        catch(Exception e)
            {
            System.err.println("Failure to write out current XML to "+currentXMLFile);
            e.printStackTrace();
            }
        
        System.out.println("ML value = "+mlValues[0].toString());
        System.out.println("ML newick  = "+mlTrees[0].makeNewickString(true, fio));
        System.out.println("ML posteriors : "+TreeUtils.mapToString(mlPosteriors[0]));        
        
        //write ML XML
        coa_lineage mlTreeForPhyloXML=mcmcTGens[0].tagForPhyloXML(mlTrees[0]);
        String thePhyloXML=mlTreeForPhyloXML.makePhyloXML(true,0,false,mlValues[0]);
        try
            {
            TreeUtils.writeStringToFile(thePhyloXML+"\n",mlXMLPath);
            System.err.println("ML PhyloXML written to "+mlXMLPath);            
            }
        catch(Exception e)
            {
            e.printStackTrace();
            System.err.println("WARNING ! FAILURE to write ML XML to file "+mlXMLPath);
            }        

        
        
        
	for(int b=0;b<descendingBetas.length;b++)
	    {
	    String arTable=proposalManagers[b].getARTable(mcmcTGens[b]);
	    System.out.println("For chain "+(b+1)+", with beta = "+
	            descendingBetas[b]+", the acceptance/rejection data is \n"+arTable);                    
	    }        
        
        
	if(descendingBetas.length>1)
		{
	        /*
	        Print out the acceptance/rejection status
		if MCMCMC was used
	        */
	        
	        System.out.println("MCMCMC Swap Data");
	        System.out.println("Chains/Swap\tAccept\tReject\tTotal\tAcc. Ratio");
	        String[] mcmcmcKeys=MCMCMC.generatePossibleMCMCMCSwapKeys(descendingBetas.length);
	        int aTotal=0;
	        int rTotal=0;
	        int cTotal=0;
	        for(int k=0;k<mcmcmcKeys.length;k++)
	            {
	            if(swapAcceptCounter.containsKey(mcmcmcKeys[k]))
	                {
	                aTotal+=(int)this.swapAcceptCounter.get(mcmcmcKeys[k]);
	                }
	            if(swapRejectCounter.containsKey(mcmcmcKeys[k]))
	                {
	                rTotal+=(int)this.swapRejectCounter.get(mcmcmcKeys[k]);
	                }
	            }
	        cTotal=cTotal+aTotal+rTotal;
	        for(int k=0;k<mcmcmcKeys.length;k++)
			{
	            int aCount=0;
	            if(swapAcceptCounter.containsKey(mcmcmcKeys[k]))
	                {
	                aCount=(int)this.swapAcceptCounter.get(mcmcmcKeys[k]);
	                }
	            int rCount=0;
	            if(swapRejectCounter.containsKey(mcmcmcKeys[k]))
	                {
	                rCount=(int)this.swapRejectCounter.get(mcmcmcKeys[k]);
        	        }
	            int tCount=aCount+rCount;
	            double aRatio;
        	    if(tCount==0 || aCount==0)
	                {
	                aRatio=0.0;
        	        }
	            else
	                {
	                aRatio=((double)(aCount)/(double)(tCount))*100.0;
	                }
	            //double aPDF=((double)(aCount)/(double)(aTotal))*100.0;
	            //double rPDF=((double)(rCount)/(double)(rTotal))*100.0;
	            //double tPDF=((double)(tCount)/(double)(cTotal))*100.0;
	            System.out.println(
	                mcmcmcKeys[k]+"\t"+
	                aCount+"\t"+
        	        rCount+"\t"+
	                tCount+"\t"+
        	        aRatio
                	//aPDF+"\t"+
	                //rPDF+"\t"+
        	        //tPDF
                	);
            		}
 
		}//write swap info if more than one chain!
        
        

        }
    

    

    
    public static boolean MCMCMCTest(
            BigDecimal likei,BigDecimal betai,
            BigDecimal likej,BigDecimal betaj
            )
        {
        /*
         * see Mol. Evol : A Statistical Approach, eq 7.59
            Beta=1/T ; pg 247
         */
            
        if(betai.compareTo(betaj)==0)
            {
            return true;
            }
        
            
        BigDecimal jiRatio=likej.subtract(likei);
        BigDecimal betaijDiff=betai.subtract(betaj);
        BigDecimal prodfirst=new BigDecimal(jiRatio.doubleValue()*betaijDiff.doubleValue());
        double expFirst=Math.expm1(prodfirst.doubleValue())+1.0;
        /*BigDecimal ijRatio=likei.subtract(likej);
        BigDecimal betajiDiff=betaj.subtract(betai);
        BigDecimal prodsecond=new BigDecimal(ijRatio.doubleValue()*betajiDiff.doubleValue());*/

        double alpha1=Math.min(1,expFirst);
        //double alpha2=Math.min(1,prodsecond.doubleValue());
        //double alpha2=alpha1;

        double randNum1=TreeUtils.getRandNumBet(0, 1, false);
        double randNum2=TreeUtils.getRandNumBet(0, 1, false);
        /*System.out.println("In MCMCMCTest");
        System.out.println("i="+i+" and j="+j);
        System.out.println("like i is "+likei);
        System.out.println("betai is "+betai);
        System.out.println("likej is "+likej);
        System.out.println("betaj is "+betaj);
        System.out.println("ji ratio is "+jiRatio);
        System.out.println("betaijDiff is "+betaijDiff);
        System.out.println("prodfirst is "+prodfirst);
        System.out.println("ijRatio is "+ijRatio);
        System.out.println("betajiDiff is "+betajiDiff);
        System.out.println("prodsecond is "+prodsecond);        
        System.out.println("alpha1="+alpha1);
        System.out.println("alpha2="+alpha2);        
        System.out.println("rn1 is "+randNum1);
        System.out.println("rn2 is "+randNum2);*/
        
        if(randNum1<=alpha1 && randNum2<=alpha1)
            {
            //System.out.println("Do swap!\n\n");
            return true;
            }
        else
            {
            //System.out.println("Fail to swap\n\n");
            return false;
            }

        }
    
    
    public static double[] makeStandardBetas(double mainBeta)
        {
        String step;
        int numFactors=0;
        if(mainBeta<=0.25)
            {
            step="0.025";
            numFactors=4;
            }
        else if(mainBeta<=0.5)
            {
            step="0.05";
            numFactors=4;
            }
        else
            {
            step="0.2";
            numFactors=4;
            }
        double[] betaFactors=makeBetaFactors(numFactors,new BigDecimal(step));
        double[] betas=getBetasFromFactors(mainBeta,betaFactors);
        return betas;
        }

    
    public static boolean testIfFirstIs1PointZeroAndIfIsSorted(double[] betaFactors)
        {
        if(betaFactors==null)
            {
            return false;
            }
        else
            {
            if(betaFactors.length==0)
                {
                return false;
                }
            else if(betaFactors[0]==1.0)
                {
                for(int b=1;b<betaFactors.length;b++)
                    {
                    if(betaFactors[b]>betaFactors[b-1])
                        {
                        //not sorted!
                        return false;
                        }
                    }
                return true;
                }
            else
                {
                System.err.println("First isn't 1 it's "+betaFactors[0]);
                return false;
                }                
            }
        }
    

    public static double[] getAutoBetaFactors(double mainBeta)
        {
        double[] autoBetas;
        String envBetaFactors=System.getenv("COMET_BETA_FACTORS");
        if(envBetaFactors==null)
            {
            autoBetas=MCMCMC.makeStandardBetas(mainBeta);
            }
        else
            {
            try
                {
                String[] pieces=envBetaFactors.split(",");
                if(pieces.length==0)
                    {
                    autoBetas=MCMCMC.makeStandardBetas(mainBeta);
                    }
                else
                    {
                    double[] betaFromEnv=new double[pieces.length];
                    for(int b=0;b<betaFromEnv.length;b++)
                        {
                        betaFromEnv[b]=Double.parseDouble(pieces[b].trim());
                        }
                    boolean betasFromEnvAreOkay=testIfFirstIs1PointZeroAndIfIsSorted(betaFromEnv);
                    if(betasFromEnvAreOkay)
                        {
                        for(int b=0;b<betaFromEnv.length;b++)
                            {
                            betaFromEnv[b]=Double.parseDouble(pieces[b].trim())*mainBeta;
                            }                            
                        System.out.println("Picked up COMET_BETA_FACTORS : "+envBetaFactors);
                        autoBetas=betaFromEnv;
                        System.out.println("Computed auto betas : "+Arrays.toString(autoBetas));
                        }
                    else
                        {
                        System.err.println("Picked up COMET_BETA_FACTORS "+envBetaFactors);
                        System.err.println("Beta factors from environment should have first factor as 1.0 and be non-increasing if more than one factor!");
                        autoBetas=MCMCMC.makeStandardBetas(mainBeta);
                        System.err.println("Using autoBetas : "+Arrays.toString(autoBetas));
                        }
                    }
                }
            catch(Exception e)
                {
                System.err.println("Error in reading betas from "+envBetaFactors+"...");
                autoBetas=MCMCMC.makeStandardBetas(mainBeta);
                System.err.println("To use as beta factors : "+Arrays.toString(autoBetas));
                }
            }
        return autoBetas;
        }
        
    
    
    public static double[] makeBetaFactors(int numFactors,BigDecimal factorStep)
        {
        ArrayList<BigDecimal> factors=new ArrayList<BigDecimal>();
        BigDecimal startFactor=new BigDecimal("1.0");
        BigDecimal currentFactor=startFactor;
        for(int i=0;i<numFactors;i++)
            {
            factors.add(currentFactor);
            currentFactor=currentFactor.subtract(factorStep);
            }
        double[] factorsToReturn=new double[factors.size()];
        for(int i=0;i<factorsToReturn.length;i++)
            {
            factorsToReturn[i]=factors.get(i).doubleValue();
            }
        return factorsToReturn;
        }
    
    public static double[] getBetasFromFactors(double mainBeta,double[] factors)
        {
        double[] betas=new double[factors.length];
        for(int b=0;b<betas.length;b++)
            {
            betas[b]=mainBeta*factors[b];
            }
        return betas;        
        }
    
    
    public MCMCMC(double[] betaVals,
            String modelName,
            FastaIO fastaIO,
            double minKappa,
            double maxKappa,
            long maxIter,
            long recordingFreq,
            String baseLogTracePath,
            String phyloXMLStartFileOfFiles,
            Double initMaxNumHoursToRun,
            Double initMinESSScoreRequired            
            )
        {
            
        if(initMaxNumHoursToRun!=null)
            {
            numHoursToRun=initMaxNumHoursToRun;
            }
        if(initMinESSScoreRequired!=null)
            {
            minESSValue=initMinESSScoreRequired;
            }
	

        //sort beta vals so that beta at 0-index is highest
        Arrays.sort(betaVals);
        double[] descendingBetas=new double[betaVals.length];
        BigDecimal[] bdDescendingBetas=new BigDecimal[betaVals.length];
        for(int b=0;b<betaVals.length;b++)
            {
            descendingBetas[b]=betaVals[betaVals.length-b-1];
            bdDescendingBetas[b]=new BigDecimal(""+descendingBetas[b]+"");
            }
        
        //init num_lineages
        int num_lineages=fastaIO.getNumSeqs();
        double nminpsi=0.05;
        double nmaxpsi=0.95;        

        
        KHYLikelihoodCalculator[] myLCs=new KHYLikelihoodCalculator[betaVals.length];
        ProposalManager[] proposalManagers=new ProposalManager[betaVals.length];
        HashMap lpMap=makeInitialLPMap(fastaIO);
        TreeGenerator[] mcmcTGens=new TreeGenerator[betaVals.length];
        String[] logTracePaths=new String[betaVals.length];
        String[] tracerLogPaths=new String[betaVals.length];
        String mlXMLFile=null;
        for(int c=0;c<descendingBetas.length;c++)
            {
            String mcmcmcSuffix;
            if(c==0)
                {
                mcmcmcSuffix="";
                }
            else
                {
                mcmcmcSuffix="_warm";
                }
            logTracePaths[c]=baseLogTracePath+".mcmcmc_chain_"+
                    (c+1)+"_mainbeta_"+descendingBetas[0]+
                    mcmcmcSuffix+"."+bdDescendingBetas[c].toPlainString().trim()+".log";
            tracerLogPaths[c]=logTracePaths[c]+".tracer.log";
            if(c==0)
                {
                mlXMLFile=logTracePaths[c]+".xml";
                System.out.println("To write XML ML tree to "+mlXMLFile);
                }
            System.out.println("For beta="+descendingBetas[c]+" to write log to "+logTracePaths[c]);
            System.out.println("For beta="+descendingBetas[c]+" to write tracer-ready log to "+tracerLogPaths[c]);
            proposalManagers[c]=new ProposalManager("0.0,0.0,0.0,1.00",fastaIO);
            myLCs[c]=new KHYLikelihoodCalculator(lpMap,minKappa,maxKappa);
            double maxDistDueToTransitionProbabilityMatrix=KHYLikelihoodCalculator.
                    getMaxDistUsingMinMaxKappaAndFreqs(
                            minKappa,
                            maxKappa,
                            TreeUtils.strFreqsToDoubleArray(lpMap.get("pi_freqs").toString())
                        );
            double minDistDueToTransitionProbabilityMatrix=KHYLikelihoodCalculator.
                    getMinDistUsingMinMaxKappaAndFreqs(
                            minKappa,
                            maxKappa,
                            TreeUtils.strFreqsToDoubleArray(lpMap.get("pi_freqs").toString())
                        );
            if(modelName!=null &&    modelName.equalsIgnoreCase("KING"))
                {
                KingmanCoalescent myKingmanGenerator=new KingmanCoalescent(num_lineages);
                myKingmanGenerator.setMaxDistanceFromProbMatrix(maxDistDueToTransitionProbabilityMatrix);
                myKingmanGenerator.setMinDistanceFromProbMatrix(minDistDueToTransitionProbabilityMatrix);
                mcmcTGens[c]=myKingmanGenerator;
                }
            else if(modelName!=null && modelName.equalsIgnoreCase("SWA1"))
                {
                String doAddBLLike=System.getenv("COMET_SWA1_ADDBLL");
                boolean addBranchLogLike=false;
                if(doAddBLLike!=null)
                    {
                    //env var is set!
                    addBranchLogLike=true;
                    }
                SWPaperAlgOneGenerator myAlgOneGenerator=new SWPaperAlgOneGenerator(num_lineages,nminpsi,nmaxpsi,addBranchLogLike);
                myAlgOneGenerator.setMaxDistanceFromProbMatrix(maxDistDueToTransitionProbabilityMatrix);
                myAlgOneGenerator.setMinDistanceFromProbMatrix(minDistDueToTransitionProbabilityMatrix);
                mcmcTGens[c]=myAlgOneGenerator;     
                }
            else if(modelName!=null && modelName.equalsIgnoreCase("SWA2"))
                {
                SWPaperAlgTwoGenerator myAlgTwoGenerator=new SWPaperAlgTwoGenerator(num_lineages,nminpsi,nmaxpsi);
                mcmcTGens[c]=myAlgTwoGenerator;   
                }
            mcmcTGens[c].setFasta(fastaIO);
            }
        
        //launch MCMCMC !
        prepMCMCMC(
            mcmcTGens,        
            myLCs,
            proposalManagers,
            logTracePaths,
            tracerLogPaths,
            phyloXMLStartFileOfFiles,
            descendingBetas,
            bdDescendingBetas,
            fastaIO,
            maxIter,
            recordingFreq,
            mlXMLFile
            );
            
        }
    
    
    
}
