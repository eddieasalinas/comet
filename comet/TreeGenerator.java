/*
COMET CODE
COPYRIGHT 2015, EDWARD A. SALINAS
 */
package comet;


import java.math.BigDecimal;
import java.util.HashMap;

/**
 *
 * @author esalina
 */
public interface TreeGenerator {

    
    public double getBeta();
    public void setBeta(double b);
    public HashMap getParams();
    public HashMap cloneParams();
    public void setParams(HashMap paramMap);
    public Object[] generateTree();
    public void proposeParams();
    //public BigDecimal getPrior();
    public Object[] proposeTree(coa_lineage current,BigDecimal cPrior);
    public int getLastProposalCode();
    public String getProposalDescription(int code);    
    //public double computePriordouble(coa_lineage current);
    public void setFasta(FastaIO fio);
    public coa_lineage tagForPhyloXML(coa_lineage root);
    public double estThetaHatIP278(coa_lineage current);

}





