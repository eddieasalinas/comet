RELEASE_LINK=$(echo "comet.`git describe`.zip") 
DIST_DIR=$(echo "comet.`git describe`")

#default to the comet.jar
all: release

release: comet_release.zip  $(RELEASE_LINK) cleanclasses comet.jar
	mv -vf comet_release.zip $(value RELEASE_LINK) 

comet_release.zip: comet.jar
	rm -vrf $(value DIST_DIR) ;
	mkdir -v $(value DIST_DIR) ;
	ln -vs ../lib $(value DIST_DIR)
	ln -vs ../comet.jar $(value DIST_DIR)
	ln -vs ../scripts $(value DIST_DIR)
	cp -v README $(value DIST_DIR)
	zip -r comet_release.zip $(value DIST_DIR)
	#rm -v comet.jar
	rm -vrf $(value DIST_DIR) ;

#the main binary
comet.jar: classes MANIFEST.MF
	jar cvfm comet.jar MANIFEST.MF comet/*.class

#the Manifest to indicate the libs for running and the manifest
#assumes all jars in the directory called lib
MANIFEST.MF:
	echo "Manifest-Version: 1.0\nClass-Path: lib/commons-math3-3.3.jar lib/biojava3-core-3.0.7.jar lib/BigDecimalMath.jar lib/json.jar lib/forester.jar lib/pal.jar\nMain-Class: comet.Comet\n" > MANIFEST.MF
	
#for compiling
classes:
	#assumes all jars needed are in the directory called lib
	#assumes a compatible JAVA is in the PATH !
	javac -cp `find lib/*.jar|tr "\n" ":"` comet/*.java

cleanclasses:
	rm -vf comet/*.class

#for deleting temp files
clean:	cleanclasses
	rm -vf MANIFEST.MF comet/*.class comet.jar comet_release.zip
	rm -vf $(value RELEASE_LINK)
