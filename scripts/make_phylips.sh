#!/bin/bash

for TREE in `find *.newick`;
	do
	echo "GOT TREE FILE $TREE" ;
	seq-gen  -l 20 -mHKY -t 1 < $TREE | tee $TREE.phylip | phylip_to_fasta.pl > $TREE.phylip.fasta
	done ;


