#!/usr/bin/env python
import glob
from comet_utils import extractAsItemOrFirstFromList
from ti_like_multi import obtainBetaFromLogPath
import argparse
import math
import re
from numpy import *
import numpy
import os


#def getAutoCorrelationTime():
#	#corr(firstN-1 obs, and the next N-1 obs)

        
        
#for var IHat see Mol. Evol: A Statistical Approach, eq 6.86        
def getVarIHat(vals,log_space=True):
	if(log_space):
		#values are in log space
		s=0.0
		v_reg=list()
	 	for v in vals:
			v_reg.append(numpy.expm1(v)+1.0)
                #v_reg now in real space
		for v in v_reg:
			s=s+(v_reg)
		I_hat=(1.0/float(len(vals)))*s
                #for I_hat see Mol. Evol: A Statistical Approach, eq 6.85
		sh=0.0
		for v in v_reg:
			sh=sh+((v-I_hat)*(v-I_hat))
		num=sh
		denom=len(v_reg)*(len(v_reg)-1)
		varIHat=num/denom
                #for var IHat see Mol. Evol: A Statistical Approach, eq 6.86
		return varIHat
	else:
		#require data to be in log space!
		assert(log_space)
			
	
def getCorrelationAtLag(y,k):
	#http://www.itl.nist.gov/div898/handbook/eda/section3/eda35c.htm
	#Box, G. E. P., and Jenkins, G. (1976), Time Series Analysis: Forecasting and Control, Holden-Day.
	#y_sum=0.0
	#for x in y:
	#	#print "x is ",x," and its float is ",float(x)
	#	y_sum=y_sum+float(x)
	#y_mean=y_sum/float(len(y))
	#numerator_sum=0.0
	#for i in range(len(y)-k):
	#	numerator_sum=numerator_sum+(float(y[i])-y_mean)*(float(y[i+k])-y_mean)
	#denominator_sum=0.0
	#for i in range(len(y)):
	#	factor=(float(y[i])-y_mean)
	#	denominator_sum=denominator_sum+(factor*factor)
	#coeff=numerator_sum/denominator_sum
	#print "The coeff at lag=",k," is ",coeff	
	#return coeff
	y_reg=list()
	y_lag=list()
	#assert(k<len(y))
	for i in range(len(y)-k):
		y_reg.append(float(y[i]))
		y_lag.append(float(y[i+k]))
	coeff=corrcoef(y_reg,y_lag)[1,0]
	#print "The coeff at lag=",k," is ",coeff
	return coeff
	
		
	
	
def getMaxLagPossible(y):
	max_lag=len(y)-1
	return max_lag


def getAutoCorrelationTime(y):
	max_lag=getMaxLagPossible(y)
	#print "The max lag is ",max_lag
	p_list=list()
	for lag in range(0,max_lag):
		p=getCorrelationAtLag(y,lag)
		p_list.append(p)
	p_sum=sum(p_list)
	ACT=1.0+2.0*p_sum
	return ACT
		


#given a *current likelihood and a *proposed likelihood
#compute whether the proposed one is accepted!
def mh_test(like_curr,like_pro):
	if(like_pro>like_curr):
		#accept unconditionally if proposed is greater than current
		return True
	else:
		#accept according to MH ratio
		#assume input is in log-space
		log_diff=like_pro-like_curr
		#convert to real space
		exp_diff=numpy.expm1(log_diff)+1.0
		#compute alpha per
		#Mol. Evol. : A Statistical Approach : Yang Z., pg 215  eq 7.5
		alpha=min(1,exp_diff)
		#generate a random number
		rand_num=numpy.random.uniform(low=0.0, high=1.0, size=1)[0]
		#print "log_diff is ",log_diff
		#print "exp_diff is ",exp_diff
		#print "alpha is ",alpha
		#print "rand_num is ",rand_num
		if(rand_num<=alpha):
			#print "Accept"
			return True
		else:
			#print "Reject"
			return False


def multi_mcmcmc_test(like_i,beta_i,like_j,beta_j,num_tests):
	for t in range(num_tests):
		test_result=mcmcmc_test(like_i,beta_i,like_j,beta_j)
		if(test_result):
			return True
	return False



def mcmcmc_test(like_i,beta_i,like_j,beta_j):
	jiRatio=like_i-like_j
	betaijDiff=beta_i-beta_j
	prodfirst=jiRatio*betaijDiff
	exp_first==numpy.expm1(prodfirst)+1.0
	alpha_1=min(1,exp_first)
	rand_num_1=numpy.random.uniform(low=0.0, high=1.0, size=1)[0]
	rand_num_2=numpy.random.uniform(low=0.0, high=1.0, size=1)[0]
	if(rand_num_1<=alpha_1 and rand_num_2<=alpha_1):
		#swap!
		return True
	else:
		return False

#test if string looks like an integer with a regex
def strIsInt(s):
	intRE=re.compile('^\d+$')
	matchResult=intRE.match(s)
	return matchResult

#class for trace data
class mcmc_trace:


	def stateDict(self,state):
		if(not(state in self.state_to_data_dict)):
			return None
		t_dict=dict()
		specific_dict=self.state_to_data_dict[state]
		for sk in specific_dict:
			t_dict[sk]=specific_dict[sk]
		return t_dict

	def getValue(self,state,col_header):
		assert(state in self.state_to_data_dict)
		specific_dict=self.state_to_data_dict[state]
		assert(col_header in specific_dict)
		specific_val=specific_dict[col_header]
		return specific_val
            
	
	def is_other_state_list_same(self,other_state_list):
		if(len(self.state_list)==len(other_state_list)):
			#good
			for i in range(len(self.state_list)):
				if(self.state_list[i]==other_state_list[i]):
					#good
					pass
				else:
					return False
			#found no differences!
			return True
		else:
			#different lengths!
			return False


	def chop_first_and_last(self):
		#drop the first and last states in the chain
		new_state_list=list()
		new_state_to_data_dict=dict()
		num_states=len(self.state_list)
		if(num_states<=3):
			#this will throw away all the states!
			assert(num_states>=3)
		else:
			for i in range(len(self.state_list)):
				if(i==0 or i==len(self.state_list)-1):
					pass
				else:
					new_state_list.append(self.state_list[i])
					new_state_to_data_dict[self.state_list[i]]=self.state_to_data_dict[self.state_list[i]]
		self.state_list=new_state_list
		self.state_to_data_dict=new_state_to_data_dict
		


	def load_state_step(self):
		diffs_observed=dict()
		a_diff=None
		min_state=self.state_list[0]
		max_state=self.state_list[len(self.state_list)-1]
		for s_idx in range(len(self.state_list)-1):
			if(self.state_list[s_idx]<min_state):
				min_state=self.state_list[s_idx]
			if(self.state_list[s_idx]>max_state):
				max_state=self.state_list[s_idx]
			diff=self.state_list[s_idx+1]-self.state_list[s_idx]
			a_diff=diff
			if(a_diff in diffs_observed):
				diffs_observed[a_diff]=diffs_observed[a_diff]+1
			else:
				diffs_observed[a_diff]=1
		#print "Diffs observed : ",diffs_observed
		assert(a_diff is not None)
		most_observed_diff=a_diff
		for d in diffs_observed:
			if(diffs_observed[d]>diffs_observed[most_observed_diff]):
				most_observed_diff=d		
		self.state_step=most_observed_diff
		self.min_state=min_state
		self.max_state=max_state
		#print "start, stop, step are ",self.min_state," ",self.max_state," and ",self.state_step

	def getStateInfo(self,info_str):
		if(info_str=="min"):
			return self.min_state
		elif(info_str=="max"):
			return self.max_state
		elif(info_str=="step"):
			return self.state_step
		else:
			assert (info_str in ["min","max","step"])


	def printChain(self,output_path,abbreviated):
		w=open(output_path,'w')
		for h in range(len(self.header_lines)):
			w.write(self.header_lines[h]+"\n")
		if(abbreviated):
			w.write("State\tLikelihood\n")
			for s in range(len(self.state_list)):
				w.write(str(self.state_list[s])+"\t"+self.getValue(self.state_list[s],"Likelihood")+"\n")
		else:
			w.write("\t".join(self.column_headers)+"\n")
			for s in range(len(self.state_list)):
				list_to_print=list()
				for h in range(len(self.column_headers)):
					header_str=self.column_headers[h]
					value_str=self.getValue(self.state_list[s],header_str)
					list_to_print.append(value_str)
				w.write("\t".join(list_to_print)+"\n")
	

		
	def get_state_list(self):
		return self.state_list


	def __init__(self,new_header_lines,new_col_headers,new_beta,new_state_list,new_state_dict):
		self.header_lines=new_header_lines
		self.beta_value=new_beta
		self.state_list=list()
		self.state_list.append(new_state_list)
		self.state_to_data_dict=dict()
		self.column_headers=new_col_headers #to preserve ordering
		self.source_file=None

	def addToUnFinalized(self,new_state,new_dict):
		self.state_list.append(new_state)
		self.state_to_data_dict[new_state]=new_dict

                
	def finalize(self):
		self.load_state_step()


	def get_beta(self):
		return self.beta_value

	def get_source_file(self):
		if(self.source_file):
			return self.source_file
		else:
			return None

	def report(self,col_name,burnin=0):
		vals=list()
		for s in self.state_list:
			if(s>=burnin):
				specific_dict=self.state_to_data_dict[s]
				val=specific_dict[col_name]
				vals.append(val)
		print "want act of ",vals
		act=getAutoCorrelationTime(vals)
		print "For burnin=",burnin,"  val=",col_name," the ACT is ",act


	
	def get_header_lines(self):
		return self.header_lines


	def get_col_headers(self):
		return self.column_headers

        #read chain data from log file
	def __init__(self, in_file,clean_up=True):
		self.source_file=in_file
		beta_value=obtainBetaFromLogPath(in_file,False)
		if(beta_value==(-1)):
			beta_value=obtainBetaFromLogPath(in_file,True)
		assert (0<=beta_value and beta_value<=1.0)
		print  "Loading file",in_file,"at beta =",beta_value
		tracer_reader=open(in_file)
		header_lines=list()
		column_headers=list()
		column_headers_dict=dict()
		state_to_data_dict=dict()
		trace_data=list()
		state_list=list()
		got_columns=False
		num_columns=None
		for line in tracer_reader:
			temp_line=line.strip()
			#print "Got temp_line ",temp_line
			if(temp_line.startswith("#")):
				#is a header
				header_lines.append(temp_line)
			else:
				if(not(got_columns)):
					#first non-# line must be column headers
					col_vals=temp_line.split('\t')
					for c in range(len(col_vals)):
						column_headers.append(col_vals[c])
						column_headers_dict[col_vals[c]]=1
					got_columns=True
					num_columns=len(col_vals)
					#make sure first col_name is 'State' and there are at least two cols
					assert num_columns>=2
					assert column_headers[0]=="State"
					
				else:
					#non-# lines if got columns must be trace_data
					assert num_columns is not None
					this_row_pieces=temp_line.split('\t')
					this_row_num_pieces=len(this_row_pieces)
					if(this_row_num_pieces!=num_columns):
						#best way to deal with column length mis-match?
						print "col headers : ",col_vals
						print "row vals : ",this_row_pieces
						assert this_row_num_pieces==num_columns
					else:
						#dictionary for this row
						this_row_dict=dict()
						for p in range(len(this_row_pieces)):
							this_row_dict[column_headers[p]]=this_row_pieces[p]
						#assert first piece is an integer that is State
						assert strIsInt(this_row_pieces[0])
						#here now save the data!
						State=int(this_row_pieces[0])
						state_to_data_dict[State]=this_row_dict
						state_list.append(State)
		tracer_reader.close()
		#make sure there are at least two states!
		assert(len(state_list)>=2)

		#save to the object
		self.column_headers=column_headers
		self.state_list=state_list
		self.state_to_data_dict=state_to_data_dict
		self.beta_value=beta_value
		self.header_lines=header_lines

		#clean up chain?
		if(clean_up):
			self.load_state_step()


						

def post_mcmc_mcmcmc(files_to_use,out_file="/dev/stdout"):
	first_trace=mcmc_trace(files_to_use[0])
	first_trace_state_list=first_trace.get_state_list()
	grab_from_chain_list=list()
	#first compose a new chain by choosing states from among the chains
	for s_idx in range(len(first_trace_state_list)):
		if(s_idx==0):
			#new chain starts with data from first state
			grab_from_chain_list.append(0)
		else:
			#since betas are equal, the MCMCMC test is always true.
			#The value in the exponent is zero.  Anything to the zero power is 1
			new_idx=numpy.random.choice(len(files_to_use), 1)[0]
			grab_from_chain_list.append(new_idx)
	#second, extract the values and build a new chain
	mcmc_chain_objs=list()
	for i in range(len(files_to_use)):
		mcmc_chain_objs.append(mcmc_trace(files_to_use[i]))
	state_to_data_dict=dict()
	for s_idx in range(len(grab_from_chain_list)):
		chosen_chain=grab_from_chain_list[s_idx]
		chosen_chain_state_list=mcmc_chain_objs[chosen_chain].get_state_list()
		chosen_state=chosen_chain_state_list[s_idx]
		state_to_data_dict[chosen_state]=mcmc_chain_objs[chosen_chain].stateDict(chosen_state)
	#third, write out the new chain!
	new_header_lines=first_trace.get_header_lines()
	new_col_headers =first_trace.get_col_headers()
	new_beta=first_trace.get_beta()

	#write out headers
	writer=open(out_file,'w')
	for h in range(len(new_header_lines)):
		writer.write(new_header_lines[h].strip()+"\n")
	#writer out col headers
	joined_col_headers="\t".join(new_col_headers)
	writer.write(joined_col_headers.strip()+"\n")
	#write out data!
	for s_idx in range(len(first_trace_state_list)):
		specific_dict=state_to_data_dict[first_trace_state_list[s_idx]]
		ordered_data=list()
		for c in range(len(new_col_headers)):
			ordered_data.append(specific_dict[new_col_headers[c]])
		joined_ordered_data="\t".join(ordered_data)
		writer.write(joined_ordered_data+"\n")
	writer.close()
	
	

def interleaveTraces(files_to_use,out_file="/dev/stdout"):
	#first find the maximal state value
	highest_state=0
	lowest_state=0
	traces=list()
	keys_to_use=None
	state_step=None
	for f in files_to_use:
		#get the states available
		trace=mcmc_trace(f)
		traces.append(trace)
		state_list=trace.get_state_list()
		#print "A state list is ",state_list
		highest_state_this_trace=max(state_list)
		lowest_state_this_trace=min(state_list)
		#print "The highest state for this trace is ",highest_state_this_trace
		highest_state=max(highest_state,highest_state_this_trace)
		lowest_state=highest_state+1
		lowest_state=min(lowest_state,lowest_state_this_trace)
		if(keys_to_use is None):
			keys_to_use=5
			state_to_data_dict=trace.stateDict(lowest_state_this_trace)
			print "\n\n\nstate_to_data_dict lowest trace first chain examined : "+str(state_to_data_dict)+"\n\n\n\n"
			keys_to_use=list()
			for k in state_to_data_dict:
				if(not(k=="State")):
					keys_to_use.append(k)
			keys_to_use.sort()
			state_step=trace.state_step
			print "the state_step retrieved is ",state_step
			
			


	#now with the highest state retrieved and also the lowest state
	#iterate over the range and then for each iteration, grabe any value
	#found in the traces.  Write out the header first
	writer=open(out_file,'w')
	new_header_lines=traces[0].get_header_lines()
	for h in range(len(new_header_lines)):
		writer.write(new_header_lines[h].strip()+" ; interleaved_output\n")
	output_state=lowest_state
	printedDataHeaderYet=False
	firstDataOutput=True
	for state in range(lowest_state,highest_state+1):
		for trace in traces:
			state_to_data_dict=trace.stateDict(state)
			if(state_to_data_dict is not None):
				#use the data and increment the output state
				if(not(printedDataHeaderYet)):
					writer.write("State\t"+"\t".join(keys_to_use)+"\n")
					printedDataHeaderYet=True
				writer.write(str(output_state)+"\t")
				data_to_write=list()
				for k in keys_to_use:
					data_to_write.append(state_to_data_dict[k])
				writer.write("\t".join(data_to_write))
				writer.write("\n")
				if(firstDataOutput):
					output_state=output_state+state_step-1
					firstDataOutput=False
				else:
					output_state=output_state+state_step				
			else:
				pass



if (__name__=="__main__"):
	parser=argparse.ArgumentParser(description='Read log files at beta levels for MCMCMC')
	parser.add_argument('-interleave',help="force interleaved output for input logs at highest beta",action="store_true")
	parser.add_argument('out_file',type=str,nargs=1,help="(non-existent) output directory for MCMCMC trace files")
	parser.add_argument('trace_files',type=str,nargs="+",help="input trace files")
	args=parser.parse_args()
	if(args):
		interleave=args.interleave
		print "The status of interleave is ",interleave
		output_file=args.out_file
		print "To write to ",output_file[0]
		trace_files=args.trace_files
		print "Got trace_files :",trace_files
		highest_beta=None
		for tf in trace_files:
			tf_beta=obtainBetaFromLogPath(tf,True)
			assert(0.0<=tf_beta and tf_beta<=1.0)
			if(highest_beta is None):
				highest_beta=tf_beta
			else:
				if(tf_beta>highest_beta):
					highest_beta=tf_beta
		print "Highest beta detected : ",highest_beta
		files_in_use=list()
		for tf in trace_files:
			this_beta=obtainBetaFromLogPath(tf,True)
			if(this_beta==highest_beta):
				#keep
				files_in_use.append(tf)
			else:
				#skip!
				print "***WARNING*** skipping file ",tf," because its beta is too low!"
		print "Files to use : ",files_in_use
		if(len(files_in_use)<=1):
			print "To few files! Cannot MCMCMC ! Not enough chains!"
		else:
			if(not(interleave)):
				post_mcmc_mcmcmc(files_in_use,output_file[0])
			else:
				interleaveTraces(files_in_use,output_file[0])
	else:	
		parser.print_help()
















