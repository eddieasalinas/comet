#!/usr/bin/perl

sub ltrim { my $s = shift; $s =~ s/^\s+//;       return $s };
sub rtrim { my $s = shift; $s =~ s/\s+$//;       return $s };
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

my $line_num=0;
my @lines=();
my $seq_hash={}; 
# 5 40
#4         ACAGTGTAGCAGAGTGCGTATACTGACGAGGGCTCTGAGT
#5         CCAGTGTTGAGGACTGTGTATACAGTAGACACCGAGGAAT
#3         CCAGTGTTGTGGACTGTGTATACAGTAGACACCGAGGAGT
#1         CCAGTGTTGTGGACTGTGTATACAGTAGACACCGAGGAGA
#2         CCAGTGTTGTGGACTGTGTATACAGTAGACACCGAGGAGA
my $num_seqs=0;
my $num_sites=0;
my $line="";
while($line=<STDIN>)
	{
	#print "got $line";
	$t_line=trim($line);
	#print "t_line is $t_line\n";
	if($line_num>0)
		{
		if($num_seqs<=0 || $num_sites<=0)
			{
			print STDERR "Warning ! Error in acquiring num seqences and num sites !\n";
			}
		if($t_line=~m/^\s*(\S+)\s+(\S+)\s*$/)
			{
			my $seq_nm=$1;
			my $seq_dt=$2;
			#print "Got seq name $seq_nm and seq_data $seq_dt\n";
			if(exists $seq_hash->{$seq_nm})
				{
				#print "NON-FIRST!\n";
				$seq_hash->{$seq_nm}.=$seq_dt;
				}
			else
				{
				$seq_hash->{$seq_nm}=$seq_dt;
				}			
			}
		}
	else
		{
		if($t_line=~m/^\s*(\d+)\s+(\d+)\s*$/)
			{
			$num_seqs=$1;
			$num_sites=$2;
			#print "read num_seqs=$num_seqs and num_sites=$num_sites\n";
			}
		}

	$line_num++;
	}
	
my $count_seqs=0;
my $count_len=0;
for (keys %$seq_hash)
    {
    my $m_key=$_;
    #    delete $hash_ref1->{$_};
    print ">".$m_key."\n";
    print $seq_hash->{$m_key}."\n";
    $count_seqs++;
    $count_len=length($seq_hash->{$m_key});
    if($count_len!=$num_sites)
	{
	print STDERR "WARNING ! this sequence's length ($count_len) not equal to the delcared sequence length ($num_sites) !\n";
	}
    }
if($count_seqs!=$num_seqs)
	{
	print STDERR "WARNING ! Declared number of seqs ($num_seqs) not equal to actual number of seqs ($count_seqs) !\n";
	}






