#!/usr/bin/perl

#get the directory
use File::Basename;

sub computeAvgLogLike()
	{
	my $inFile=shift;
	print "i NEED TO COMPTUE ALL from $inFile\n";
	open(READER,"<$inFile") or die "ERROR, FAIL TO OPEN FILE $inFile FOR COMPUTING AVG. LOG LIKELIHOOD!\n";
	my $sum=0.0;
	my $num=0;
	while(<READER>)
		{
		my $line=$_;
		chomp($line);
		if(!($line=~m/^#/))
			{
			if(!($line=~m/state/i))
				{
				my @pieces=split('\t',$line);
				if(scalar(@pieces)>2)
					{
					my $ll=$pieces[1];
					if($ll=~m/^\s*(\-[0-9\.]+)\s*/)
						{
						#print "READ LOG LIKE $ll from $inFile\n";
						$sum+=$ll;
						$num++;
						}
					}
				}
			}
		}
	close(READER);
	my $all=$sum/$num;
	return $all;
	}


sub max ($$) { $_[$_[0] < $_[1]] }
sub min ($$) { $_[$_[0] > $_[1]] }
#(min/max) from http://www.perlmonks.org/?node_id=406883

#get the BETA 0 file
my $inFileBase=$ARGV[0];

my $usage="<script> INFILE\nLook for BETA 0 trace file (full path required).  Find similarly name *.BETA.log files in the same directory.  Perform thermodynamic integration.\n";

if(length($inFileBase)==0)
	{
	print $usage;
	exit(1);
	}


if(!(-e $inFileBase) || !(-f $inFileBase)  || !($inFileBase=~m/^\//))
	{
	print "Error, fail to find file (using full path!) for BETA zero '$inFileBase' ! ABORT !\n";
	exit(1);
	}
else
	{
	my $baseDir=dirname($inFileBase);
	print "To scan for BETA files in directory $baseDir using $inFileBase as BETA 0 ...\n";
	if($inFileBase=~m/(.*[^0123456789]\.)([0\.]+)(\.log)/)
		{
		my $base=$1;
		my $beta=$2;
		my $log=$3;
		#print "GOT BETA $beta\n";
		#print "GOT BASE $base\n";
		my @inputs=`ls -1 $baseDir/*`;
		my $regex=$base."([0-9\.]+)\.log\$";
		my %betaHash = ();
		for my $input (@inputs)
			{
			chomp($input);
			#print "Scanning $input\n";
			if($input=~m/$regex/)
				{
				my $scanBeta=$1;
				#print "succeed regex!\n";
				print "Obtained beta $scanBeta for file $input \n";
				$betaHash{$scanBeta}=$input;
				}
			else
				{
				#print "fail regex!\n";
				}
			}
		my %allFileHash=();
		my %allBetaHash=();
		my @betaList=();
		for my $bKey (sort(keys(%betaHash)))
			{
			print "To use beta $bKey on ".$betaHash{$bKey}."...\n";
			my $avgLogLike=&computeAvgLogLike($betaHash{$bKey});
			print "Avg log like from file $betaHash{$bKey} is $avgLogLike !\n";
			$allFileHash{$betaHash{$bKey}}=$avgLogLike;
			$allBetaHash{$bKey}=$avgLogLike;
			push(@betaList,$bKey);
			}
		my $integral=0.0;
		for(my $bi=0;$bi<scalar(@betaList)-1;$bi++)
			{
			print "#############################################\n";
			my $bLeft=$betaList[$bi];
			my $bRight=$betaList[$bi+1];
			my $rectHeight=&max($allBetaHash{$bLeft},$allBetaHash{$bRight});
			print "Left val is ".$allBetaHash{$bLeft}."\n";
			print "Right val is ".$allBetaHash{$bRight}."\n";
			print "bLeft is $bLeft\n";
			print "bRight is $bRight\n";
			print "rectHeight is $rectHeight\n";
			my $triHeight=&min($allBetaHash{$bLeft},$allBetaHash{$bRight});
			$triHeight=$triHeight-$rectHeight;
			print "triHeight is $triHeight\n";
			my $triWidth=abs($bLeft-$bRight);
			my $rectWidth=$triWidth;
			print "triWidth is $triWidth\n";
			print "rectWidth is $rectWidth\n";
			my $rectArea=$rectWidth*$rectHeight;
			print "rectArea is $rectArea\n";
			my $triArea=$triWidth*$triHeight*0.5;
			#multiply tri area by (-1) because we are negative
			#must be done because we have a positive value
			print "triArea is $triArea\n";
			$thisSum=$rectArea+$triArea;
			print "RectArea and triArea Sum : $thisSum\n";
			$integral=$integral+$thisSum;
			print "Running  integral : $integral\n";
			print "#############################################\n\n\n";
			}
		print "Thermodynamically integrated marginal like likelihood : $integral\n";
		for(my $bi=0;$bi<scalar(@betaList);$bi++)
			{
			my $bLeft=$betaList[$bi];
			print $bLeft."\t".$allBetaHash{$bLeft}."\n";
			}		
		}
	else
		{
		print "Error, fail to extract BETA and BASE from input ! ABORT !\n";
		exit(1);
		}
	}










