#!/usr/bin/perl

my $usage="USAGE ./tracer_prep.pl [BURN_IN_VAL] [INFILE] [OUTFILE]";
# READ a COMET output file and remove
# columns that are NOT decimal values (can't be used with tracer)
# Only lines with state >= BURN_IN_VAL are written out after being read
# FOR example, columns that are timestamps, or 4-tuples, or newick strings are filtered OUT
# FOR example, columns are values such as log-likelihoods or Tree-heights or integers are LEFT IN
# This is to make some file more readable/ready/usable by TRACER
# Default to read from STDIN and WRITE (*APPEND*) to stdout



my $burnVal=$ARGV[0];
my $inFile=$ARGV[1];
my $outFile=$ARGV[2];

if(scalar(@ARGV)!=3) {
	print $usage."\n";
	exit(1);
	}


#print "infile is $inFile and out is $outFile\n\n\n";

if($burnVal eq "") {
	$burnVal=0;
	}
else {
	$burnVal=int($burnVal);
}


#set /dev/stdin and /dev/stdout for I/O if no arguments
if($inFile eq "") {
	$inFile="/dev/stdin";
	}
if($outFile eq "") {
	$outFile="/dev/stdout";
	}


#die "infile is $inFile and out is $outFile";

#open input and output
open(INPUT,"<$inFile") or die "ERROR, fail to open $inFile for input ! ABORT !";
open(OUTPUT,">>$outFile") or die "ERROR, fail to open $outFile for ouput (append) ! ABORT !";



#flag to indicate not encountered header yet
my $numHeaderEncounter=0;
#array to hold column header strings
my @headerStrings=();
#array to hold indices to acceptable columns filtered in
my @goodColumns=();
#flag to indicate if data has been scanned to determine good columns or not
my $scannedForGoodCol=0;
while(<INPUT>)
	{
	my $line=$_;
	#print "read $line \n";
	chomp($line);
	if($line=~m/^\s*#/)
		{
		#is # so just echo
		print OUTPUT "$line\n";
		}
	else
		{
		#if isn't # then it's header or data.


		#if haven't encountered header, then should be header!
		if($numHeaderEncounter==0)
			{
			@headerStrings=split('\t',$line);
			$numHeaderEncounter=1;
			}
		else
			{
			#must be data if already encounterd a header
			if($scannedForGoodCol==0)
				{
				#############################################
				#if not scanned for good data, do it here
				#here, it's determined which columns are suitable as input for TRACER
				my @dataPiecesToScan=split('\t',$line);
				for(my $i=0;$i<scalar(@dataPiecesToScan);$i++)
					{
					$datToScan=$dataPiecesToScan[$i];
					#regex from http://www.regular-expressions.info/floatingpoint.html
					if($datToScan=~m/^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$/)
						{
						#if it passes, add the column to the good columns array
						push(@goodColumns,$i);
						}
					}

				############################################
				#print good headers here
				$scannedForGoodCol=1;
				for(my $i=0;$i<scalar(@goodColumns);$i++)
					{
					print OUTPUT $headerStrings[$goodColumns[$i]];
					if($i<scalar(@goodColumns)-1)
						{
						print OUTPUT "\t";
						}
					}
				print OUTPUT "\n"; #this newline for the headers

				#######################################
				#here print the data
				my @dataPiecesToPrint=split('\t',$line);
				my $thisState=int($dataPiecesToPrint[0]);
				if($thisState>=$burnVal)
					{
					for(my $i=0;$i<scalar(@goodColumns);$i++)
						{
						print OUTPUT $dataPiecesToPrint[$goodColumns[$i]];
						if($i<scalar(@goodColumns)-1)
							{
							print OUTPUT "\t";
							}
						}
					}
				if($thisState>=$burnVal)
					{
					print OUTPUT "\n"; #this newline for the data
					}


			
				}
			else
				{

				#############################################
				#use the ids of good columns to print the data
				my @dataPiecesToPrint=split('\t',$line);
				my $thisState=int($dataPiecesToPrint[0]);
				if($thisState>=$burnVal)
					{
					for(my $i=0;$i<scalar(@goodColumns);$i++)
						{
						print OUTPUT $dataPiecesToPrint[$goodColumns[$i]];
						if($i<scalar(@goodColumns)-1)
							{
							print OUTPUT "\t";
							}
						}
					print OUTPUT "\n"; #this newline for the data
					}

				}
			}

		}
	}
close(INPUT);
close(OUTPUT);





