#!/usr/bin/env python

import argparse

from comet_utils import extractAsItemOrFirstFromList



def readFastaToList(fasta_path):
	#require sequences to exist on ONE line
	#thus every other line is a descriptor
	#other lines are sequence
	line_num=0
	reader=open(fasta_path,'r')
	descriptors=list()
	seqs=list()
	for line in reader:
		if(line_num%2==0):
			#descriptor
			descriptor=line[1:].strip()
			descriptors.append(descriptor)
		else:
			sequence=line.strip()
			seqs.append(sequence)
		line_num=line_num+1
	reader.close()
	if(len(descriptors)==0):
		raise Exception("Error, fasta file "+fasta_path+" is empty!")
	if(len(descriptors)!=len(seqs)):
		raise Exception("Error, descriptor, sequence length mismatch!")
		sys.exit(0)
	firstSeqLen=len(seqs[0])
	for i in range(len(descriptors)):
		len_of_this_seq=len(seqs[i])
		if(firstSeqLen!=len_of_this_seq):
			raise Exception("Error! length of first sequence is "+str(firstSeqLen)+" but length of sequence # "+str((i+1))+" is "+str(len_of_this_seq))
	return [descriptors,seqs]



# 5 40
#4         ACAGTGTAGCAGAGTGCGTATACTGACGAGGGCTCTGAGT
#5         CCAGTGTTGAGGACTGTGTATACAGTAGACACCGAGGAAT
#3         CCAGTGTTGTGGACTGTGTATACAGTAGACACCGAGGAGT
#1         CCAGTGTTGTGGACTGTGTATACAGTAGACACCGAGGAGA
#2         CCAGTGTTGTGGACTGTGTATACAGTAGACACCGAGGAGA


def write_phylip(fasta_data,out_path):
	desc_data=fasta_data[0]
	seq_data=fasta_data[1]
	num_seqs=len(seq_data)
	seq_len=len(seq_data[0])
	phylip_writer=open(out_path,'w')
	phylip_header=str(num_seqs)+" "+str(seq_len)
	phylip_writer.write(phylip_header+"\n")
	for i in range(len(desc_data)):
		#seq name and seq separated by a space
		phylip_data_line=desc_data[i]+" "+seq_data[i]
		phylip_writer.write(phylip_data_line+"\n")
	phylip_writer.close()



if (__name__=="__main__"):
	parser=argparse.ArgumentParser(description='fasta to phylip')
	parser.add_argument('-in_fna',type=str,default="/dev/stdin",nargs=1,help="path to input FNA (one-line descriptors and one-line sequences) ; default /dev/stdin")
	parser.add_argument('-out_phylip',default="/dev/stdout",type=str,nargs=1,help="path to output phylip; default /dev/stdout")
	args=parser.parse_args()
	import sys
	if(not(args)):
		parser.print_help()
		sys.exit(0)
	else:
		fasta_path=extractAsItemOrFirstFromList(args.in_fna)
		out_path=extractAsItemOrFirstFromList(args.out_phylip)
		fasta_data=readFastaToList(fasta_path)
		write_phylip(fasta_data,out_path)
