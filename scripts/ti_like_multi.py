#!/usr/bin/env python

import sys
import re
import os
import glob
import argparse
import math
from comet_utils import extractAsItemOrFirstFromList
import numpy
from decimal import *
import scipy
from scipy import integrate

def basic_integrate(x1,y1,x2,y2):
        if(x1==x2):
                return 0
        elif(x1>x2):
		return basic_integrate(x2,y2,x1,y2)
	else:
		#scipy.integrate.quad(func, a, b, args=(), full_output=0, 
		#epsabs=1.49e-08, epsrel=1.49e-08, limit=50, points=None, weight=None, wvar=None, wopts=None, maxp1=50, limlst=50)[source]
		run=x2-x1
		rise=y2-y1
		m=rise/run
		b=y2-m*x2
		linear_lambda=(lambda x:x*m+b)
		return scipy.integrate.quad(linear_lambda,x1,x2,full_output=0)[0]
		



#Compute partial marginal likelihoods for thermodynamic integration
#These values the areas of trapezoids like in Fig 1.b, 1.c, 1.d from NICOLAS LARTILLOT, HERVE PHILIPPE, Computing Bayes Factors Using Thermodynamic Integration, Syst. Biol. 55(2):195-207,2006
#See also the beerli paper referenced a few lines below
def obtainPartialMarginalLikelihood(leftBeta,rightBeta,thirdBeta,leftAvgLogLike,rightAvgLogLike,thirdAvgLogLike):
        if(leftAvgLogLike>=0 or rightAvgLogLike>=0):
                print "In obtaining partial log likelihoods. got "
                print "leftAvgLogLike=",leftAvgLogLike
                print "rightAvgLogLike=",rightAvgLogLike
                raise Exception("Error, avg. log likelihood should be negative!")
	if(leftBeta==0):
                if(thirdAvgLogLike>=0):
                        print "In obtaining partial log likelihoods. got "
                        print "thirdAvgLogLike=",thirdAvgLogLike
                        raise Exception("Error, avg. log likelihood should be negative!")
		if(leftAvgLogLike==None):
                        raise Excpetion("Error, leftAvgLogLike is None!")
		elif(rightBeta==None):
                        raise Excpetion("Error, rightBeta is None!")
		elif(thirdBeta==None):
                        raise Excpetion("Error, thirdBeta is None!")
		elif(leftAvgLogLike==None):
                        raise Excpetion("Error, leftAvgLogLike is None!")
		elif(rightAvgLogLike==None):
                        raise Excpetion("Error, rightAvgLogLike is None!")
		elif(thirdAvgLogLike==None):
                        raise Excpetion("Error, thirdAvgLogLike is None!")


		#for most heated interval....
		#use bezier per Peter Beerli and Michal Palczewski, Unified Framework to Evaluate Panmixia 
		#and Migration Direction Among Multiple Sampling Locations, Genetics 185: 313-326 (May 2010)

		#obtain y values of control points per eq 10 and 11 in the paper
		c_0_y=(1.0/5.0)*leftAvgLogLike+(4.0/5.0)*rightAvgLogLike
		c_1_y=(rightBeta*thirdAvgLogLike-thirdBeta*rightAvgLogLike)/(rightBeta-thirdBeta)

		#use eq 15 in the paper
		firstFactor=1/20.0
		secondFactor=(rightBeta-leftBeta)
		thirdFactor=(leftAvgLogLike+3.0*c_0_y+6.0*c_1_y+10.0*rightAvgLogLike)

		#compute product and return
		product=firstFactor*secondFactor*thirdFactor
		return product
	else:
		#use trapezoidal style for other intervals
		trapezoidArea=basic_integrate(leftBeta,leftAvgLogLike,rightBeta,rightAvgLogLike)
		return trapezoidArea 		





#given a path to a log file extract the beta value with regex
def obtainBetaFromLogPath(logPath,requireTracer=False):
	if(requireTracer):
		beta_re=re.compile(r'\.(\d+)\.(\d+)\.log\.tracer')
	else:
		beta_re=re.compile(r'\.(\d+)\.(\d+)\.log$')		
	beta_result=re.search(beta_re,logPath)
	if(beta_result):
		whole_part=beta_result.group(1)
		frac_part=beta_result.group(2)
		beta_str=whole_part+"."+frac_part
		beta=float(beta_str)
		#print "beta as ",beta," whole as ",whole_part," frac as ",frac_part
		if(beta<0 or beta>1):
			return (-1)
		else:
			return beta
	else:
		#print "no!"
		return (-1)


#given a log file compute the avg. log likelihood from it and the
#harmonic mean estimator too
def computeLogLikeFuncAndHMELogFile(logPath,burn_in):
	#assume STATE in first column!!!
	#assume LOG LIKELIHOOD in second column!!!
	#Reference NICOLAS LARTILLOT, HERVE PHILIPPE, 
	#Computing Bayes Factors Using Thermodynamic Integration, Syst. Biol. 55(2):195-207,2006
	getcontext().prec=100
	reader=open(logPath,'r')
	afterState=False
	harm_mean_sum=0
	decimal_harm_mean_sum=Decimal(0)
	log_like_sum=0
	num=0
	decimal_one=Decimal(1)
	for line in reader:
		pieces=line.split('\t')
		pieces[0]=pieces[0].upper()
		if(pieces[0].startswith("STATE")):
			afterState=True
		else:
			if(afterState):
				#assume STATE in first column!!!
				state=int(pieces[0])
				if(state>=burn_in):
					num=num+1
					#assume LOG LIKELIHOOD in second column!!!
					logLikelihood=float(pieces[1])
					log_like_sum=log_like_sum+logLikelihood
					decimal_ll=Decimal(logLikelihood)
					#print "decimal ll is ",decimal_ll
					decimal_exp=decimal_ll.exp()
					#print "decimal exp is ",decimal_exp
					#print "Dec sum before : ",decimal_harm_mean_sum
					decimal_harm_mean_sum=decimal_harm_mean_sum+(decimal_one/decimal_exp)
					#print "Dec sum after : ",decimal_harm_mean_sum
	avg_log_like=log_like_sum/num
	decimal_num=Decimal(num)
	decimal_harmonic_mean_eastimator=(decimal_one/decimal_num)*(decimal_harm_mean_sum)
	#For harmonic mean estimator, see eq 9,10 from reference
        reader.close()
	return [avg_log_like,decimal_harmonic_mean_eastimator]



	
#acquire valid log files (0<=beta<=1) from directory
#return a beta->log mapping
def acquireValidLogFilesFromDirectory(logDir):
	log_files_glob_str=logDir+"/*.log"
	log_files_glob=glob.glob(log_files_glob_str)
	beta_dict=dict()
	if(len(log_files_glob)==0):
		print "Glog ",log_files_glob_str," yielded no files!"
	for log_file in log_files_glob:
		beta=float(obtainBetaFromLogPath(log_file))
		if(0<=beta and beta<=1):
			beta_dict[beta]=log_file
		else:
			print "Skipping file ",log_file," because a beta value could not be found in its file name."
	if(len(beta_dict)==0):
		print "Failed to find log files in directory ",logDir,"!"
	return beta_dict


#read log files to obtain beta and data
#scan for likelihood values and perform
#thermodynamic integration and harmonic mean calcuations
def performTIAndHME(beta_log_map,burn_in):
	beta_list=list()
	avg_log_like_list=list()
	hme_list=list()
	log_file_list=list()
	#read the data from the files
	print "Found the log files...now computing average log likelihoods.." 
	for beta in sorted(beta_log_map):
		log_file=beta_log_map[beta]
		log_file_list.append(log_file)
		print "For Beta=",beta," the coupled log file is ",log_file
		beta_list.append(beta)
		avgLogLikeAndHME=computeLogLikeFuncAndHMELogFile(log_file,burn_in)
		avg_log_like_list.append(avgLogLikeAndHME[0])
		if(beta==0):
			hme_list.append(avgLogLikeAndHME[1])
		if(beta==(-1)):
			print "ERROR, beta should not be negative!"

	


	#whitespace
	print "\n\n"

	#print average log likelihoods
	for i in range(len(beta_list)):
		print "For file ",log_file_list[i]," beta =",beta_list[i]," obtained average log likelihood : ",avg_log_like_list[i]

	#whitespace
	print "\n\n"


	#obtain the partial marginal log likelihoods
	partial_log_likelihood_list=list()
	for i in range(len(beta_list)-1):
		leftBeta=beta_list[i]
		rightBeta=beta_list[i+1]
		leftAvgLogLike=avg_log_like_list[i]
		rightAvgLogLike=avg_log_like_list[i+1]
		if(i==0 and leftBeta==0):
			thirdBeta=beta_list[i+2]	
			thirdAvgLogLike=avg_log_like_list[i+2]
		else:
			thirdBeta=None
			thirdAvgLogLike=None
		partial_marg_like=obtainPartialMarginalLikelihood(leftBeta,rightBeta,thirdBeta,leftAvgLogLike,rightAvgLogLike,thirdAvgLogLike)
		partial_log_likelihood_list.append(partial_marg_like)
		print "For interval (",leftBeta,",",rightBeta,") obtained partial log likelihood : ",partial_marg_like
	#output harmonic mean estimator
	if(len(hme_list)==1):
		print "Harmonic mean estimator : ",hme_list[0]
		print "ln(HME) = ",math.log(hme_list[0])
	else:
		print "ERROR, failed to compute harmonic mean?!?!"


	#acquire TI sum!
	ti_mll_sum=0
	for i in range(len(partial_log_likelihood_list)):
		ti_mll_sum=ti_mll_sum+partial_log_likelihood_list[i]
	print "Thermodynamically computed marginal likelihood (for "+str(log_file_list)+")  : ",ti_mll_sum








if (__name__=="__main__"):
	parser=argparse.ArgumentParser(description='Read log files at beta levels for thermodynamic integration and harmonic mean computations')
	parser.add_argument('burn_in',type=int,nargs=1,default="0",help="(non-negative!) burn_in value ; ignore log-likelihood with state less than this")
	parser.add_argument('log_dir',type=str,nargs=1,help="directory to scan for log files")
	args=parser.parse_args()
	if(args):
		#print "in args"
		burn_in=extractAsItemOrFirstFromList(args.burn_in)
		if(burn_in<0):
			print "Error, require burn_in to be non-negative!"
			parse.print_help()
			#sys.exit(0)
		else:
			log_dir=extractAsItemOrFirstFromList(args.log_dir)
			if(os.path.isdir(log_dir)):
				#good!
				beta_log_map=acquireValidLogFilesFromDirectory(log_dir)
				print "Acquired beta files : ",beta_log_map
				if(len(beta_log_map)!=0):
					performTIAndHME(beta_log_map,burn_in)
				else:
					print "Error, failed to find any log files!"
			else:
				print "Error, require directory to scan for beta log files!"
				parse.print_help()				
	else:
		parser.print_help()	
