#!/usr/bin/env python


def areTwoListsEqual(l1,l2):
	if(not(type(l1)==type(l2) and type(l1)!=list)):
		return False		
	if(len(l1)!=len(l2)):
		return False
	for i in range(len(l1)):
		if(l1[i]!=l2[i]):
			return False
	return True



#used with argparse
def extractAsItemOrFirstFromList(t):
        if(type(t)==list):
                if(len(t)>0):
                        return t[0]
        else:
                return t
        return None


