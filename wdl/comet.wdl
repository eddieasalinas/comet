task prepare_runs_tsv {

	File prepTSV

	command <<<
		set -x

scan_tsv_py="
import sys
import numpy
num_scatters=sys.argv[0]
beta=sys.argv[1]
seed=sys.argv[2]
if(seed.lower()=='auto'):
	#get random series
	numpy.random.seed()
else:
	numpy.random.seed(int(seed))
for s in range(num_scatters):
	sseed=numpy.random.random_integers(0)
	print "doing a scatter with beta=",beta,"and seed=",sseed			
"

		#TSV format:
		#num_scatters	beta	seed(AUTO or INT)		


		>>>

	runtime {
		docker : "eddieasalinas/comet:v0.9.951"
		}


	}


task prepare_runs
	{

	Int runsPerMode
	String algorithm
	Float betaStart
	Float betaStep
        Int? seed
        Int preemptible
        Boolean useDynBeta
        Float dynBetaMinStep
        Float dynBetaMaxStep

	command <<<

        #increase verbosity and adjust error tolerance
	set -eux -o pipefail

        #initialize beta values
        if [ ${useDynBeta} == "false" ] ; 
        then
            #constant beta step
            for BETA in `seq ${betaStart} ${betaStep} 1` ; do
                    for R in `seq 1 ${runsPerMode}` ; do
                            echo $BETA | awk '{if($1>=0.999) { print "1.0" } else {print $1}}'   >> beta_file.dat
                            echo ${algorithm} >> algorithms.dat
                    done ;
            done ;
        else
            #dynamic BETA step
python_dyn_beta_script="
import math
def dyn_beta_step(beta):
    dyn_step=beta/2.0
    if(dyn_step<${dynBetaMinStep}):
        return ${dynBetaMinStep}
    elif(dyn_step>${dynBetaMaxStep}):
        return ${dynBetaMaxStep}
    else:
        return dyn_step

beta_dyn_start=${betaStart}
beta=beta_dyn_start
while(beta<=1.0):
	for run in range(${runsPerMode}):
		print str(min(beta,1.0))
	dbs=dyn_beta_step(beta)
	beta=beta+dbs
	if(beta-dbs<0.99 and beta>1.0):
		#if we never got close to 1.0, but did go over, then print it
		for run in range(${runsPerMode}):
			print "1.0"
"
        python -c "$python_dyn_beta_script" > beta_file.dat
        cat beta_file.dat | awk '{print "${algorithm}"}' > algorithms.dat
            
        fi ;


	#make indices for scatter ; indexing is 0-based
	cat algorithms.dat|awk '{print NR-1}' >> scatter_indices.dat

        #make seeds

        #first make master_seed
        DATE_S=`date  +%s`
        echo "DATE_S IS $DATE_S" ; 
        MASTER_SEED="${if defined(seed) then seed else '$DATE_S'}" ; 
        echo "MASTER_SEED IS $MASTER_SEED" ;

        #use master_seed to generate further seeds
gen_rand_py="
import random
import sys
def printRands(seed,num):
    random.seed(seed)
    for r in range(num):
        print int(random.random()*1000000000)
printRands(int(sys.argv[1]),int(sys.argv[2]))
"
        NUM_SEEDS_TO_GET=`wc -l algorithms.dat|grep -Po '^\d+'` ; 
        /usr/bin/python -c "$gen_rand_py" $MASTER_SEED $NUM_SEEDS_TO_GET > rand_seeds.dat

	>>>

	runtime {
		docker : "eddieasalinas/comet:v0.9.951"
	    	disks: "local-disk 1 HDD"
	    	memory: "0.1 GB"
                preemptible: "${preemptible}"

		}

	output {
		Array[String] beta_values=read_lines("beta_file.dat")
		Array[String] algorithms=read_lines("algorithms.dat")
		Array[Int] scatter_indices=read_lines("scatter_indices.dat")
                Array[Int] seeds=read_lines("rand_seeds.dat")
		}
	}


task comet_run {

	String maxIterStr
	String algorithm
	Float beta
	Float minKappa
	Float maxKappa
	String comet_beta_factors
	File? cometJar
	String? seed
	Array[File] xmls_starts
	File xml_start_fof=write_lines(xmls_starts)
	File fasta_data
	Boolean useXMLFof
        Int preemptible
        String zone_str
        Boolean setTheDisablePhyloXML
        File? tarballOfLastXMLs

	command <<<

                #increase verbosity and adjust error tolerance
                set -eux -o pipefail

		#chose executable
		COMET_JAR=${if defined(cometJar) then cometJar else '/usr/local/comet/comet.jar'}
		echo "COMET_JAR IS $COMET_JAR" ;

		#build conf file
		CONF_FILE="CONF_FILE"
		echo "#iter data" >> $CONF_FILE && echo "${maxIterStr}" >> $CONF_FILE ; 		
		echo "#recording freq" >> $CONF_FILE && echo "100" >> $CONF_FILE ; 
		echo "#beta start, stop, step" >> $CONF_FILE ; 
		BETA_EFFECTIVE=`echo ${beta}|awk '{if($1>=0.999) { print "1.0" } else {print "${beta}"}}'` ; 
		echo -ne "$BETA_EFFECTIVE\n$BETA_EFFECTIVE\n1\n" >> $CONF_FILE ; 
		echo "#model" >> $CONF_FILE && echo ${algorithm} >> $CONF_FILE
		echo "#seed" >> $CONF_FILE && echo "${if defined(seed) then seed else "AUTO"}" >> $CONF_FILE ; 
		echo "#MAX_LL_THREADS" >> $CONF_FILE && echo "AUTO" >> $CONF_FILE ;
		echo "#min and max kappa" >> $CONF_FILE && echo "${minKappa}" >> $CONF_FILE && echo "${maxKappa}" >> $CONF_FILE ; 

		#show conf file
		cat $CONF_FILE|grep -Pin '.'

		#move input to PWD because output is generated adjacent to it
		FASTA_NAME=`basename ${fasta_data}`; 
		mv -vf ${fasta_data} .

                #setup for disable phyloxml
                ${if setTheDisablePhyloXML then "export COMET_DISABLE_PHYLOXML=TRUE" else ""}

		#set up libraries for passed-in jar
		JAR_DIR=`dirname $COMET_JAR` ; 
                if [ "$JAR_DIR" = "/usr/local/comet" ] ; then
                    echo "Not making a link." ; 
                else
                    ln -vs /usr/local/comet/lib $JAR_DIR ;
                fi ;

                #setup for MCMCMC (or not)
                export COMET_BETA_FACTORS="${comet_beta_factors}" ; 

		#run comet using xmls if flag is there
		if [ "${useXMLFof}" = "true" ] ; then
                        echo "To use xml_start_fof";
                        tar -xvzf ${tarballOfLastXMLs}
                        #extract XMLs and get their BETAs and write priority.xmls.fof
make_fof_py="
import subprocess,sys,re
from subprocess import call
from random import shuffle
get_beta_cmd='gunzip -c ${tarballOfLastXMLs} | tar -tf /dev/stdin'
this_run_beta=${beta}
print 'About to run ',get_beta_cmd
output=subprocess.check_output(get_beta_cmd,shell=True)
output_pieces=output.split('\n')
xml_to_beta_dict=dict()
nearest_beta=None
for p in range(len(output_pieces)):
        #print 'for p=',p,' piece=',output_pieces[p]
        if(output_pieces[p].endswith('.xml')):
                print 'For xml ',output_pieces[p],' now attempting to extract beta from it...'
                beta_re=re.compile(r'(\d+\.\d+)\.xml')
                re_res=re.search(beta_re,output_pieces[p])
                if(re_res):
                        beta=float(re_res.group(1))
                        print 'For XML file ',output_pieces[p],' beta is ',beta
                        xml_to_beta_dict[output_pieces[p]]=beta
                        if(nearest_beta is None):
                                nearest_beta=beta
                        else:
                                comp_dist=abs(beta-nearest_beta)
                                beta_dist=abs(beta-this_run_beta)
                                if(beta_dist<comp_dist):
                                        nearest_beta=beta
                else:
                        print 'Error in obtaining beta from ',output_pieces[p]
print 'This run beta is ',this_run_beta
print 'nearest beta is ',nearest_beta
if(nearest_beta is None):
        print 'Making empty priority.xmls.fof'
        touch_cmd='touch priority.xmls.fof'
        subprocess.check_output(touch_cmd)
else:
        xmls_to_write=list()
        for xml in xml_to_beta_dict:
                this_xml_beta=xml_to_beta_dict[xml]
                if(this_xml_beta==nearest_beta):
                        xmls_to_write.append(xml)
        print 'before shuffle : ',xmls_to_write
        shuffle(xmls_to_write)
        print 'after shuffle : ',xmls_to_write
        writer=open('priority.xmls.fof','w')
        for x in range(len(xmls_to_write)):
                print 'Writing ',xmls_to_write[x],' to priority.xmls.fof'
                writer.write(xmls_to_write[x]+'\n')
        writer.close()
"
                        /usr/bin/python -c "$make_fof_py"
			java -jar $COMET_JAR $CONF_FILE $FASTA_NAME priority.xmls.fof ;
			COMET_RET=$? 
		else 
                        echo "To start with random-trees (no fof) " ; 
			java -jar $COMET_JAR $CONF_FILE $FASTA_NAME ; 
			COMET_RET=$?
		fi ; 
                echo "COMET_RET is $COMET_RET" ; 

		#prepare for output
                cat *.out|grep -Pi 'current.newick'|sed -r "s/current[^:]+://ig" | sed -r "s/^\s+//g" > comet.0.0.0.0.last.newick
                cat *.out|grep -Pi 'newick'| grep -Pi '^ml'|grep -Po '=.*'|tr -d '='|sed -r "s/^\s+//g" > comet.0.0.0.0.ml.newick
		find *.out    | xargs -tI {} mv -vf {} ./comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.out 
		find *.log    | grep -Piv 'tracer'|xargs -tI {} mv -vf {} ./comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.log 
		find *.log    | grep -Pi  'tracer'|xargs -tI {} mv -vf {} ./comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.log.tracer.log
		find *.last.xml |xargs -tI {} mv -vf {} comet.last_tree.${beta}.xml
		find *.xml|grep -Piv 'last'|xargs -tI {} mv -vf {} comet.ml.${beta}.xml 
		find *.nxs    | xargs -tI {} mv -vf {} ./comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.nxs 
		find *.phylip | xargs -tI {} mv -vf {} ./comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.phylip
		echo "$BETA_EFFECTIVE" > beta.dat 

		#propagate comet return value
		/bin/bash -c "exit $COMET_RET" ;
 
		>>>

	runtime {
	    	disks: "local-disk 20 HDD"
		docker : "eddieasalinas/comet:v0.9.951"
                preemptible: "${preemptible}"
                zones: "${zone_str}"
		}
	
	output
		{
                File last_newick="comet.0.0.0.0.last.newick"
                File ml_newick="comet.0.0.0.0.ml.newick"
                File conf_file="CONF_FILE"
		File last_xml="comet.last_tree.${beta}.xml"
		File ml_xml="comet.ml.${beta}.xml"
		File phylip_dat="comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.phylip"
		File nxs_dat="comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.nxs"
		File log="comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.out"
		File posterior_trace="comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.log"
		File tracer_posterior_trace="comet.0.0.0.0.log.mcmcmc_chain_1_mainbeta_${beta}.${beta}.log.tracer.log"
		Float beta_out=read_float("beta.dat")
		}

	}

task gather_for_ti 
        {
	File phyml_phylip
        Array[File] beta_zero_logs
        Array[File] beta_nonzero_logs
        Array[File] last_xmls_bzero
        Array[File] last_xmls_bnzero
        Array[File] ml_xmls_bzero 
        Array[File] ml_xmls_bnzero
	Array[File] ml_newicks_bzero
	Array[File] ml_newicks_bnzero
        String alg_name
        File? cometJar
        File? tracerLogsNonZeroOtherRuns
        File? tarballOfLastXMLs
        Int preemptible
        String memStr

        command <<<

                #increase verbosity and adjust error tolerance
                set -eux -o pipefail

		#create empty other files
                touch empty
                tar -cvzf empty.tar.gz empty
                rm -v empty

                #obtain betas found
                beta_zero_fof=${write_lines(beta_zero_logs)}
                beta_non_zero_fof=${write_lines(beta_nonzero_logs)}
                tar -cvzf tracer_logs_nonzero_preburnin_this_run.tar.gz `cat $beta_non_zero_fof` ;
                tar -cvzf tracer_logs_zero_preburnin_this_run.tar.gz `cat $beta_zero_fof` ; 
                gunzip -c ${if defined(tracerLogsNonZeroOtherRuns) then tracerLogsNonZeroOtherRuns else "empty.tar.gz"}  | tar -tf /dev/stdin | awk '{if($0~/\.log$/) { print $0 }}' >> $beta_non_zero_fof
                tar -xvzf ${if defined(tracerLogsNonZeroOtherRuns) then tracerLogsNonZeroOtherRuns else "empty.tar.gz"}
                tar -cvzf tracer_logs_nonzero_preburnin_this_run_AND_others.tar.gz `cat $beta_non_zero_fof` ;
                tar -cvzf all_tracer_logs_preburnin_this_run_AND_others.tar.gz ` cat $beta_non_zero_fof $beta_zero_fof`
                cat $beta_zero_fof $beta_non_zero_fof | grep -Po '\d+\.\d+\.log' | grep -Po '\d+\.\d+' | sort -g   | uniq > beta_list.dat

		#chose executable
		COMET_JAR=${if defined(cometJar) then cometJar else '/usr/local/comet/comet.jar'}
		echo "COMET_JAR IS $COMET_JAR" ;

                #combine logs
                for BETA in `cat beta_list.dat` ; do
                    echo "Processing for BETA=$BETA" ; 
                    BETA_REGEX=`echo $BETA | sed -r "s/\./\\\\\./g"`; 
                    echo "BETA_REGEX=$BETA_REGEX" ; 
                    TO_COMBINE=`cat $beta_zero_fof $beta_non_zero_fof | grep -P "\.$BETA_REGEX\.log.tracer.log" | tr "\n" " "`
                    echo "For BETA=$BETA to combine files $TO_COMBINE" ; 
                    java -jar  $COMET_JAR LOG_COMBINE combined.$BETA.log $TO_COMBINE ; 
                done ;

                #make ESS data
                for LOG in `find combined.*.log`; do
                    echo "Gathering ESS data from log $LOG" ;
                    for COL_NUM in `grep -Pi '^State' $LOG  |tr "\t" "\n"|grep -Pin '.'|grep -Pio '^\d+'|grep -Piv '^1$'` ; do 
                        echo "Gathering ESS data from column $COL_NUM in log $LOG" ; 
                        ESS_VAL=`cut -f 1,$COL_NUM $LOG |grep -Piv '^#'|grep -Piv '^State'|grep -Pi '^\d+\s'|java -jar $COMET_JAR ESS`;
                        COL_NAME=`grep -Pi '^State' $LOG |tr "\t" "\n"|grep -Pin '.'|grep -Po "^$COL_NUM:.*" | grep -Po ':.*'|tr -d ":"` ; 
                        echo -ne "$LOG\t$COL_NAME\t$ESS_VAL\n" >> ess.dat ;
                        echo "Gathered $ESS_VAL ..." ;
                    done ;
                done ;

                #get flag for ESS>=200 for all data
py_good_ess_script="
import re
ess_reader=open('ess.dat')
lowest_ess=float('inf')
for line in ess_reader:
	pieces=line.strip().split('\t')
	good_col_re_res=re.search('_ESS',pieces[1])
	if(good_col_re_res):
		#ignore _ESS
		pass
	else:
		ess_str=pieces[2].lower()
		non_digit_match=re.search(r'[^0-9\.]',ess_str)
		if(non_digit_match is None):
			ess=float(pieces[2])
			if(ess<lowest_ess):
				lowest_ess=float(pieces[2])
		else:
			#ignore NaN's
			pass
if(lowest_ess<200.0 or (lowest_ess==float('inf'))):
	print '0'
else:
	print '1'

"

                GOOD_ESS=`python -c "$py_good_ess_script" ` ; 
                
                #compute TI
                echo "To compute TI from files..." ;
                /usr/local/comet/scripts/ti_like_multi.py 0 `pwd` 1>${alg_name}.ti.out 2>${alg_name}.ti.err
                tail -1 ${alg_name}.ti.out|grep -Pio '\-?\d+\.\d+\s*$' > ${alg_name}.ti.dat

                #plot for TI
                echo -ne "BETA\tAVG_LOG_LIKE\n" > avg_log_liks.txt
                cat ${alg_name}.ti.out |grep -Pi 'obtained average log likelihood'|grep -Po 'beta.*'|sed -r "s/obtained.average.log.likelihood.://g"  |grep -Po '=.*'|tr -d "="|grep -Po '\d.*'|sed -r "s/\s+/\t/g" >> avg_log_liks.txt
gen_ti_plot_py="
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
from matplotlib import pyplot as plt
import csv
reader = csv.DictReader(open('avg_log_liks.txt', 'rb'),delimiter='\t')
x=[]
y=[]
for line in reader:
	x.append(line['BETA'])
	y.append(line['AVG_LOG_LIKE'])
plt.scatter(x, y)
plt.xlim(0, 1)
plt.xlabel('Beta')
plt.ylabel('Avg Log Likelihood')
plt.savefig('ti.png')
plt.savefig('ti.pdf')
"
                /usr/bin/python -c "$gen_ti_plot_py" ;

                #get the XMLs and their likelihoods
                for XML in `cat ${write_lines(ml_xmls_bnzero)} ${write_lines(ml_xmls_bzero)}  ` ; do  
                    echo -ne "$XML" >> xmls.liks.txt ;
                    echo -ne "\t" >> xmls.liks.txt ;
                    grep -Po '>\-?\d+\.\d+</prop' $XML |sed -r "s/<\/prop//g"|sed -r "s/>//g"|tr ":" "\t" >> xmls.liks.txt ; 
                done ;


                #get the ML XML and ML newick
                ML_XML=`sort -k2g xmls.liks.txt | tail -1|cut -f1`;
                cp -v $ML_XML ${alg_name}.ml.xml
		ML_XML_DIR=`dirname $ML_XML` ;
		cp -v $ML_XML_DIR/*.newick ${alg_name}.ml.newick

                #run phyml to get its ML tree for comparison/plotting purposes
                PHYML_ALPHA="1"
                KAPPA_COL=`grep -P 'State' combined.1.0.log |tr "\t" "\n"|grep -Pin '^kappa$'|grep -Po '^\d+'` ; 
                AVG_KAPPA=`cat combined.1.0.log |grep -Pv '^#'|grep -Pv '^State'|awk -v my_var="$KAPPA_COL" '{ sum += $my_var; n++ } END { if (n > 0) print sum / n; }'`
		echo "Computed average kappa for phyml is $AVG_KAPPA " ; 
                phyml -i ${phyml_phylip} -t $AVG_KAPPA --nclasses 1 -a 1
		cp -v ${phyml_phylip}_phyml_stats.txt phyml_stats.txt
		cp -v ${phyml_phylip}_phyml_tree.txt phyml_tree.txt.ml.newick

		#get figtree plots
		find *.newick|xargs -tI {} java -jar /usr/local/bin/figtree.jar  -graphic PNG  {} {}.png

		#generate report for summary
                echo -ne "<html><head><title>COMET Output and Related Results</title>\n" > comet.html
                echo -ne "<style> \ndiv_bad\n { color: red; } \ndiv_good \n{ color: green;   }\n</style>\n" >> comet.html
                echo -ne "</head><body>\n" >> comet.html
                echo "<h1>COMET Output and Related Results</h1>" >> comet.html
                echo "<br>Model name : ${alg_name} " >> comet.html
                echo "<br>All ESS >= 200 ? : " >> comet.html
                echo "$GOOD_ESS" | awk '{if($1==0) { print "<div_bad>no</div_bad><br>" } else { print "<div_good>yes</div_good><br>"}}' >> comet.html
                echo -ne "Click <a href=\"#ESStable\">here</a> for ESS table.<br>" >> comet.html 
                echo "<br>Thermodynamically Integrated Marginal Likelihood : " >> comet.html
                cat *.ti.dat  >> comet.html
                echo "<br>Thermodynamic Integration Plot" >> comet.html
                echo "<br><IMG SRC=\"ti.png\"> " >> comet.html
                echo -ne "<br><table border=1>\n" >> comet.html
                cat avg_log_liks.txt | awk '{print "<tr><td>" $1 "</td><td>" $2 "</td></tr>\n"}' >> comet.html
                echo -ne "\n</table>\n" >> comet.html
                echo -ne "\n<br><br><table border=1><tr><td colspan=2>Partial Log Likelihoods for TI</td></tr>" >> comet.html
                echo -ne "<tr><td>Interval</td><td>Partial Log Likelihood</td></tr>\n" >> comet.html
                grep -Pi 'interval' ${alg_name}.ti.out |sed -r "s/[a-zA-Z:]//g"|sed -r "s/\s//g"|sed -r "s/\)/\)\t/g" | awk '{print "<tr><td>" $1 "</td><td>" $2 "</td></tr>"}' >> comet.html
                echo -ne "</table>" >> comet.html
                echo "<br><table border='1'><tr><td align='center'   colspan='2' border='1'  >ML Tree Plots</td></tr>" >> comet.html
                echo "<tr><td  align='center'  >COMET ML Tree<br>" >> comet.html
                echo "Log-likelihood:" >> comet.html
                grep -Po '>\-?\d+\.\d+</prop' $ML_XML |sed -r "s/<\/prop//g"|sed -r "s/>//g"|tr ":" "\t" >> comet.html
                echo -ne "<br><IMG SRC=\"" >> comet.html
                echo -ne ${alg_name}.ml.newick.png >> comet.html
                echo -ne "\">" >> comet.html
                echo "</td><td  align='center'  >PhyML ML Tree<br>" >> comet.html
                echo "Log-likelihood:" >> comet.html
                grep -Pi 'log.like' phyml_stats.txt|grep -Po ':.*'|tr -d ":"|sed -r "s/\s//g" >> comet.html
                echo -ne "<br><IMG SRC=\"" >> comet.html
                echo -ne "phyml_tree.txt.ml.newick.png" >> comet.html
                echo -ne "\">" >> comet.html
                echo "</td></tr>" >> comet.html
                echo "</table>" >> comet.html
                echo -ne "<br><table   border='1'><tr><td align='center' colspan='3' border='1'><a name='ESStable'>ESS Table</div></td></tr>\n" >> comet.html  
                echo -ne "<tr><td>File</td><td>Value</td><td>ESS</td></tr>\n" >> comet.html
                tac ess.dat | grep -Pv '_ESS' |  awk '{if($3!="NaN") {print $0}}' |  awk '{if($3<200) { $3="<div_bad>" $3 "</div_bad>"}   print "<tr><td>" $1 "</td><td>" $2 "</td><td>" $3 "</td></tr>"}  ' >> comet.html
                echo "</table>" >> comet.html
                echo "</body></html>" >> comet.html


                #pack up last XMLs and ML XMLs
                last_xmls_bzero_fof=${write_lines(last_xmls_bzero)}
                last_xmls_bnzero_fof=${write_lines(last_xmls_bnzero)}
                tar -cvzf last_xmls_bzero.tar.gz `cat $last_xmls_bzero_fof` ;
                tar -cvzf last_xmls_bnzero.tar.gz `cat $last_xmls_bnzero_fof` ;
                tar -xvzf last_xmls_bzero.tar.gz
                tar -xvzf last_xmls_bnzero.tar.gz
                #aggregate lastXMLs from workflow input from previous run
                touch empty
                tar -cvzf empty_xmls.tar.gz empty
                rm -v empty
                gunzip -c ${if defined(tarballOfLastXMLs) then tarballOfLastXMLs else "empty_xmls.tar.gz"}  | tar -tf /dev/stdin | awk '{if($0~/\.xml$/) { print $0 }}' > workflow_xmls.txt
                tar -xvzf ${if defined(tarballOfLastXMLs) then tarballOfLastXMLs else "empty_xmls.tar.gz"}
                tar -cvzf aggregated_last_xmls_this_run_and_prev_input.tar.gz `cat workflow_xmls.txt $last_xmls_bzero_fof $last_xmls_bnzero_fof` ;

                >>>

	output
		{
                File aggregated_all_last_xmls="aggregated_last_xmls_this_run_and_prev_input.tar.gz"
                File aggregated_all_tracer_logs_preburnin="all_tracer_logs_preburnin_this_run_AND_others.tar.gz"
        	File this_run_nonzero_tracer_logs="tracer_logs_nonzero_preburnin_this_run.tar.gz"
                File this_run_zero_tracer_logs="tracer_logs_zero_preburnin_this_run.tar.gz"
                File this_run_non_zero_all_in_logs="tracer_logs_nonzero_preburnin_this_run_AND_others.tar.gz"
                File last_xmls_bzero_tarball="last_xmls_bzero.tar.gz"
                File last_xmls_bnzero_tarball="last_xmls_bnzero.tar.gz"
		File comet_html_report="comet.html"
		File comet_ml_newick="${alg_name}.ml.newick"
		File comet_ml_figtree="${alg_name}.ml.newick.png"
		File phyml_ml_figtree="phyml_tree.txt.ml.newick.png"
		File phyml_ml_tree="phyml_tree.txt.ml.newick"
		File phyml_ml_stats="phyml_stats.txt" 
                File ml_xml="${alg_name}.ml.xml"
                File xmls_and_likes="xmls.liks.txt"
                File ti_out="${alg_name}.ti.out"
                File ti_err="${alg_name}.ti.err"
                File ess_dat="ess.dat"
                File ti_png="ti.png"
                File ti_pdf="ti.pdf"
                File ti_dat="avg_log_liks.txt"
                Array[File] combined_out_logs=glob("*.log")
                Float ti_marginal_likelihood=read_float("${alg_name}.ti.dat")
		}        

	runtime {
    		memory: "${memStr}"
	    	disks: "local-disk 20 HDD"
                preemptible: "${preemptible}"
		docker : "eddieasalinas/comet:v0.9.951"
		}        

        }


workflow comet_wf
	{

	String betaZeroMaxIterStr
	String betaRegularMaxIterStr
	Float minKappa
	Float maxKappa
	Array[File] emptyXMLArr=[]
	File fasta_data
	Int numBetaZero
	Int numBetaRegular
        String algorithm
        Int preemptible_prepare
        Int preemptible_scatter
        String scatter_zone_str
        String comet_beta_factors
        Boolean setTheDisablePhyloXML
        File? tarballOfLastXMLs
        Boolean useXMLFoFRegular

	


	#prepare to run beta 0s first
	call prepare_runs as prepare_beta_zero_runs
		{
		input:
                        useDynBeta=false,
                        dynBetaMinStep=0.333333,
                        dynBetaMaxStep=0.333333,
			runsPerMode=numBetaZero,
			algorithm=algorithm,
			betaStart=0.0,
			betaStep=1.1,
                        preemptible=preemptible_prepare			
		}

	#run beta_zeros
	scatter(idx in prepare_beta_zero_runs.scatter_indices) {
	call comet_run as beta_zero_run {
		input:
                        tarballOfLastXMLs=tarballOfLastXMLs,
			maxIterStr=betaZeroMaxIterStr,
			beta=prepare_beta_zero_runs.beta_values[idx],
			algorithm=prepare_beta_zero_runs.algorithms[idx],
                        seed=prepare_beta_zero_runs.seeds[idx],
			minKappa=minKappa,
			maxKappa=maxKappa,
			comet_beta_factors="1.0",
			xmls_starts=emptyXMLArr,
			fasta_data=fasta_data,
			useXMLFof=false,
                        preemptible=preemptible_scatter,
                        zone_str=scatter_zone_str,
                        comet_beta_factors=comet_beta_factors,
                        setTheDisablePhyloXML=setTheDisablePhyloXML
		}
	}


        #setup for beta>0 runs
	call prepare_runs as prepare_regular_runs
		{
		input:
			runsPerMode=numBetaRegular,
			algorithm=algorithm,
                        preemptible=preemptible_prepare
		}

        #runs for beta>0
	scatter(idx in prepare_regular_runs.scatter_indices)
		{
		call comet_run as beta_regular_run
			{
			input:
                                tarballOfLastXMLs=tarballOfLastXMLs,
				maxIterStr=betaRegularMaxIterStr,
				beta=prepare_regular_runs.beta_values[idx],
				algorithm=prepare_regular_runs.algorithms[idx],
                                seed=prepare_regular_runs.seeds[idx],
				minKappa=minKappa,
				maxKappa=maxKappa,
				comet_beta_factors="1.0",
				xmls_starts=emptyXMLArr,
				fasta_data=fasta_data,
				useXMLFof=useXMLFoFRegular,
                                preemptible=preemptible_scatter,
                                zone_str=scatter_zone_str,
                                comet_beta_factors=comet_beta_factors,
                                setTheDisablePhyloXML=setTheDisablePhyloXML
			}
		}

        call gather_for_ti 
            {
                input:
                    tarballOfLastXMLs=tarballOfLastXMLs,
                    last_xmls_bzero=beta_zero_run.last_xml,
                    last_xmls_bnzero=beta_regular_run.last_xml,
		    phyml_phylip=beta_zero_run.phylip_dat[0],
                    ml_xmls_bzero=beta_zero_run.ml_xml,
                    ml_xmls_bnzero=beta_regular_run.ml_xml,
		    ml_newicks_bzero=beta_zero_run.ml_newick,
		    ml_newicks_bnzero=beta_regular_run.ml_newick,
                    beta_zero_logs=beta_zero_run.tracer_posterior_trace,
                    beta_nonzero_logs=beta_regular_run.tracer_posterior_trace,
                    alg_name=algorithm,
                    preemptible=preemptible_prepare
            }
            
        output {
                File aggregated_all_last_xmls=gather_for_ti.aggregated_all_last_xmls
                File aggregated_all_tracer_logs_preburnin=gather_for_ti.aggregated_all_tracer_logs_preburnin
                File comet_html_report=gather_for_ti.comet_html_report
                File comet_ml_figtree=gather_for_ti.comet_ml_figtree
                File phyml_ml_figtree=gather_for_ti.phyml_ml_figtree
                File ml_newick=gather_for_ti.comet_ml_newick
                File phyml_ml_tree=gather_for_ti.phyml_ml_tree
                File phyml_ml_stats=gather_for_ti.phyml_ml_stats
                File comet_ml_newick=gather_for_ti.comet_ml_newick
                File xmls_and_likes=gather_for_ti.ml_xml
                File ti_out=gather_for_ti.ti_out 
                File ti_err=gather_for_ti.ti_err
                File ess_dat=gather_for_ti.ess_dat
                File ti_png=gather_for_ti.ti_png
                File ti_pdf=gather_for_ti.ti_pdf
                File ti_dat=gather_for_ti.ti_dat
                Array[File] combined_out_logs=gather_for_ti.combined_out_logs
                Float ti_marginal_likelihood=gather_for_ti.ti_marginal_likelihood
        }


}

